
/*
 * Created: 17-02-2017
 * STEPHANOV_Modified: Fri 17 Feb 2017 10:41:37 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_MOCK_NOISE_HPP
#define STEPHANOV_MOCK_NOISE_HPP

#include <gmock/gmock.h>
#include <utilities/white_noise.hpp>

namespace unit_test {
namespace stephanov {

struct Mock_Noise : public rmt::White_Noise
{
  Mock_Noise(value_type noise_val) : value{noise_val}
  {
    ON_CALL(*this, noise()).WillByDefault(::testing::Return(value));
  }

  MOCK_METHOD0(noise, value_type());
  MOCK_METHOD0(get_std_dev, double());
  MOCK_METHOD1(set_std_dev, void(double));

private:
  value_type value;
};

} // namespace stephanov
} // namespace unit_test

#endif /* STEPHANOV_MOCK_NOISE_HPP */
