/*
 * Created: 08-05-2016
 * STEPHANOV_Modified: Fri 01 Sep 2017 11:25:35 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_MOCK_FORCE_HPP
#define STEPHANOV_MOCK_FORCE_HPP

#include <gmock/gmock.h>
#include <stephanov/langevin/force.hpp>

using ::testing::ReturnRef;

namespace unit_test {

class Mock_Cartesian_Force : public rmt::stephanov::cartesian::Force
{
public:
  using number_type = rmt::numeric::number_type;
  using complex_type = rmt::numeric::complex_type;

  Mock_Cartesian_Force(
      const std::shared_ptr<const rmt::stephanov::cartesian::Matrix_Storage>
          &storage,
      const rmt::M_type &mat_a, const rmt::M_type &mat_b)
      : Force{storage}, mock_a_force{mat_a}, mock_b_force{mat_b}
  {
    delegate_to_fake();
  }

  Mock_Cartesian_Force(
      const std::shared_ptr<const rmt::stephanov::cartesian::Matrix_Storage>
          &storage,
      const complex_type &val_a)
      : Mock_Cartesian_Force{storage, val_a, val_a}
  {
  }

  Mock_Cartesian_Force(
      const std::shared_ptr<const rmt::stephanov::cartesian::Matrix_Storage>
          &storage,
      const complex_type &val_a, const complex_type &val_b)
      : Force{storage}
  {
    mock_a_force = val_a * rmt::matrix::Identity_Matrix();
    mock_b_force = val_b * rmt::matrix::Identity_Matrix();
    delegate_to_fake();
  }

  MOCK_CONST_METHOD0(get_force_a, const rmt::M_type &());
  MOCK_CONST_METHOD0(get_force_b, const rmt::M_type &());
  MOCK_METHOD0(do_update, void());
  MOCK_METHOD0(conditional_update, void());

  void delegate_to_fake()
  {
    ON_CALL(*this, get_force_a()).WillByDefault(ReturnRef(mock_a_force));

    ON_CALL(*this, get_force_b()).WillByDefault(ReturnRef(mock_b_force));
  }

private:
  rmt::M_type mock_a_force;
  rmt::M_type mock_b_force;
};

class Mock_Polar_Force : public rmt::stephanov::polar::Force
{
public:
  using number_type = rmt::numeric::number_type;
  using complex_type = rmt::numeric::complex_type;

  Mock_Polar_Force(const std::shared_ptr<
                       const rmt::stephanov::polar::Matrix_Storage> &storage,
                   const rmt::M_type &mat_r, const rmt::M_type &mat_p)
      : Force{storage}, mock_r_force{mat_r}, mock_p_force{mat_p}
  {
    delegate_to_fake();
  }

  Mock_Polar_Force(const std::shared_ptr<
                       const rmt::stephanov::polar::Matrix_Storage> &storage,
                   const complex_type &val_r)
      : Mock_Polar_Force{storage, val_r, val_r}
  {
  }

  Mock_Polar_Force(const std::shared_ptr<
                       const rmt::stephanov::polar::Matrix_Storage> &storage,
                   const complex_type &val_r, const complex_type &val_p)
      : Force{storage}
  {
    mock_r_force = val_r * rmt::matrix::Identity_Matrix();
    mock_p_force = val_p * rmt::matrix::Identity_Matrix();
    delegate_to_fake();
  }

  MOCK_CONST_METHOD0(get_force_r, const rmt::M_type &());
  MOCK_CONST_METHOD0(get_force_phi, const rmt::M_type &());
  MOCK_METHOD0(update, void());
  MOCK_METHOD0(conditional_update, void());

  void delegate_to_fake()
  {
    ON_CALL(*this, get_force_r()).WillByDefault(ReturnRef(mock_r_force));

    ON_CALL(*this, get_force_phi()).WillByDefault(ReturnRef(mock_p_force));
  }

private:
  rmt::M_type mock_r_force;
  rmt::M_type mock_p_force;
};

} // namespace Unit_Test

#endif /* STEPHANOV_MOCK_FORCE_HPP */
