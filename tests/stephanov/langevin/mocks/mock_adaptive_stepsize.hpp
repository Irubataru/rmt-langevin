/*
 * Created: 09-05-2016
 * Modified: Fri 17 Feb 2017 10:25:31 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef MOCK_STEPHANOV_ADAPTIVE_STEPSIZE_HPP
#define MOCK_STEPHANOV_ADAPTIVE_STEPSIZE_HPP

#include <gmock/gmock.h>
#include <stephanov/langevin/adaptive_stepsize.hpp>

using ::testing::Return;

namespace unit_test {

class Mock_Adaptive_Stepsize_Core
    : public rmt::stephanov::Adaptive_Stepsize_Core
{
public:
  Mock_Adaptive_Stepsize_Core(const value_type &the_force) : force{the_force}
  {
    delegate_to_fake();
  }

  void delegate_to_fake()
  {
    ON_CALL(*this, Force_Strength()).WillByDefault(Return(force));
  }

  MOCK_METHOD0(Force_Strength, value_type());

private:
  value_type force;
};

class Mock_Stepsize_Updater : public rmt::stephanov::Stepsize_Updater
{
public:
  MOCK_METHOD1(update, void(const value_type &));
};

} // namespace unit_test

#endif /* MOCK_STEPHANOV_ADAPTIVE_STEPSIZE_HPP */
