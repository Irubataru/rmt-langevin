/*
 * Created: 08-05-2016
 * Modified: Fri 01 Sep 2017 11:25:10 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <stephanov/resource/initialiser.hpp>
#include <stephanov/langevin/langevin_step.hpp>

#include "mocks/mock_force.hpp"
#include "mocks/mock_noise.hpp"


#include "../../matchers.hpp"

using rmt::M_type;
using n = rmt::numeric::number_type;
using rmt::numeric::complex_type;

using unit_test::MatrixApprox;

namespace model = rmt::stephanov;

TEST(Langevin_Step, Cartesian_Step)
{
  namespace storage = model::cartesian;
  using name_tag = storage::M_name;

  auto data = std::make_shared<storage::Matrix_Storage>();
  auto force = std::make_shared<unit_test::Mock_Cartesian_Force>(
      data, complex_type{0.5, 0.5});

  auto initialise_to_zero = [](M_type &m1, M_type &m2) {
    m1 = rmt::matrix::Zero_Matrix();
    m2 = rmt::matrix::Zero_Matrix();
  };

  storage::Initialise(*data, {n{1.}, n{0.}, 1ul}, initialise_to_zero);

  auto stepsize_ptr = std::make_shared<rmt::stepsize::Stepsize>(n{.55});
  auto mock_noise = std::make_unique<unit_test::stephanov::Mock_Noise>(n{.75});

  EXPECT_CALL(*mock_noise, noise()).Times(2 * M_size * M_size);
  EXPECT_CALL(*mock_noise, set_std_dev(sqrt(2. * stepsize_ptr->value)))
      .Times(1);

  storage::Langevin_Step step{data, force, stepsize_ptr, std::move(mock_noise)};

  EXPECT_CALL(*force, do_update()).Times(1);
  EXPECT_CALL(*force, get_force_a()).Times(1);
  EXPECT_CALL(*force, get_force_b()).Times(1);

  step.Update();

  auto noise_matrix = rmt::matrix::Matrix(n{.75});

  M_type expected_a_matrix =
      (stepsize_ptr->value) * complex_type{0.5, 0.5} * rmt::matrix::Identity_Matrix() +
      noise_matrix;
  M_type expected_b_matrix =
      (stepsize_ptr->value) * complex_type{0.5, 0.5} * rmt::matrix::Identity_Matrix() +
      noise_matrix;

  EXPECT_THAT(data->read(name_tag::A), MatrixApprox(expected_a_matrix));
  EXPECT_THAT(data->read(name_tag::B), MatrixApprox(expected_b_matrix));
}

TEST(Langevin_Step, Cartesian_Step_W_Stepsize_Update)
{
  namespace storage = model::cartesian;
  using name_tag = storage::M_name;

  auto data = std::make_shared<storage::Matrix_Storage>();

  auto initialise_to_zero = [](M_type &m1, M_type &m2) {
    m1 = rmt::matrix::Zero_Matrix();
    m2 = rmt::matrix::Zero_Matrix();
  };

  storage::Initialise(*data, {n{1.}, n{0.}, 1ul}, initialise_to_zero);

  auto force = std::make_shared<unit_test::Mock_Cartesian_Force>(
      data, complex_type{0.5, 0.5});

  auto stepsize_ptr = std::make_shared<rmt::stepsize::Stepsize>(n{.55});
  auto mock_noise = std::make_unique<unit_test::stephanov::Mock_Noise>(n{.75});

  EXPECT_CALL(*mock_noise, noise()).Times(2 * 2 * M_size * M_size);
  EXPECT_CALL(*mock_noise, set_std_dev(sqrt(1.1)))
      .Times(1);
  EXPECT_CALL(*mock_noise, set_std_dev(sqrt(1.5)))
      .Times(1);

  storage::Langevin_Step step{data, force, stepsize_ptr, std::move(mock_noise)};

  EXPECT_CALL(*force, do_update()).Times(2);
  EXPECT_CALL(*force, get_force_a()).Times(2);
  EXPECT_CALL(*force, get_force_b()).Times(2);

  step.Update();

  stepsize_ptr->value = n{.75};

  step.Update();

  auto noise_matrix = rmt::matrix::Matrix(n{.75});

  M_type expected_a_matrix =
      n{.55 + .75} * complex_type{0.5, 0.5} * rmt::matrix::Identity_Matrix() +
      2. * noise_matrix;
  M_type expected_b_matrix =
      n{.55 + .75} * complex_type{0.5, 0.5} * rmt::matrix::Identity_Matrix() +
      2. * noise_matrix;

  EXPECT_THAT(data->read(name_tag::A), MatrixApprox(expected_a_matrix));
  EXPECT_THAT(data->read(name_tag::B), MatrixApprox(expected_b_matrix));
}

TEST(Langevin_Step, Polar_Step)
{
  namespace storage = model::polar;
  using name_tag = storage::M_name;

  auto data = std::make_shared<storage::Matrix_Storage>();

  auto initialise_to_zero = [](M_type &m1, M_type &m2) {
    m1 = rmt::matrix::Zero_Matrix();
    m2 = rmt::matrix::Zero_Matrix();
  };

  storage::Initialise(*data, {n{1.}, n{0.}, 1ul}, initialise_to_zero);

  auto force = std::make_shared<unit_test::Mock_Polar_Force>(
      data, complex_type{0.5, 0.5});

  auto stepsize_ptr = std::make_shared<rmt::stepsize::Stepsize>(n{.55});
  auto mock_noise = std::make_unique<unit_test::stephanov::Mock_Noise>(n{.75});

  EXPECT_CALL(*mock_noise, noise()).Times(2 * M_size * M_size);
  EXPECT_CALL(*mock_noise, set_std_dev(sqrt(2. * stepsize_ptr->value)))
      .Times(1);

  storage::Langevin_Step step{data, force, stepsize_ptr, std::move(mock_noise)};

  EXPECT_CALL(*force, update()).Times(1);
  EXPECT_CALL(*force, get_force_r()).Times(1);
  EXPECT_CALL(*force, get_force_phi()).Times(1);

  step.Update();

  auto noise_matrix = rmt::matrix::Matrix(n{.75});

  M_type expected_r_matrix =
      (stepsize_ptr->value) * complex_type{0.5, 0.5} * rmt::matrix::Identity_Matrix() +
      noise_matrix;
  M_type expected_phi_matrix =
      (stepsize_ptr->value) * complex_type{0.5, 0.5} * rmt::matrix::Identity_Matrix() +
      noise_matrix;

  EXPECT_THAT(data->read(name_tag::R), MatrixApprox(expected_r_matrix));
  EXPECT_THAT(data->read(name_tag::PHI), MatrixApprox(expected_phi_matrix));
}

TEST(Langevin_Step, Polar_Step_W_Stepsize_Update)
{
  namespace storage = model::polar;
  using name_tag = storage::M_name;

  auto data = std::make_shared<storage::Matrix_Storage>();

  auto initialise_to_zero = [](M_type &m1, M_type &m2) {
    m1 = rmt::matrix::Zero_Matrix();
    m2 = rmt::matrix::Zero_Matrix();
  };

  storage::Initialise(*data, {n{1.}, n{0.}, 1ul}, initialise_to_zero);

  auto force = std::make_shared<unit_test::Mock_Polar_Force>(
      data, complex_type{0.5, 0.5});

  auto stepsize_ptr = std::make_shared<rmt::stepsize::Stepsize>(n{.1});
  auto mock_noise = std::make_unique<unit_test::stephanov::Mock_Noise>(n{.75});

  EXPECT_CALL(*mock_noise, noise()).Times(2 * 2 * M_size * M_size);
  EXPECT_CALL(*mock_noise, set_std_dev(sqrt(0.2)))
      .Times(1);
  EXPECT_CALL(*mock_noise, set_std_dev(sqrt(1.5)))
      .Times(1);

  storage::Langevin_Step step{data, force, stepsize_ptr, std::move(mock_noise)};

  EXPECT_CALL(*force, update()).Times(2);
  EXPECT_CALL(*force, get_force_r()).Times(2);
  EXPECT_CALL(*force, get_force_phi()).Times(2);

  step.Update();

  stepsize_ptr->value = n{.75};

  step.Update();

  auto noise_matrix = rmt::matrix::Matrix(n{.75});

  M_type expected_r_matrix =
      n{0.1 + 0.75} * rmt::matrix::Identity_Matrix(complex_type{0.5, 0.5}) +
      n{2.} * noise_matrix;
  M_type expected_phi_matrix =
      n{0.1 + 0.75} * rmt::matrix::Identity_Matrix(complex_type{0.5, 0.5}) +
      n{2.} * noise_matrix;

  EXPECT_THAT(data->read(name_tag::R), MatrixApprox(expected_r_matrix));
  EXPECT_THAT(data->read(name_tag::PHI), MatrixApprox(expected_phi_matrix));
}
