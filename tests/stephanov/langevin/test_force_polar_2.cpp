/*
 * Created: 08-05-2016
 * Modified: Fri 17 Feb 2017 11:32:47 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <boost/math/constants/constants.hpp>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <stephanov/resource/initialiser.hpp>
#include <stephanov/langevin/force.hpp>

#include "../../matchers.hpp"

using rmt::M_type;

using n = rmt::numeric::number_type;
using rmt::numeric::imag_i;

using boost::math::constants::pi;

namespace model = rmt::stephanov;
namespace storage = model::polar;

using unit_test::MatrixApprox;
using unit_test::MatrixIszero;

using base_name = model::M_name;
using strg_name = storage::M_name;

TEST(Stephanov_Polar_Force, Two_By_Two)
{
  static_assert(M_size == 2u, "Polar force only implemented for 2x2 matrices");

  auto par = rmt::Physical_Parameters{{1.1, 0.2},  // chemical_potential
                                      {-2.1, 4.2}, // fermion_mass
                                      11};         // number_of_flavours

  auto data = std::make_shared<storage::Matrix_Storage>();

  auto initialise_alg = [](M_type &m1, M_type &m2) {
    m1 = {{n{0.5}, n{0.5}}, {n{0.5}, n{0.5}}};
    m2 = {{n{0.0}, n{0.0}}, {n{0.0}, pi<n>()}};
  };

  storage::Initialise(*data, par, initialise_alg);

  storage::Force force{data, par};
  EXPECT_EQ(0ul, force.get_force_revision());

  force.update();
  EXPECT_EQ(1ul, force.get_force_revision());

  auto mf = par.fermion_mass, mu = par.chemical_potential;
  auto nf_d = static_cast<n>(par.number_of_flavours);
  auto g_norm = n{0.25} + mf * mf * (n{1.} + mf * mf) + mu * mu -
                mu * mu * (n{2.} * mf * mf - mu * mu);

  auto g = rmt::matrix::Matrix();
  g = {
      {(n{0.5} + mf * mf - (mu - imag_i) * mu) / g_norm, imag_i * mu / g_norm},
      {imag_i * mu / g_norm, (n{0.5} + mf * mf - (mu + imag_i) * mu) / g_norm}};

  auto e_th = rmt::matrix::Matrix();
  e_th = {{n{1.}, n{1.}}, {n{1.}, n{-1.}}};

  auto e_th_g = M_type{e_th * g};
  auto expected_r_matrix = rmt::matrix::Matrix();

  for (auto i = 0ul; i < 2ul; ++i)
    for (auto j = 0ul; j < 2ul; ++j)
      expected_r_matrix(i, j) =
          nf_d * (e_th(i, j) * e_th_g(i, j) -
                  n{2.} * imag_i * mu * e_th(i, j) * g(i, j));

  EXPECT_THAT(force.get_force_r(), MatrixApprox(expected_r_matrix));
  EXPECT_THAT(force.get_force_phi(), MatrixIszero(1e-12));
}
