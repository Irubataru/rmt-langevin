/*
 * Created: 08-05-2016
 * Modified: Fri 17 Feb 2017 11:31:51 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <stephanov/resource/initialiser.hpp>
#include <stephanov/langevin/force.hpp>

#include "../../matchers.hpp"

using rmt::M_type;
using n = rmt::numeric::number_type;

using rmt::numeric::imag_i;
using unit_test::MatrixApprox;

namespace model = rmt::stephanov;
namespace storage = model::cartesian;

using base_name = model::M_name;
using strg_name = storage::M_name;

TEST(Stephanov_Cartesian_Force, Zero_Matrices)
{
  auto par = rmt::Physical_Parameters{{1.1, 0.2},  // chemical_potential
                                      {-2.1, 4.2}, // fermion_mass
                                      11};         // number_of_flavours

  auto data = std::make_shared<storage::Matrix_Storage>();
  auto initialise_to_zero = [](M_type &m1, M_type &m2) {
    m1 = rmt::matrix::Zero_Matrix();
    m2 = rmt::matrix::Zero_Matrix();
  };

  storage::Initialise(*data, par, initialise_to_zero);

  storage::Force force{data, par};

  EXPECT_EQ(0ul, force.get_force_revision());

  force.update();
  EXPECT_EQ(1ul, force.get_force_revision());

  auto mf = par.fermion_mass, mu = par.chemical_potential;
  auto nf_d = static_cast<n>(par.number_of_flavours);
  auto g_val = n{1.} / (mf * mf - mu * mu);

  auto expected_a_matrix =
      rmt::matrix::Identity_Matrix(imag_i * nf_d * mu * n{-2.} * g_val);

  auto zero_matrix = rmt::matrix::Zero_Matrix();
  EXPECT_THAT(force.get_force_a(), MatrixApprox(expected_a_matrix));
  EXPECT_THAT(force.get_force_b(), MatrixApprox(zero_matrix));
}

TEST(Stephanov_Cartesian_Force, Identity_Matrices)
{
  auto par = rmt::Physical_Parameters{{1.1, 0.2},  // chemical_potential
                                      {-2.1, 4.2}, // fermion_mass
                                      11};         // number_of_flavours

  auto data = std::make_shared<storage::Matrix_Storage>();
  auto initialise_to_one = [](M_type &m1, M_type &m2) {
    m1 = rmt::matrix::Identity_Matrix();
    m2 = rmt::matrix::Identity_Matrix();
  };

  storage::Initialise(*data, par, initialise_to_one);

  storage::Force force{data, par};

  EXPECT_EQ(0ul, force.get_force_revision());

  force.update();
  EXPECT_EQ(1ul, force.get_force_revision());

  auto mf = par.fermion_mass, mu = par.chemical_potential;
  auto nf_d = static_cast<n>(par.number_of_flavours);
  auto g_val = n{1.} / (mf * mf - mu * mu + n{2.} - n{2.} * imag_i * mu);

  auto expected_a_matrix = rmt::matrix::Identity_Matrix(
      -n{2. * M_size} + nf_d * g_val * n{2.} * (n{1.} - imag_i * mu));
  auto expected_b_matrix =
      rmt::matrix::Identity_Matrix(-n{2. * M_size} + n{2.} * nf_d * g_val);

  EXPECT_THAT(force.get_force_a(), MatrixApprox(expected_a_matrix, 1e-12));
  EXPECT_THAT(force.get_force_b(), MatrixApprox(expected_b_matrix, 1e-12));
}
