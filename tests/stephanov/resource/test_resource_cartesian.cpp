/*
 * Created: 08-05-2016
 * Modified: Tue 01 Nov 2016 18:41:00 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <stephanov/resource/initialiser.hpp>
#include <stephanov/resource/resource.hpp>
#include <utilities/matrix/identity_constructor.hpp>

#include "../../matchers.hpp"

using rmt::M_type;
using n = rmt::numeric::number_type;
using rmt::numeric::imag_i;

using unit_test::MatrixApprox;

namespace model = rmt::stephanov;
namespace storage = model::cartesian;

using base_name = model::M_name;
using strg_name = storage::M_name;

TEST(Stephanov_Resource_Cartesian, Zero_Matrices)
{
  auto par = rmt::Physical_Parameters{{1.1, 0.2},  // chemical_potential
                                      {-2.1, 4.2}, // fermion_mass
                                      11};         // number_of_flavours

  storage::Matrix_Storage data{};

  auto initialise_to_zero = [](M_type &m1, M_type &m2) {
    m1 = rmt::matrix::Zero_Matrix();
    m2 = rmt::matrix::Zero_Matrix();
  };

  EXPECT_EQ(0ul, data.get_storage_revision());

  storage::Initialise(data, par, initialise_to_zero);
  EXPECT_EQ(1ul, data.get_storage_revision());

  auto mf = par.fermion_mass, mu = par.chemical_potential;

  auto zero_matrix = rmt::matrix::Zero_Matrix();
  auto expected_m_matrix = rmt::matrix::Identity_Matrix(mf * mf);
  auto expected_mu_matrix = rmt::matrix::Identity_Matrix(mu * mu);
  auto expected_g_matrix =
      rmt::matrix::Identity_Matrix(n{1.} / (mf * mf - mu * mu));

  EXPECT_THAT(data.read(strg_name::A), MatrixApprox(zero_matrix, 1e-10));
  EXPECT_THAT(data.read(strg_name::B), MatrixApprox(zero_matrix, 1e-10));
  EXPECT_THAT(data.read(base_name::M), MatrixApprox(expected_m_matrix, 1e-10));
  EXPECT_THAT(data.read(base_name::MU),
              MatrixApprox(expected_mu_matrix, 1e-10));
  EXPECT_THAT(data.read(base_name::G), MatrixApprox(expected_g_matrix, 1e-10));
}

TEST(Stephanov_Resource_Cartesian, Identity_Matrices)
{
  auto par = rmt::Physical_Parameters{{1.1, 0.2},  // chemical_potential
                                      {-2.1, 4.2}, // fermion_mass
                                      11};         // number_of_flavours

  storage::Matrix_Storage data{};

  auto initialise_to_one = [](M_type &m1, M_type &m2) {
    m1 = rmt::matrix::Identity_Matrix();
    m2 = rmt::matrix::Identity_Matrix();
  };

  EXPECT_EQ(0ul, data.get_storage_revision());

  storage::Initialise(data, par, initialise_to_one);
  EXPECT_EQ(1ul, data.get_storage_revision());

  auto mf = par.fermion_mass, mu = par.chemical_potential;

  auto eye_matrix = rmt::matrix::Identity_Matrix();
  auto expected_m_matrix = rmt::matrix::Identity_Matrix(mf * mf);
  auto expected_mu_matrix = rmt::matrix::Identity_Matrix(mu * mu);
  auto expected_g_matrix = rmt::matrix::Identity_Matrix(
      n{1.} / (mf * mf - mu * mu + n{2.} - n{2.} * imag_i * mu));

  EXPECT_THAT(data.read(strg_name::A), MatrixApprox(eye_matrix, 1e-10));
  EXPECT_THAT(data.read(strg_name::B), MatrixApprox(eye_matrix, 1e-10));
  EXPECT_THAT(data.read(base_name::M), MatrixApprox(expected_m_matrix, 1e-10));
  EXPECT_THAT(data.read(base_name::MU),
              MatrixApprox(expected_mu_matrix, 1e-10));
  EXPECT_THAT(data.read(base_name::G), MatrixApprox(expected_g_matrix, 1e-10));
}
