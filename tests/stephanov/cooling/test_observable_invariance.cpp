/*
 * Created: 10-06-2016
 * Modified: Fri 01 Sep 2017 11:00:42 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <random>
#include <stephanov/observables/chiral_condensate.hpp>
#include <stephanov/cooling/core/index_conversion_algorithm.hpp>
#include <stephanov/cooling/general_cooling.hpp>
#include <utilities/matrix/default_constructor.hpp>
#include <utilities/matrix/eigenvalues/eigenvalue_solver.hpp>
#include <utilities/matrix/random_constructor.hpp>

#include "../../matchers.hpp"
#include "mocks/mock_cooling_helper_algs.hpp"

using rmt::M_type;
using rmt::numeric::number_type;
using rmt::numeric::complex_type;

using norm_arg_type = unit_test::stephanov::Mock_Index_Alg::arg_type;

constexpr auto num_gens = std::size_t{M_size * M_size};
using gen_array = std::array<number_type, num_gens>;

using unit_test::MatrixApprox;
using unit_test::FloatEq;

namespace model = ::rmt::stephanov;

template <typename Container_Type, typename Dist_Type, typename Generator_type>
Container_Type generate_random_container(Dist_Type &dist, Generator_type &gen)
{
  Container_Type cont{};

  std::generate(cont.begin(), cont.end(),
                [&dist, &gen]() { return number_type{dist(gen)}; });

  return cont;
};

std::pair<M_type, M_type> exponensiate_g_indices(const gen_array &indices,
                                                 number_type param)
{
  using rmt::numeric::exp;

  model::cooling::core::Advanced_Index_Conversion_Algorithm alg{};
  auto gen_vector = std::vector<number_type>{indices.begin(), indices.end()};

  rmt::matrix::Eigenvalue_Solver<
      rmt::matrix::precission_type<complex_type>::value,
      rmt::matrix::Symmetries::Hermitian>
      solver;

  solver.Compute(alg(gen_vector), rmt::matrix::eigenvalues_and_eigenvectors);

  auto eigen_values = solver.Eigenvalues();
  auto eigen_vectors = solver.Eigenvectors();

  auto diag_mat = rmt::matrix::Zero_Matrix(),
       diag_mat_i = rmt::matrix::Zero_Matrix();

  for (auto i = 0u; i < M_size; ++i) {
    diag_mat(i, i) = exp(-param * eigen_values[i]);
    diag_mat_i(i, i) = exp(param * eigen_values[i]);
  }

  return {static_cast<M_type>(eigen_vectors * diag_mat *
                              blaze::ctrans(eigen_vectors)),
          static_cast<M_type>(eigen_vectors * diag_mat_i *
                              blaze::ctrans(eigen_vectors))};
}

TEST(Stephanov_Cooling_Invariance_Test, Random_Matrix_Chiral_Condensate)
{
  namespace representation = model::cartesian;

  using core_type = model::cooling::core::Cooling_Core<
      unit_test::stephanov::Mock_Proxy<unit_test::stephanov::Mock_Index_Alg>,
      unit_test::stephanov::Mock_Proxy<
          unit_test::stephanov::Mock_Index_Converter>,
      unit_test::stephanov::Mock_Proxy<
          unit_test::stephanov::Mock_Optimise_Preset_Alg>>;

  using step_type =
      model::cooling::core::cartesian::General_Cartesian_Cooling_Step<
          core_type>;

  std::random_device rd{};
  std::mt19937 rng{rd()};
  std::uniform_real_distribution<> uniform_dist(-1., 1.);

  std::size_t updates = 10;

  // auto mu = std::complex<double> {uniform_dist(rng), uniform_dist(rng)};
  // auto mass = std::complex<double> {uniform_dist(rng), uniform_dist(rng)};
  auto mu = complex_type{uniform_dist(rng), 0.};
  auto mass = complex_type{uniform_dist(rng), 0.};

  auto a_mat = rmt::matrix::Random_Matrix(uniform_dist, rng);
  auto b_mat = rmt::matrix::Random_Matrix(uniform_dist, rng);

  auto data = std::make_shared<representation::Matrix_Storage>();

  data->set_mu(mu);
  data->set_mass(mass);

  data->read_write(representation::M_name::A) = a_mat;
  data->read_write(representation::M_name::B) = b_mat;

  data->update();

  auto observable = model::observable::Chiral_Condensate{data, {mu, mass, 1}};
  observable.Calculate();

  auto h_mat = rmt::matrix::Random_Matrix();
  auto hi_mat = static_cast<M_type>(blaze::inv(h_mat));

  unit_test::stephanov::Mock_Index_Alg idx_alg;
  unit_test::stephanov::Mock_Index_Converter idx_conv;
  unit_test::stephanov::Mock_Optimise_Preset_Alg opt_alg{h_mat, hi_mat};

  EXPECT_CALL(idx_alg, idx_alg(::testing::_)).Times(updates);
  EXPECT_CALL(idx_conv, conv_alg(::testing::_)).Times(updates);
  EXPECT_CALL(opt_alg, optimise(::testing::_, ::testing::_)).Times(updates);

  core_type core{{idx_alg}, {idx_conv}, {opt_alg}};
  step_type step{data, core};

  for (auto i = 0u; i < updates; ++i) {
    std::uniform_real_distribution<> small_dist(-1e-2, 1e-2);

    step.Update();
    observable.Calculate();

    h_mat = rmt::matrix::Identity_Matrix() +
            rmt::matrix::Random_Matrix(small_dist, rng);
    hi_mat = static_cast<M_type>(blaze::inv(h_mat));

    opt_alg.replace_matrices(h_mat, hi_mat);
  }

  auto observations = observable.data();

  ASSERT_THAT(observations.size(), ::testing::Eq(updates + 1u));
  EXPECT_THAT(observations,
              ::testing::Each(FloatEq(observations.front(), 1e-10)));
}

TEST(Stephanov_Cooling_Invariance_Test, Random_Unitary_Chiral_Condensate)
{
  namespace representation = model::cartesian;

  using core_type = model::cooling::core::Cooling_Core<
      unit_test::stephanov::Mock_Proxy<unit_test::stephanov::Mock_Index_Alg>,
      unit_test::stephanov::Mock_Proxy<
          unit_test::stephanov::Mock_Index_Converter>,
      unit_test::stephanov::Mock_Proxy<
          unit_test::stephanov::Mock_Optimise_Preset_Alg>>;

  using step_type =
      model::cooling::core::cartesian::General_Cartesian_Cooling_Step<
          core_type>;

  std::random_device rd{};
  std::mt19937 rng{rd()};
  std::uniform_real_distribution<> uniform_dist(-1., 1.);

  std::size_t updates = 10;

  auto mu = complex_type{uniform_dist(rng), 0.};
  auto mass = complex_type{uniform_dist(rng), 0.};

  auto a_mat = rmt::matrix::Random_Matrix(uniform_dist, rng);
  auto b_mat = rmt::matrix::Random_Matrix(uniform_dist, rng);

  auto data = std::make_shared<representation::Matrix_Storage>();

  data->set_mu(mu);
  data->set_mass(mass);

  data->read_write(representation::M_name::A) = a_mat;
  data->read_write(representation::M_name::B) = b_mat;

  data->update();

  auto observable = model::observable::Chiral_Condensate{data, {mu, mass, 1}};
  observable.Calculate();

  unit_test::stephanov::Mock_Index_Alg idx_alg;
  unit_test::stephanov::Mock_Index_Converter idx_conv;
  unit_test::stephanov::Mock_Optimise_Preset_Alg opt_alg{};

  EXPECT_CALL(idx_alg, idx_alg(::testing::_)).Times(updates);
  EXPECT_CALL(idx_conv, conv_alg(::testing::_)).Times(updates);
  EXPECT_CALL(opt_alg, optimise(::testing::_, ::testing::_)).Times(updates);

  core_type core{{idx_alg}, {idx_conv}, {opt_alg}};
  step_type step{data, core};

  for (auto i = 0u; i < updates; ++i) {
    std::uniform_real_distribution<> small_dist{-1e-2, 1e-2};
    auto g_indices = generate_random_container<gen_array>(small_dist, rng);
    auto transformations = exponensiate_g_indices(g_indices, number_type{1.0});

    opt_alg.replace_matrices(transformations.first, transformations.second);

    auto x = data->read(model::M_name::X);
    auto y = data->read(model::M_name::XC);

    step.Update();

    auto x_conv =
        static_cast<M_type>(transformations.first * x * transformations.second);
    auto y_conv =
        static_cast<M_type>(transformations.first * y * transformations.second);

    EXPECT_THAT(data->read(model::M_name::X), MatrixApprox(x_conv, 1e-12));
    EXPECT_THAT(data->read(model::M_name::XC), MatrixApprox(y_conv, 1e-12));

    observable.Calculate();
  }

  auto observations = observable.data();

  ASSERT_THAT(observations.size(), ::testing::Eq(updates + 1u));
  EXPECT_THAT(observations,
              ::testing::Each(FloatEq(observations.front(), 1e-10)));
}
