/*
 * Created: 06-06-2016
 * Modified: Fri 17 Feb 2017 13:15:00 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <stephanov/cooling/general_cooling.hpp>
#include <utilities/matrix/random_constructor.hpp>

#include "../../matchers.hpp"
#include "mocks/mock_cooling_helper_algs.hpp"

using rmt::M_type;
using rmt::numeric::number_type;
using rmt::numeric::complex_type;
using rmt::numeric::imag_i;

namespace model = rmt::stephanov;
using norm_arg_type = unit_test::stephanov::Mock_Index_Alg::arg_type;

using unit_test::MatrixApprox;

TEST(Stephanov_Cooling_Core_Test, Simple_Alg)
{
  using core_type = model::cooling::core::Cooling_Core<
      unit_test::stephanov::Mock_Proxy<unit_test::stephanov::Mock_Index_Alg>,
      unit_test::stephanov::Mock_Proxy<
          unit_test::stephanov::Mock_Index_Converter>,
      unit_test::stephanov::Mock_Proxy<
          unit_test::stephanov::Mock_Optimise_Alg>>;

  auto storage = std::make_shared<model::cartesian::Matrix_Storage>();

  auto norm_args = norm_arg_type{storage->read(model::M_name::X),
                                 storage->read(model::M_name::XC),
                                 storage->read_mu(), storage->read_mass()};

  unit_test::stephanov::Mock_Index_Alg idx_alg;
  unit_test::stephanov::Mock_Index_Converter idx_conv;
  unit_test::stephanov::Mock_Optimise_Alg opt_alg;

  EXPECT_CALL(idx_alg, idx_alg(norm_args)).Times(1);
  EXPECT_CALL(idx_conv, conv_alg(::testing::_)).Times(1);
  EXPECT_CALL(opt_alg, optimise(norm_args, ::testing::_)).Times(1);

  core_type core{{idx_alg}, {idx_conv}, {opt_alg}};

  auto trafo = core.get_transformation(storage);
  auto identity_matrix = rmt::matrix::Identity_Matrix();

  EXPECT_THAT(trafo.g, MatrixApprox(identity_matrix, 1e-12));
  EXPECT_THAT(trafo.g_inverse, MatrixApprox(identity_matrix, 1e-12));
}

TEST(Stephanov_General_Cartesian_Cooling_Step_Test, Random_Matrix)
{
  using core_type = model::cooling::core::Cooling_Core<
      unit_test::stephanov::Mock_Proxy<unit_test::stephanov::Mock_Index_Alg>,
      unit_test::stephanov::Mock_Proxy<
          unit_test::stephanov::Mock_Index_Converter>,
      unit_test::stephanov::Mock_Proxy<
          unit_test::stephanov::Mock_Optimise_Preset_Alg>>;

  using step_type =
      model::cooling::core::cartesian::General_Cartesian_Cooling_Step<
          core_type>;

  auto storage = std::make_shared<model::cartesian::Matrix_Storage>();

  storage->read_write(model::cartesian::M_name::A) =
      rmt::matrix::Random_Matrix();
  storage->read_write(model::cartesian::M_name::B) =
      rmt::matrix::Random_Matrix();

  auto a_cpy = storage->read(model::cartesian::M_name::A);
  auto b_cpy = storage->read(model::cartesian::M_name::B);

  storage->update();

  auto norm_args = norm_arg_type{storage->read(model::M_name::X),
                                 storage->read(model::M_name::XC),
                                 storage->read_mu(), storage->read_mass()};

  auto g = rmt::matrix::Random_Matrix();
  auto gi = rmt::matrix::Random_Matrix();

  unit_test::stephanov::Mock_Index_Alg idx_alg;
  unit_test::stephanov::Mock_Index_Converter idx_conv;
  unit_test::stephanov::Mock_Optimise_Preset_Alg opt_alg{g, gi};

  EXPECT_CALL(idx_alg, idx_alg(norm_args)).Times(1);
  EXPECT_CALL(idx_conv, conv_alg(::testing::_)).Times(1);
  EXPECT_CALL(opt_alg, optimise(norm_args, ::testing::_)).Times(1);

  core_type core{{idx_alg}, {idx_conv}, {opt_alg}};
  step_type step{storage, core};

  step.Update();

  auto x = static_cast<M_type>(a_cpy + imag_i * b_cpy);
  auto xc =
      static_cast<M_type>(blaze::trans(a_cpy) - imag_i * blaze::trans(b_cpy));

  auto x_exp = static_cast<M_type>(g * x * gi);
  auto xc_exp = static_cast<M_type>(g * xc * gi);

  EXPECT_THAT(storage->read(model::M_name::X), MatrixApprox(x_exp, 1e-10));
  EXPECT_THAT(storage->read(model::M_name::XC), MatrixApprox(xc_exp, 1e-10));

  auto a_exp = static_cast<M_type>(
      number_type{0.5} *
      (g * a_cpy * gi + blaze::trans(gi) * a_cpy * blaze::trans(g) +
       imag_i * (g * b_cpy * gi - blaze::trans(gi) * b_cpy * blaze::trans(g))));

  auto b_exp = static_cast<M_type>(
      number_type{0.5} *
      (g * b_cpy * gi + blaze::trans(gi) * b_cpy * blaze::trans(g) -
       imag_i * (g * a_cpy * gi - blaze::trans(gi) * a_cpy * blaze::trans(g))));

  EXPECT_THAT(storage->read(model::cartesian::M_name::A),
              MatrixApprox(a_exp, 1e-12));
  EXPECT_THAT(storage->read(model::cartesian::M_name::B),
              MatrixApprox(b_exp, 1e-12));
}
