/*
 * Created: 06-06-2016
 * Modified: Fri 17 Feb 2017 11:54:38 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_MOCK_COOLING_HELPER_ALGS_HPP
#define STEPHANOV_MOCK_COOLING_HELPER_ALGS_HPP

#include <functional>
#include <gmock/gmock.h>
#include <stephanov/cooling/cooling_types.hpp>
#include <utilities/matrix/default_constructor.hpp>
#include <utilities/matrix/identity_constructor.hpp>

namespace unit_test {

namespace stephanov {

class Mock_Index_Alg
{
public:
  using arg_type = rmt::stephanov::cooling::norm::Norm_Arguments;
  using return_type = rmt::stephanov::cooling::core::Generator_Indices;

  return_type operator()(const arg_type &arg)
  {
    using rmt::numeric::number_type;

    idx_alg(arg);
    return return_type(M_size * M_size - 1, number_type{0.});
  }

  MOCK_METHOD1(idx_alg, void(const arg_type &));
};

class Mock_Index_Converter
{
public:
  using arg_type = rmt::stephanov::cooling::core::Generator_Indices;
  using return_type = rmt::stephanov::cooling::core::Generator_Matrix;

  return_type operator()(const arg_type &v)
  {
    conv_alg(v);
    return rmt::matrix::Zero_Matrix();
  }

  MOCK_METHOD1(conv_alg, void(const arg_type &));
};

class Mock_Optimise_Alg
{
public:
  using norm_arg_type = rmt::stephanov::cooling::norm::Norm_Arguments;
  using arg_type = rmt::stephanov::cooling::core::Generator_Matrix;
  using return_type = rmt::stephanov::cooling::core::Transformation_Matrices;

  return_type operator()(const norm_arg_type &args, const arg_type &g)
  {
    optimise(args, g);
    return {rmt::matrix::Identity_Matrix(), rmt::matrix::Identity_Matrix()};
  }

  MOCK_METHOD2(optimise, void(const norm_arg_type &, const arg_type &));
};

class Mock_Optimise_Preset_Alg
{
public:
  using norm_arg_type = rmt::stephanov::cooling::norm::Norm_Arguments;
  using arg_type = rmt::stephanov::cooling::core::Generator_Matrix;
  using return_type = rmt::stephanov::cooling::core::Transformation_Matrices;

  Mock_Optimise_Preset_Alg() = default;

  Mock_Optimise_Preset_Alg(const rmt::M_type &t, const rmt::M_type &ti)
      : h{t}, hi{ti}
  {
  }

  void replace_matrices(const rmt::M_type &new_h, const rmt::M_type &new_hi)
  {
    h = new_h;
    hi = new_hi;
  }

  return_type operator()(const norm_arg_type &args, const arg_type &g)
  {
    optimise(args, g);
    return {h, hi};
  }

  MOCK_METHOD2(optimise, void(const norm_arg_type &, const arg_type &));

private:
  rmt::M_type h, hi;
};

template <typename Mock>
class Mock_Proxy
{
public:
  Mock_Proxy(Mock &m) : proxy(m)
  {
  }

private:
  Mock &proxy;

public:
  template <typename... Args>
  auto operator()(Args &&... args)
      -> decltype(this->proxy(std::forward<Args>(args)...))
  {
    return proxy(std::forward<Args>(args)...);
  }
};

} // namespace stephanov

} // namespace unit_test

namespace rmt {
namespace stephanov {
namespace cooling {
namespace norm {

inline bool operator==(const Norm_Arguments &lhs, const Norm_Arguments &rhs)
{
  return (&(lhs.x) == &(rhs.x)) and (&(lhs.xc) == &(rhs.xc)) and
         (&(lhs.mu) == &(rhs.mu)) and (&lhs.mass) == &(rhs.mass);
};

} // namespace norm
} // namespace cooling
} // namespace stephanov
} // namespace rmt

#endif /* STEPHANOV_MOCK_COOLING_HELPER_ALGS_HPP */
