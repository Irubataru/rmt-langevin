/*
 * Created: 06-06-2016
 * TESTING_Modified: Fri 17 Feb 2017 12:00:12 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef TESTING_MOCK_NORM_HPP
#define TESTING_MOCK_NORM_HPP

#include <functional>
#include <gmock/gmock.h>
#include <stephanov/cooling/cooling_norm/norm_interface.hpp>

namespace unit_test {
namespace stephanov {

class Mock_Norm : public rmt::stephanov::cooling::norm::Norm
{
public:
  using number_type = rmt::stephanov::cooling::norm::Norm::number_type;
  using arg_type = rmt::stephanov::cooling::norm::Norm::arg_type;

  template <typename Function>
  explicit Mock_Norm(const Function &f) : norm_func{f}
  {
    Delegate_Return_Values();
  }

  explicit Mock_Norm(number_type def_val = number_type{1.0})
      : Mock_Norm{[def_val](const arg_type &) { return def_val; }}
  {
  }

  MOCK_METHOD1(compute, number_type(const arg_type &));

  number_type forward_to_functor(const arg_type &args)
  {
    return norm_func(args);
  }

private:
  void Delegate_Return_Values()
  {
    ON_CALL(*this, compute(::testing::_))
        .WillByDefault(testing::Invoke(this, &Mock_Norm::forward_to_functor));
  }

private:
  std::function<number_type(const arg_type &)> norm_func;
};

} // namespace stephanov 
} // namespace unit_test

#endif /* TESTING_MOCK_NORM_HPP */
