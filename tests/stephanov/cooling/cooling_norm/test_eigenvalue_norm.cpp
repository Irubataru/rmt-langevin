/*
 * Created: 08-06-2016
 * Modified: Fri 17 Feb 2017 13:15:20 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <stephanov/cooling/cooling_norm/eigenvalue_norm.hpp>
#include <utilities/matrix/block_constructors.hpp>
#include <utilities/matrix/eigenvalues/eigenvalue_solver.hpp>
#include <utilities/matrix/eigenvalues/lowest_eigenvalues.hpp>
#include <utilities/matrix/random_constructor.hpp>

#include "../../../matchers.hpp"

using rmt::numeric::number_type;
using rmt::numeric::complex_type;
using rmt::numeric::imag_i;

using rmt::M_type;
using rmt::block_matrix_type;

using norm_arg_type = rmt::stephanov::cooling::norm::Norm_Arguments;
using rmt::stephanov::cooling::norm::Eigenvalue_Norm;

using unit_test::FloatEq;

namespace unit_test {

number_type random_float()
{
  static std::random_device rd{};
  static std::mt19937 rng{rd()};
  static std::uniform_real_distribution<double> dist{-1., 1.};

  return number_type{dist(rng)};
}

complex_type random_complex()
{
  return {random_float(), random_float()};
}

} // namespace unit_test

TEST(Stephanov_Eigenvalue_Norm_Test, Too_Many_Eigenvalues)
{
  auto x = rmt::matrix::Random_Matrix();
  auto xc = rmt::matrix::Random_Matrix();
  norm_arg_type norm_args{x, xc, {}, {}};

  Eigenvalue_Norm eig_norm{number_type{1.0}, 2 * M_size + 1,
                           rmt::matrix::Lowest_Eigenvalues{}};

  EXPECT_THROW({ eig_norm(norm_args); }, std::runtime_error);
}

TEST(Stephanov_Eigenvalue_Norm_Test, Random_Matrices)
{
  using rmt::numeric::exp;

  auto x = rmt::matrix::Random_Matrix();
  auto xc = rmt::matrix::Random_Matrix();
  auto mass = unit_test::random_complex(), mu = unit_test::random_complex();

  auto block_matrix = rmt::matrix::Block_Zero_Matrix();

  for (auto i = 0u; i < M_size; ++i) {
    block_matrix(i, i) = mass;
    block_matrix(i + M_size, i + M_size) = mass;

    block_matrix(i + M_size, i) = mu;
    block_matrix(i, i + M_size) = mu;
    for (auto j = 0u; j < M_size; ++j) {
      block_matrix(i, j + M_size) += imag_i * x(i, j);
      block_matrix(i + M_size, j) += imag_i * xc(i, j);
    }
  }

  auto delta_matrix = static_cast<block_matrix_type>(
      blaze::ctrans(block_matrix) * block_matrix);

  rmt::matrix::Eigenvalue_Solver<
      rmt::matrix::precission_type<complex_type>::value,
      rmt::matrix::Symmetries::Hermitian>
      solver;

  solver.Compute(delta_matrix, rmt::matrix::eigenvalues_only);

  auto eigvals = solver.Eigenvalues();
  std::sort(eigvals.begin(), eigvals.end());

  norm_arg_type norm_args{x, xc, mu, mass};

  for (auto n = 1u; n <= 2 * M_size; ++n) {
    auto xi = number_type{0.1};

    auto expected_norm = number_type{0.};
    for (auto i = 0u; i < n; ++i)
      expected_norm += exp(-xi * eigvals[i]);

    Eigenvalue_Norm eigval_norm{xi, n, rmt::matrix::Lowest_Eigenvalues{}};

    auto comp_norm = eigval_norm(norm_args);
    EXPECT_THAT(comp_norm, FloatEq(expected_norm, 1e-12));
  }
}
