/*
 * Created: 06-06-2016
 * Modified: Fri 17 Feb 2017 13:15:38 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <gtest/gtest.h>
#include <random>
#include <stephanov/cooling/core/index_computation_algorithms.hpp>
#include <stephanov/cooling/core/index_conversion_algorithm.hpp>
#include <stephanov/cooling/core/transformation_conversion_algorithms.hpp>

#include <utilities/matrix/default_constructor.hpp>
#include <utilities/matrix/eigenvalues/eigenvalue_solver.hpp>
#include <utilities/matrix/trace.hpp>

#include "../../../matchers.hpp"
#include "../mocks/mock_norm.hpp"

using rmt::M_type;
using rmt::numeric::number_type;
using rmt::numeric::complex_type;

using norm_arg_type = unit_test::stephanov::Mock_Norm::arg_type;

using unit_test::MatrixApprox;
using unit_test::FloatEq;
using unit_test::FloatNear;

namespace model = rmt::stephanov;

using nx = number_type;
using cx = complex_type;
using rmt::numeric::from_string;

TEST(Stephanov_Simple_Index_Computation_Algorithm_Test, Two_by_Two_Mathematica_Compare)
{
  static_assert(
      M_size == 2u,
      "Compiling Index Conversion for 2x2 with the wrong matrix size");

  auto mock_norm_ptr = std::make_shared<unit_test::stephanov::Mock_Norm>(
  [](const norm_arg_type &arg) {
    const auto &x = arg.x;
    return rmt::matrix::Trace(blaze::ctrans(x) * x).real();
  });

  model::cooling::core::Simple_Index_Computation_Algorithm alg{
      mock_norm_ptr, nx{1e-5}};

  EXPECT_CALL(*mock_norm_ptr, compute(::testing::_)).Times(M_size * M_size);

  auto x_mat = rmt::matrix::Matrix();
  auto xc_mat = rmt::matrix::Matrix();

  x_mat = {
      {cx{from_string("0.9223966421728178"), from_string("0.6644165859544979")},
       cx{from_string("0.5845513889406591"), from_string("0.710127410483905")}},
      {cx{from_string("0.2966393409997976"), from_string("0.6421515642527165")},
       cx{from_string("0.5512709152926916"),
          from_string("0.8537751068513109")}}};

  x_mat = blaze::trans(x_mat);

  auto norm_args = norm_arg_type{x_mat, xc_mat, {}, {}};

  auto indices = alg(norm_args);

  auto expected_indices = std::vector<nx>{
      from_string("-1.3824570879362799584553862201567289672000000"),
      from_string("0.3759242715825558221749609628745921988000000"),
      from_string("2.6749615377030460821807562500960746516000000")};

  ASSERT_EQ(indices.size(), expected_indices.size());
  EXPECT_THAT(indices,
              ::testing::Pointwise(FloatNear(1e-10), expected_indices));
}

TEST(Stephanov_Advanced_Index_Conversion_Algorithm_Test, Two_by_Two_Mathematica_Compare)
{
  static_assert(
      M_size == 2u,
      "Compiling Index Conversion for 2x2 with the wrong matrix size");

  auto alg = model::cooling::core::Advanced_Index_Conversion_Algorithm{};

  auto indices = std::vector<nx>{from_string("0.7482127438810204"),
                                 from_string("0.852800501512788"),
                                 from_string("0.7044015920127127")};

  auto expected_mat = rmt::matrix::Matrix();

  expected_mat = {
      {cx{from_string("0.7482127438810204"), from_string("0.")},
       cx{from_string("0.852800501512788"),
          from_string("-0.7044015920127127")}},
      {cx{from_string("0.852800501512788"), from_string("0.7044015920127127")},
       cx{from_string("-0.7482127438810204"), from_string("0.")}}};

  auto comp_mat = alg(indices);

  EXPECT_THAT(comp_mat, MatrixApprox(expected_mat, 1e-12));
}

// TODO: Update test to reflect change of minimisation algorithm
TEST(Stephanov_Brent_Minimalisation_Algorithm_Test, DISABLED_Two_by_Two_Mathematica_Compare)
{
  static_assert(
      M_size == 2u,
      "Compiling Index Conversion for 2x2 with the wrong matrix size");
  using rmt::numeric::exp;

  auto mock_norm_ptr = std::make_shared<unit_test::stephanov::Mock_Norm>(
  [](const norm_arg_type &arg) {
    const auto &x = arg.x;
    return rmt::matrix::Trace(blaze::ctrans(x) * x).real();
  });

  auto alg = model::cooling::core::Brent_Minimalisation_Algorithm{
      mock_norm_ptr, 0.01, 10};

  auto x_mat = rmt::matrix::Matrix();
  auto xc_mat = rmt::matrix::Matrix();

  x_mat = {
      {cx{from_string("0.9223966421728178"), from_string("0.6644165859544979")},
       cx{from_string("0.5845513889406591"), from_string("0.710127410483905")}},
      {cx{from_string("0.2966393409997976"), from_string("0.6421515642527165")},
       cx{from_string("0.5512709152926916"),
          from_string("0.8537751068513109")}}};

  x_mat = blaze::trans(x_mat);

  auto norm_args = norm_arg_type{x_mat, xc_mat, {}, {}};

  auto g_mat = rmt::matrix::Matrix();

  g_mat = {{cx{from_string("-1.3824570879362799584553862201567289672"),
               from_string("0.")},
            cx{from_string("0.3759242715825558221749609628745921988"),
               from_string("-2.6749615377030460821807562500960746516")}},
           {cx{from_string("0.3759242715825558221749609628745921988"),
               from_string("2.6749615377030460821807562500960746516")},
            cx{from_string("1.3824570879362799584553862201567289672"),
               from_string("0.")}}};

  rmt::matrix::Eigenvalue_Solver<rmt::matrix::precission_type<cx>::value,
                                 rmt::matrix::Symmetries::Hermitian>
      solver;

  solver.Compute(g_mat, rmt::matrix::eigenvalues_and_eigenvectors);

  auto g_eigen_values = solver.Eigenvalues();
  auto g_eigen_vectors = solver.Eigenvectors();

  auto optimal_alpha = nx{0.0475};
  EXPECT_CALL(*mock_norm_ptr, compute(::testing::_)).Times(11);

  auto d = rmt::matrix::Zero_Matrix(), di = rmt::matrix::Zero_Matrix();
  for (auto i = 0u; i < M_size; ++i) {
    d(i, i) = exp(-optimal_alpha * g_eigen_values[i]);
    di(i, i) = exp(optimal_alpha * g_eigen_values[i]);
  }

  auto trafo =
      static_cast<M_type>(g_eigen_vectors * d * blaze::ctrans(g_eigen_vectors));
  auto trafo_inv = static_cast<M_type>(g_eigen_vectors * di *
                                       blaze::ctrans(g_eigen_vectors));

  auto computed = alg(norm_args, g_mat);

  EXPECT_THAT(computed.g, MatrixApprox(trafo, 1e-12));
  EXPECT_THAT(computed.g_inverse, MatrixApprox(trafo_inv, 1e-12));
}
