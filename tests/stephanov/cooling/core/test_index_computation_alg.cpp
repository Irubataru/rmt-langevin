
/*
 * Created: 17-02-2017
 * Modified: Fri 01 Sep 2017 11:03:26 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <gtest/gtest.h>
#include <stephanov/cooling/core/index_computation_algorithms.hpp>

#include <algorithm>
#include <random>
#include <utilities/matrix/identity_constructor.hpp>

#include "../../../matchers.hpp"
#include "../mocks/mock_norm.hpp"

using rmt::M_type;
using rmt::numeric::number_type;
using rmt::numeric::complex_type;
using norm_arg_type = unit_test::stephanov::Mock_Norm::arg_type;

using unit_test::MatrixApprox;
using unit_test::FloatEq;

namespace model = rmt::stephanov;

TEST(Stephanov_Numerical_Derivative_IdxCmpAlg, Constant_Norm)
{
  auto elem_x = rmt::matrix::Identity_Matrix();
  auto elem_xc = rmt::matrix::Identity_Matrix();

  auto mock_norm_ptr =
      std::make_shared<unit_test::stephanov::Mock_Norm>(number_type{1.});

  model::cooling::core::Numerical_Derivative_IdxCmpAlg alg{mock_norm_ptr,
                                                           number_type{1.}};

  EXPECT_CALL(*mock_norm_ptr, compute(::testing::_)).Times(M_size * M_size + 1);

  auto norm_args = norm_arg_type{elem_x, elem_xc, {}, {}};
  auto indices = alg(norm_args);

  for (auto gi : indices)
    EXPECT_THAT(gi, FloatEq(number_type{0.}));
};

TEST(Stephanov_Numerical_Derivative_IdxCmpAlg, Counting_Norm)
{
  auto elem_x = rmt::matrix::Identity_Matrix();
  auto elem_xc = rmt::matrix::Identity_Matrix();

  auto mock_norm_ptr = std::make_shared<unit_test::stephanov::Mock_Norm>(
      [](const norm_arg_type &) {
        static int it = 0;
        return static_cast<number_type>(it++);
      });

  model::cooling::core::Numerical_Derivative_IdxCmpAlg alg{mock_norm_ptr,
                                                           number_type{1.}};

  EXPECT_CALL(*mock_norm_ptr, compute(::testing::_)).Times(M_size * M_size + 1);

  auto norm_args = norm_arg_type{elem_x, elem_xc, {}, {}};
  auto indices = alg(norm_args);

  auto i = 1.0;
  for (auto gi : indices)
    EXPECT_THAT(gi, FloatEq(number_type{i++}));
};
