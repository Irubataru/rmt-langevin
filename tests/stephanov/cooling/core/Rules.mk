SRCS := *.cpp
SRCS_EXCLUDES :=

ifeq (,$(findstring DMATSIZE=2,$(CXXFLAGS)))
	SRCS_EXCLUDES += test_cooling_core_2.cpp
endif

ifeq (,$(findstring DMATSIZE=3,$(CXXFLAGS)))
	SRCS_EXCLUDES += test_cooling_core_3.cpp
endif
