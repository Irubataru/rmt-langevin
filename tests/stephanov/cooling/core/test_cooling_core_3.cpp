/*
 * Created: 06-06-2016
 * Modified: Fri 17 Feb 2017 13:15:53 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <gtest/gtest.h>
#include <random>
#include <stephanov/cooling/core/index_computation_algorithms.hpp>
#include <stephanov/cooling/core/index_conversion_algorithm.hpp>
#include <stephanov/cooling/core/transformation_conversion_algorithms.hpp>

#include <utilities/matrix/default_constructor.hpp>
#include <utilities/matrix/eigenvalues/eigenvalue_solver.hpp>
#include <utilities/matrix/trace.hpp>

#include "../../../matchers.hpp"
#include "../mocks/mock_norm.hpp"

using rmt::M_type;
using rmt::numeric::number_type;
using rmt::numeric::complex_type;

using norm_arg_type = unit_test::stephanov::Mock_Norm::arg_type;

using unit_test::MatrixApprox;
using unit_test::FloatEq;
using unit_test::FloatNear;

namespace model = rmt::stephanov;

using nx = number_type;
using cx = complex_type;
using rmt::numeric::from_string;

TEST(Stephanov_Simple_Index_Computation_Algorithm_Test,
     Three_by_Three_Mathematica_Compare)
{
  static_assert(
      M_size == 3u,
      "Compiling Index Conversion for 3x3 with the wrong matrix size");

  auto mock_norm_ptr = std::make_shared<unit_test::stephanov::Mock_Norm>(
  [](const norm_arg_type &arg) {
    const auto &x = arg.x;
    return rmt::matrix::Trace(blaze::ctrans(x) * x).real();
  });

  model::cooling::core::Simple_Index_Computation_Algorithm alg{
      mock_norm_ptr, 1e-5};

  EXPECT_CALL(*mock_norm_ptr, compute(::testing::_)).Times(M_size * M_size);

  auto x_mat = rmt::matrix::Matrix();
  auto xc_mat = rmt::matrix::Matrix();

  x_mat = {
      {cx{from_string("0.6003457060723938"), from_string("0.6186527255449521")},
       cx{from_string("0.9023489283429673"), from_string("0.9186848633475626")},
       cx{from_string("0.41973832436987246"),
          from_string("0.06650444372339348")}},
      {cx{from_string("0.2687637978654256"), from_string("0.7471500324329758")},
       cx{from_string("0.503732986456821"), from_string("0.0879420385641636")},
       cx{from_string("0.6175076882592296"),
          from_string("0.8865056508708054")}},
      {cx{from_string("0.7357396395111542"),
          from_string("0.16257442010152712")},
       cx{from_string("0.09904486980337679"),
          from_string("0.22259321889584882")},
       cx{from_string("0.49701115880518"), from_string("0.9531270599946622")}}};

  x_mat = blaze::trans(x_mat);

  auto norm_args = norm_arg_type{x_mat, xc_mat, {}, {}};

  auto indices = alg(norm_args);

  auto expected_indices = std::vector<nx>{
      from_string("-1.1209008820634376441109246557813054628710000"),
      from_string("-2.4965542227414458515707446274430481874704799"),
      from_string("-0.2273308409163338185245846384648399204710000"),
      from_string("0.0725912340872417481808251273319607163290000"),
      from_string("2.1357576767226836166911602730777811952090000"),
      from_string("1.1876851544955400428710406293828447559770000"),
      from_string("-1.8010712022755452349378011400666753854860000"),
      from_string("-0.8167401742574088553449283228520305365260000")};

  ASSERT_EQ(indices.size(), expected_indices.size());
  EXPECT_THAT(indices, ::testing::Pointwise(FloatNear(1e-9), expected_indices));
}

TEST(Stephanov_Advanced_Index_Conversion_Algorithm_Test, Three_by_Three)
{
  static_assert(
      M_size == 3u,
      "Compiling Index Conversion for 3x3 with the wrong matrix size");

  auto alg = model::cooling::core::Advanced_Index_Conversion_Algorithm{};

  auto indices = std::vector<nx>{
      from_string("0.8519666830670936"),  from_string("0.6425146957763215"),
      from_string("0.7609911014644066"),  from_string("0.24029072549511632"),
      from_string("0.20490887942329095"), from_string("0.7369865089304857"),
      from_string("0.7669194739463201"),  from_string("0.6459603330244441")};

  auto expected_mat = rmt::matrix::Matrix();

  expected_mat = {
      {cx{from_string("1.2229227156318433222504316113562262865208452316033"),
          from_string("0.")},
       cx{from_string("0.7609911014644066"),
          from_string("-0.24029072549511632")},
       cx{from_string("0.20490887942329095"),
          from_string("-0.7369865089304857")}},
      {cx{from_string("0.7609911014644066"),
          from_string("0.24029072549511632")},
       cx{from_string("-0.4810106505023438777495683886437737134791547683967"),
          from_string("0.")},
       cx{from_string("0.7669194739463201"),
          from_string("-0.6459603330244441")}},
      {cx{from_string("0.20490887942329095"),
          from_string("0.7369865089304857")},
       cx{from_string("0.7669194739463201"), from_string("0.6459603330244441")},
       cx{from_string("-0.74191206512949944450086322271245257304169046320664"),
          from_string("0.")}}};

  auto comp_mat = alg(indices);
  EXPECT_THAT(comp_mat, MatrixApprox(expected_mat, nx{1e-12}));
}

// TODO: Update test to reflect change of minimisation algorithm
TEST(Stephanov_Brent_Minimalisation_Algorithm_Test, DISABLED_Three_by_Three_Mathematica_Compare)
{
  static_assert(
      M_size == 3u,
      "Compiling Index Conversion for 3x3 with the wrong matrix size");
  using rmt::numeric::exp;

  auto mock_norm_ptr = std::make_shared<unit_test::stephanov::Mock_Norm>(
  [](const norm_arg_type &arg) {
    const auto &x = arg.x;
    return rmt::matrix::Trace(blaze::ctrans(x) * x).real();
  });

  auto alg = model::cooling::core::Brent_Minimalisation_Algorithm{
      mock_norm_ptr, nx{0.01}, 10};

  auto x_mat = rmt::matrix::Matrix();
  auto xc_mat = rmt::matrix::Matrix();

  x_mat = {
      {cx{from_string("0.6003457060723938"), from_string("0.6186527255449521")},
       cx{from_string("0.9023489283429673"), from_string("0.9186848633475626")},
       cx{from_string("0.41973832436987246"),
          from_string("0.06650444372339348")}},
      {cx{from_string("0.2687637978654256"), from_string("0.7471500324329758")},
       cx{from_string("0.503732986456821"), from_string("0.0879420385641636")},
       cx{from_string("0.6175076882592296"),
          from_string("0.8865056508708054")}},
      {cx{from_string("0.7357396395111542"),
          from_string("0.16257442010152712")},
       cx{from_string("0.09904486980337679"),
          from_string("0.22259321889584882")},
       cx{from_string("0.49701115880518"), from_string("0.9531270599946622")}}};

  x_mat = blaze::trans(x_mat);

  auto norm_args = norm_arg_type{x_mat, xc_mat, {}, {}};

  auto g_mat = rmt::matrix::Matrix();

  g_mat = {{cx{from_string("-2.5622871346097083270901185210004065824117052"),
               from_string("0.")},
            cx{from_string("-0.227330840916333818524584638464839920471"),
               from_string("-0.072591234087241748180825127331960716329")},
            cx{from_string("2.135757676722683616691160273077781195209"),
               from_string("-1.187685154495540042871040629382844755977")}},
           {cx{from_string("-0.2273308409163338185245846384648399204710000"),
               from_string("0.0725912340872417481808251273319607163290000")},
            cx{from_string("-0.3204853704828330388682692094377956566697052"),
               from_string("0.")},
            cx{from_string("-1.8010712022755452349378011400666753854860000"),
               from_string("0.8167401742574088553449283228520305365260000")}},
           {cx{from_string("2.1357576767226836166911602730777811952090000"),
               from_string("1.1876851544955400428710406293828447559770000")},
            cx{from_string("-1.8010712022755452349378011400666753854860000"),
               from_string("-0.8167401742574088553449283228520305365260000")},
            cx{from_string("2.8827725050925413659583877304382022390814105"),
               from_string("0.")}}};

  rmt::matrix::Eigenvalue_Solver<rmt::matrix::precission_type<cx>::value,
                                 rmt::matrix::Symmetries::Hermitian>
      solver;

  solver.Compute(g_mat, rmt::matrix::eigenvalues_and_eigenvectors);

  auto g_eigen_values = solver.Eigenvalues();
  auto g_eigen_vectors = solver.Eigenvectors();

  auto optimal_alpha = nx{0.04};
  EXPECT_CALL(*mock_norm_ptr, compute(::testing::_)).Times(11);

  auto d = rmt::matrix::Zero_Matrix(), di = rmt::matrix::Zero_Matrix();
  for (auto i = 0u; i < M_size; ++i) {
    d(i, i) = exp(-optimal_alpha * g_eigen_values[i]);
    di(i, i) = exp(optimal_alpha * g_eigen_values[i]);
  }

  auto trafo =
      static_cast<M_type>(g_eigen_vectors * d * blaze::ctrans(g_eigen_vectors));
  auto trafo_inv = static_cast<M_type>(g_eigen_vectors * di *
                                       blaze::ctrans(g_eigen_vectors));

  auto computed = alg(norm_args, g_mat);

  EXPECT_THAT(computed.g, MatrixApprox(trafo, 1e-12));
  EXPECT_THAT(computed.g_inverse, MatrixApprox(trafo_inv, 1e-12));
}
