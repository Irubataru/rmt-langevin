
/*
 * Created: 17-02-2017
 * Modified: Fri 17 Feb 2017 13:26:29 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <gtest/gtest.h>
#include <stephanov/cooling/core/index_conversion_algorithm.hpp>

#include <algorithm>
#include <random>
#include <utilities/matrix/default_constructor.hpp>

#include "../../../matchers.hpp"
#include "../mocks/mock_norm.hpp"

using rmt::M_type;
using rmt::numeric::number_type;
using rmt::numeric::complex_type;
using norm_arg_type = unit_test::stephanov::Mock_Norm::arg_type;

using unit_test::MatrixApprox;
using unit_test::FloatEq;

namespace model = rmt::stephanov;

// TODO: A norm that actually checks the generators

TEST(Stephanov_Advanced_Index_Conversion_Algorithm_Test, Idetity_Vector)
{
  using rmt::numeric::sqrt;
  auto alg = model::cooling::core::Advanced_Index_Conversion_Algorithm{};

  auto expected_mat = rmt::matrix::Zero_Matrix();

  for (auto i = 0ul; i < M_size; ++i)
    expected_mat(i, i) += sqrt(2. / static_cast<number_type>(M_size));

  for (auto i = 1u; i < M_size; ++i) {
    auto d = static_cast<number_type>(i + 1);
    auto f = sqrt(number_type{2.} / (d * (d - number_type{1.})));

    for (auto j = 0u; j < i; ++j)
      expected_mat(j, j) += f;

    expected_mat(i, i) += f * (number_type{1.} - d);
  }

  for (auto i = 0u; i < M_size; ++i) {
    for (auto j = i + 1; j < M_size; ++j) {
      expected_mat(i, j) = complex_type{1., -1.};
      expected_mat(j, i) = complex_type{1., 1.};
    }
  }

  auto indices = std::vector<number_type>(M_size * M_size, 1.);
  auto comp_mat = alg(indices);

  EXPECT_THAT(comp_mat, MatrixApprox(expected_mat));
}

TEST(Stephanov_Advanced_Index_Conversion_Algorithm_Test, Random_Vector)
{
  using rmt::numeric::sqrt;
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dist{0., 1.};

  auto alg = model::cooling::core::Advanced_Index_Conversion_Algorithm{};

  auto indices = std::vector<number_type>(M_size * M_size);
  std::generate(indices.begin(), indices.end(),
                [&gen, &dist]() { return number_type{dist(gen)}; });

  auto expected_mat = rmt::matrix::Zero_Matrix();

  auto it = indices.begin();

  {
    auto v = sqrt(2. / static_cast<number_type>(M_size)) * (*(it++));
    for (auto i = 0ul; i < M_size; ++i)
      expected_mat(i,i) += v;
  }

  for (auto i = 1u; i < M_size; ++i) {
    auto d = static_cast<number_type>(i + 1);
    auto f = sqrt(number_type{2.} / (d * (d - number_type{1.})));
    auto v = *(it++);

    for (auto j = 0u; j < i; ++j)
      expected_mat(j, j) += f * v;

    expected_mat(i, i) += f * (number_type{1.} - d) * v;
  }

  for (auto i = 0u; i < M_size; ++i) {
    for (auto j = i + 1; j < M_size; ++j) {
      auto vr = *(it++), vi = *(it++);
      expected_mat(i, j) = complex_type{vr, -vi};
      expected_mat(j, i) = complex_type{vr, vi};
    }
  }

  auto comp_mat = alg(indices);

  EXPECT_THAT(comp_mat, MatrixApprox(expected_mat));
}
