/*
 * Created: 07-04-2016
 * Modified: Fri 01 Sep 2017 10:59:51 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <boost/math/constants/constants.hpp>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <stephanov/observables/determinant.hpp>
#include <stephanov/resource/initialiser.hpp>
#include <types.hpp>

#include "../../matchers.hpp"

using rmt::M_type;
using n = rmt::numeric::number_type;
using rmt::numeric::complex_type;
using rmt::numeric::imag_i;

using unit_test::FloatEq;

TEST(Stephanov_Determinant_Cartesian, Test_Zero_Init)
{
  namespace model = rmt::stephanov;
  namespace storage = model::cartesian;

  auto par = rmt::Physical_Parameters{{1.1, 0.2},  // chemical_potential
                                      {-2.1, 4.2}, // fermion_mass
                                      11};         // number_of_flavours

  auto data = std::make_shared<storage::Matrix_Storage>();

  auto initialise_to_zero = [](M_type &m1, M_type &m2) {
    m1 = rmt::matrix::Zero_Matrix();
    m2 = rmt::matrix::Zero_Matrix();
  };

  EXPECT_EQ(0ul, data->get_storage_revision());

  storage::Initialise(*data, par, initialise_to_zero);
  EXPECT_EQ(1ul, data->get_storage_revision());

  auto obs = model::observable::Determinant{data};
  obs.Calculate();

  auto det_val = obs.extract().front();
  auto mf = par.fermion_mass, mu = par.chemical_potential;
  auto ms_d = static_cast<n>(M_size);

  auto expected_real_value = rmt::numeric::pow(mf * mf - mu * mu, ms_d).real();
  auto expected_imag_value = rmt::numeric::pow(mf * mf - mu * mu, ms_d).imag();
  EXPECT_THAT(det_val.real(), FloatEq(expected_real_value, 1e-12));
  EXPECT_THAT(det_val.imag(), FloatEq(expected_imag_value, 1e-12));
}

TEST(Stephanov_Determinant_Cartesian, Test_One_Init)
{
  namespace model = rmt::stephanov;
  namespace storage = model::cartesian;

  auto par = rmt::Physical_Parameters{{1.1, 0.2},  // chemical_potential
                                      {-2.1, 4.2}, // fermion_mass
                                      11};         // number_of_flavours

  auto data = std::make_shared<storage::Matrix_Storage>();

  auto initialise_to_one = [](M_type &m1, M_type &m2) {
    m1 = rmt::matrix::Identity_Matrix();
    m2 = rmt::matrix::Identity_Matrix();
  };

  EXPECT_EQ(0ul, data->get_storage_revision());

  storage::Initialise(*data, par, initialise_to_one);
  EXPECT_EQ(1ul, data->get_storage_revision());

  auto obs = model::observable::Determinant{data};
  obs.Calculate();

  auto mf = par.fermion_mass, mu = par.chemical_potential;
  auto ms_d = static_cast<n>(M_size);

  auto det_val = obs.extract().front();

  auto expected_value =
      rmt::numeric::pow(mf * mf - mu * mu + n{2.} - n{2.} * imag_i * mu, ms_d);

  EXPECT_THAT(det_val.real(), FloatEq(expected_value.real(), 1e-12));
  EXPECT_THAT(det_val.imag(), FloatEq(expected_value.imag(), 1e-12));
}

TEST(Stephanov_Determinant_Polar, Test_Zero_Init)
{
  namespace model = rmt::stephanov;
  namespace storage = model::polar;

  auto par = rmt::Physical_Parameters{{1.1, 0.2},  // chemical_potential
                                      {-2.1, 4.2}, // fermion_mass
                                      11};         // number_of_flavours

  auto data = std::make_shared<storage::Matrix_Storage>();

  auto initialise_to_zero = [](M_type &m1, M_type &m2) {
    m1 = rmt::matrix::Zero_Matrix();
    m2 = rmt::matrix::Zero_Matrix();
  };

  EXPECT_EQ(0ul, data->get_storage_revision());

  storage::Initialise(*data, par, initialise_to_zero);
  EXPECT_EQ(1ul, data->get_storage_revision());

  auto obs = model::observable::Determinant{data};
  obs.Calculate();

  auto det_val = obs.extract().front();
  auto mf = par.fermion_mass, mu = par.chemical_potential;
  auto ms_d = static_cast<n>(M_size);

  auto expected_real = rmt::numeric::pow(mf * mf - mu * mu, ms_d).real();
  auto expected_imag = rmt::numeric::pow(mf * mf - mu * mu, ms_d).imag();
  EXPECT_THAT(det_val.real(), FloatEq(expected_real, 1e-12));
  EXPECT_THAT(det_val.imag(), FloatEq(expected_imag, 1e-12));
}

TEST(Stephanov_Determinant_Polar, Test_One_Init)
{
  namespace model = rmt::stephanov;
  namespace storage = model::polar;

  auto par = rmt::Physical_Parameters{{1.1, 0.2},  // chemical_potential
                                      {-2.1, 4.2}, // fermion_mass
                                      11};         // number_of_flavours

  auto data = std::make_shared<storage::Matrix_Storage>();

  auto initialise_to_one = [](M_type &m1, M_type &m2) {
    using rmt::numeric::sqrt;
    using boost::math::constants::pi;
    m1 = rmt::matrix::Identity_Matrix(rmt::numeric::sqrt(n{2.}));
    m2 = rmt::matrix::Identity_Matrix(n{0.25} * pi<n>());
  };

  EXPECT_EQ(0ul, data->get_storage_revision());

  storage::Initialise(*data, par, initialise_to_one);
  EXPECT_EQ(1ul, data->get_storage_revision());

  auto obs = model::observable::Determinant{data};
  obs.Calculate();

  auto mf = par.fermion_mass, mu = par.chemical_potential;
  auto ms_d = static_cast<n>(M_size);

  auto det_val = obs.extract().front();

  auto expected_value =
      rmt::numeric::pow(mf * mf - mu * mu + n{2.} - n{2.} * imag_i * mu, ms_d);

  EXPECT_THAT(det_val.real(), FloatEq(expected_value.real(), 1e-12));
  EXPECT_THAT(det_val.imag(), FloatEq(expected_value.imag(), 1e-12));
}
