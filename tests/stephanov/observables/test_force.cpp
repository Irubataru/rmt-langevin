/*
 * Created: 09-05-2016
 * Modified: Fri 01 Sep 2017 11:33:13 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <gtest/gtest.h>
#include <stephanov/observables/force_max.hpp>
#include <utilities/matrix/random_constructor.hpp>

#include "../../matchers.hpp"
#include "../langevin/mocks/mock_force.hpp"

using rmt::M_type;
using rmt::numeric::number_type;
using rmt::numeric::complex_type;

using unit_test::FloatEq;

namespace model = rmt::stephanov;

TEST(Stephanov_Force_Max, Test_Random_Matrices_Cartesian)
{
  namespace storage = model::cartesian;
  using rmt::numeric::abs;

  auto force_a = rmt::matrix::Random_Matrix();
  auto force_b = rmt::matrix::Random_Matrix();

  auto data = std::make_shared<storage::Matrix_Storage>();
  auto force =
      std::make_shared<unit_test::Mock_Cartesian_Force>(data, force_a, force_b);

  storage::observable::Force_Max obs{force};

  EXPECT_CALL(*force, do_update()).Times(0);
  EXPECT_CALL(*force, get_force_a()).Times(1);
  EXPECT_CALL(*force, get_force_b()).Times(1);

  auto max_a = number_type{0.}, max_b = number_type{0.};

  for (auto i = 0; i < force_a.rows(); ++i) {
    for (auto j = 0ul; j < force_a.columns(); ++j) {
      if (abs(force_a(i, j)) > max_a)
        max_a = abs(force_a(i, j));

      if (abs(force_b(i, j)) > max_b)
        max_b = abs(force_b(i, j));
    }
  }

  auto tot_max = (max_a > max_b) ? max_a : max_b;

  obs.Calculate();

  auto measured = obs.data().front();
  EXPECT_THAT(measured, FloatEq(tot_max));
}

TEST(Stephanov_Force_Max, Test_Random_Matrices_Polar)
{
  namespace storage = model::polar;

  auto force_r = rmt::matrix::Random_Matrix();
  auto force_p = rmt::matrix::Random_Matrix();

  auto data = std::make_shared<storage::Matrix_Storage>();
  auto force =
      std::make_shared<unit_test::Mock_Polar_Force>(data, force_r, force_p);

  storage::observable::Force_Max obs{force};

  EXPECT_CALL(*force, update()).Times(0);
  EXPECT_CALL(*force, get_force_r()).Times(1);
  EXPECT_CALL(*force, get_force_phi()).Times(1);

  auto max_r = number_type{0.}, max_p = number_type{0.};

  for (auto i = 0; i < force_r.rows(); ++i) {
    for (auto j = 0ul; j < force_r.columns(); ++j) {
      if (abs(force_r(i, j)) > max_r)
        max_r = abs(force_r(i, j));

      if (abs(force_p(i, j)) > max_p)
        max_p = abs(force_p(i, j));
    }
  }

  auto tot_max = (max_r > max_p) ? max_r : max_p;

  obs.Calculate();

  auto measured = obs.data().front();
  EXPECT_THAT(measured, FloatEq(tot_max));
}
