/*
 * Created: 07-04-2016
 * Modified: Fri 01 Sep 2017 10:59:46 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <boost/math/constants/constants.hpp>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <stephanov/observables/chiral_condensate.hpp>
#include <stephanov/resource/initialiser.hpp>
#include <types.hpp>

#include "../../matchers.hpp"

using rmt::M_type;
using n = rmt::numeric::number_type;
using rmt::numeric::complex_type;
using rmt::numeric::imag_i;

using unit_test::FloatEq;

TEST(Stephanov_Chiral_Condensate_Cartesian, Test_Zero_Init)
{
  namespace model = rmt::stephanov;
  namespace storage = model::cartesian;

  auto par = rmt::Physical_Parameters{{1.1, 0.2},  // chemical_potential
                                      {-2.1, 4.2}, // fermion_mass
                                      11};         // number_of_flavours

  auto data = std::make_shared<storage::Matrix_Storage>();

  auto initialise_to_zero = [](M_type &m1, M_type &m2) {
    m1 = rmt::matrix::Zero_Matrix();
    m2 = rmt::matrix::Zero_Matrix();
  };

  EXPECT_EQ(0ul, data->get_storage_revision());

  storage::Initialise(*data, par, initialise_to_zero);
  EXPECT_EQ(1ul, data->get_storage_revision());

  auto obs = model::observable::Chiral_Condensate{data, par};
  obs.Calculate();

  auto mf = par.fermion_mass, mu = par.chemical_potential;
  auto nf_d = static_cast<n>(par.number_of_flavours);

  auto det_val = obs.extract().front();
  auto expected_val = n{2.} * mf * nf_d / (mf * mf - mu * mu);

  EXPECT_THAT(det_val.real(), FloatEq(expected_val.real(), 1e-12));
  EXPECT_THAT(det_val.imag(), FloatEq(expected_val.imag(), 1e-12));
}

TEST(Stephanov_Chiral_Condensate_Cartesian, Test_One_Init)
{
  namespace model = rmt::stephanov;
  namespace storage = model::cartesian;

  auto par = rmt::Physical_Parameters{{1.1, 0.2},  // chemical_potential
                                      {-2.1, 4.2}, // fermion_mass
                                      11};         // number_of_flavours

  auto data = std::make_shared<storage::Matrix_Storage>();

  auto initialise_to_one = [](M_type &m1, M_type &m2) {
    m1 = rmt::matrix::Identity_Matrix();
    m2 = rmt::matrix::Identity_Matrix();
  };

  EXPECT_EQ(0ul, data->get_storage_revision());

  storage::Initialise(*data, par, initialise_to_one);
  EXPECT_EQ(1ul, data->get_storage_revision());

  auto obs = model::observable::Chiral_Condensate{data, par};
  obs.Calculate();

  auto mf = par.fermion_mass, mu = par.chemical_potential;
  auto nf_d = static_cast<n>(par.number_of_flavours);

  auto det_val = obs.extract().front();
  auto expected_value =
      n{2.} * mf * nf_d / (mf * mf - mu * mu + n{2.} - n{2.} * imag_i * mu);

  EXPECT_THAT(det_val.real(), FloatEq(expected_value.real(), 1e-12));
  EXPECT_THAT(det_val.imag(), FloatEq(expected_value.imag(), 1e-12));
}

TEST(Stephanov_Chiral_Condensate_Polar, Test_Zero_Init)
{
  namespace model = rmt::stephanov;
  namespace storage = model::polar;

  auto par = rmt::Physical_Parameters{{1.1, 0.2},  // chemical_potential
                                      {-2.1, 4.2}, // fermion_mass
                                      11};         // number_of_flavours

  auto data = std::make_shared<storage::Matrix_Storage>();

  auto initialise_to_zero = [](M_type &m1, M_type &m2) {
    m1 = rmt::matrix::Zero_Matrix();
    m2 = rmt::matrix::Zero_Matrix();
  };

  EXPECT_EQ(0ul, data->get_storage_revision());

  storage::Initialise(*data, par, initialise_to_zero);
  EXPECT_EQ(1ul, data->get_storage_revision());

  auto obs = model::observable::Chiral_Condensate{data, par};
  obs.Calculate();

  auto mf = par.fermion_mass, mu = par.chemical_potential;
  auto nf_d = static_cast<n>(par.number_of_flavours);

  auto det_val = obs.extract().front();
  auto expected_val = n{2.} * mf * nf_d / (mf * mf - mu * mu);

  EXPECT_THAT(det_val.real(), FloatEq(expected_val.real(), 1e-12));
  EXPECT_THAT(det_val.imag(), FloatEq(expected_val.imag(), 1e-12));
}

TEST(Stephanov_Chiral_Condensate_Polar, Test_One_Init)
{
  namespace model = rmt::stephanov;
  namespace storage = model::polar;

  auto par = rmt::Physical_Parameters{{1.1, 0.2},  // chemical_potential
                                      {-2.1, 4.2}, // fermion_mass
                                      11};         // number_of_flavours

  auto data = std::make_shared<storage::Matrix_Storage>();

  auto r_val = complex_type{1.231, -6.71};
  auto phi_val = complex_type{21.51, 3.11};

  auto initialise_to_one = [r_val, phi_val](M_type &m1, M_type &m2) {
    using rmt::numeric::sqrt;
    using boost::math::constants::pi;
    m1 = rmt::matrix::Identity_Matrix(r_val);
    m2 = rmt::matrix::Identity_Matrix(phi_val);
  };

  EXPECT_EQ(0ul, data->get_storage_revision());

  storage::Initialise(*data, par, initialise_to_one);
  EXPECT_EQ(1ul, data->get_storage_revision());

  auto obs = model::observable::Chiral_Condensate{data, par};
  obs.Calculate();

  auto mf = par.fermion_mass, mu = par.chemical_potential;
  auto nf_d = static_cast<n>(par.number_of_flavours);

  auto det_val = obs.extract().front();

  auto x_val = r_val * rmt::numeric::exp(imag_i * phi_val);
  auto x_star_val = r_val * rmt::numeric::exp(-imag_i * phi_val);
  auto expected_value =
      n{2.} * mf * nf_d / (mf * mf - mu * mu + x_star_val * x_val -
                           imag_i * mu * (x_val + x_star_val));

  EXPECT_THAT(det_val.real(), FloatEq(expected_value.real(), 1e-12));
  EXPECT_THAT(det_val.imag(), FloatEq(expected_value.imag(), 1e-12));
}
