/*
 * Created: 19-04-2016
 * Modified: Tue 19 Apr 2016 19:44:21 CEST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef CUSTOM_MATCHERS_HPP
#define CUSTOM_MATCHERS_HPP

#include <complex>
#include <gmock/gmock.h>

namespace unit_test {

class DoubleEqualMatcher
    : public ::testing::MatcherInterface<::testing::tuple<double, double>>
{
public:
  virtual bool
  MatchAndExplain(::testing::tuple<double, double> arg,
                  ::testing::MatchResultListener *listener) const override
  {
    auto double_eq = static_cast<::testing::Matcher<double>>(
        ::testing::DoubleEq(std::get<0>(arg)));
    return double_eq.MatchAndExplain(std::get<1>(arg), listener);
  }

  virtual void DescribeTo(std::ostream *os) const override
  {
    *os << "are equal";
  }

  virtual void DescribeNegationTo(std::ostream *os) const override
  {
    *os << "are unequal";
  }
};

inline ::testing::Matcher<::testing::tuple<double, double>> DoubleEqual()
{
  return MakeMatcher(new DoubleEqualMatcher);
}

class DoubleRealMatcher : public ::testing::MatcherInterface<
                              ::testing::tuple<std::complex<double>, double>>
{
public:
  virtual bool
  MatchAndExplain(::testing::tuple<std::complex<double>, double> arg,
                  ::testing::MatchResultListener *listener) const override
  {
    auto double_eq = static_cast<::testing::Matcher<double>>(
        ::testing::DoubleEq(std::get<0>(arg).real()));
    return double_eq.MatchAndExplain(std::get<1>(arg), listener);
  }

  virtual void DescribeTo(std::ostream *os) const override
  {
    *os << "are equal";
  }

  virtual void DescribeNegationTo(std::ostream *os) const override
  {
    *os << "are unequal";
  }
};

inline ::testing::Matcher<::testing::tuple<std::complex<double>, double>>
DoubleRealEqual()
{
  return MakeMatcher(new DoubleRealMatcher);
}

class DoubleImagMatcher : public ::testing::MatcherInterface<
                              ::testing::tuple<std::complex<double>, double>>
{
public:
  virtual bool
  MatchAndExplain(::testing::tuple<std::complex<double>, double> arg,
                  ::testing::MatchResultListener *listener) const override
  {
    auto double_eq = static_cast<::testing::Matcher<double>>(
        ::testing::DoubleEq(std::get<0>(arg).imag()));
    return double_eq.MatchAndExplain(std::get<1>(arg), listener);
  }

  virtual void DescribeTo(std::ostream *os) const override
  {
    *os << "are equal";
  }

  virtual void DescribeNegationTo(std::ostream *os) const override
  {
    *os << "are unequal";
  }
};

inline ::testing::Matcher<::testing::tuple<std::complex<double>, double>>
DoubleImagEqual()
{
  return MakeMatcher(new DoubleImagMatcher);
}

} // namespace unit_test

#endif /* CUSTOM_MATCHERS_HPP */
