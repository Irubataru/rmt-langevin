/*
 * Created: 06-04-2016
 * Modified: Wed 06 Apr 2016 23:27:27 CEST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef MOCK_WHITE_NOISE_HPP
#define MOCK_WHITE_NOISE_HPP

namespace unit_test {

class Mock_White_Noise
{
public:
  Mock_White_Noise(double val) : noise_value{val}
  {
  }

  double operator()()
  {
    return noise_value;
  }

private:
  double noise_value;
};

} // namespace unit_test

#endif /* MOCK_WHITE_NOISE_HPP */
