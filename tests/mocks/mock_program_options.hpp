/*
 * Created: 22-04-2016
 * Modified: Fri 22 Apr 2016 13:26:28 CEST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef MOCK_PROGRAM_OPTIONS_HPP
#define MOCK_PROGRAM_OPTIONS_HPP

#include <gmock/gmock.h>
#include <utilities/program_options.hpp>

namespace unit_tests {

class Mock_Progam_Options : public rmt::Program_Options
{
  MOCK_CONST_METHOD0(get_parameters, rmt::Simulation_Parameters());
  MOCK_CONST_METHOD0(imag_mu, bool());
  MOCK_CONST_METHOD0(imag_mass, bool());
};

} // namespace unit_tests

#endif /* MOCK_PROGRAM_OPTIONS_HPP */
