SUBDIRS := stephanov utility osborn

INCLUDES_$(d) := $(TOP)/external/googletest-src/googletest/include\
	$(TOP)/external/googletest-src/googlemock/include $(TOP)/src\
	$(TOP)/external/simskel/include
INHERIT_DIR_VARS_$(d) := CXXFLAGS INCLUDES
