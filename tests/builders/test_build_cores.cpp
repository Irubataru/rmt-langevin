/*
 * Created: 13-05-2016
 * Modified: Fri 13 May 2016 17:47:13 CEST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "mocks/mock_dependent_objects.hpp"
#include <builders/builder_core.hpp>
#include <gtest/gtest.h>

using unit_test::Mock_No_Dependence;

TEST(Builder_Core_Cartesian_Test, No_Binding)
{
  using rmt::builder::builder_core::cartesian::Builder_Core;
  auto core = Builder_Core{};
  auto constructed_object = core.construct_dependable<Mock_No_Dependence>();
};

TEST(Builder_Core_Cartesian_Test, No_Binding_Arg_Forwarding)
{
  using rmt::builder::builder_core::cartesian::Builder_Core;
  auto core = Builder_Core{};
  auto constructed_object =
      core.construct_dependable<Mock_No_Dependence>(5.5, "Hello");
};

TEST(Builder_Core_Cartesian_Test, Resource_Binding)
{
  using rmt::builder::builder_core::cartesian::Builder_Core;
  using unit_test::cartesian::Mock_Resource_Dependence;
  auto core = Builder_Core{};
  auto constructed_object =
      core.construct_dependable<Mock_Resource_Dependence>();
};

TEST(Builder_Core_Cartesian_Test, Resource_Binding_Arg_Forwarding)
{
  using rmt::builder::builder_core::cartesian::Builder_Core;
  using unit_test::cartesian::Mock_Resource_Dependence;
  auto core = Builder_Core{};
  auto constructed_object =
      core.construct_dependable<Mock_Resource_Dependence>(5.5, "Hello");
};

TEST(Builder_Core_Cartesian_Test, Force_Binding)
{
  using rmt::builder::builder_core::cartesian::Builder_Core;
  using unit_test::cartesian::Mock_Force_Dependence;
  auto core = Builder_Core{};
  auto constructed_object = core.construct_dependable<Mock_Force_Dependence>();
};

TEST(Builder_Core_Cartesian_Test, Resource_Force_Binding)
{
  using rmt::builder::builder_core::cartesian::Builder_Core;
  using unit_test::cartesian::Mock_Both_Dependence;
  auto core = Builder_Core{};
  auto constructed_object = core.construct_dependable<Mock_Both_Dependence>();
};
