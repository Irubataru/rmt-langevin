/*
 * Created: 13-05-2016
 * Modified: Fri 13 May 2016 17:38:56 CEST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef MOCK_DEPENDENT_OBJECTS_HPP
#define MOCK_DEPENDENT_OBJECTS_HPP

#include <stephanov/resource/resource.hpp>
#include <stephanov/update/force.hpp>

namespace unit_test {

class Mock_No_Dependence
{
public:
  Mock_No_Dependence() = default;

  template <typename Arg1, typename Arg2>
  Mock_No_Dependence(Arg1 &&, Arg2 &&){};
};

namespace cartesian {

class Mock_Resource_Dependence
    : public rmt::stephanov::builder::builder_core::requires_resource_type
{
public:
  using resource_type = rmt::stephanov::cartesian::Matrix_Storage;
  Mock_Resource_Dependence(const std::shared_ptr<const resource_type> &)
  {
  }

  template <typename Arg1, typename Arg2>
  Mock_Resource_Dependence(const std::shared_ptr<const resource_type> &,
                           Arg1 &&, Arg2 &&)
  {
  }
};

class Mock_Force_Dependence
    : public rmt::stephanov::builder::builder_core::requires_force_type
{
public:
  using force_type = rmt::stephanov::cartesian::Force;
  Mock_Force_Dependence(const std::shared_ptr<const force_type> &)
  {
  }
};

class Mock_Both_Dependence
    : public rmt::stephanov::builder::builder_core::requires_resource_type,
      public rmt::stephanov::builder::builder_core::requires_force_type
{
public:
  using resource_type = rmt::stephanov::cartesian::Matrix_Storage;
  using force_type = rmt::stephanov::cartesian::Force;
  Mock_Both_Dependence(const std::shared_ptr<const resource_type> &,
                       const std::shared_ptr<const force_type> &)
  {
  }
};

} // namespace cartesian

} // namespace unit_test

#endif /* MOCK_DEPENDENT_OBJECTS_HPP */
