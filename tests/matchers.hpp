/*
 * Created: 12-06-2016
 * UNIT_TEST_Modified: Wed 02 Nov 2016 11:08:04 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef UNIT_TEST_MATCHERS_HPP
#define UNIT_TEST_MATCHERS_HPP

#include <gmock/gmock.h>
#include <limits>
#include <types.hpp>
#include <utilities/matrix/is_equal.hpp>
#include <utilities/matrix/is_zero.hpp>

namespace unit_test {

MATCHER(FloatRealEq, "")
{
  using rmt::numeric::abs;
  auto val1 = std::get<0>(arg).real();
  auto val2 = std::get<1>(arg);

  if (abs(val1 + val2) == rmt::numeric::number_type{0.})
    return abs(val1 - val2) <
           std::numeric_limits<rmt::numeric::number_type>::epsilon();
  else
    return abs(val1 - val2) <
           (rmt::numeric::number_type{0.5} * abs(val1 + val2) *
            std::numeric_limits<rmt::numeric::number_type>::epsilon());
}

MATCHER_P(FloatRealPrecisionEq, error, "")
{
  using rmt::numeric::abs;
  auto val1 = std::get<0>(arg).real();
  auto val2 = std::get<1>(arg);

  if (abs(val1 + val2) == rmt::numeric::number_type{0.})
    return abs(val1 - val2) < error;
  else
    return abs(val1 - val2) <
           (rmt::numeric::number_type{0.5} * abs(val1 + val2) * error);
}

MATCHER(FloatImagEq, "")
{
  using rmt::numeric::abs;
  auto val1 = std::get<0>(arg).imag();
  auto val2 = std::get<1>(arg);
  if (abs(val1 + val2) == rmt::numeric::number_type{0.})
    return abs(val1 - val2) <
           std::numeric_limits<rmt::numeric::number_type>::epsilon();
  else
    return abs(val1 - val2) <
           (rmt::numeric::number_type{0.5} * abs(val1 + val2) *
            std::numeric_limits<rmt::numeric::number_type>::epsilon());
}

MATCHER(FloatNear, "")
{
  using rmt::numeric::abs;
  auto val1 = std::get<0>(arg);
  auto val2 = std::get<1>(arg);
  if (abs(val1 + val2) == rmt::numeric::number_type{0.})
    return abs(val1 - val2) <
           std::numeric_limits<rmt::numeric::number_type>::epsilon();
  else
    return abs(val1 - val2) <
           (rmt::numeric::number_type{0.5} * abs(val1 + val2) *
            std::numeric_limits<rmt::numeric::number_type>::epsilon());
}

MATCHER_P(FloatNear, error, "")
{
  using rmt::numeric::abs;
  auto val1 = std::get<0>(arg);
  auto val2 = std::get<1>(arg);
  if (abs(val1 + val2) == rmt::numeric::number_type{0.})
    return abs(val1 - val2) < error;
  else
    return abs(val1 - val2) <
           (rmt::numeric::number_type{0.5} * abs(val1 + val2) * error);
}

MATCHER(FloatEq, "")
{
  using rmt::numeric::abs;
  auto val1 = std::get<0>(arg);
  auto val2 = std::get<1>(arg);
  if (abs(val1 + val2) == rmt::numeric::number_type{0.})
    return abs(val1 - val2) <
           std::numeric_limits<rmt::numeric::number_type>::epsilon();
  else
    return abs(val1 - val2) <
           (rmt::numeric::number_type{0.5} * abs(val1 + val2) *
            std::numeric_limits<rmt::numeric::number_type>::epsilon());
}

MATCHER_P(FloatEq, val, "")
{
  using rmt::numeric::abs;
  if (abs(arg + val) == rmt::numeric::number_type{0.})
    return abs(arg - val) <
           std::numeric_limits<rmt::numeric::number_type>::epsilon();
  else
    return abs(arg - val) <
           (abs(arg + val) *
            std::numeric_limits<rmt::numeric::number_type>::epsilon());
}

MATCHER_P2(FloatEq, val, error, "")
{
  using rmt::numeric::abs;
  if (abs(arg + val) == rmt::numeric::number_type{0.})
    return abs(arg - val) < error;
  else
    return abs(arg - val) < (abs(arg + val) * error);
}

MATCHER(MatrixIszero, "")
{
  return rmt::matrix::Is_Zero(arg);
}

MATCHER_P(MatrixIszero, error, "")
{
  return rmt::matrix::Is_Zero(arg, rmt::numeric::number_type{error});
}

MATCHER_P(MatrixApprox, mat, "")
{
  return rmt::matrix::Is_Equal(arg, mat);
}

MATCHER_P2(MatrixApprox, mat, error, "")
{
  return rmt::matrix::Is_Equal(arg, mat, rmt::numeric::number_type{error});
}

} // namespace unit_test

#endif /* UNIT_TEST_MATCHERS_HPP */
