
/*
 * Created: 21-07-2016
 * Modified: Wed 08 Feb 2017 15:18:05 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "../../matchers.hpp"
#include <boost/math/constants/constants.hpp>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <utilities/minimum/core/bracket_min.hpp>

using rmt::numeric::number_type;
using rmt::numeric::sqrt;
using rmt::numeric::sin;
using boost::math::constants::pi;

TEST(Bracket_Test, Test_Linear_Function)
{
  std::function<number_type(number_type)> linear = [](number_type x) {
    return -x;
  };

  auto result = rmt::minimise::core::Bracketing::bracket(
      {number_type{0.}, number_type{1.}}, linear);

  EXPECT_TRUE(result.no_bracket_found);
  EXPECT_TRUE(result.left < result.mid);
  EXPECT_TRUE(result.mid < result.right);
};

TEST(Bracket_Test, Test_Square_Overstep)
{
  const auto golden_ratio =
      number_type{0.5} * (number_type{1.} + sqrt(number_type{5.}));

  std::function<number_type(number_type)> square = [](number_type x) {
    return (x - 1.) * (x - 1.);
  };

  auto result = rmt::minimise::core::Bracketing::bracket(
      {number_type{0.}, number_type{1.}}, square);

  EXPECT_THAT(result.left, unit_test::FloatEq(number_type{0.}));
  EXPECT_THAT(result.f_left, unit_test::FloatEq(number_type{1.}));
  EXPECT_THAT(result.mid, unit_test::FloatEq(number_type{1.}));
  EXPECT_THAT(result.f_mid, unit_test::FloatEq(number_type{0.}));

  auto right_expected = number_type{1.} + golden_ratio;
  auto f_right_expected = square(right_expected);

  EXPECT_THAT(result.right, unit_test::FloatEq(right_expected));
  EXPECT_THAT(result.f_right, unit_test::FloatEq(f_right_expected));
};

TEST(Bracket_Test, Test_Square_Full)
{
  std::function<number_type(number_type)> square = [](number_type x) {
    return (x - 1.) * (x - 1.);
  };

  auto result = rmt::minimise::core::Bracketing::bracket(
      {number_type{0.}, number_type{1e-3}}, square);

  EXPECT_TRUE(result.left < number_type{1.});
  EXPECT_TRUE(result.right > number_type{1.});

  EXPECT_THAT(result.f_left, unit_test::FloatEq(square(result.left)));
  EXPECT_THAT(result.f_right, unit_test::FloatEq(square(result.right)));
};

TEST(Bracket_Test, Sine)
{
  std::function<number_type(number_type)> sine = [](number_type x) {
    return sin(x);
  };

  std::vector<double> v;

  auto result = rmt::minimise::core::Bracketing::bracket(
      {pi<number_type>(), pi<number_type>() + number_type{1e-1}}, sine);

  EXPECT_TRUE(result.left < number_type{3. / 2.} * pi<number_type>());
  EXPECT_TRUE(result.right > number_type{3. / 2.} * pi<number_type>());
  EXPECT_TRUE(result.mid <= result.right and result.mid > result.left);

  EXPECT_THAT(result.f_left, unit_test::FloatEq(sine(result.left)));
  EXPECT_THAT(result.f_right, unit_test::FloatEq(sine(result.right)));
};
