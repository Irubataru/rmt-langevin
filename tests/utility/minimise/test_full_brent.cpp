
/*
 * Created: 06-12-2016
 * Modified: Tue 06 Dec 2016 14:31:52 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "../../matchers.hpp"
#include <boost/math/constants/constants.hpp>
#include <gtest/gtest.h>
#include <utilities/minimum/minimisers.hpp>

using rmt::numeric::number_type;
using boost::math::constants::pi;
using rmt::minimise::Brent_Minimisation;

using unit_test::FloatEq;

TEST(Brent_Full_Algorithm_Test, Square_Function)
{
  Brent_Minimisation min_alg{{}};

  auto result = min_alg.minimise(
      {0., 1e-2}, {}, [](number_type x) { return (x - 1.) * (x - 1.); });

  EXPECT_THAT(result.min, FloatEq(1., 1e-10));
  EXPECT_THAT(result.f_min, FloatEq(0., 1e-10));
}

TEST(Brent_Full_Algorithm_Test, Square_Function_Outside)
{
  Brent_Minimisation min_alg{{}};

  auto result =
      min_alg.minimise({0., 1e-2, 100, 1.}, {},
                       [](number_type x) { return (x - 5.) * (x - 5.); });

  EXPECT_THAT(result.min, FloatEq(1., 1e-10));
  EXPECT_THAT(result.f_min, FloatEq(16., 1e-10));
}

TEST(Brent_Full_Algorithm_Test, Minus_Sine)
{
  using rmt::numeric::sin;

  Brent_Minimisation min_alg{{}};

  auto result =
      min_alg.minimise({0., 1e-2}, {}, [](number_type x) { return -sin(x); });

  EXPECT_THAT(result.min, FloatEq(0.5 * pi<number_type>(), 1e-10));
  EXPECT_THAT(result.f_min, FloatEq(-1, 1e-10));
}

TEST(Brent_Full_Algorithm_Test, Fast_Sine)
{
  using rmt::numeric::sin;

  Brent_Minimisation min_alg{{}};

  auto result = min_alg.minimise({0., 1e-5}, {},
                                 [](number_type x) { return -sin(10. * x); });

  EXPECT_THAT(result.min, FloatEq(0.05 * pi<number_type>(), 1e-10));
  EXPECT_THAT(result.f_min, FloatEq(-1, 1e-10));
}
