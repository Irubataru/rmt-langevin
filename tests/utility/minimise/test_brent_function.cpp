
/*
 * Created: 05-12-2016
 * Modified: Tue 06 Dec 2016 13:20:33 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "../../matchers.hpp"
#include "mock_functor.hpp"
#include <boost/math/constants/constants.hpp>
#include <functional>
#include <gtest/gtest.h>
#include <memory>
#include <number_type.hpp>
#include <random>
#include <utilities/minimum/core/brent_core.hpp>

using rmt::numeric::number_type;
using unit_test::FloatEq;
using unit_test::Mock_Functor;
using unit_test::Callable_SharedPtr;
using testing::_;

TEST(Brent_Test, Linear_Left_Function)
{
  using f_type = Callable_SharedPtr<Mock_Functor>;

  auto linear_ptr =
      std::make_shared<Mock_Functor>([](number_type x) { return x; });

  EXPECT_CALL(*linear_ptr, function_call(_)).Times(testing::AtMost(100));

  auto result = rmt::minimise::core::Brent::minimise<f_type>(
      {-2., 0., 2., -2., 0., 2., {linear_ptr}, true}, {});

  EXPECT_THAT(result.min, FloatEq(-2., 1e-10));
  EXPECT_THAT(result.f_min, FloatEq(-2., 1e-10));
  EXPECT_EQ(&(*result.f), &(*linear_ptr));
}

TEST(Brent_Test, Linear_Right_Function)
{
  using f_type = Callable_SharedPtr<Mock_Functor>;

  auto linear_ptr =
      std::make_shared<Mock_Functor>([](number_type x) { return -x; });

  EXPECT_CALL(*linear_ptr, function_call(_)).Times(testing::AtMost(100));

  auto result = rmt::minimise::core::Brent::minimise<f_type>(
      {-2., 0., 2., -2., 0., 2., {linear_ptr}, true}, {});

  EXPECT_THAT(result.min, FloatEq(2., 1e-10));
  EXPECT_THAT(result.f_min, FloatEq(-2., 1e-10));
  EXPECT_EQ(&(*result.f), &(*linear_ptr));
}

TEST(Brent_Test, Sine)
{
  using rmt::numeric::sin;
  using boost::math::constants::pi;
  using f_type = Callable_SharedPtr<Mock_Functor>;

  const auto pi_val = pi<number_type>();

  auto def_min_param = rmt::minimise::core::Minimisation_Input_Type{};
  def_min_param.tolerance = 1e-10;

  auto arr = std::array<number_type, 3>{{2.1, 4.2, 5.5}};

  std::function<number_type(number_type)> sine = [](number_type x) {
    return sin(x);
  };

  auto f_arr = std::array<number_type, 3>{};
  std::transform(arr.begin(), arr.end(), f_arr.begin(), sine);

  auto sine_ptr = std::make_shared<Mock_Functor>(sine);

  EXPECT_CALL(*sine_ptr, function_call(_))
      .Times(testing::AtMost(def_min_param.max_steps));

  auto result = rmt::minimise::core::Brent::minimise<f_type>(
      {arr[0], arr[1], arr[2], f_arr[0], f_arr[1], f_arr[2], {sine_ptr}, true},
      def_min_param);

  EXPECT_EQ(&(*result.f), &(*sine_ptr));

  EXPECT_THAT(result.min, FloatEq(number_type{3. / 2.} * pi_val,
                                  10 * def_min_param.tolerance));
}
