
/*
 * Created: 05-12-2016
 * Modified: Mon 05 Dec 2016 17:05:33 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef FUNCTOR_MOCKS_HPP
#define FUNCTOR_MOCKS_HPP

#include <functional>
#include <gmock/gmock.h>
#include <memory>
#include <number_type.hpp>

namespace unit_test {

class Mock_Functor
{
public:
  using return_type = rmt::numeric::number_type;
  using input_type = rmt::numeric::number_type;

  template <typename Functor>
  Mock_Functor(Functor &&foo) : f{std::forward<Functor>(foo)}
  {
    delegate_to_fake();
  }

  void delegate_to_fake()
  {
    using testing::_;

    ON_CALL(*this, function_call(_)).WillByDefault(testing::Invoke(f));
  }

  return_type operator()(input_type x)
  {
    return function_call(x);
  }

  MOCK_METHOD1(function_call, return_type(input_type));

private:
  std::function<return_type(input_type)> f;
};

template <typename Type>
class Callable_SharedPtr
{
public:
  Callable_SharedPtr(std::shared_ptr<Type> the_ptr) : ptr{std::move(the_ptr)}
  {
  }

  Callable_SharedPtr(const Callable_SharedPtr &) = default;
  Callable_SharedPtr(Callable_SharedPtr &&) = default;

  Callable_SharedPtr &operator=(const Callable_SharedPtr &) = default;
  Callable_SharedPtr &operator=(Callable_SharedPtr &&) = default;

  Type &operator*() const
  {
    return *ptr;
  }

  Type operator->() const
  {
    return ptr.operator->();
  }

private:
  std::shared_ptr<Type> ptr;

public:
  template <typename... Args>
  auto operator()(Args &&... args)
      -> decltype((this->ptr)->operator()(std::forward<Args>(args)...))
  {
    return ptr->operator()(std::forward<Args>(args)...);
  }
};

} // namespace unit_test

#endif /* FUNCTOR_MOCKS_HPP */
