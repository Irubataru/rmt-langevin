
/*
 * Created: 06-12-2016
 * Modified: Tue 06 Dec 2016 13:47:39 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef MOCK_BRACKET_CORE_HPP
#define MOCK_BRACKET_CORE_HPP

#include <functional>
#include <gmock/gmock.h>
#include <memory>
#include <utilities/minimum/core/minimisation_types.hpp>

namespace unit_test {

class Core_Reference
{
public:
  MOCK_METHOD0(bracket, void());
  MOCK_METHOD0(minimise, void());
};

class Mock_Bracket_Core
{
public:
  using number_type = rmt::numeric::number_type;
  using functor = std::function<number_type(number_type)>;

  using bracket_input = rmt::minimise::core::Bracket_Input_Type;
  using bracket_output = rmt::minimise::core::Bracket_Return_Type<functor>;
  using minimise_input = rmt::minimise::core::Minimisation_Input_Type;
  using minimise_output =
      rmt::minimise::core::Minimisation_Return_Type<functor>;

  explicit Mock_Bracket_Core(std::shared_ptr<Core_Reference> c_ptr)
      : mock_ptr{std::move(c_ptr)}
  {
  }

  Mock_Bracket_Core(const Mock_Bracket_Core &) = default;
  Mock_Bracket_Core(Mock_Bracket_Core &&) = default;

  Mock_Bracket_Core &operator=(const Mock_Bracket_Core &) = default;
  Mock_Bracket_Core &operator=(Mock_Bracket_Core &&) = default;

  bracket_output bracket(bracket_input, functor)
  {
    mock_ptr->bracket();
    return default_bracket_output;
  }

  minimise_output minimise(bracket_output, minimise_input)
  {
    mock_ptr->minimise();
    return default_min_output;
  }

  void set_bracket_out(bracket_output br_out)
  {
    default_bracket_output = std::move(br_out);
  }

  void set_min_out(minimise_output min_out)
  {
    default_min_output = std::move(min_out);
  }

private:
  bracket_output default_bracket_output;
  minimise_output default_min_output;
  std::shared_ptr<Core_Reference> mock_ptr;
};

} // namespace unit_test

#endif /* MOCK_BRACKET_CORE_HPP */
