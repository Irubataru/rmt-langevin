
/*
 * Created: 06-12-2016
 * Modified: Tue 06 Dec 2016 14:02:43 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "mock_bracket_core.hpp"
#include <gtest/gtest.h>
#include <utilities/minimum/general_minimisation.hpp>

using rmt::numeric::number_type;
using functor_type = std::function<number_type(number_type)>;

using unit_test::Mock_Bracket_Core;
using rmt::minimise::Bracket_Minimisation;
using rmt::minimise::core::Bracket_Return_Type;

TEST(General_Cooling_Test, Bracket_Found)
{
  auto mock_ptr = std::make_shared<unit_test::Core_Reference>();
  auto functor = functor_type{[](number_type) { return number_type{1.}; }};

  EXPECT_CALL(*mock_ptr, bracket()).Times(1);
  EXPECT_CALL(*mock_ptr, minimise()).Times(1);

  Mock_Bracket_Core core{mock_ptr};

  auto result = Bracket_Return_Type<functor_type>{};

  core.set_bracket_out({0., 1., 2., 0., 1., 2., functor, false});
  core.set_min_out({11.22, 33.44, functor});

  Bracket_Minimisation<Mock_Bracket_Core> minimiser{core};

  auto return_val = minimiser.minimise({}, {}, functor);

  EXPECT_EQ(return_val.min, 11.22);
  EXPECT_EQ(return_val.f_min, 33.44);
}

TEST(General_Cooling_Test, No_Bracket_Found)
{
  auto mock_ptr = std::make_shared<unit_test::Core_Reference>();
  auto functor = functor_type{[](number_type) { return number_type{1.}; }};

  EXPECT_CALL(*mock_ptr, bracket()).Times(1);

  Mock_Bracket_Core core{mock_ptr};

  auto result = Bracket_Return_Type<functor_type>{};

  core.set_bracket_out({0., 1., 2.45, 0., 1., 6.77, functor, true});
  core.set_min_out({11.22, 33.44, functor});

  Bracket_Minimisation<Mock_Bracket_Core> minimiser{core};

  auto return_val = minimiser.minimise({}, {}, functor);

  EXPECT_EQ(return_val.min, 2.45);
  EXPECT_EQ(return_val.f_min, 6.77);
}
