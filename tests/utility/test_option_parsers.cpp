
/*
 * Created: 31-08-2017
 * Modified: Thu 31 Aug 2017 17:42:36 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <utilities/json_names.hpp>
#include <utilities/json_parser_helpers.hpp>
#include <utilities/program_options.hpp>

using namespace rmt;
using namespace rmt::build;
using namespace rmt::parse;
using json = nlohmann::json;

using ::testing::ElementsAreArray;

TEST(Config_Filename, Default_Filename)
{
  auto config_filename = get_config_filename(0, nullptr);
  EXPECT_EQ(default_config_filename, config_filename);
}

TEST(Config_Filename, Argument_Filename)
{
  char exec_name[] = "exec_name";
  char file_name[] = "dummy_filename";
  char *args[] = {&exec_name[0], &file_name[0]};
  const std::string expected_filename{file_name};

  auto config_filename = get_config_filename(2, args);
  EXPECT_EQ(expected_filename, config_filename);
}

TEST(JSON_Scan_Parser, Missing_Parameters)
{
  { /* No scan section */
    auto j = R"(
    {
      "no-scan": "hello world"
    }
    )"_json;

    EXPECT_THROW({ auto params = parse_scan_parameters(j); },
                 invalid_parse_exception);
  }

  { /* No thermalisation-steps */
    auto j = R"(
    {"scan":
      {
        "measure-skips": 1,
        "final-time": 1.00,
        "update-type": "constant",
        "delta-t": 1e-5
      }
    }
    )"_json;

    EXPECT_THROW({ auto params = parse_scan_parameters(j); },
                 std::runtime_error);
  }

  { /* No measure-skips */
    auto j = R"(
    {"scan":
      {
        "thermalisation-steps": 1000,
        "final-time": 1.00,
        "update-type": "constant",
        "delta-t": 1e-5
      }
    }
    )"_json;

    EXPECT_THROW({ auto params = parse_scan_parameters(j); },
                 invalid_parse_exception);
  }

  { /* No final-time or total-steps */
    auto j = R"(
    {"scan":
      {
        "thermalisation-steps": 1000,
        "measure-skips": 1,
        "update-type": "constant",
        "delta-t": 1e-5
      }
    }
    )"_json;

    EXPECT_THROW({ auto params = parse_scan_parameters(j); },
                 std::runtime_error);
  }

  { /* No delta-t */
    auto j = R"(
    {"scan":
      {
        "thermalisation-steps": 1000,
        "measure-skips": 1,
        "final-time": 1.00,
        "update-type": "constant"
      }
    }
    )"_json;

    EXPECT_THROW({ auto params = parse_scan_parameters(j); },
                 invalid_parse_exception);
  }

  { /* No update-type */
    auto j = R"(
    {"scan":
      {
        "thermalisation-steps": 1000,
        "measure-skips": 1,
        "final-time": 1.00,
        "delta-t": 1e-5
      }
    }
    )"_json;

    EXPECT_THROW({ auto params = parse_scan_parameters(j); },
                 invalid_parse_exception);
  }
}

TEST(JSON_Scan_Parser, All_Parameters)
{
  { /* Using final-time */
    auto j = R"(
    {"scan":
      {
        "thermalisation-time": 2e-2,
        "measure-skips": 33,
        "final-time": 1.54,
        "update-type": "constant",
        "delta-t": 1e-5
      }
    }
    )"_json;

    auto params = parse_scan_parameters(j);

    EXPECT_EQ(33, params.skip_steps);
    EXPECT_DOUBLE_EQ(2e-2, params.therm_time);
    EXPECT_DOUBLE_EQ(1.54, params.final_time);
    EXPECT_EQ(Stepsize_Type::Constant, params.step_update);
    EXPECT_DOUBLE_EQ(1e-5, params.base_stepsize);
  }

  { /* Using total-steps */
    auto j = R"(
    {"scan":
      {
        "thermalisation-time": 1e-4,
        "measure-skips": 33,
        "total-steps": 200,
        "update-type": "constant",
        "delta-t": 2e-3
      }
    }
    )"_json;

    auto params = parse_scan_parameters(j);

    EXPECT_EQ(33, params.skip_steps);
    EXPECT_DOUBLE_EQ(1e-4, params.therm_time);
    EXPECT_DOUBLE_EQ(200 * 2e-3, params.final_time);
    EXPECT_EQ(Stepsize_Type::Constant, params.step_update);
    EXPECT_DOUBLE_EQ(2e-3, params.base_stepsize);
  }

  { /* Using thermalisation-steps */
    auto j = R"(
    {"scan":
      {
        "thermalisation-steps": 100,
        "measure-skips": 11,
        "final-time": 1.54,
        "update-type": "constant",
        "delta-t": 4.4e-5
      }
    }
    )"_json;

    auto params = parse_scan_parameters(j);

    EXPECT_EQ(11, params.skip_steps);
    EXPECT_DOUBLE_EQ(4.4e-5 * 100, params.therm_time);
    EXPECT_DOUBLE_EQ(1.54, params.final_time);
    EXPECT_EQ(Stepsize_Type::Constant, params.step_update);
    EXPECT_DOUBLE_EQ(4.4e-5, params.base_stepsize);
  }
}

TEST(JSON_Scan_Parser, Stepsize_Update_Options)
{
  { /* Constant stepsize */
    auto j = R"(
    {"scan":
      {
        "thermalisation-time": 2e-2,
        "measure-skips": 33,
        "final-time": 1.54,
        "update-type": "constant",
        "delta-t": 1e-5
      }
    }
    )"_json;

    auto params = parse_scan_parameters(j);
    EXPECT_EQ(Stepsize_Type::Constant, params.step_update);
  }

  { /* Adaptive stepsize */
    auto j = R"(
    {"scan":
      {
        "thermalisation-time": 2e-2,
        "measure-skips": 33,
        "final-time": 1.54,
        "update-type": "adaptive",
        "delta-t": 1e-5
      }
    }
    )"_json;

    auto params = parse_scan_parameters(j);
    EXPECT_EQ(Stepsize_Type::Adaptive, params.step_update);
  }

  { /* Invalid stepsize */
    auto j = R"(
    {"scan":
      {
        "thermalisation-time": 2e-2,
        "measure-skips": 33,
        "final-time": 1.54,
        "update-type": "invalid",
        "delta-t": 1e-5
      }
    }
    )"_json;

    EXPECT_THROW({ auto params = parse_scan_parameters(j); },
                 std::runtime_error);
  }
}

TEST(JSON_Model_Parser, Missing_Parameters)
{
  { /* No model section */
    auto j = R"(
    {
      "no-scan": "hello world"
    }
    )"_json;

    EXPECT_THROW({ auto params = parse_model_parameters(j); },
                 invalid_parse_exception);
  }

  { /* No model */
    // clang-format off
    auto j = R"(
    {
      "parameters": {
        "storage": "cartesian",
        "nf": 1,
        "mu-real": "seq 1.1 2.0 0.1",
        "mu-imag": "0.0",
        "mass-real": "0.2",
        "mass-imag": "0.0"
      }
    }
    )"_json;
    // clang-format on

    EXPECT_THROW({ auto params = parse_model_parameters(j); },
                 invalid_parse_exception);
  }

  { /* No storage */
    // clang-format off
    auto j = R"(
    {
      "parameters": {
        "model": "stephanov",
        "nf": 1,
        "mu-real": "seq 1.1 2.0 0.1",
        "mu-imag": "0.0",
        "mass-real": "0.2",
        "mass-imag": "0.0"
      }
    }
    )"_json;
    // clang-format on

    EXPECT_THROW({ auto params = parse_model_parameters(j); },
                 invalid_parse_exception);
  }

  { /* No nf */
    // clang-format off
    auto j = R"(
    {
      "parameters": {
        "model": "stephanov",
        "storage": "cartesian",
        "mu-real": "seq 1.1 2.0 0.1",
        "mu-imag": "0.0",
        "mass-real": "0.2",
        "mass-imag": "0.0"
      }
    }
    )"_json;
    // clang-format on

    EXPECT_THROW({ auto params = parse_model_parameters(j); },
                 invalid_parse_exception);
  }

  { /* No mu-real */
    // clang-format off
    auto j = R"(
    {
      "parameters": {
        "model": "stephanov",
        "storage": "cartesian",
        "nf": 1,
        "mu-imag": "0.0",
        "mass-real": "0.2",
        "mass-imag": "0.0"
      }
    }
    )"_json;
    // clang-format on

    EXPECT_THROW({ auto params = parse_model_parameters(j); },
                 invalid_parse_exception);
  }

  { /* No mu-imag */
    // clang-format off
    auto j = R"(
    {
      "parameters": {
        "model": "stephanov",
        "storage": "cartesian",
        "nf": 1,
        "mu-real": "seq 1.1 2.0 0.1",
        "mass-real": "0.2",
        "mass-imag": "0.0"
      }
    }
    )"_json;
    // clang-format on

    EXPECT_THROW({ auto params = parse_model_parameters(j); },
                 invalid_parse_exception);
  }

  { /* No mass-real */
    // clang-format off
    auto j = R"(
    {
      "parameters": {
        "model": "stephanov",
        "storage": "cartesian",
        "nf": 1,
        "mu-real": "seq 1.1 2.0 0.1",
        "mu-imag": "0.0",
        "mass-imag": "0.0"
      }
    }
    )"_json;
    // clang-format on

    EXPECT_THROW({ auto params = parse_model_parameters(j); },
                 invalid_parse_exception);
  }

  { /* No mass-imag */
    // clang-format off
    auto j = R"(
    {
      "parameters": {
        "model": "stephanov",
        "storage": "cartesian",
        "nf": 1,
        "mu-real": "seq 1.1 2.0 0.1",
        "mu-imag": "0.0",
        "mass-real": "0.2"
      }
    }
    )"_json;
    // clang-format on

    EXPECT_THROW({ auto params = parse_model_parameters(j); },
                 invalid_parse_exception);
  }
}

TEST(JSON_Model_Parser, Struct_Options)
{
  { /* stephanov cartesian */
    // clang-format off
    auto j = R"(
    {
      "parameters": {
        "model": "stephanov",
        "storage": "cartesian",
        "nf": 1,
        "mu-real": "0.0",
        "mu-imag": "0.0",
        "mass-real": "0.0",
        "mass-imag": "0.0"
      }
    }
    )"_json;
    // clang-format on

    auto params = parse_model_parameters(j);
    EXPECT_EQ(params.model, Model_Type::Stephanov);
    EXPECT_EQ(params.storage, Storage_Type::Cartesian);
  }

  { /* osborn polar */
    // clang-format off
    auto j = R"(
    {
      "parameters": {
        "model": "osborn",
        "storage": "polar",
        "nf": 1,
        "mu-real": "0.0",
        "mu-imag": "0.0",
        "mass-real": "0.0",
        "mass-imag": "0.0"
      }
    }
    )"_json;
    // clang-format on

    auto params = parse_model_parameters(j);
    EXPECT_EQ(params.model, Model_Type::Osborn);
    EXPECT_EQ(params.storage, Storage_Type::Polar);
  }

  { /* Invalid model */
    // clang-format off
    auto j = R"(
    {
      "parameters": {
        "model": "invalid",
        "storage": "polar",
        "nf": 1,
        "mu-real": "0.0",
        "mu-imag": "0.0",
        "mass-real": "0.0",
        "mass-imag": "0.0"
      }
    }
    )"_json;
    // clang-format on

    EXPECT_THROW({ auto params = parse_model_parameters(j); },
                 std::runtime_error);
  }

  { /* Invalid model */
    // clang-format off
    auto j = R"(
    {
      "parameters": {
        "model": "cartesian",
        "storage": "invalid",
        "nf": 1,
        "mu-real": "0.0",
        "mu-imag": "0.0",
        "mass-real": "0.0",
        "mass-imag": "0.0"
      }
    }
    )"_json;
    // clang-format on

    EXPECT_THROW({ auto params = parse_model_parameters(j); },
                 std::runtime_error);
  }
}

TEST(JSON_Model_Parser, All_Parameters)
{
  // clang-format off
    auto j = R"(
    {
      "parameters": {
        "model": "stephanov",
        "storage": "polar",
        "nf": 5,
        "mu-real": "1.22",
        "mu-imag": "1e-4",
        "mass-real": "3.3",
        "mass-imag": "-1.5"
      }
    }
    )"_json;
  // clang-format on

  auto params = parse_model_parameters(j);
  EXPECT_EQ(params.model, Model_Type::Stephanov);
  EXPECT_EQ(params.storage, Storage_Type::Polar);
  EXPECT_EQ(params.number_of_flavours, 5);

  auto expected_mu_scan = std::vector<numeric::complex_type>{{1.22, 1e-4}};
  EXPECT_THAT(params.mu_scan, ElementsAreArray(expected_mu_scan));

  auto expected_mass_scan = std::vector<numeric::complex_type>{{3.3, -1.5}};
  EXPECT_THAT(params.mass_scan, ElementsAreArray(expected_mass_scan));
}

TEST(JSON_Model_Parser, Cartesian_Product)
{
  { /* N x 1 */
    // clang-format off
    auto j = R"(
    {
      "parameters": {
        "model": "stephanov",
        "storage": "cartesian",
        "nf": 1,
        "mu-real": "1.5 2.2 5.5 7.7",
        "mu-imag": "4.4",
        "mass-real": "0.0",
        "mass-imag": "0.0"
      }
    }
    )"_json;
    // clang-format on

    auto params = parse_model_parameters(j);

    auto expected_mu_scan =
        std::vector<numeric::complex_type>{1.5, 2.2, 5.5, 7.7};

    std::transform(expected_mu_scan.begin(), expected_mu_scan.end(),
                   expected_mu_scan.begin(), [](numeric::complex_type c) {
                     return c + numeric::complex_type{0.0, 4.4};
                   });

    EXPECT_THAT(params.mu_scan, ElementsAreArray(expected_mu_scan));
  }

  { /* 1 x N */
    // clang-format off
    auto j = R"(
    {
      "parameters": {
        "model": "stephanov",
        "storage": "cartesian",
        "nf": 1,
        "mu-real": "4.4",
        "mu-imag": "1.5 2.2 5.5 7.7",
        "mass-real": "0.0",
        "mass-imag": "0.0"
      }
    }
    )"_json;
    // clang-format on

    auto params = parse_model_parameters(j);

    auto expected_mu_scan =
        std::vector<numeric::complex_type>{1.5, 2.2, 5.5, 7.7};

    std::transform(expected_mu_scan.begin(), expected_mu_scan.end(),
                   expected_mu_scan.begin(), [](numeric::complex_type c) {
                     return 4.4 + numeric::complex_type{0.0, c.real()};
                   });

    EXPECT_THAT(params.mu_scan, ElementsAreArray(expected_mu_scan));
  }

  { /* N x N */
    // clang-format off
    auto j = R"(
    {
      "parameters": {
        "model": "stephanov",
        "storage": "cartesian",
        "nf": 1,
        "mu-real": "1.5 24.1 8.8",
        "mu-imag": "1.5 2.2 5.5 7.7",
        "mass-real": "0.0",
        "mass-imag": "0.0"
      }
    }
    )"_json;
    // clang-format on

    auto params = parse_model_parameters(j);

    auto mu_reals = std::vector<numeric::number_type>{1.5, 24.1, 8.8};
    auto mu_imags = std::vector<numeric::number_type>{1.5, 2.2, 5.5, 7.7};

    auto expected_mu_scan = std::vector<numeric::complex_type>{};

    for (auto re : mu_reals)
      for (auto im : mu_imags)
        expected_mu_scan.push_back({re, im});

    EXPECT_THAT(params.mu_scan, ElementsAreArray(expected_mu_scan));
  }
}
