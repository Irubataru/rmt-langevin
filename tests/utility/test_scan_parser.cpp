/*
 * Created: 22-04-2016
 * Modified: Fri 22 Apr 2016 15:12:31 CEST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <utilities/scan_parser.hpp>

using ::testing::ElementsAreArray;

TEST(Scan_Parser, Empty)
{
  auto test_str = std::string{};
  ASSERT_THROW({ auto parsed_str = rmt::scan::parse(test_str, false); },
               std::runtime_error);
}

TEST(Scan_Parser, One_Number)
{
  auto test_str = std::string{"2.5"};
  auto parsed_vec = rmt::scan::parse(test_str, false);

  auto expected = std::vector<std::complex<double>>{{2.5, 0}};
  EXPECT_THAT(parsed_vec, ElementsAreArray(expected));
}

TEST(Scan_Parser, Three_Imag_Numbers)
{
  auto test_str = std::string{"2.5 5.6 -1.7"};
  auto parsed_vec = rmt::scan::parse(test_str, true);

  auto expected =
      std::vector<std::complex<double>>{{0, 2.5}, {0, 5.6}, {0, -1.7}};
  EXPECT_THAT(parsed_vec, ElementsAreArray(expected));
}

TEST(Scan_Parser, Sequence_Real)
{
  auto test_str = std::string{"seq 0.0 5.0 1.0"};
  auto parsed_vec = rmt::scan::parse(test_str, false);

  auto expected = std::vector<std::complex<double>>{{0.0}, {1.0}, {2.0},
                                                    {3.0}, {4.0}, {5.0}};
  EXPECT_THAT(parsed_vec, ElementsAreArray(expected));
}

TEST(Scan_Parser, Sequence_Imag)
{
  auto test_str = std::string{"seq 0.0 2.0 0.1"};
  auto parsed_vec = rmt::scan::parse(test_str, true);

  auto expected = std::vector<std::complex<double>>(21);
  std::generate(expected.begin(), expected.end(), []() {
    static double current = -0.1;
    current += 0.1;
    return std::complex<double>{0., current};
  });

  EXPECT_THAT(parsed_vec, ElementsAreArray(expected));
}

TEST(Scan_Parser, Unfinished_Sequence)
{
  auto test_str = std::string{"seq 0.0 2.0"};

  ASSERT_THROW({ auto parsed_vec = rmt::scan::parse(test_str, false); },
               std::runtime_error);
}
