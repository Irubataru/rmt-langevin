/*
 * Created: 01-11-2016
 * Modified: Tue 01 Nov 2016 18:26:08 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <utilities/matrix/is_zero.hpp>
#include <utilities/matrix/matrix_constructors.hpp>

using rmt::numeric::number_type;
using rmt::numeric::complex_type;

TEST(Matrix_Is_Zero, Zero_Matrix)
{
  auto mat = rmt::matrix::Zero_Matrix();
  EXPECT_TRUE(rmt::matrix::Is_Zero(mat));
}

TEST(Matrix_Is_Zero, Single_Element_Non_Zero)
{
  auto mat = rmt::matrix::Zero_Matrix();
  mat(0, 0) = complex_type{1., 1.};

  EXPECT_FALSE(rmt::matrix::Is_Zero(mat));
}

TEST(Matrix_Is_Zero, Within_Error)
{
  auto error = number_type{1.};
  auto mat = rmt::matrix::Matrix(number_type{0.1} * error);

  EXPECT_TRUE(rmt::matrix::Is_Zero(mat, error));
}

TEST(Matrix_Is_Zero, Without_Error)
{
  auto error = number_type{1.};
  auto mat = rmt::matrix::Matrix(number_type{0.1} * error);
  mat(0, 0) = number_type{10.} * error;

  EXPECT_FALSE(rmt::matrix::Is_Zero(mat, error));
}
