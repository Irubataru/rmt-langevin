/*
 * Created: 08-11-2016
 * Modified: Tue 08 Nov 2016 13:49:16 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <types.hpp>
#include <utilities/matrix/default_constructor.hpp>
#include <utilities/matrix/eigenvalues/eigenvalue_solver.hpp>
#include <utilities/matrix/eigenvalues/lowest_eigenvalues.hpp>
#include <utilities/matrix/random_constructor.hpp>

#include "../../../matchers.hpp"

using rmt::M_type;
using rmt::numeric::number_type;
using rmt::matrix::Lowest_Eigenvalues;

using float_matcher = ::testing::Matcher<number_type>;

TEST(Lowest_Eigenvalue_Algorithm_Test, Too_Many_Eigenvalues)
{
  Lowest_Eigenvalues ev_alg;

  ASSERT_THROW({ ev_alg(rmt::matrix::Matrix(), M_size + 1); },
               std::runtime_error);
}

TEST(Lowest_Eigenvalue_Algorithm_Test, Test_Random_Matrix)
{
  auto m = rmt::matrix::Random_Matrix();
  auto mm = static_cast<M_type>(blaze::ctrans(m) * m);

  rmt::matrix::Eigenvalue_Solver<
      rmt::matrix::precission_type<rmt::numeric::complex_type>::value,
      rmt::matrix::Symmetries::Hermitian>
      solver;

  solver.Compute(mm, rmt::matrix::eigenvalues_only);
  auto evals = solver.Eigenvalues();
  std::sort(evals.begin(), evals.end());

  Lowest_Eigenvalues ev_alg;
  for (auto i = 1u; i < M_size; ++i) {
    auto lowest_vals = ev_alg(mm, i);

    auto matchers = std::vector<float_matcher>{};
    std::transform(
        evals.begin(), evals.begin() + i, std::back_inserter(matchers),
        [](number_type d) { return ::unit_test::FloatEq(d, 1e-12); });

    EXPECT_THAT(lowest_vals, ::testing::ElementsAreArray(matchers));
  }
}
