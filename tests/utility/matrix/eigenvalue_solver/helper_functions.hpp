/*
 * Created: 07-11-2016
 * Modified: Mon 07 Nov 2016 14:40:44 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef TESTING_MATRIX_EIGENVALUES_HELPER_FUNCTIONS_HPP
#define TESTING_MATRIX_EIGENVALUES_HELPER_FUNCTIONS_HPP

#include <fstream>
#include <types.hpp>

namespace unit_test {

template <typename eval_type>
struct eigenvalue_pair
{
  eval_type eval;
  blaze::DynamicVector<std::complex<double>> evec;
};

using generic_eigenvalue_pair = eigenvalue_pair<std::complex<double>>;
using hermitian_eigenvalue_pair = eigenvalue_pair<double>;

inline blaze::DynamicMatrix<std::complex<double>>
Read_Complex_Matrix(std::string filename, std::size_t rows, std::size_t cols)
{
  std::ifstream ifs{filename};

  if (!ifs)
    throw std::runtime_error{"Cannot open file \"" + filename +
                             "\" for reading"};

  auto result = blaze::DynamicMatrix<std::complex<double>>(rows, cols);

  double re, im;

  for (auto i = 0; i < rows; ++i) {
    for (auto j = 0; j < cols; ++j) {
      if (!(ifs >> re >> im))
        throw std::runtime_error{"Not enough elements in \"" + filename + "\""};

      result(i, j) = std::complex<double>{re, im};
    }
  }

  return result;
}

inline blaze::DynamicVector<std::complex<double>>
Read_Complex_Vector(std::string filename, std::size_t size)
{
  std::ifstream ifs{filename};

  if (!ifs)
    throw std::runtime_error{"Cannot open file \"" + filename +
                             "\" for reading"};

  auto result = blaze::DynamicVector<std::complex<double>>(size);

  double re, im;
  for (auto i = 0; i < size; ++i) {
    if (!(ifs >> re >> im))
      throw std::runtime_error{"Not enough elements in \"" + filename + "\""};

    result[i] = std::complex<double>{re, im};
  }

  return result;
}

inline blaze::DynamicVector<double> Read_Real_Vector(std::string filename,
                                                     std::size_t size)
{
  std::ifstream ifs{filename};

  if (!ifs)
    throw std::runtime_error{"Cannot open file \"" + filename +
                             "\" for reading"};

  auto result = blaze::DynamicVector<double>(size);

  double val;
  for (auto i = 0; i < size; ++i) {
    if (!(ifs >> val))
      throw std::runtime_error{"Not enough elements in \"" + filename + "\""};

    result[i] = val;
  }

  return result;
}

template <typename eval_type>
inline std::vector<eigenvalue_pair<eval_type>>
Decompose_Eigensystem(const blaze::DynamicVector<eval_type> &evals,
                      const blaze::DynamicMatrix<std::complex<double>> &evecs,
                      bool evec_in_rows = true)
{
  auto result = std::vector<eigenvalue_pair<eval_type>>(evals.size());

  for (auto i = 0ul; i < evals.size(); ++i) {
    result[i].eval = evals[i];

    result[i].evec.resize(evals.size());
    for (auto j = 0ul; j < evals.size(); ++j) {
      if (evec_in_rows)
        result[i].evec[j] = evecs(i, j);
      else
        result[i].evec[j] = evecs(j, i);
    }
  }

  return result;
}

template <typename eval_type>
inline void Make_Same_Signature(eigenvalue_pair<eval_type> &lhs,
                                const eigenvalue_pair<eval_type> &rhs)
{
  if ((blaze::ctrans(lhs.evec) * rhs.evec).real() < 0.)
    lhs.evec *= -1.;
}

} // namespace unit_test

#endif /* TESTING_MATRIX_EIGENVALUES_HELPER_FUNCTIONS_HPP */
