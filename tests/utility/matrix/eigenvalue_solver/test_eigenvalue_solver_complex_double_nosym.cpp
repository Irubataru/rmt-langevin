/*
 * Created: 03-11-2016
 * Modified: Mon 07 Nov 2016 11:58:13 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <algorithm>
#include <fstream>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <sstream>
#include <string>
#include <utilities/matrix/eigenvalues/eigenvalue_solver.hpp>

#include "../../../matchers.hpp"
#include "helper_functions.hpp"

using rmt::matrix::Precission;
using rmt::matrix::Symmetries;
using rmt::matrix::Eigenvalue_Solver;

using unit_test::FloatEq;
using unit_test::Read_Complex_Vector;
using unit_test::Read_Complex_Matrix;

const std::string matrix_folder{
    "tests/utility/matrix/eigenvalue_solver/test_data/"};

TEST(Eigenvalues_Complex_Double_NoSym, One_By_One)
{
  Eigenvalue_Solver<Precission::Complex_Double, Symmetries::None> solver{};

  blaze::DynamicMatrix<std::complex<double>> mat(1ul, 1ul);

  auto val = std::complex<double>{12.34, 45.67};
  mat(0, 0) = val;

  solver.Compute(mat, true);

  auto eigenvalues = solver.Eigenvalues();

  ASSERT_EQ(eigenvalues.size(), 1ul);
  EXPECT_EQ(eigenvalues[0].real(), val.real());
  EXPECT_EQ(eigenvalues[0].imag(), val.imag());
}

TEST(Eigenvalues_Complex_Double_NoSym, Diagonal_Matrix)
{
  constexpr std::size_t size{24ul};

  Eigenvalue_Solver<Precission::Complex_Double, Symmetries::None> solver{};

  blaze::DynamicMatrix<std::complex<double>> mat(size, size);

  for (auto i = 0; i < size; ++i)
    mat(i, i) = std::complex<double>{static_cast<double>(i),
                                     static_cast<double>(2. * i)};

  solver.Compute(mat, true);

  auto eigenvalues = solver.Eigenvalues();

  ASSERT_EQ(eigenvalues.size(), size);

  for (auto i = 0; i < size; ++i) {
    EXPECT_EQ(eigenvalues[i].real(), static_cast<double>(i));
    EXPECT_EQ(eigenvalues[i].imag(), static_cast<double>(2. * i));
  }
}

TEST(Eigenvalues_Complex_Double_NoSym, Ten_By_Ten_Test_Matrix)
{
  auto size = std::size_t{10ul};

  Eigenvalue_Solver<Precission::Complex_Double, Symmetries::None> solver{};

  auto read_mat =
      Read_Complex_Matrix(matrix_folder + "test_matrix_10x10.dat", size, size);

  solver.Compute(read_mat, true);
  auto eigenvalues = solver.Eigenvalues();

  ASSERT_EQ(eigenvalues.size(), size);

  auto correct_evals =
      Read_Complex_Vector(matrix_folder + "test_matrix_eigenvalues.dat", size);

  auto sort_operator = [](const std::complex<double> &lhs,
                          const std::complex<double> &rhs) {
    return std::abs(lhs) < std::abs(rhs);
  };

  std::sort(eigenvalues.begin(), eigenvalues.end(), sort_operator);
  std::sort(correct_evals.begin(), correct_evals.end(), sort_operator);

  for (auto i = 0; i < size; ++i) {
    EXPECT_THAT(eigenvalues[i].real(), FloatEq(correct_evals[i].real(), 1e-10));
    EXPECT_THAT(eigenvalues[i].imag(), FloatEq(correct_evals[i].imag(), 1e-10));
  }
}

TEST(Eigenvalues_Complex_Double_NoSym, Non_Square_Matrix)
{
  Eigenvalue_Solver<Precission::Complex_Double, Symmetries::None> solver{};
  blaze::DynamicMatrix<std::complex<double>> mat(5ul, 10ul);

  ASSERT_ANY_THROW({ solver.Compute(mat, true); });
}

TEST(Eigenvalues_Complex_Double_NoSym, Compute_Eigenvectors)
{
  Eigenvalue_Solver<Precission::Complex_Double, Symmetries::None> solver{};
  blaze::DynamicMatrix<std::complex<double>> mat(10ul, 10ul);

  ASSERT_ANY_THROW({ solver.Compute(mat, false); });
}
