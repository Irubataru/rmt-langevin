/*
 * Created: 07-11-2016
 * Modified: Mon 07 Nov 2016 14:50:12 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <gtest/gtest.h>
#include <iterator>
#include <random>
#include <utilities/matrix/eigenvalues/eigenvalue_solver.hpp>

#include "../../../matchers.hpp"
#include "helper_functions.hpp"

using rmt::matrix::Precission;
using rmt::matrix::Symmetries;
using rmt::matrix::Eigenvalue_Solver;

using unit_test::FloatEq;
using unit_test::Read_Real_Vector;
using unit_test::Read_Complex_Matrix;

const std::string matrix_folder{
    "tests/utility/matrix/eigenvalue_solver/test_data/"};

TEST(Eigenvalues_Complex_Double_Hermitian, One_By_One)
{
  Eigenvalue_Solver<Precission::Complex_Double, Symmetries::Hermitian> solver{};

  blaze::DynamicMatrix<std::complex<double>> mat(1ul, 1ul);

  auto val = std::complex<double>{12.34, 0.};
  mat(0, 0) = val;

  solver.Compute(mat, true);

  auto eigenvalues = solver.Eigenvalues();

  ASSERT_EQ(eigenvalues.size(), 1ul);
  EXPECT_EQ(eigenvalues[0], val.real());
}

TEST(Eigenvalues_Complex_Double_Hermitian, Not_Hermitian)
{
  Eigenvalue_Solver<Precission::Complex_Double, Symmetries::Hermitian> solver{};

  blaze::DynamicMatrix<std::complex<double>> mat(2ul, 2ul);

  for (auto i = 0ul; i < mat.rows(); ++i)
    for (auto j = 0ul; j < mat.columns(); ++j)
      mat(i, j) =
          std::complex<double>{static_cast<double>(i), static_cast<double>(j)};

  ASSERT_ANY_THROW({ solver.Compute(mat, true); });
}

TEST(Eigenvalues_Complex_Double_Hermitian, Non_Square_Matrix)
{
  Eigenvalue_Solver<Precission::Complex_Double, Symmetries::Hermitian> solver{};
  blaze::DynamicMatrix<std::complex<double>> mat(5ul, 10ul);

  ASSERT_ANY_THROW({ solver.Compute(mat, true); });
}

TEST(Eigenvalues_Complex_Double_Hermitian, Diagonal_Matrix)
{
  std::random_device rd{};
  std::mt19937 rng{rd()};
  std::uniform_real_distribution<double> dist{-1., 1.};

  Eigenvalue_Solver<Precission::Complex_Double, Symmetries::Hermitian> solver{};

  blaze::DynamicMatrix<std::complex<double>> mat(10ul, 10ul);
  std::vector<double> diags(10ul);

  std::generate(diags.begin(), diags.end(),
                [&rng, &dist]() { return dist(rng); });

  for (auto i = 0ul; i < mat.rows(); ++i)
    mat(i, i) = diags[i];

  solver.Compute(mat, true);
  auto eigenvalues = solver.Eigenvalues();

  std::sort(diags.begin(), diags.end());

  ASSERT_EQ(eigenvalues.size(), diags.size());
  for (auto i = 0ul; i < eigenvalues.size(); ++i)
    EXPECT_THAT(eigenvalues[i], FloatEq(diags[i]));

  auto eigenvectors = solver.Eigenvectors();
  EXPECT_EQ(eigenvectors.rows(), 0ul);
  EXPECT_EQ(eigenvectors.columns(), 0ul);
}

TEST(Eigenvalues_Complex_Double_Hermitian,
     Ten_By_Ten_Test_Matrix_Eigenvalues_Only)
{
  auto size = std::size_t{10ul};

  Eigenvalue_Solver<Precission::Complex_Double, Symmetries::Hermitian> solver{};

  auto read_mat = Read_Complex_Matrix(
      matrix_folder + "test_matrix_hermitian_10x10.dat", size, size);

  solver.Compute(read_mat, true);
  auto eigenvalues = solver.Eigenvalues();

  ASSERT_EQ(eigenvalues.size(), size);

  auto correct_evals = Read_Real_Vector(
      matrix_folder + "test_matrix_hermitian_eigenvalues.dat", size);

  std::sort(eigenvalues.begin(), eigenvalues.end());
  std::sort(correct_evals.begin(), correct_evals.end());

  for (auto i = 0; i < size; ++i)
    EXPECT_THAT(eigenvalues[i], FloatEq(correct_evals[i], 1e-10));
}

TEST(Eigenvalues_Complex_Double_Hermitian,
     Ten_By_Ten_Test_Matrix_w_Eigenvectors)
{
  auto size = std::size_t{10ul};

  Eigenvalue_Solver<Precission::Complex_Double, Symmetries::Hermitian> solver{};

  auto read_mat = Read_Complex_Matrix(
      matrix_folder + "test_matrix_hermitian_10x10.dat", size, size);

  solver.Compute(read_mat, false);
  auto eigenvalues = solver.Eigenvalues();
  auto eigenvectors = solver.Eigenvectors();

  ASSERT_EQ(eigenvalues.size(), size);
  ASSERT_EQ(eigenvectors.rows(), size);
  ASSERT_EQ(eigenvectors.columns(), size);

  auto sorted_evals =
      unit_test::Decompose_Eigensystem(eigenvalues, eigenvectors, false);

  auto sorted_expected_evals = unit_test::Decompose_Eigensystem(
      Read_Real_Vector(matrix_folder + "test_matrix_hermitian_eigenvalues.dat",
                       size),
      Read_Complex_Matrix(matrix_folder +
                              "test_matrix_hermitian_eigenvectors.dat",
                          size, size));

  ASSERT_EQ(sorted_expected_evals.size(), size);

  for (auto i = 0ul; i < size; ++i)
    unit_test::Make_Same_Signature(sorted_evals[i], sorted_expected_evals[i]);

  for (auto i = 0ul; i < size; ++i) {

    EXPECT_THAT(sorted_evals[i].eval,
                FloatEq(sorted_expected_evals[i].eval, 1e-10));

    for (auto j = 0ul; j < size; ++j) {
      EXPECT_THAT(sorted_evals[i].evec[j].real(),
                  FloatEq(sorted_expected_evals[i].evec[j].real(), 1e-10));
      EXPECT_THAT(sorted_evals[i].evec[j].imag(),
                  FloatEq(sorted_expected_evals[i].evec[j].imag(), 1e-10));
    }
  }
}
