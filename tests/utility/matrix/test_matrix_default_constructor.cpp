/*
 * Created: 01-11-2016
 * Modified: Tue 01 Nov 2016 16:23:02 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <utilities/matrix/default_constructor.hpp>

TEST(Matrix_Default_Constructor, Test_Size)
{
  auto mat = rmt::matrix::Matrix();

  EXPECT_EQ(mat.rows(), M_size);
  EXPECT_EQ(mat.columns(), M_size);
}

TEST(Matrix_Default_Constructor, Default_Element)
{
  auto mat = rmt::matrix::Matrix();

  for (auto i = 0; i < mat.rows(); ++i)
    for (auto j = 0; j < mat.columns(); ++j)
      EXPECT_EQ(mat(i, j), rmt::numeric::complex_type{0.});
}

TEST(Matrix_Default_Constructor, With_Default_Element)
{
  auto value = rmt::numeric::complex_type{1.1, -5.4};
  auto mat = rmt::matrix::Matrix(value);

  for (auto i = 0; i < mat.rows(); ++i)
    for (auto j = 0; j < mat.columns(); ++j)
      EXPECT_EQ(mat(i, j), value);
}

TEST(Matrix_Default_Constructor, Zero_Matrix)
{
  auto mat = rmt::matrix::Zero_Matrix();

  EXPECT_EQ(mat.rows(), M_size);
  EXPECT_EQ(mat.columns(), M_size);

  for (auto i = 0; i < mat.rows(); ++i)
    for (auto j = 0; j < mat.columns(); ++j)
      EXPECT_EQ(mat(i, j), rmt::numeric::complex_type{0.});
}
