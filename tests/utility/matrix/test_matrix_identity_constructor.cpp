/*
 * Created: 01-11-2016
 * Modified: Tue 01 Nov 2016 18:20:42 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <utilities/matrix/identity_constructor.hpp>

using rmt::numeric::number_type;
using rmt::numeric::complex_type;

TEST(Matrix_Identity_Constructor, No_Args)
{
  auto identity = rmt::matrix::Identity_Matrix();

  EXPECT_EQ(identity.rows(), M_size);
  EXPECT_EQ(identity.columns(), M_size);

  EXPECT_TRUE(blaze::isIdentity(identity));
}

TEST(Matrix_Identity_Constructor, One_Arg)
{
  auto value = complex_type{6.123, 56.78};
  auto identity = rmt::matrix::Identity_Matrix(value);

  EXPECT_EQ(identity.rows(), M_size);
  EXPECT_EQ(identity.columns(), M_size);

  ASSERT_TRUE(blaze::isDiagonal(identity));

  for (auto i = 0; i < identity.rows(); ++i)
    EXPECT_EQ(identity(i, i), value);
}

TEST(Matrix_Identity_Constructor, Reset_Matrix)
{
  auto mat = rmt::matrix::Matrix();

  for (auto i = 0; i < mat.rows(); ++i)
    for (auto j = 0; j < mat.columns(); ++j)
      mat(i, j) = complex_type{static_cast<number_type>(i),
                               static_cast<number_type>(j)};

  rmt::matrix::Identity_Matrix(mat);

  EXPECT_EQ(mat.rows(), M_size);
  EXPECT_EQ(mat.columns(), M_size);
  EXPECT_TRUE(blaze::isIdentity(mat));
};

TEST(Matrix_Identity_Constructor, Reset_Matrix_With_Arg)
{
  auto mat = rmt::matrix::Matrix();

  for (auto i = 0; i < mat.rows(); ++i)
    for (auto j = 0; j < mat.columns(); ++j)
      mat(i, j) = complex_type{static_cast<number_type>(i),
                               static_cast<number_type>(j)};

  auto value = complex_type{-12.34, 567.89};
  rmt::matrix::Identity_Matrix(mat, value);

  EXPECT_EQ(mat.rows(), M_size);
  EXPECT_EQ(mat.columns(), M_size);
  EXPECT_TRUE(blaze::isDiagonal(mat));

  for (auto i = 0; i < mat.rows(); ++i)
    EXPECT_EQ(mat(i, i), value);
};
