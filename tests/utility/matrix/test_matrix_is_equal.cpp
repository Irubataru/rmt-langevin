/*
 * Created: 01-11-2016
 * Modified: Wed 02 Nov 2016 12:11:38 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <utilities/matrix/is_equal.hpp>
#include <utilities/matrix/matrix_constructors.hpp>

using rmt::numeric::number_type;
using rmt::numeric::complex_type;

TEST(Matrix_Is_Equal, Two_Zero_Matrices)
{
  auto mat1 = rmt::matrix::Zero_Matrix();
  auto mat2 = rmt::M_type{mat1};

  EXPECT_TRUE(rmt::matrix::Is_Equal(mat1, mat2));
}

TEST(Matrix_Is_Equal, Two_Matrices_By_Loop)
{
  auto mat1 = rmt::matrix::Matrix(), mat2 = rmt::matrix::Matrix();

  for (auto i = 0; i < mat1.rows(); ++i) {
    for (auto j = 0; j < mat1.columns(); ++j) {
      mat1(i, j) = complex_type{static_cast<number_type>(i),
                                static_cast<number_type>(j)};
      mat2(i, j) = complex_type{static_cast<number_type>(i),
                                static_cast<number_type>(j)};
    }
  }

  EXPECT_TRUE(rmt::matrix::Is_Equal(mat1, mat2));
}

TEST(Matrix_Is_Equal, One_Off)
{
  auto mat1 = rmt::matrix::Matrix(number_type{12.34});
  auto mat2 = rmt::M_type{mat1};

  mat1(0, 0) *= number_type{2.};

  EXPECT_FALSE(rmt::matrix::Is_Equal(mat1, mat2));
}

TEST(Matrix_Is_Equal, Within_Error)
{
  auto error = number_type{1.};
  auto mat1 =
      rmt::matrix::Matrix(complex_type{number_type{12.1}, number_type{-11.1}});
  auto mat2 = rmt::M_type{mat1};

  mat1(0, 0) += number_type{0.1} * error;

  EXPECT_TRUE(rmt::matrix::Is_Equal(mat1, mat2, error));
}

TEST(Matrix_Is_Equal, Without_Error)
{
  auto error = number_type{0.1};
  auto mat1 = rmt::matrix::Matrix(
      complex_type{number_type{-24.44}, number_type{88.11}});
  auto mat2 = rmt::M_type{mat1};

  mat1(0, 0) += number_type{10.} * error * mat1(0, 0);

  EXPECT_FALSE(rmt::matrix::Is_Equal(mat1, mat2, error));
}
