
/*
 * Created: 31-08-2017
 * Modified: Thu 31 Aug 2017 19:47:35 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <utilities/filename_generator.hpp>

using rmt::build::Filename_Generator;
using namespace rmt;

using ::testing::StrEq;

TEST(Filename_Generator, Is_Valid_Empty_Path)
{
  Filename_Generator gen{"a"};
  EXPECT_FALSE(gen.is_valid());
}

TEST(Filename_Generator, Is_Valid_No_Obs)
{
  Filename_Generator gen{"some/file/path/file.dat"};
  EXPECT_FALSE(gen.is_valid());
}

TEST(Filename_Generator, Is_Valid_Invalid_Pattern)
{
  Filename_Generator gen{"var@[()]/@{obs}.dat"};
  EXPECT_FALSE(gen.is_valid());
}

TEST(Filename_Generator, Is_Valid_Valid_Pattern)
{
  Filename_Generator gen{"@{model}_@{storage}/nf@{nf}/@{mat_size}x@{mat_size}/"
                         "mu@{mu_n}/mass@{mass_n}/@{obs}.dat"};
  EXPECT_TRUE(gen.is_valid());
}

TEST(Filename_Generator, Filename_Mu_Scan)
{
  Filename_Generator gen{"@{model}_@{storage}/nf@{nf}/@{mat_size}x@{mat_size}/"
                         "mu@{mu_n}/@{mu_re}_i@{mu_im}/@{obs}.dat"};

  ASSERT_TRUE(gen.is_valid());

  Model_Parameters model_parms;
  model_parms.model = Model_Type::Osborn;
  model_parms.storage = Storage_Type::Polar;
  model_parms.number_of_flavours = 22;
  model_parms.mu_scan = {{0.10, 0.22}, {0.11, 3.22}, {11.22, 0.01}};
  model_parms.mass_scan = {0.0};

  for (auto i = 0; i < model_parms.mu_scan.size(); ++i) {
    Physical_Parameters phys_parms;
    phys_parms.number_of_flavours = model_parms.number_of_flavours;
    phys_parms.chemical_potential = model_parms.mu_scan[i];
    phys_parms.fermion_mass = 0.0;

    std::ostringstream oss;
    oss << std::setprecision(4);
    oss << "osborn_polar/nf" << model_parms.number_of_flavours << "/" << M_size
        << "x" << M_size << "/mu" << i << "/" << model_parms.mu_scan[i].real()
        << "_i" << model_parms.mu_scan[i].imag();

    EXPECT_THAT(gen.directory(phys_parms, model_parms), StrEq(oss.str()));

    oss << "/something.dat";
    EXPECT_THAT(gen.filename(phys_parms, model_parms, "something"),
                StrEq(oss.str()));
  }
}

TEST(Filename_Generator, Filename_Mass_Scan)
{
  Filename_Generator gen{"@{model}_@{storage}/nf@{nf}/@{mat_size}x@{mat_size}/"
                         "mass@{mass_n}/@{mass_re}_i@{mass_im}/@{obs}.dat"};

  ASSERT_TRUE(gen.is_valid());

  Model_Parameters model_parms;
  model_parms.model = Model_Type::Stephanov;
  model_parms.storage = Storage_Type::Cartesian;
  model_parms.number_of_flavours = 22;
  model_parms.mu_scan = {0.0};
  model_parms.mass_scan = {{2.22, 4.11}, {0.54, 0.02}, {6.71, 99.1}};

  for (auto i = 0; i < model_parms.mass_scan.size(); ++i) {
    Physical_Parameters phys_parms;
    phys_parms.number_of_flavours = model_parms.number_of_flavours;
    phys_parms.chemical_potential = 0.0;
    phys_parms.fermion_mass = model_parms.mass_scan[i];

    std::ostringstream oss;
    oss << std::setprecision(4);
    oss << "stephanov_cartesian/nf" << model_parms.number_of_flavours << "/"
        << M_size << "x" << M_size << "/mass" << i << "/"
        << model_parms.mass_scan[i].real() << "_i"
        << model_parms.mass_scan[i].imag();

    EXPECT_THAT(gen.directory(phys_parms, model_parms), StrEq(oss.str()));

    oss << "/something.dat";
    EXPECT_THAT(gen.filename(phys_parms, model_parms, "something"),
                StrEq(oss.str()));
  }
}

TEST(Filename_Generator, Value_Not_In_Range)
{
  Filename_Generator gen{"@{model}_@{storage}/nf@{nf}/@{mat_size}x@{mat_size}/"
                         "mass@{mass_n}/@{mass_re}_i@{mass_im}/@{obs}.dat"};

  ASSERT_TRUE(gen.is_valid());

  Model_Parameters model_parms;
  model_parms.model = Model_Type::Osborn;
  model_parms.storage = Storage_Type::Polar;
  model_parms.number_of_flavours = 22;
  model_parms.mu_scan = {0.0};
  model_parms.mass_scan = {0.0};

  Physical_Parameters phys_parms;
  phys_parms.number_of_flavours = model_parms.number_of_flavours;
  phys_parms.chemical_potential = {0.5, 2.2};
  phys_parms.fermion_mass = {0.0};

  EXPECT_THROW({
      auto fname = gen.directory(phys_parms, model_parms);
  }, std::runtime_error);
}
