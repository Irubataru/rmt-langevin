/*
 * Created: 22-04-2016
 * Modified: Thu 31 Aug 2017 19:14:58 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <gtest/gtest.h>
#include <utilities/filename_parser.hpp>

TEST(Filename_Parser, Empty)
{
  auto test_str = std::string{};
  rmt::filename::parameter_symbols symb;
  EXPECT_FALSE(rmt::filename::parse(test_str, symb).second);
}

TEST(Filename_Parser, Simple_String)
{
  auto test_str = std::string{"Hello"};
  rmt::filename::parameter_symbols symb;
  auto parsed_str = rmt::filename::parse(test_str, symb).first;

  EXPECT_EQ(test_str, parsed_str);
}

TEST(Filename_Parser, Simple_Symbol)
{
  auto test_str = std::string{"@{x}"};
  rmt::filename::parameter_symbols symb;

  auto replace_str = std::string{"Hello"};
  symb.add("x", replace_str);

  auto parsed_str = rmt::filename::parse(test_str, symb).first;
}

TEST(Filename_Parser, Unknown_Symbol)
{
  auto test_str = std::string{"@{x}"};
  rmt::filename::parameter_symbols symb;
  EXPECT_FALSE(rmt::filename::parse(test_str, symb).second);
}

TEST(Filename_Parser, Simple_Arithmetic)
{
  auto test_str = std::string{"@[(5.6 + 4.2) - 1.0]"};
  rmt::filename::parameter_symbols symb;

  auto parsed_str = rmt::filename::parse(test_str, symb).first;
  EXPECT_EQ(std::string{"8.8"}, parsed_str);
}

TEST(Filename_Parser, Arithmetic_With_Symbol)
{
  auto test_str = std::string{"@[(5.6 + @{var}) - 1.0]"};
  rmt::filename::parameter_symbols symb;
  symb.add("var", "4.2");

  auto parsed_str = rmt::filename::parse(test_str, symb).first;
  EXPECT_EQ(std::string{"8.8"}, parsed_str);
}

TEST(Filename_Parser, Arithmetic_With_Escaped_Symbol)
{
  auto test_str = std::string{"@[(5.6 + var) - 1.0]"};
  rmt::filename::parameter_symbols symb;
  symb.add("var", "4.2");

  auto parsed_str = rmt::filename::parse(test_str, symb).first;
  EXPECT_EQ(std::string{"8.8"}, parsed_str);
}

TEST(Filename_Parser, Faulty_Arithmetic)
{
  auto test_str = std::string{"Hello @[(5.6 + var) - 1.0 jonas"};
  rmt::filename::parameter_symbols symb;
  symb.add("var", "4.2");
  symb.add("jonas", "glesaaen");

  EXPECT_FALSE(rmt::filename::parse(test_str, symb).second);
}

TEST(Filename_Parser, Uncastable_Variable)
{
  auto test_str = std::string{"@[(5.6 + var) - 1.0]"};
  rmt::filename::parameter_symbols symb;
  symb.add("var", "hello");

  ASSERT_THROW({ auto parsed_str = rmt::filename::parse(test_str, symb); },
               boost::bad_lexical_cast);
}

TEST(Filename_Parser, Dummy_Variable)
{
  auto test_str = std::string{"@{dummy}"};
  rmt::filename::parameter_symbols symb;
  symb.add_dummy("dummy");

  auto parsed_str = rmt::filename::parse(test_str, symb).first;
  EXPECT_EQ(test_str, parsed_str);
}
