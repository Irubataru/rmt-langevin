/*
 * Created: 07-04-2016
 * Modified: Fri 01 Sep 2017 11:39:22 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <boost/math/constants/constants.hpp>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <osborn/observables/chiral_condensate.hpp>
#include <osborn/resource/initialiser.hpp>
#include <types.hpp>

#include "../../matchers.hpp"

using rmt::M_type;
using n = rmt::numeric::number_type;
using rmt::numeric::complex_type;
using rmt::numeric::imag_i;

using unit_test::FloatEq;

TEST(Osborn_Chiral_Condensate_Cartesian, Test_Zero_Init)
{
  namespace model = rmt::osborn;
  namespace storage = model::cartesian;

  auto par = rmt::Physical_Parameters{{1.1, 0.2},  // chemical_potential
                                      {-2.1, 4.2}, // fermion_mass
                                      11};         // number_of_flavours

  auto data = std::make_shared<storage::Matrix_Storage>();

  auto initialise_to_zero = [](M_type &m1, M_type &m2, M_type &m3, M_type &m4) {
    m1 = rmt::matrix::Zero_Matrix();
    m2 = rmt::matrix::Zero_Matrix();
    m3 = rmt::matrix::Zero_Matrix();
    m4 = rmt::matrix::Zero_Matrix();
  };

  EXPECT_EQ(0ul, data->get_storage_revision());

  storage::Initialise(*data, par, initialise_to_zero);
  EXPECT_EQ(1ul, data->get_storage_revision());

  auto obs = model::observable::Chiral_Condensate{data, par};
  obs.Calculate();

  auto mf = par.fermion_mass;
  auto nf_d = static_cast<n>(par.number_of_flavours);
  auto expected_val = n{2.} * mf * nf_d / (mf * mf);

  auto cc_val = obs.extract().front();

  EXPECT_THAT(cc_val.real(), FloatEq(expected_val.real(), 1e-12));
  EXPECT_THAT(cc_val.imag(), FloatEq(expected_val.imag(), 1e-12));
}

TEST(Osborn_Chiral_Condensate_Cartesian, Test_One_Init)
{
  using rmt::numeric::cosh;
  using rmt::numeric::sinh;

  namespace model = rmt::osborn;
  namespace storage = model::cartesian;

  auto par = rmt::Physical_Parameters{{1.1, 0.2},  // chemical_potential
                                      {-2.1, 4.2}, // fermion_mass
                                      11};         // number_of_flavours

  auto data = std::make_shared<storage::Matrix_Storage>();

  auto initialise_to_one = [](M_type &m1, M_type &m2, M_type &m3, M_type &m4) {
    m1 = rmt::matrix::Identity_Matrix();
    m2 = rmt::matrix::Identity_Matrix();
    m3 = rmt::matrix::Identity_Matrix();
    m4 = rmt::matrix::Identity_Matrix();
  };

  EXPECT_EQ(0ul, data->get_storage_revision());

  storage::Initialise(*data, par, initialise_to_one);
  EXPECT_EQ(1ul, data->get_storage_revision());

  auto obs = model::observable::Chiral_Condensate{data, par};
  obs.Calculate();

  auto mf = par.fermion_mass;
  auto c_mu = cosh(par.chemical_potential), s_mu = sinh(par.chemical_potential);
  auto nf_d = static_cast<n>(par.number_of_flavours);

  auto expected_value =
      n{2.} * mf * nf_d /
      (mf * mf - n{2.} * (n{2.} * imag_i * c_mu * s_mu - n{1.}));

  auto cc_val = obs.extract().front();
  EXPECT_THAT(cc_val.real(), FloatEq(expected_value.real(), 1e-12));
  EXPECT_THAT(cc_val.imag(), FloatEq(expected_value.imag(), 1e-12));
}
