/*
 * Created: 07-04-2016
 * Modified: Fri 01 Sep 2017 11:39:11 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <boost/math/constants/constants.hpp>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <osborn/observables/baryon_number_density.hpp>
#include <osborn/resource/initialiser.hpp>

#include "../../matchers.hpp"

using rmt::M_type;
using n = rmt::numeric::number_type;
using rmt::numeric::complex_type;

using rmt::numeric::imag_i;
using unit_test::FloatEq;

TEST(Osborn_Baryon_Number_Density_Cartesian, Test_Zero_Init)
{
  namespace model = rmt::osborn;
  namespace storage = model::cartesian;

  auto par = rmt::Physical_Parameters{{1.1, 0.2},  // chemical_potential
                                      {-2.1, 4.2}, // fermion_mass
                                      11};         // number_of_flavours

  auto data = std::make_shared<storage::Matrix_Storage>();

  auto initialise_to_zero = [](M_type &m1, M_type &m2, M_type &m3, M_type &m4) {
    m1 = rmt::matrix::Zero_Matrix();
    m2 = rmt::matrix::Zero_Matrix();
    m3 = rmt::matrix::Zero_Matrix();
    m4 = rmt::matrix::Zero_Matrix();
  };

  EXPECT_EQ(0ul, data->get_storage_revision());

  storage::Initialise(*data, par, initialise_to_zero);
  EXPECT_EQ(1ul, data->get_storage_revision());

  auto obs = model::observable::Baryon_Number_Density{data, par};
  obs.Calculate();

  auto nb_val = obs.extract().front();

  EXPECT_THAT(nb_val.real(), FloatEq(n{0.}));
  EXPECT_THAT(nb_val.imag(), FloatEq(n{0.}));
}

TEST(Osborn_Baryon_Number_Density_Cartesian, Test_One_Init)
{
  using rmt::numeric::cosh;
  using rmt::numeric::sinh;

  namespace model = rmt::osborn;
  namespace storage = model::cartesian;

  auto par = rmt::Physical_Parameters{{1.1, 0.2},  // chemical_potential
                                      {-2.1, 4.2}, // fermion_mass
                                      11};         // number_of_flavours

  auto data = std::make_shared<storage::Matrix_Storage>();

  auto initialise_to_one = [](M_type &m1, M_type &m2, M_type &m3, M_type &m4) {
    m1 = rmt::matrix::Identity_Matrix();
    m2 = rmt::matrix::Identity_Matrix();
    m3 = rmt::matrix::Identity_Matrix();
    m4 = rmt::matrix::Identity_Matrix();
  };

  EXPECT_EQ(0ul, data->get_storage_revision());

  storage::Initialise(*data, par, initialise_to_one);
  EXPECT_EQ(1ul, data->get_storage_revision());

  auto obs = model::observable::Baryon_Number_Density{data, par};
  obs.Calculate();

  auto mf = par.fermion_mass;
  auto c_mu = cosh(par.chemical_potential), s_mu = sinh(par.chemical_potential);
  auto nf_d = static_cast<n>(par.number_of_flavours);

  auto inv_g =
      n{1.} / (mf * mf - n{2.} * (n{2.} * imag_i * c_mu * s_mu - n{1.}));

  auto expected_value =
      nf_d * inv_g * (-n{4.} * imag_i * (s_mu * s_mu + c_mu * c_mu));

  auto nb_val = obs.extract().front();

  EXPECT_THAT(nb_val.real(), FloatEq(expected_value.real(), 1e-12));
  EXPECT_THAT(nb_val.imag(), FloatEq(expected_value.imag(), 1e-12));
}
