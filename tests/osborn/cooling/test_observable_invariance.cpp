
/*
 * Created: 10-06-2016
 * Modified: Fri 01 Sep 2017 11:40:42 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <osborn/cooling/core/index_conversion_algorithm.hpp>
#include <osborn/cooling/general_cooling.hpp>
#include <osborn/langevin/force.hpp>
#include <osborn/observables/baryon_number_density.hpp>
#include <osborn/observables/chiral_condensate.hpp>
#include <random>
#include <utilities/matrix/eigenvalues/eigenvalue_solver.hpp>
#include <utilities/matrix/identity_constructor.hpp>
#include <utilities/matrix/random_constructor.hpp>

#include "../../matchers.hpp"
#include "mocks/mock_cooling_helper_algs.hpp"
#include "mocks/mock_proxy.hpp"

#include <fstream>

using rmt::M_type;
using rmt::numeric::number_type;
using rmt::numeric::complex_type;

using norm_arg_type = unit_test::osborn::Mock_Index_Alg::arg_type;

using rmt::osborn::cooling::core::Transformation_Indices;
using rmt::osborn::cooling::core::Transformation_Matrices;

using unit_test::MatrixApprox;
using unit_test::FloatEq;

namespace model = ::rmt::osborn;

template <typename Dist, typename Rng>
std::vector<number_type> random_vector(std::size_t size, Dist dist, Rng &rng)
{
  auto res = std::vector<number_type>(size);
  std::generate(res.begin(), res.end(), [&dist, &rng]() { return dist(rng); });

  return res;
}

Transformation_Matrices exponensiate_indices(const Transformation_Indices &ind)
{
  using rmt::numeric::exp;

  model::cooling::core::Direct_Index_Conversion_Algorithm alg{};
  auto gen_matrices = alg(ind);

  rmt::matrix::Eigenvalue_Solver<
      rmt::matrix::precission_type<complex_type>::value,
      rmt::matrix::Symmetries::Hermitian>
      solver;

  solver.Compute(gen_matrices.g_gen, rmt::matrix::eigenvalues_and_eigenvectors);

  auto g_eigen_values = solver.Eigenvalues();
  auto g_eigen_vectors = solver.Eigenvectors();

  auto g_diag_mat = rmt::matrix::Zero_Matrix(),
       g_diag_mat_i = rmt::matrix::Zero_Matrix();

  for (auto i = 0u; i < M_size; ++i) {
    g_diag_mat(i, i) = exp(-g_eigen_values[i]);
    g_diag_mat_i(i, i) = exp(g_eigen_values[i]);
  }

  solver.Compute(gen_matrices.h_gen, rmt::matrix::eigenvalues_and_eigenvectors);

  auto h_eigen_values = solver.Eigenvalues();
  auto h_eigen_vectors = solver.Eigenvectors();

  auto h_diah_mat = rmt::matrix::Zero_Matrix(),
       h_diah_mat_i = rmt::matrix::Zero_Matrix();

  for (auto i = 0u; i < M_size; ++i) {
    h_diah_mat(i, i) = exp(-h_eigen_values[i]);
    h_diah_mat_i(i, i) = exp(h_eigen_values[i]);
  }

  return {static_cast<M_type>(g_eigen_vectors * g_diag_mat *
                              blaze::ctrans(g_eigen_vectors)),
          static_cast<M_type>(g_eigen_vectors * g_diag_mat_i *
                              blaze::ctrans(g_eigen_vectors)),
          static_cast<M_type>(h_eigen_vectors * h_diah_mat *
                              blaze::ctrans(h_eigen_vectors)),
          static_cast<M_type>(h_eigen_vectors * h_diah_mat_i *
                              blaze::ctrans(h_eigen_vectors))};
}

number_type Norm(const M_type &m)
{
  auto norm = number_type{0.};

  for (auto i = 0ul; i < m.rows(); ++i)
    for (auto j = 0ul; j < m.columns(); ++j)
      norm += std::norm(m(i, j));

  return rmt::numeric::sqrt(norm);
};

TEST(Osborn_Cooling_Invariance_Test, Random_Matrix_Chiral_Condensate)
{
  namespace representation = model::cartesian;

  using core_type = model::cooling::core::Cooling_Core<
      unit_test::osborn::Mock_Proxy<unit_test::osborn::Mock_Index_Alg>,
      unit_test::osborn::Mock_Proxy<unit_test::osborn::Mock_Index_Converter>,
      unit_test::osborn::Mock_Proxy<
          unit_test::osborn::Mock_Optimise_Preset_Alg>>;

  using step_type =
      model::cooling::core::cartesian::General_Cartesian_Cooling_Step<
          core_type>;

  std::random_device rd{};
  std::mt19937 rng{rd()};
  std::uniform_real_distribution<> uniform_dist(-1., 1.);

  std::size_t updates = 100;

  auto mu = complex_type{uniform_dist(rng), 0.};
  auto mass = complex_type{uniform_dist(rng), 0.};

  auto a_mat = rmt::matrix::Random_Matrix(uniform_dist, rng);
  auto b_mat = rmt::matrix::Random_Matrix(uniform_dist, rng);
  auto al_mat = rmt::matrix::Random_Matrix(uniform_dist, rng);
  auto bt_mat = rmt::matrix::Random_Matrix(uniform_dist, rng);

  auto data = std::make_shared<representation::Matrix_Storage>();

  data->set_mu(mu);
  data->set_mass(mass);

  data->read_write(representation::M_name::A) = a_mat;
  data->read_write(representation::M_name::B) = b_mat;
  data->read_write(representation::M_name::ALPH) = al_mat;
  data->read_write(representation::M_name::BETA) = bt_mat;

  data->update();

  auto observable = model::observable::Chiral_Condensate{data, {mu, mass, 1}};
  observable.Calculate();

  std::uniform_real_distribution<double> small_dist{-1e-1, 1e-1};

  auto g_mat = static_cast<M_type>(rmt::matrix::Identity_Matrix() +
                                   rmt::matrix::Random_Matrix(small_dist, rng));
  auto gi_mat = static_cast<M_type>(blaze::inv(g_mat));
  auto h_mat = static_cast<M_type>(rmt::matrix::Identity_Matrix() +
                                   rmt::matrix::Random_Matrix(small_dist, rng));
  auto hi_mat = static_cast<M_type>(blaze::inv(h_mat));

  unit_test::osborn::Mock_Index_Alg idx_alg;
  unit_test::osborn::Mock_Index_Converter idx_conv;
  unit_test::osborn::Mock_Optimise_Preset_Alg opt_alg{
      {g_mat, gi_mat, h_mat, hi_mat}};

  EXPECT_CALL(idx_alg, idx_alg(::testing::_)).Times(updates);
  EXPECT_CALL(idx_conv, conv_alg(::testing::_)).Times(updates);
  EXPECT_CALL(opt_alg, optimise(::testing::_, ::testing::_)).Times(updates);

  core_type core{{idx_alg}, {idx_conv}, {opt_alg}};
  step_type step{data, core};

  for (auto i = 0u; i < updates; ++i) {
    step.Update();
    observable.Calculate();

    g_mat = rmt::matrix::Identity_Matrix() +
            rmt::matrix::Random_Matrix(small_dist, rng);
    gi_mat = blaze::inv(g_mat);
    h_mat = rmt::matrix::Identity_Matrix() +
            rmt::matrix::Random_Matrix(small_dist, rng);
    hi_mat = blaze::inv(h_mat);

    opt_alg.replace_matrices({g_mat, gi_mat, h_mat, hi_mat});
  }

  auto observations = observable.data();

  ASSERT_THAT(observations.size(), ::testing::Eq(updates + 1u));
  EXPECT_THAT(observations,
              ::testing::Each(FloatEq(observations.front(), 1e-12)));
}

TEST(Osborn_Cooling_Invariance_Test, Random_Unitary_Chiral_Condensate)
{
  namespace representation = model::cartesian;

  using core_type = model::cooling::core::Cooling_Core<
      unit_test::osborn::Mock_Proxy<unit_test::osborn::Mock_Index_Alg>,
      unit_test::osborn::Mock_Proxy<unit_test::osborn::Mock_Index_Converter>,
      unit_test::osborn::Mock_Proxy<
          unit_test::osborn::Mock_Optimise_Preset_Alg>>;

  using step_type =
      model::cooling::core::cartesian::General_Cartesian_Cooling_Step<
          core_type>;

  std::random_device rd{};
  std::mt19937 rng{rd()};
  std::uniform_real_distribution<> uniform_dist{-1., 1.};

  std::size_t updates = 100;

  auto mu = complex_type{uniform_dist(rng), 0.};
  auto mass = complex_type{uniform_dist(rng), 0.};

  auto a_mat = rmt::matrix::Random_Matrix(uniform_dist, rng);
  auto b_mat = rmt::matrix::Random_Matrix(uniform_dist, rng);
  auto al_mat = rmt::matrix::Random_Matrix(uniform_dist, rng);
  auto bt_mat = rmt::matrix::Random_Matrix(uniform_dist, rng);

  auto data = std::make_shared<representation::Matrix_Storage>();

  data->set_mu(mu);
  data->set_mass(mass);

  data->read_write(representation::M_name::A) = a_mat;
  data->read_write(representation::M_name::B) = b_mat;
  data->read_write(representation::M_name::ALPH) = al_mat;
  data->read_write(representation::M_name::BETA) = bt_mat;

  data->update();

  auto observable = model::observable::Chiral_Condensate{data, {mu, mass, 1}};
  observable.Calculate();

  unit_test::osborn::Mock_Index_Alg idx_alg;
  unit_test::osborn::Mock_Index_Converter idx_conv;
  unit_test::osborn::Mock_Optimise_Preset_Alg opt_alg{};

  EXPECT_CALL(idx_alg, idx_alg(::testing::_)).Times(updates);
  EXPECT_CALL(idx_conv, conv_alg(::testing::_)).Times(updates);
  EXPECT_CALL(opt_alg, optimise(::testing::_, ::testing::_)).Times(updates);

  core_type core{{idx_alg}, {idx_conv}, {opt_alg}};
  step_type step{data, core};

  std::uniform_real_distribution<> small_dist{-1e-3, 1e-3};

  for (auto i = 0u; i < updates; ++i) {
    auto g_indices = random_vector(M_size * M_size, small_dist, rng);
    auto h_indices = random_vector(M_size * M_size, small_dist, rng);

    auto trafos = exponensiate_indices({g_indices, h_indices});

    opt_alg.replace_matrices({trafos.g, trafos.g_inv, trafos.h, trafos.h_inv});

    auto phi_new = static_cast<M_type>(
        trafos.g * data->read(model::M_name::PHI) * trafos.h_inv);
    auto phi_adj_new = static_cast<M_type>(
        trafos.h * data->read(model::M_name::PHIADJ) * trafos.g_inv);
    auto psi_new = static_cast<M_type>(
        trafos.g * data->read(model::M_name::PSI) * trafos.h_inv);
    auto psi_adj_new = static_cast<M_type>(
        trafos.h * data->read(model::M_name::PSIADJ) * trafos.g_inv);

    step.Update();

    EXPECT_THAT(data->read(model::M_name::PHI), MatrixApprox(phi_new, 1e-12));
    EXPECT_THAT(data->read(model::M_name::PHIADJ),
                MatrixApprox(phi_adj_new, 1e-12));
    EXPECT_THAT(data->read(model::M_name::PSI), MatrixApprox(psi_new, 1e-12));
    EXPECT_THAT(data->read(model::M_name::PSIADJ),
                MatrixApprox(psi_adj_new, 1e-12));

    observable.Calculate();
  }

  auto observations = observable.data();

  ASSERT_THAT(observations.size(), ::testing::Eq(updates + 1u));
  EXPECT_THAT(observations,
              ::testing::Each(FloatEq(observations.front(), 1e-10)));
}

TEST(Osborn_Cooling_Invariance_Test, Random_Unitary_Baryon_Number_Density)
{
  namespace representation = model::cartesian;

  using core_type = model::cooling::core::Cooling_Core<
      unit_test::osborn::Mock_Proxy<unit_test::osborn::Mock_Index_Alg>,
      unit_test::osborn::Mock_Proxy<unit_test::osborn::Mock_Index_Converter>,
      unit_test::osborn::Mock_Proxy<
          unit_test::osborn::Mock_Optimise_Preset_Alg>>;

  using step_type =
      model::cooling::core::cartesian::General_Cartesian_Cooling_Step<
          core_type>;

  std::random_device rd{};
  std::mt19937 rng{rd()};
  std::uniform_real_distribution<> uniform_dist{-1., 1.};

  std::size_t updates = 100;

  auto mu = complex_type{uniform_dist(rng), 0.};
  auto mass = complex_type{uniform_dist(rng), 0.};

  auto a_mat = rmt::matrix::Random_Matrix(uniform_dist, rng);
  auto b_mat = rmt::matrix::Random_Matrix(uniform_dist, rng);
  auto al_mat = rmt::matrix::Random_Matrix(uniform_dist, rng);
  auto bt_mat = rmt::matrix::Random_Matrix(uniform_dist, rng);

  auto data = std::make_shared<representation::Matrix_Storage>();

  data->set_mu(mu);
  data->set_mass(mass);

  data->read_write(representation::M_name::A) = a_mat;
  data->read_write(representation::M_name::B) = b_mat;
  data->read_write(representation::M_name::ALPH) = al_mat;
  data->read_write(representation::M_name::BETA) = bt_mat;

  data->update();

  auto observable =
      model::observable::Baryon_Number_Density{data, {mu, mass, 1}};
  observable.Calculate();

  unit_test::osborn::Mock_Index_Alg idx_alg;
  unit_test::osborn::Mock_Index_Converter idx_conv;
  unit_test::osborn::Mock_Optimise_Preset_Alg opt_alg{};

  EXPECT_CALL(idx_alg, idx_alg(::testing::_)).Times(updates);
  EXPECT_CALL(idx_conv, conv_alg(::testing::_)).Times(updates);
  EXPECT_CALL(opt_alg, optimise(::testing::_, ::testing::_)).Times(updates);

  core_type core{{idx_alg}, {idx_conv}, {opt_alg}};
  step_type step{data, core};

  std::uniform_real_distribution<> small_dist{-1e-3, 1e-3};

  for (auto i = 0u; i < updates; ++i) {
    auto g_indices = random_vector(M_size * M_size, small_dist, rng);
    auto h_indices = random_vector(M_size * M_size, small_dist, rng);

    auto trafos = exponensiate_indices({g_indices, h_indices});

    opt_alg.replace_matrices({trafos.g, trafos.g_inv, trafos.h, trafos.h_inv});

    auto phi_new = static_cast<M_type>(
        trafos.g * data->read(model::M_name::PHI) * trafos.h_inv);
    auto phi_adj_new = static_cast<M_type>(
        trafos.h * data->read(model::M_name::PHIADJ) * trafos.g_inv);
    auto psi_new = static_cast<M_type>(
        trafos.g * data->read(model::M_name::PSI) * trafos.h_inv);
    auto psi_adj_new = static_cast<M_type>(
        trafos.h * data->read(model::M_name::PSIADJ) * trafos.g_inv);

    step.Update();

    EXPECT_THAT(data->read(model::M_name::PHI), MatrixApprox(phi_new, 1e-12));
    EXPECT_THAT(data->read(model::M_name::PHIADJ),
                MatrixApprox(phi_adj_new, 1e-12));
    EXPECT_THAT(data->read(model::M_name::PSI), MatrixApprox(psi_new, 1e-12));
    EXPECT_THAT(data->read(model::M_name::PSIADJ),
                MatrixApprox(psi_adj_new, 1e-12));

    observable.Calculate();
  }

  auto observations = observable.data();

  ASSERT_THAT(observations.size(), ::testing::Eq(updates + 1u));
  EXPECT_THAT(observations,
              ::testing::Each(FloatEq(observations.front(), 1e-10)));
}
