/*
 * Created: 14-11-2016
 * Modified: Thu 16 Feb 2017 12:45:30 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <algorithm>
#include <gtest/gtest.h>
#include <osborn/cooling/core/index_computation_algorithms.hpp>
#include <random>
#include <utilities/matrix/default_constructor.hpp>
#include <utilities/matrix/identity_constructor.hpp>

#include "../mocks/mock_norm.hpp"
#include "../mocks/mock_proxy.hpp"
#include "../mocks/norm_functions.hpp"

#include "../../../matchers.hpp"

using rmt::M_type;
using n = rmt::numeric::number_type;
using rmt::numeric::complex_type;
using unit_test::osborn::Mock_Norm;
using norm_arg_type = Mock_Norm::arg_type;

using unit_test::MatrixApprox;
using unit_test::FloatEq;

namespace model = rmt::osborn;

namespace {

void Normalise(std::vector<n> &vec)
{
  auto norm = n{0.};
  for (auto v : vec)
    norm += v * v;

  norm = rmt::numeric::sqrt(norm);

  for (auto &v : vec)
    v /= norm;
}
}

TEST(Osborn_Numerical_Derivative_Index_Computation_Algorithm_Test,
     Constant_Norm)
{
  auto elem_x = rmt::matrix::Identity_Matrix();
  auto elem_y = rmt::matrix::Identity_Matrix();

  auto mock_norm = std::make_shared<Mock_Norm>(n{1.}, false);
  model::cooling::core::Numerical_Derivative_Index_Computation_Algorithm alg{
      mock_norm, n{1.}};

  EXPECT_CALL(*mock_norm, compute(::testing::_)).Times(1 + 2 * M_size * M_size);

  EXPECT_CALL(*mock_norm, needs_phi_psi()).Times(1);

  auto norm_args =
      norm_arg_type{elem_x, elem_y, elem_x, elem_y, elem_x, elem_y, {}, {}};

  auto indices = alg(norm_args);

  for (auto gi : indices.g_indices)
    EXPECT_THAT(gi, FloatEq(n{0.}));

  for (auto hi : indices.h_indices)
    EXPECT_THAT(hi, FloatEq(n{0.}));
};

TEST(Osborn_Numerical_Derivative_Index_Computation_Algorithm_Test,
     Counting_Norm)
{
  auto elem_x = rmt::matrix::Identity_Matrix();
  auto elem_xc = rmt::matrix::Identity_Matrix();

  auto mock_norm = std::make_shared<Mock_Norm>([](const norm_arg_type &) {
    static int it = 0;
    return static_cast<n>(it++);
  });

  model::cooling::core::Numerical_Derivative_Index_Computation_Algorithm alg{
      mock_norm, n{1.}};

  EXPECT_CALL(*mock_norm, compute(::testing::_)).Times(1 + 2 * M_size * M_size);

  EXPECT_CALL(*mock_norm, needs_phi_psi()).Times(1);

  auto norm_args =
      norm_arg_type{elem_x, elem_xc, elem_x, elem_xc, elem_x, elem_xc, {}, {}};
  auto indices = alg(norm_args);

  auto i = 1.0;
  for (auto gi : indices.g_indices) {
    EXPECT_THAT(gi, FloatEq(n{i}));
    i += 2.;
  }

  i = 2.0;
  for (auto hi : indices.h_indices) {
    EXPECT_THAT(hi, FloatEq(n{i}));
    i += 2.;
  }
};

TEST(Osborn_Normalised_Numerical_Derivative_Index_Computation_Algorithm_Test,
     Random_Norm)
{
  auto elem_x = rmt::matrix::Identity_Matrix();
  auto elem_xc = rmt::matrix::Identity_Matrix();

  std::random_device rand_device{};
  std::mt19937 rng{rand_device()};
  std::uniform_real_distribution<double> dist{-100, 100};

  auto norms = std::vector<n>(2 * M_size * M_size);
  std::generate(norms.begin(), norms.end(),
                [&rng, &dist]() { return dist(rng); });

  unit_test::Non_Identity_Vector_Norm norm_func{norms};

  auto mock_norm = std::make_shared<Mock_Norm>(norm_func, false);

  model::cooling::core::
      Normalised_Numerical_Derivative_Index_Computation_Algorithm alg{mock_norm,
                                                                      n{1.}};

  EXPECT_CALL(*mock_norm, compute(::testing::_)).Times(1 + 2 * M_size * M_size);

  EXPECT_CALL(*mock_norm, needs_phi_psi()).Times(1);

  auto norm_args =
      norm_arg_type{elem_x, elem_xc, elem_x, elem_xc, elem_x, elem_xc, {}, {}};

  auto indices = alg(norm_args);

  auto expected_g_ind = std::vector<n>(M_size * M_size);
  auto expected_h_ind = std::vector<n>(M_size * M_size);

  for (auto pos = norms.begin(), g_pos = expected_g_ind.begin(),
            h_pos = expected_h_ind.begin();
       pos != norms.end(); ++pos, ++g_pos, ++h_pos) {
    *g_pos = *(pos++);
    *h_pos = *pos;
  }

  Normalise(expected_g_ind);
  Normalise(expected_h_ind);

  ASSERT_EQ(indices.g_indices.size(), expected_g_ind.size());

  auto pos = expected_g_ind.cbegin();
  for (auto g : indices.g_indices)
    EXPECT_EQ(g, *(pos++));

  ASSERT_EQ(indices.g_indices.size(), expected_h_ind.size());

  pos = expected_h_ind.cbegin();
  for (auto h : indices.h_indices)
    EXPECT_EQ(h, *(pos++));
}
