
/*
 * Created: 14-11-2016
 * Modified: Tue 14 Feb 2017 13:22:37 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <algorithm>
#include <gtest/gtest.h>
#include <osborn/cooling/core/index_conversion_algorithm.hpp>
#include <random>
#include <utilities/matrix/default_constructor.hpp>

#include "../../../matchers.hpp"

using rmt::M_type;
using n = rmt::numeric::number_type;
using rmt::numeric::complex_type;

using unit_test::MatrixApprox;
using unit_test::FloatEq;

namespace model = rmt::osborn;

TEST(Osborn_Direct_Index_Conversion_Algorithm_Test, Idetity_Vector)
{
  using rmt::numeric::sqrt;
  auto alg = model::cooling::core::Direct_Index_Conversion_Algorithm{};

  auto expected_mat = rmt::matrix::Zero_Matrix();

  expected_mat(0, 0) = sqrt(n{2.}) * complex_type{1.}; // U(1) part

  for (auto i = 1u; i < M_size; ++i) { // SU(3) part
    auto d = static_cast<n>(i + 1);
    auto f = sqrt(n{2.} / (d * (d - n{1.})));

    for (auto j = 0u; j < i; ++j)
      expected_mat(j, j) += f;

    expected_mat(i, i) += f * (n{1.} - d);
  }

  for (auto i = 0u; i < M_size; ++i) {
    for (auto j = i + 1; j < M_size; ++j) {
      expected_mat(i, j) = complex_type{1., -1.};
      expected_mat(j, i) = complex_type{1., 1.};
    }
  }

  auto indices = rmt::osborn::cooling::core::Transformation_Indices{
      std::vector<n>(M_size * M_size, 1.), std::vector<n>(M_size * M_size, 1.)};

  auto comp_mats = alg(indices);

  EXPECT_THAT(comp_mats.g_gen, MatrixApprox(expected_mat));
  EXPECT_THAT(comp_mats.h_gen, MatrixApprox(expected_mat));
}

TEST(Osborn_Direct_Index_Conversion_Algorithm_Test, Random_Vector)
{
  using rmt::numeric::sqrt;
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dist{0., 1.};

  auto alg = model::cooling::core::Direct_Index_Conversion_Algorithm{};

  auto g_indices = std::vector<n>(M_size * M_size);
  std::generate(g_indices.begin(), g_indices.end(),
                [&gen, &dist]() { return n{dist(gen)}; });

  auto h_indices = std::vector<n>(M_size * M_size);
  std::generate(h_indices.begin(), h_indices.end(),
                [&gen, &dist]() { return n{dist(gen)}; });

  auto expected_g_mat = rmt::matrix::Zero_Matrix();
  auto expected_h_mat = rmt::matrix::Zero_Matrix();

  auto git = g_indices.begin();
  auto hit = h_indices.begin();

  expected_g_mat(0, 0) = sqrt(n{2.}) * (*(git++));
  expected_h_mat(0, 0) = sqrt(n{2.}) * (*(hit++));

  for (auto i = 1u; i < M_size; ++i) {
    auto d = static_cast<n>(i + 1);
    auto f = sqrt(n{2.} / (d * (d - n{1.})));

    auto gv = *(git++);
    auto hv = *(hit++);
    for (auto j = 0u; j < i; ++j) {
      expected_g_mat(j, j) += f * gv;
      expected_h_mat(j, j) += f * hv;
    }

    expected_g_mat(i, i) += f * (n{1.} - d) * gv;
    expected_h_mat(i, i) += f * (n{1.} - d) * hv;
  }

  for (auto i = 0u; i < M_size; ++i) {
    for (auto j = i + 1; j < M_size; ++j) {
      auto gvr = *(git++), gvi = *(git++);
      auto hvr = *(hit++), hvi = *(hit++);
      expected_g_mat(i, j) = complex_type{gvr, -gvi};
      expected_g_mat(j, i) = complex_type{gvr, gvi};
      expected_h_mat(i, j) = complex_type{hvr, -hvi};
      expected_h_mat(j, i) = complex_type{hvr, hvi};
    }
  }

  auto indices =
      rmt::osborn::cooling::core::Transformation_Indices{g_indices, h_indices};

  auto comp_mat = alg(indices);

  EXPECT_THAT(comp_mat.g_gen, MatrixApprox(expected_g_mat));
  EXPECT_THAT(comp_mat.h_gen, MatrixApprox(expected_h_mat));
}
