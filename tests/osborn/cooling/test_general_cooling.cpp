/*
 * Created: 06-06-2016
 * Modified: Tue 14 Feb 2017 15:58:20 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <osborn/cooling/general_cooling.hpp>
#include <utilities/matrix/identity_constructor.hpp>
#include <utilities/matrix/random_constructor.hpp>

#include "../../matchers.hpp"
#include "mocks/mock_cooling_helper_algs.hpp"
#include "mocks/mock_proxy.hpp"

using rmt::M_type;
using rmt::numeric::number_type;
using rmt::numeric::complex_type;
using rmt::numeric::imag_i;

namespace model = rmt::osborn;
using norm_arg_type = unit_test::osborn::Mock_Index_Alg::arg_type;

using unit_test::MatrixApprox;

TEST(Osborn_Cooling_Core_Test, Simple_Alg)
{
  using core_type = model::cooling::core::Cooling_Core<
      unit_test::osborn::Mock_Proxy<unit_test::osborn::Mock_Index_Alg>,
      unit_test::osborn::Mock_Proxy<unit_test::osborn::Mock_Index_Converter>,
      unit_test::osborn::Mock_Proxy<unit_test::osborn::Mock_Optimise_Alg>>;

  auto storage = std::make_shared<model::cartesian::Matrix_Storage>();

  auto norm_args = norm_arg_type{storage->read(model::M_name::X),
                                 storage->read(model::M_name::Y),
                                 storage->read(model::M_name::PHI),
                                 storage->read(model::M_name::PHIADJ),
                                 storage->read(model::M_name::PSI),
                                 storage->read(model::M_name::PSIADJ),
                                 storage->read_mu(),
                                 storage->read_mass()};

  unit_test::osborn::Mock_Index_Alg idx_alg;
  unit_test::osborn::Mock_Index_Converter idx_conv;
  unit_test::osborn::Mock_Optimise_Alg opt_alg;

  EXPECT_CALL(idx_alg, idx_alg(norm_args)).Times(1);
  EXPECT_CALL(idx_conv, conv_alg(::testing::_)).Times(1);
  EXPECT_CALL(opt_alg, optimise(norm_args, ::testing::_)).Times(1);

  core_type core{{idx_alg}, {idx_conv}, {opt_alg}};

  auto trafo = core.get_transformation(storage);
  auto identity_matrix = rmt::matrix::Identity_Matrix();

  EXPECT_THAT(trafo.g, MatrixApprox(identity_matrix, 1e-12));
  EXPECT_THAT(trafo.g_inv, MatrixApprox(identity_matrix, 1e-12));
  EXPECT_THAT(trafo.h, MatrixApprox(identity_matrix, 1e-12));
  EXPECT_THAT(trafo.h_inv, MatrixApprox(identity_matrix, 1e-12));
}

TEST(Osborn_General_Cartesian_Cooling_Step_Test, Random_Matrix)
{
  using rmt::numeric::cosh;
  using rmt::numeric::sinh;

  using core_type = model::cooling::core::Cooling_Core<
      unit_test::osborn::Mock_Proxy<unit_test::osborn::Mock_Index_Alg>,
      unit_test::osborn::Mock_Proxy<unit_test::osborn::Mock_Index_Converter>,
      unit_test::osborn::Mock_Proxy<
          unit_test::osborn::Mock_Optimise_Preset_Alg>>;

  using step_type =
      model::cooling::core::cartesian::General_Cartesian_Cooling_Step<
          core_type>;

  auto storage = std::make_shared<model::cartesian::Matrix_Storage>();

  storage->read_write(model::cartesian::M_name::A) =
      rmt::matrix::Random_Matrix();
  storage->read_write(model::cartesian::M_name::B) =
      rmt::matrix::Random_Matrix();

  storage->read_write(model::cartesian::M_name::ALPH) =
      rmt::matrix::Random_Matrix();
  storage->read_write(model::cartesian::M_name::BETA) =
      rmt::matrix::Random_Matrix();

  auto mu = number_type{1.};
  storage->set_mu(mu);

  auto a_cpy = storage->read(model::cartesian::M_name::A);
  auto b_cpy = storage->read(model::cartesian::M_name::B);

  auto al_cpy = storage->read(model::cartesian::M_name::ALPH);
  auto bt_cpy = storage->read(model::cartesian::M_name::BETA);

  storage->update();

  auto norm_args = norm_arg_type{storage->read(model::M_name::X),
                                 storage->read(model::M_name::Y),
                                 storage->read(model::M_name::PHI),
                                 storage->read(model::M_name::PHIADJ),
                                 storage->read(model::M_name::PSI),
                                 storage->read(model::M_name::PSIADJ),
                                 storage->read_mu(),
                                 storage->read_mass()};

  auto g_mat = rmt::matrix::Random_Matrix();
  auto gi_mat = static_cast<M_type>(blaze::inv(g_mat));
  auto h_mat = rmt::matrix::Random_Matrix();
  auto hi_mat = static_cast<M_type>(blaze::inv(h_mat));

  unit_test::osborn::Mock_Index_Alg idx_alg;
  unit_test::osborn::Mock_Index_Converter idx_conv;
  unit_test::osborn::Mock_Optimise_Preset_Alg opt_alg{
      {g_mat, gi_mat, h_mat, hi_mat}};

  EXPECT_CALL(idx_alg, idx_alg(norm_args)).Times(1);
  EXPECT_CALL(idx_conv, conv_alg(::testing::_)).Times(1);
  EXPECT_CALL(opt_alg, optimise(norm_args, ::testing::_)).Times(1);

  core_type core{{idx_alg}, {idx_conv}, {opt_alg}};
  step_type step{storage, core};

  step.Update();

  auto c_mu = cosh(mu), s_mu = sinh(mu);

  auto x = static_cast<M_type>(imag_i * c_mu * (a_cpy + imag_i * b_cpy) +
                               s_mu * (al_cpy + imag_i * bt_cpy));
  auto y = static_cast<M_type>(
      imag_i * c_mu * (blaze::trans(a_cpy) - imag_i * blaze::trans(b_cpy)) +
      s_mu * (blaze::trans(al_cpy) - imag_i * blaze::trans(bt_cpy)));

  auto x_exp = static_cast<M_type>(g_mat * x * hi_mat);
  auto y_exp = static_cast<M_type>(h_mat * y * gi_mat);

  EXPECT_THAT(storage->read(model::M_name::X), MatrixApprox(x_exp, 1e-12));
  EXPECT_THAT(storage->read(model::M_name::Y), MatrixApprox(y_exp, 1e-12));

  auto a_exp = static_cast<M_type>(
      number_type{0.5} *
      (g_mat * a_cpy * hi_mat +
       blaze::trans(gi_mat) * a_cpy * blaze::trans(h_mat) +
       imag_i * (g_mat * b_cpy * hi_mat -
                 blaze::trans(gi_mat) * b_cpy * blaze::trans(h_mat))));

  auto b_exp = static_cast<M_type>(
      number_type{0.5} *
      (g_mat * b_cpy * hi_mat +
       blaze::trans(gi_mat) * b_cpy * blaze::trans(h_mat) -
       imag_i * (g_mat * a_cpy * hi_mat -
                 blaze::trans(gi_mat) * a_cpy * blaze::trans(h_mat))));

  EXPECT_THAT(storage->read(model::cartesian::M_name::A),
              MatrixApprox(a_exp, 1e-12));
  EXPECT_THAT(storage->read(model::cartesian::M_name::B),
              MatrixApprox(b_exp, 1e-12));

  auto al_exp = static_cast<M_type>(
      number_type{0.5} *
      (g_mat * al_cpy * hi_mat +
       blaze::trans(gi_mat) * al_cpy * blaze::trans(h_mat) +
       imag_i * (g_mat * bt_cpy * hi_mat -
                 blaze::trans(gi_mat) * bt_cpy * blaze::trans(h_mat))));

  auto bt_exp = static_cast<M_type>(
      number_type{0.5} *
      (g_mat * bt_cpy * hi_mat +
       blaze::trans(gi_mat) * bt_cpy * blaze::trans(h_mat) -
       imag_i * (g_mat * al_cpy * hi_mat -
                 blaze::trans(gi_mat) * al_cpy * blaze::trans(h_mat))));

  EXPECT_THAT(storage->read(model::cartesian::M_name::ALPH),
              MatrixApprox(al_exp, 1e-12));
  EXPECT_THAT(storage->read(model::cartesian::M_name::BETA),
              MatrixApprox(bt_exp, 1e-12));
}
