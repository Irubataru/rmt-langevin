/*
 * Created: 15-11-2016
 * Modified: Thu 16 Feb 2017 12:19:43 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef TESTING_OSBORN_NORM_FUNCTIONS_HPP
#define TESTING_OSBORN_NORM_FUNCTIONS_HPP

#include <osborn/cooling/cooling_types.hpp>
#include <utility>
#include <vector>

namespace unit_test {

struct Non_Identity_Vector_Norm
{
public:
  using number_type = rmt::numeric::number_type;

  Non_Identity_Vector_Norm() = default;
  Non_Identity_Vector_Norm(Non_Identity_Vector_Norm &&) = default;
  Non_Identity_Vector_Norm(const Non_Identity_Vector_Norm &) = default;

  Non_Identity_Vector_Norm(const std::vector<number_type> &v) : vec{v}
  {
    pos = vec.begin();
  }

  Non_Identity_Vector_Norm(std::vector<number_type> &&v) : vec{v}
  {
    pos = vec.begin();
  }

  void set_vector(std::vector<number_type> &&v)
  {
    vec = std::forward<std::vector<number_type>>(v);
    pos = vec.begin();
  }

  const std::vector<number_type> &get_vector() const
  {
    return vec;
  }

  number_type operator()(const rmt::osborn::cooling::norm::Norm_Arguments &args)
  {
    if (blaze::isIdentity(args.X) and blaze::isIdentity(args.Y))
      return 0;

    auto val = *pos;
    ++pos;

    return val;
  }

private:
  std::vector<number_type> vec;
  std::vector<number_type>::const_iterator pos;
};

} // namespace unit_test

#endif /* TESTING_OSBORN_NORM_FUNCTIONS_HPP */
