/*
 * Created: 06-06-2016
 * Modified: Thu 16 Feb 2017 11:52:20 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef MOCK_OSBORN_COOLING_HELPER_ALGS_HPP
#define MOCK_OSBORN_COOLING_HELPER_ALGS_HPP

#include <functional>
#include <gmock/gmock.h>
#include <osborn/cooling/cooling_types.hpp>
#include <utilities/matrix/default_constructor.hpp>
#include <utilities/matrix/identity_constructor.hpp>

namespace unit_test {

namespace osborn {

class Mock_Index_Alg
{
public:
  using arg_type = rmt::osborn::cooling::norm::Norm_Arguments;
  using return_type = rmt::osborn::cooling::core::Transformation_Indices;

  return_type operator()(const arg_type &arg)
  {
    using rmt::numeric::number_type;

    idx_alg(arg);
    return return_type{
        std::vector<number_type>(M_size * M_size - 1, number_type{0.}),
        std::vector<number_type>(M_size * M_size - 1, number_type{0.})};
  }

  MOCK_METHOD1(idx_alg, void(const arg_type &));
};

class Mock_Index_Converter
{
public:
  using arg_type = rmt::osborn::cooling::core::Transformation_Indices;
  using return_type = rmt::osborn::cooling::core::Generator_Matrices;

  return_type operator()(const arg_type &v)
  {
    conv_alg(v);
    return return_type{rmt::matrix::Zero_Matrix(), rmt::matrix::Zero_Matrix()};
  }

  MOCK_METHOD1(conv_alg, void(const arg_type &));
};

class Mock_Optimise_Alg
{
public:
  using arg1_type = rmt::osborn::cooling::norm::Norm_Arguments;
  using arg2_type = rmt::osborn::cooling::core::Generator_Matrices;
  using return_type = rmt::osborn::cooling::core::Transformation_Matrices;

  return_type operator()(const arg1_type &args, const arg2_type &g)
  {
    optimise(args, g);
    return return_type{
        rmt::matrix::Identity_Matrix(), rmt::matrix::Identity_Matrix(),
        rmt::matrix::Identity_Matrix(), rmt::matrix::Identity_Matrix()};
  }

  MOCK_METHOD2(optimise, void(const arg1_type &, const arg2_type &));
};

class Mock_Optimise_Preset_Alg
{
public:
  using arg1_type = rmt::osborn::cooling::norm::Norm_Arguments;
  using arg2_type = rmt::osborn::cooling::core::Generator_Matrices;
  using return_type = rmt::osborn::cooling::core::Transformation_Matrices;

  Mock_Optimise_Preset_Alg() = default;

  Mock_Optimise_Preset_Alg(const return_type &ret) : preset{ret}
  {
  }

  void replace_matrices(const return_type &new_preset)
  {
    preset = new_preset;
  }

  return_type operator()(const arg1_type &args, const arg2_type &gens)
  {
    optimise(args, gens);
    return preset;
  }

  MOCK_METHOD2(optimise, void(const arg1_type &, const arg2_type &));

private:
  return_type preset;
};

} // namespace osborn

} // namespace unit_test

namespace rmt {
namespace osborn {
namespace cooling {
namespace norm {

inline bool operator==(const Norm_Arguments &lhs, const Norm_Arguments &rhs)
{
  return (&(lhs.X) == &(rhs.X)) and (&(lhs.Y) == &(rhs.Y)) and
         (&(lhs.mu) == &(rhs.mu)) and (&lhs.mass) == &(rhs.mass);
};

} // namespace norm
} // namespace cooling
} // namespace osborn
} // namespace rmt

#endif /* MOCK_OSBORN_COOLING_HELPER_ALGS_HPP */
