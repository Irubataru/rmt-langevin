/*
 * Created: 03-07-2016
 * TESTING_OSBORN_Modified: Thu 16 Feb 2017 12:11:43 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef TESTING_OSBORN_MOCK_PROXY_HPP
#define TESTING_OSBORN_MOCK_PROXY_HPP

#include <utility>

namespace unit_test {
namespace osborn {

template <typename Mock>
class Mock_Proxy
{
public:
  Mock_Proxy(Mock &m) : proxy(m)
  {
  }

private:
  Mock &proxy;

public:
  template <typename... Args>
  auto operator()(Args &&... args)
      -> decltype(this->proxy(std::forward<Args>(args)...))
  {
    return proxy(std::forward<Args>(args)...);
  }
};

} // namespace osborn
} // namespace unit_test

#endif /* TESTING_OSBORN_MOCK_PROXY_HPP */
