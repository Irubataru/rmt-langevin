
/*
 * Created: 06-06-2016
 * Modified: Thu 16 Feb 2017 12:53:35 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef TESTING_OSBORN_MOCK_NORM_HPP
#define TESTING_OSBORN_MOCK_NORM_HPP

#include <functional>
#include <gmock/gmock.h>
#include <osborn/cooling/norms/norm_interface.hpp>

namespace unit_test {
namespace osborn {

class Mock_Norm : public rmt::osborn::cooling::norm::Norm
{
public:
  using number_type = rmt::osborn::cooling::norm::Norm::number_type;
  using arg_type = rmt::osborn::cooling::norm::Norm::arg_type;

  template <typename Function>
  explicit Mock_Norm(Function &&f, bool reqs = false)
      : norm_func{std::forward<Function>(f)}, req_phi_psi{reqs}
  {
    Delegate_Return_Values();
  }

  explicit Mock_Norm(number_type def_val, bool reqs = false)
      : Mock_Norm{[def_val](const arg_type &) { return def_val; }, reqs}
  {
  }

  MOCK_METHOD1(compute, number_type(const arg_type &));
  MOCK_CONST_METHOD0(needs_phi_psi, bool());

  number_type forward_to_functor(const arg_type &args)
  {
    return norm_func(args);
  }

private:
  void Delegate_Return_Values()
  {
    ON_CALL(*this, compute(::testing::_))
        .WillByDefault(testing::Invoke(this, &Mock_Norm::forward_to_functor));

    ON_CALL(*this, needs_phi_psi()).WillByDefault(testing::Return(req_phi_psi));
  }

  std::function<number_type(const arg_type &)> norm_func;
  bool req_phi_psi;
};

} // namespace osborn

} // namespace unit_test

#endif /* TESTING_OSBORN_MOCK_NORM_HPP */
