/*
 * Created: 08-05-2016
 * Modified: Wed 09 Nov 2016 15:02:00 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <osborn/resource/initialiser.hpp>
#include <osborn/resource/resource.hpp>
#include <utilities/matrix/identity_constructor.hpp>

#include "../../matchers.hpp"

using rmt::M_type;
using n = rmt::numeric::number_type;
using rmt::numeric::complex_type;
using rmt::numeric::imag_i;

using unit_test::MatrixApprox;

namespace model = rmt::osborn;
namespace storage = model::cartesian;

using base_name = model::M_name;
using strg_name = storage::M_name;

TEST(Osborn_Resource_Cartesian, Zero_Matrices)
{
  auto par = rmt::Physical_Parameters{{1.1, 0.2},  // chemical_potential
                                      {-2.1, 4.2}, // fermion_mass
                                      11};         // number_of_flavours

  storage::Matrix_Storage data{};

  auto initialise_to_zero = [](M_type &m1, M_type &m2, M_type &m3, M_type &m4) {
    m1 = rmt::matrix::Zero_Matrix();
    m2 = rmt::matrix::Zero_Matrix();
    m3 = rmt::matrix::Zero_Matrix();
    m4 = rmt::matrix::Zero_Matrix();
  };

  EXPECT_EQ(0ul, data.get_storage_revision());

  storage::Initialise(data, par, initialise_to_zero);
  EXPECT_EQ(1ul, data.get_storage_revision());

  auto mf = par.fermion_mass;

  auto zero_matrix = rmt::matrix::Zero_Matrix();
  EXPECT_THAT(data.read(strg_name::A), MatrixApprox(zero_matrix));
  EXPECT_THAT(data.read(strg_name::B), MatrixApprox(zero_matrix));
  EXPECT_THAT(data.read(strg_name::ALPH), MatrixApprox(zero_matrix));
  EXPECT_THAT(data.read(strg_name::BETA), MatrixApprox(zero_matrix));
  EXPECT_THAT(data.read(base_name::X), MatrixApprox(zero_matrix));
  EXPECT_THAT(data.read(base_name::Y), MatrixApprox(zero_matrix));
  EXPECT_THAT(data.read(base_name::XY), MatrixApprox(zero_matrix));
  EXPECT_THAT(data.read(base_name::R), MatrixApprox(zero_matrix));
  EXPECT_THAT(data.read(base_name::T), MatrixApprox(zero_matrix));

  auto expected_g_matrix = rmt::matrix::Identity_Matrix(n{1.} / (mf * mf));
  auto expected_m_matrix = rmt::matrix::Identity_Matrix(mf * mf);

  EXPECT_THAT(data.read(base_name::G), MatrixApprox(expected_g_matrix, 1e-12));
  EXPECT_THAT(data.read(base_name::M), MatrixApprox(expected_m_matrix, 1e-12));
}

TEST(Osborn_Resource_Cartesian, Identity_Matrices)
{
  using rmt::numeric::cosh;
  using rmt::numeric::sinh;

  auto par = rmt::Physical_Parameters{{1.1, 0.2},  // chemical_potential
                                      {-2.1, 4.2}, // fermion_mass
                                      11};         // number_of_flavours

  storage::Matrix_Storage data{};

  auto initialise_to_one = [](M_type &m1, M_type &m2, M_type &m3, M_type &m4) {
    m1 = rmt::matrix::Identity_Matrix();
    m2 = rmt::matrix::Identity_Matrix();
    m3 = rmt::matrix::Identity_Matrix();
    m4 = rmt::matrix::Identity_Matrix();
  };

  EXPECT_EQ(0ul, data.get_storage_revision());

  storage::Initialise(data, par, initialise_to_one);
  EXPECT_EQ(1ul, data.get_storage_revision());

  auto mf = par.fermion_mass;
  auto c_mu = cosh(par.chemical_potential), s_mu = sinh(par.chemical_potential);
  auto phi = complex_type{1., 1.};

  auto eye_matrix = rmt::matrix::Identity_Matrix(n{1.});
  auto expected_x_matrix =
      rmt::matrix::Identity_Matrix((imag_i * c_mu + s_mu) * phi);
  auto expected_y_matrix =
      rmt::matrix::Identity_Matrix((imag_i * c_mu + s_mu) * std::conj(phi));
  auto expected_xy_matrix = rmt::matrix::Identity_Matrix(
      n{2.} * (n{2.} * imag_i * c_mu * s_mu - n{1.}));
  auto expected_m_matrix = rmt::matrix::Identity_Matrix(mf * mf);
  auto expected_g_matrix = rmt::matrix::Identity_Matrix(
      n{1.} / (mf * mf - n{2.} * (n{2.} * imag_i * c_mu * s_mu - n{1.})));
  auto expected_r_matrix = rmt::matrix::Identity_Matrix(
      (imag_i * c_mu + s_mu) * std::conj(phi) /
      (mf * mf - n{2.} * (n{2.} * imag_i * c_mu * s_mu - n{1.})));
  auto expected_t_matrix = rmt::matrix::Identity_Matrix(
      (imag_i * c_mu + s_mu) * phi /
      (mf * mf - n{2.} * (n{2.} * imag_i * c_mu * s_mu - n{1.})));

  EXPECT_THAT(data.read(strg_name::A), MatrixApprox(eye_matrix));
  EXPECT_THAT(data.read(strg_name::B), MatrixApprox(eye_matrix));
  EXPECT_THAT(data.read(strg_name::ALPH), MatrixApprox(eye_matrix));
  EXPECT_THAT(data.read(strg_name::BETA), MatrixApprox(eye_matrix));
  EXPECT_THAT(data.read(base_name::X), MatrixApprox(expected_x_matrix));
  EXPECT_THAT(data.read(base_name::Y), MatrixApprox(expected_y_matrix));
  EXPECT_THAT(data.read(base_name::XY), MatrixApprox(expected_xy_matrix));
  EXPECT_THAT(data.read(base_name::M), MatrixApprox(expected_m_matrix));
  EXPECT_THAT(data.read(base_name::G), MatrixApprox(expected_g_matrix));
  EXPECT_THAT(data.read(base_name::R), MatrixApprox(expected_r_matrix));
  EXPECT_THAT(data.read(base_name::T), MatrixApprox(expected_t_matrix));
}
