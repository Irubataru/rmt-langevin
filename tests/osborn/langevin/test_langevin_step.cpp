
/*
 * Created: 08-05-2016
 * Modified: Tue 14 Feb 2017 13:20:52 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <osborn/langevin/langevin_step.hpp>
#include <osborn/resource/initialiser.hpp>
#include <utilities/matrix/identity_constructor.hpp>

#include "../../matchers.hpp"
#include "mocks/mock_force.hpp"

using rmt::M_type;
using n = rmt::numeric::number_type;
using rmt::numeric::complex_type;

using unit_test::MatrixApprox;

namespace model = rmt::osborn;

TEST(Osborn_Langevin_Step, Cartesian_Step)
{
  namespace storage = model::cartesian;
  using name_tag = storage::M_name;
  using rmt::numeric::sqrt;

  auto force_strength = complex_type{.5, .5};

  auto data = std::make_shared<storage::Matrix_Storage>();
  auto force = std::make_shared<unit_test::osborn::Mock_Cartesian_Force>(
      data, force_strength);

  auto initialise_to_zero = [](M_type &m1, M_type &m2, M_type &m3, M_type &m4) {
    m1 = rmt::matrix::Zero_Matrix();
    m2 = rmt::matrix::Zero_Matrix();
    m3 = rmt::matrix::Zero_Matrix();
    m4 = rmt::matrix::Zero_Matrix();
  };

  storage::Initialise(*data, {n{1.}, n{1.}, 1ul}, initialise_to_zero);

  auto stepsize_ptr = std::make_shared<rmt::stepsize::Stepsize>(n{.55});
  auto mock_noise = std::make_unique<unit_test::osborn::Mock_Noise>(n{.75});

  EXPECT_CALL(*mock_noise, noise()).Times(2 * 2 * M_size * M_size);
  EXPECT_CALL(*mock_noise, set_std_dev(sqrt(2. * stepsize_ptr->value)))
      .Times(1);

  storage::Langevin_Step step{data, force, stepsize_ptr, std::move(mock_noise)};

  EXPECT_CALL(*force, update()).Times(1);
  EXPECT_CALL(*force, get_force_a());
  EXPECT_CALL(*force, get_force_b());
  EXPECT_CALL(*force, get_force_alpha());
  EXPECT_CALL(*force, get_force_beta());

  step.Update();

  auto noise_matrix = rmt::matrix::Matrix(n{.75});

  M_type expected_a_matrix =
      (stepsize_ptr->value) * force_strength * rmt::matrix::Identity_Matrix() +
      noise_matrix;
  M_type expected_b_matrix =
      (stepsize_ptr->value) * force_strength * rmt::matrix::Identity_Matrix() +
      noise_matrix;
  M_type expected_al_matrix =
      (stepsize_ptr->value) * force_strength * rmt::matrix::Identity_Matrix() +
      noise_matrix;
  M_type expected_bt_matrix =
      (stepsize_ptr->value) * force_strength * rmt::matrix::Identity_Matrix() +
      noise_matrix;

  EXPECT_THAT(data->read(name_tag::A), MatrixApprox(expected_a_matrix));
  EXPECT_THAT(data->read(name_tag::B), MatrixApprox(expected_b_matrix));
  EXPECT_THAT(data->read(name_tag::ALPH), MatrixApprox(expected_al_matrix));
  EXPECT_THAT(data->read(name_tag::BETA), MatrixApprox(expected_bt_matrix));
}

TEST(Osborn_Langevin_Step, Cartesian_Step_W_Stepsize_Update)
{
  namespace storage = model::cartesian;
  using name_tag = storage::M_name;
  using rmt::numeric::sqrt;

  auto data = std::make_shared<storage::Matrix_Storage>();

  auto initialise_to_zero = [](M_type &m1, M_type &m2, M_type &m3, M_type &m4) {
    m1 = rmt::matrix::Zero_Matrix();
    m2 = rmt::matrix::Zero_Matrix();
    m3 = rmt::matrix::Zero_Matrix();
    m4 = rmt::matrix::Zero_Matrix();
  };

  storage::Initialise(*data, {n{1.}, n{1.}, 1ul}, initialise_to_zero);

  auto force_strength = complex_type{.5, .5};
  auto force = std::make_shared<unit_test::osborn::Mock_Cartesian_Force>(
      data, force_strength);

  auto stepsize_ptr = std::make_shared<rmt::stepsize::Stepsize>(n{.1});
  auto mock_noise = std::make_unique<unit_test::osborn::Mock_Noise>(n{.75});

  EXPECT_CALL(*mock_noise, noise()).Times(2 * 2 * 2 * M_size * M_size);
  EXPECT_CALL(*mock_noise, set_std_dev(sqrt(0.2))).Times(1);
  EXPECT_CALL(*mock_noise, set_std_dev(sqrt(1.0))).Times(1);

  storage::Langevin_Step step{data, force, stepsize_ptr, std::move(mock_noise)};

  EXPECT_CALL(*force, update()).Times(2);
  EXPECT_CALL(*force, get_force_a()).Times(2);
  EXPECT_CALL(*force, get_force_b()).Times(2);
  EXPECT_CALL(*force, get_force_alpha()).Times(2);
  EXPECT_CALL(*force, get_force_beta()).Times(2);

  step.Update();

  stepsize_ptr->value = n{.5};

  step.Update();

  auto noise_matrix = rmt::matrix::Matrix(n{.75});

  M_type expected_a_matrix =
      (0.6) * force_strength * rmt::matrix::Identity_Matrix() +
      2. * noise_matrix;
  M_type expected_b_matrix =
      (0.6) * force_strength * rmt::matrix::Identity_Matrix() +
      2. * noise_matrix;
  M_type expected_al_matrix =
      (0.6) * force_strength * rmt::matrix::Identity_Matrix() +
      2. * noise_matrix;
  M_type expected_bt_matrix =
      (0.6) * force_strength * rmt::matrix::Identity_Matrix() +
      2. * noise_matrix;

  EXPECT_THAT(data->read(name_tag::A), MatrixApprox(expected_a_matrix));
  EXPECT_THAT(data->read(name_tag::B), MatrixApprox(expected_b_matrix));
  EXPECT_THAT(data->read(name_tag::ALPH), MatrixApprox(expected_al_matrix));
  EXPECT_THAT(data->read(name_tag::BETA), MatrixApprox(expected_bt_matrix));
}
