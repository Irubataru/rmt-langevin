
/*
 * Created: 08-05-2016
 * OSBORN_Modified: Tue 14 Feb 2017 13:56:59 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OSBORN_MOCK_FORCE_HPP
#define OSBORN_MOCK_FORCE_HPP

#include <gmock/gmock.h>
#include <osborn/langevin/force.hpp>
#include <utilities/matrix/identity_constructor.hpp>
#include <utilities/white_noise.hpp>

using ::testing::ReturnRef;

namespace unit_test {
namespace osborn {

class Mock_Cartesian_Force : public rmt::osborn::cartesian::Force
{
public:
  using number_type = rmt::numeric::number_type;
  using complex_type = rmt::numeric::complex_type;

  Mock_Cartesian_Force(
      const std::shared_ptr<const rmt::osborn::cartesian::Matrix_Storage>
          &storage,
      const rmt::M_type &mat_a, const rmt::M_type &mat_b,
      const rmt::M_type &mat_al, const rmt::M_type &mat_bt)
      : Force{storage},
        mock_a_force{mat_a},
        mock_b_force{mat_b},
        mock_al_force{mat_al},
        mock_bt_force{mat_bt}
  {
    delegate_to_fake();
  }

  Mock_Cartesian_Force(
      const std::shared_ptr<const rmt::osborn::cartesian::Matrix_Storage>
          &storage,
      const complex_type &val_a)
      : Mock_Cartesian_Force{storage, val_a, val_a, val_a, val_a}
  {
  }

  Mock_Cartesian_Force(
      const std::shared_ptr<const rmt::osborn::cartesian::Matrix_Storage>
          &storage,
      const complex_type &val_a, const complex_type &val_b,
      const complex_type &val_al, const complex_type &val_bt)
      : Force{storage}
  {
    mock_a_force = val_a * rmt::matrix::Identity_Matrix();
    mock_b_force = val_b * rmt::matrix::Identity_Matrix();
    mock_al_force = val_al * rmt::matrix::Identity_Matrix();
    mock_bt_force = val_bt * rmt::matrix::Identity_Matrix();
    delegate_to_fake();
  }

  MOCK_CONST_METHOD0(get_force_a, const rmt::M_type &());
  MOCK_CONST_METHOD0(get_force_b, const rmt::M_type &());
  MOCK_CONST_METHOD0(get_force_alpha, const rmt::M_type &());
  MOCK_CONST_METHOD0(get_force_beta, const rmt::M_type &());
  MOCK_METHOD0(update, void());

  void delegate_to_fake()
  {
    ON_CALL(*this, get_force_a()).WillByDefault(ReturnRef(mock_a_force));

    ON_CALL(*this, get_force_b()).WillByDefault(ReturnRef(mock_b_force));

    ON_CALL(*this, get_force_alpha()).WillByDefault(ReturnRef(mock_al_force));

    ON_CALL(*this, get_force_beta()).WillByDefault(ReturnRef(mock_bt_force));
  }

private:
  rmt::M_type mock_a_force;
  rmt::M_type mock_b_force;
  rmt::M_type mock_al_force;
  rmt::M_type mock_bt_force;
};

struct Mock_Noise : public rmt::White_Noise
{
  Mock_Noise(value_type noise_val) : value{noise_val}
  {
    delegate_to_fake();
  }

  MOCK_METHOD0(noise, value_type());
  MOCK_METHOD0(get_std_dev, double());
  MOCK_METHOD1(set_std_dev, void(double));

  void delegate_to_fake()
  {
    ON_CALL(*this, noise()).WillByDefault(::testing::Return(value));
  }

private:
  value_type value;
};

} // namespace osborn
} // namespace Unit_Test

#endif /* OSBORN_MOCK_FORCE_HPP */
