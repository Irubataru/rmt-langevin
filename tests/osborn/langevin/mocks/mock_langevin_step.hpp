/*
 * Created: 09-05-2016
 * Modified: Fri 17 Feb 2017 10:35:02 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef MOCK_STEPHANOV_LANGEVIN_STEP_HPP
#define MOCK_STEPHANOV_LANGEVIN_STEP_HPP

#include <gmock/gmock.h>
#include <osborn/langevin/langevin_step.hpp>

namespace unit_test {

class Mock_Cartesian_Langevin_Step
    : public rmt::osborn::cartesian::Langevin_Step
{
public:
  using Langevin_Step::Langevin_Step;
  using value_type = rmt::numeric::number_type;

  MOCK_METHOD1(update_step_length, void(const value_type &));
};

} // namespace unit_test

#endif /* MOCK_STEPHANOV_LANGEVIN_STEP_HPP */
