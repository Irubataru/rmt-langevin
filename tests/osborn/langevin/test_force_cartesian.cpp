/*
 * Created: 08-05-2016
 * Modified: Tue 14 Feb 2017 13:20:34 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <osborn/langevin/force.hpp>
#include <osborn/resource/initialiser.hpp>
#include <utilities/matrix/default_constructor.hpp>
#include <utilities/matrix/identity_constructor.hpp>

#include "../../matchers.hpp"

using rmt::M_type;
using n = rmt::numeric::number_type;

using rmt::numeric::number_type;
using rmt::numeric::complex_type;
using rmt::numeric::imag_i;
using unit_test::MatrixApprox;

namespace model = rmt::osborn;
namespace storage = model::cartesian;

using base_name = model::M_name;
using strg_name = storage::M_name;

TEST(Osborn_Cartesian_Force, Zero_Matrices)
{
  auto par = rmt::Physical_Parameters{{1.1, 0.2},  // chemical_potential
                                      {-2.1, 4.2}, // fermion_mass
                                      11};         // number_of_flavours

  auto data = std::make_shared<storage::Matrix_Storage>();
  auto initialise_to_zero = [](M_type &m1, M_type &m2, M_type &m3, M_type &m4) {
    m1 = rmt::matrix::Zero_Matrix();
    m2 = rmt::matrix::Zero_Matrix();
    m3 = rmt::matrix::Zero_Matrix();
    m4 = rmt::matrix::Zero_Matrix();
  };

  storage::Initialise(*data, par, initialise_to_zero);

  storage::Force force{data, par};

  EXPECT_EQ(0ul, force.get_force_revision());

  force.update();
  EXPECT_EQ(1ul, force.get_force_revision());

  auto zero_matrix = rmt::matrix::Zero_Matrix();

  EXPECT_THAT(force.get_force_a(), MatrixApprox(zero_matrix));
  EXPECT_THAT(force.get_force_b(), MatrixApprox(zero_matrix));
  EXPECT_THAT(force.get_force_alpha(), MatrixApprox(zero_matrix));
  EXPECT_THAT(force.get_force_beta(), MatrixApprox(zero_matrix));
}

TEST(Osborn_Cartesian_Force, Identity_Matrices)
{
  using rmt::numeric::cosh;
  using rmt::numeric::sinh;

  auto par = rmt::Physical_Parameters{{1.1, 0.2},  // chemical_potential
                                      {-2.1, 4.2}, // fermion_mass
                                      11};         // number_of_flavours

  auto data = std::make_shared<storage::Matrix_Storage>();
  auto initialise_to_one = [](M_type &m1, M_type &m2, M_type &m3, M_type &m4) {
    m1 = rmt::matrix::Identity_Matrix();
    m2 = rmt::matrix::Identity_Matrix();
    m3 = rmt::matrix::Identity_Matrix();
    m4 = rmt::matrix::Identity_Matrix();
  };

  storage::Initialise(*data, par, initialise_to_one);

  storage::Force force{data, par};

  EXPECT_EQ(0ul, force.get_force_revision());

  force.update();
  EXPECT_EQ(1ul, force.get_force_revision());

  auto mf = par.fermion_mass;
  auto c_mu = cosh(par.chemical_potential), s_mu = sinh(par.chemical_potential);
  auto phi = complex_type{1., 1.};
  auto nf_d = static_cast<n>(par.number_of_flavours);

  auto r_val = (imag_i * c_mu + s_mu) * std::conj(phi) /
               (mf * mf - n{2.} * (n{2.} * imag_i * c_mu * s_mu - n{1.}));
  auto t_val = (imag_i * c_mu + s_mu) * phi /
               (mf * mf - n{2.} * (n{2.} * imag_i * c_mu * s_mu - n{1.}));

  auto a_force = -n{2.} * M_size - imag_i * nf_d * c_mu * (r_val + t_val);
  auto b_force = -n{2.} * M_size + nf_d * c_mu * (r_val - t_val);
  auto al_force = -n{2.} * M_size + nf_d * s_mu * (r_val + t_val);
  auto bt_force = -n{2.} * M_size - imag_i * nf_d * s_mu * (r_val - t_val);

  EXPECT_THAT(force.get_force_a(),
              MatrixApprox(rmt::matrix::Identity_Matrix(a_force)));
  EXPECT_THAT(force.get_force_b(),
              MatrixApprox(rmt::matrix::Identity_Matrix(b_force)));
  EXPECT_THAT(force.get_force_alpha(),
              MatrixApprox(rmt::matrix::Identity_Matrix(al_force)));
  EXPECT_THAT(force.get_force_beta(),
              MatrixApprox(rmt::matrix::Identity_Matrix(bt_force)));
}
