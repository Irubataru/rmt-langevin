CXX1YFLAG ?= -std=c++14

SRCS_VPATH := src

TARGETS := libgmock.a
SRCS := gmock-all.cc

CXXFLAGS_$(d) := -g -pthread $(CXX1YFLAG) -I$(d)
INCLUDES_$(d) := $(d) $(d)/include $(d)/../googletest/include

libgmock.a_DEPS = gmock-all.o
