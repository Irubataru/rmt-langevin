CXX1YFLAG ?= -std=c++14

SRCS_VPATH := src

TARGETS := libgtest.a gtest_main.o
SRCS := gtest-all.cc

CXXFLAGS_$(d) := -g -pthread $(CXX1YFLAG) -I$(d)
INCLUDES_$(d) := $(d) $(d)/include

libgtest.a_DEPS = gtest-all.o
