/*
 * Created: 19-03-2015
 * Modified: Fri 06 May 2016 13:07:57 CEST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef SIMSKEL_CONFIGURATION_INT_HPP
#define SIMSKEL_CONFIGURATION_INT_HPP

#include<simskel/update/update_int.hpp>

namespace simskel {

using SimpleConfiguration = Simple_Update;
using MonteCarloConfiguration = Monte_Carlo_Update;

} //Namespace simskel

#endif /* SIMSKEL_CONFIGURATION_INT_HPP */
