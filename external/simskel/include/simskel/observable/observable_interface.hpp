/*
 * Created: 18-03-2015
 * Modified: Thu 07 Apr 2016 17:54:07 CEST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef SIMSKEL_OBSERVABLE_INTERFACE_HPP
#define SIMSKEL_OBSERVABLE_INTERFACE_HPP

#include<vector>

namespace simskel {

class Observable
{
public:
  void Calculate()
  {
    DoCalculation();
  }

  void Reset()
  {
    DoReset();
  }

  virtual ~Observable() {};

private:
  virtual void DoCalculation() = 0;
  virtual void DoReset() {}
};

template <typename ValueType>
class ObservableWRecord : public Observable
{
public:
  virtual ~ObservableWRecord() {};

  void ClearRecord()
  {
    value_record.clear();
  }

  void SetNumberOfObservations(std::size_t num_observations)
  {
    value_record.reserve(num_observations);
  }

  const std::vector<ValueType> & GetMeasurements() const
  {
    return value_record;
  }

  std::vector<ValueType> & GetMeasurements()
  {
    return value_record;
  }

  std::vector<ValueType> ExtractMeasurements()
  {
    auto measurements = std::move(value_record);
    ClearRecord();
    return std::move(measurements);
  }

private:
  virtual void DoCalculation()
  {
    value_record.push_back(CalculateValue());
  };

  virtual ValueType CalculateValue() = 0;

  std::vector<ValueType> value_record;
};

}; //Namespace simskel

#endif /* SIMSKEL_OBSERVABLE_INTERFACE_HPP */
