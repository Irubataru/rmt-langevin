/*
 * Created: 05-05-2015
 * Modified: Thu 07 Apr 2016 17:56:40 CEST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef SIMSKEL_OBSERVE_ACCEPTANCE_RATE_HPP
#define SIMSKEL_OBSERVE_ACCEPTANCE_RATE_HPP

#include<simskel/observable/observable_interface.hpp>
#include<simskel/runner/metropolis.hpp>

namespace simskel {

template <typename AcceptRejectAlg>
class AcceptenceRateObservable : public ObservableWRecord<float>
{
public:
  typedef float value_type;

  explicit AcceptenceRateObservable(const MetropolisRunner<AcceptRejectAlg> & metro_runner)
    : runner (metro_runner) {}

private:
  virtual value_type CalculateValue()
  {
    return runner.AcceptanceRatio();
  }

  const MetropolisRunner<AcceptRejectAlg> & runner;
};

} //namespace simskel

#endif /* SIMSKEL_OBSERVE_ACCEPTANCE_RATE_HPP */
