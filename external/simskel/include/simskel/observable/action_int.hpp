/*
 * Created: 18-03-2015
 * Modified: Thu 07 Apr 2016 17:56:29 CEST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef SIMSKEL_ACTION_INT_HPP
#define SIMSKEL_ACTION_INT_HPP

#include<complex>
#include<simskel/observable/types.hpp>

#include<limits>
#include<stdexcept>

namespace simskel {

class Action
{
public:
  Float CalculateWeight()
  {
    cached_weight = CalculateValue();
    return cached_weight;
  }

  Float GetCachedWeight() const
  {
    if (cached_weight == dirty_value())
      throw std::runtime_error("No cached weight to extract from action");

    return cached_weight;
  }

  void Reset()
  {
    cached_weight = dirty_value();
  }

  virtual ~Action() {};

private:
  virtual Float CalculateValue() = 0;

  static const Float & dirty_value()
  {
    static constexpr auto value = std::numeric_limits<Float>::infinity();
    return value;
  }

  Float cached_weight = dirty_value();
};

class ComplexAction
{
public:
  std::complex<Float> CalculateWeight()
  {
    cached_weight = CalculateValue();
    return cached_weight;
  } 

  std::complex<Float> GetCachedWeight() const
  {
    if (cached_weight == dirty_value())
      throw std::runtime_error("No cached weight to extract from the complex action");

    return cached_weight;
  }

  void Reset()
  {
    cached_weight = dirty_value();
  }

  virtual ~ComplexAction() {};
  
private:
  virtual std::complex<Float> CalculateValue() = 0;

  static const std::complex<Float> & dirty_value()
  {
    static constexpr auto value = std::complex<Float> {
      std::numeric_limits<Float>::infinity(), std::numeric_limits<Float>::infinity() };
    return value;
  }

  std::complex<Float> cached_weight = dirty_value();
};


} //Namespace simskel

#endif /* SIMSKEL_ACTION_INT_HPP */
