/*
 * Created: 09-04-2015
 * Modified: Thu 07 Apr 2016 17:54:33 CEST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef SIMSKEL_METRO_TYPES_HPP
#define SIMSKEL_METRO_TYPES_HPP

namespace simskel {

typedef double Float;

};

#endif /* SIMSKEL_METRO_TYPES_HPP */
