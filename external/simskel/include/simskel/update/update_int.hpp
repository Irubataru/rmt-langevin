/*
 * Created: 19-03-2015
 * Modified: Wed 11 May 2016 13:02:30 CEST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef SIMSKEL_UPDATE_INT_HPP
#define SIMSKEL_UPDATE_INT_HPP

#include<memory>

namespace simskel {

enum class Update_Status
{
  Rejected = false,
  Accepted = true
};

class Simple_Update
{
public:
  void Update()
  {
    DoUpdate();
  }

  virtual ~Simple_Update() {};

private:
  virtual void DoUpdate() = 0;

};

class Update_Chain : public Simple_Update
{
public:
  Update_Chain() = default;

  Update_Chain(Simple_Update* &&next)
    : next_step {next} {}

  Update_Chain(std::shared_ptr<Simple_Update> next)
    : next_step {next} {}

  void set_next(Simple_Update* &&next)
  {
    next_step = std::shared_ptr<Simple_Update>{next};
  }

  void set_next(std::shared_ptr<Simple_Update> next)
  {
    next_step = next;
  }

  void push_back(std::shared_ptr<Simple_Update> next);

  void push_back(Simple_Update* &&next)
  {
    auto next_shared = std::shared_ptr<Simple_Update> {std::move(next)};
    push_back(next_shared);
  }

  virtual ~Update_Chain() {}

private:
  virtual void DoUpdate() final
  {
    SelfUpdate();

    if (next_step) 
      next_step->Update();
  }

  virtual void SelfUpdate() = 0;

private:
  std::shared_ptr<Simple_Update> next_step;

};

class Update_Chain_Storage : public Update_Chain
{
public:
  using Update_Chain::Update_Chain;
  virtual ~Update_Chain_Storage() {}

private:
  virtual void SelfUpdate() override {}
};

class Update_Chain_Adaptor : public Update_Chain
{
public:
  Update_Chain_Adaptor(Simple_Update* &&self)
    : this_update {self} {}

  Update_Chain_Adaptor(const std::shared_ptr<Simple_Update> & self)
    : this_update {self} {}

  virtual ~Update_Chain_Adaptor() {}

private:
  virtual void SelfUpdate() override
  {
    if (this_update)
      this_update->Update();
  }

private:
  std::shared_ptr<Simple_Update> this_update;
};

inline void Update_Chain::push_back(std::shared_ptr<Simple_Update> next)
{
  auto next_step_cast = std::dynamic_pointer_cast<Update_Chain>(next_step);

  if (next_step_cast) {
    next_step_cast->push_back(std::move(next));
  } else {
    if (next_step) {
      auto temp_next = std::make_shared<Update_Chain_Adaptor>(next_step);
      temp_next->push_back(next);

      next_step = temp_next;
    } else {
      next_step = next;
    }
  }
}

class Monte_Carlo_Update : public Simple_Update
{
public:
  void Accept()
  {
    state = Update_Status::Accepted;
    DoAccept();
  }

  void Reject()
  {
    state = Update_Status::Rejected;
    DoReject();
  }

  Update_Status Status() const
  {
    return state;
  }

  Monte_Carlo_Update()
    : state {Update_Status::Accepted} {}

  virtual ~Monte_Carlo_Update() {};

private:
  virtual void DoAccept() = 0;
  virtual void DoReject() = 0;
  
  Update_Status state;
};

} //Namespace simskel

#endif /* SIMSKEL_UPDATE_INT_HPP */
