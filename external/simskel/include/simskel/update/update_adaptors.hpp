/*
 * Created: 11-05-2016
 * Modified: Wed 11 May 2016 13:02:03 CEST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef SIMSKEL_UPDATE_ADAPTORS_HPP
#define SIMSKEL_UPDATE_ADAPTORS_HPP

#include"update_int.hpp"
#include"../observable/observable_interface.hpp"

namespace simskel {

class Update_Chain_Functor : public Update_Chain
{
public:
  template <typename Function>
  explicit Update_Chain_Functor(const Function uf)
    : update_f {uf} {}

private:
  virtual void SelfUpdate() override
  {
    update_f();
  }

private:
  std::function<void()> update_f;
};

class Update_Chain_Observable : public Update_Chain
{
public:
  explicit Update_Chain_Observable(Observable* && obs_ptr)
    : obs {std::move(obs_ptr)} {}

  explicit  Update_Chain_Observable(const std::shared_ptr<Observable> & obs_ptr)
    : obs {obs_ptr} {}

private:
  virtual void SelfUpdate() override
  {
    obs->Calculate();
  }

private:
  std::shared_ptr<Observable> obs;
};

} // namespace simskel 

#endif /* SIMSKEL_UPDATE_ADAPTORS_HPP */
