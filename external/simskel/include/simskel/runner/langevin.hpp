/*
 * Created: 06-04-2016
 * Modified: Tue 10 May 2016 13:58:06 CEST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef SIMSKEL_LANGEVIN_HPP
#define SIMSKEL_LANGEVIN_HPP

#include<memory>
#include<tuple>
#include<functional>

#include<simskel/update/update_int.hpp>
#include<simskel/observable/observable_interface.hpp>
#include<simskel/observable/action_int.hpp>

namespace simskel {
namespace langevin {

class LangevinRunner
{
public:
  void Run()
  {
    Thermalisation();

    after_thermalisation();
    Measure();
  }

  void Reset()
  {
    iteration = 0;
    after_thermalisation = [](){};

    for (auto & obs : observables)
      obs->Reset();
  }

  void SetThermalisationStep(std::shared_ptr<Simple_Update> therm_update)
  {
    thermalisation_step = therm_update;
  }

  void SetMeasurementStep(std::shared_ptr<Simple_Update> measure_update)
  {
    measurement_step = measure_update;
  }

  template <typename Foo>
  void AttachTransitionF(const Foo & func)
  {
    after_thermalisation = func;
  }

  void AddObservable(std::shared_ptr<Observable> obs)
  {
    observables.push_back(obs);
  }

  void SetThermalisationQ(std::function<bool()> req)
  {
    thermalisation_done_q = std::move(req);
  }

  void SetMeasurementQ(std::function<bool()> req)
  {
    measure_q = std::move(req);
  }

  void SetSimulationQ(std::function<bool()> req)
  {
    simulation_done_q = std::move(req);
  }

  const std::size_t & Count() const
  {
    return iteration;
  }

  LangevinRunner()
    : thermalisation_done_q {[](){return true;}},
      measure_q {[](){return true;}},
      simulation_done_q {[](){return true;}},
      after_thermalisation {[](){}},
      iteration {0} {}

  LangevinRunner(const std::shared_ptr<Simple_Update> & therm_update,
                 const std::shared_ptr<Simple_Update> & measure_update)
    : thermalisation_step {therm_update},
      measurement_step {measure_update},
      thermalisation_done_q {[](){return true;}},
      measure_q {[](){return true;}},
      simulation_done_q {[](){return true;}},
      after_thermalisation {[](){}},
      iteration {0} {}

  LangevinRunner(const std::shared_ptr<Simple_Update> & steps)
    : LangevinRunner {steps, steps} {}

  LangevinRunner(Simple_Update* && therm_update, Simple_Update* && measure_update)
    : LangevinRunner {}
  {
    if (therm_update == measure_update) {
      thermalisation_step = std::shared_ptr<Simple_Update>(therm_update);
      measurement_step    = thermalisation_step;
    } else {
      thermalisation_step = std::shared_ptr<Simple_Update>(therm_update);
      measurement_step    = std::shared_ptr<Simple_Update>(measure_update);
    }
  }

  LangevinRunner(Simple_Update* && steps)
    : LangevinRunner {std::move(steps), std::move(steps)} {}

  LangevinRunner(const std::shared_ptr<Simple_Update> & therm_update,
                 const std::shared_ptr<Simple_Update> & measure_update,
                 std::array<std::size_t, 3> step_requrements)
    : thermalisation_step {therm_update},
      measurement_step {measure_update},
      after_thermalisation {[](){}},
      iteration {0}
  {
    auto num_therm = step_requrements[0];
    thermalisation_done_q = [this,num_therm](){return iteration >= num_therm;};

    auto measure_diff = step_requrements[1];
    measure_q = [this,measure_diff](){return iteration % measure_diff == 0;};

    auto num_steps = step_requrements[2];
    simulation_done_q = [this, num_steps](){return iteration >= num_steps;};
  }

  LangevinRunner(const std::shared_ptr<Simple_Update> & update,
                 std::array<std::size_t, 3> step_requrements)
    : LangevinRunner {update, update, step_requrements} {}

private: /* functions */
  void Thermalisation()
  {
    while (!thermalisation_done_q())
      ThermalisationStep();
  }

  void Measure()
  {
    while (!simulation_done_q())
      MeasurementStep();
  }

  void ThermalisationStep()
  {
    thermalisation_step->Update();
    ++iteration;
  }

  void MeasurementStep()
  {
    measurement_step->Update();

    if (measure_q())
      MeasureObservables();

    ++iteration;
  }

  void MeasureObservables()
  {
    for (auto & obs : observables)
      obs->Calculate();
  }

private: /* Variables */
  std::shared_ptr<Simple_Update> thermalisation_step;
  std::shared_ptr<Simple_Update> measurement_step;
  std::vector< std::shared_ptr<Observable> > observables;

  std::function<bool()> thermalisation_done_q;
  std::function<bool()> measure_q;
  std::function<bool()> simulation_done_q;

  std::function<void()> after_thermalisation;

  std::size_t iteration;
};

}
} // namespace simskel 

#endif /* SIMSKEL_LANGEVIN_HPP */
