/*
 * Created: 19-03-2015
 * Modified: Thu 07 Apr 2016 17:55:09 CEST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef SIMSKEL_METROPOLIS_ACCEPT_REJECT_HPP
#define SIMSKEL_METROPOLIS_ACCEPT_REJECT_HPP

#include<cmath>
#include<random>

namespace simskel {

template <typename Generator>
class MetropolisAcceptRejectAlg
{
public:
  virtual bool Test(double weight)
  {
    if (weight < previous_weight or BoltzmannTest(weight)) {
      previous_weight = weight;
      return true;
    } else {
      return false;
    }
  }

  void Initialise(double init_weight)
  {
    previous_weight = init_weight;
  }

  MetropolisAcceptRejectAlg(Generator & random_number_generator)
    : previous_weight {0.},
      rng (random_number_generator) {}

private:
  bool BoltzmannTest(double weight)
  {
    return distribution(rng) < std::exp(previous_weight - weight);
  }

private:
  double previous_weight;

  std::uniform_real_distribution<> distribution;
  Generator & rng;
};

}; //Namespace simskel

#endif /* SIMSKEL_METROPOLIS_ACCEPT_REJECT_HPP */
