/*
 * Created: 19-03-2015
 * Modified: Fri 06 May 2016 13:08:28 CEST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef SIMSKEL_METROPOLIS_INT_HPP
#define SIMSKEL_METROPOLIS_INT_HPP

#include<memory>
#include<vector>

#include<simskel/update/update_int.hpp>
#include<simskel/observable/observable_interface.hpp>
#include<simskel/observable/action_int.hpp>

namespace simskel {

struct RunParameters
{
  std::size_t thermalisation_steps;
  std::size_t number_of_measurements;
  std::size_t time_between_measurements;
};

template <typename AcceptRejectAlg>
class MetropolisRunner
{
public:
  void Run()
  {
    Thermalisation();
    Measure();
  }

  void Reset()
  {
    iteration = 0;
    for (auto & obs : observables)
      obs->Reset();

    accept_count = 0;
    reject_count = 0;
  }

  void SetParameters(RunParameters new_parameters)
  {
    parameters = new_parameters;
  }

  void SetAcceptRejectAlgorithm(AcceptRejectAlg new_algorithm)
  {
    accept_reject = new_algorithm;
  }

  void AddObservable(std::shared_ptr<Observable> obs)
  {
    observables.push_back(obs);
  }

  float AcceptanceRatio() const
  {
    return static_cast<float>(accept_count) / (accept_count + reject_count);
  }

  MetropolisRunner(Monte_Carlo_Update & configuration_ref,
                   Action & action_ref,
                   const std::vector< std::shared_ptr<Observable> > & observable_ptrs,
                   RunParameters run_parameters,
                   AcceptRejectAlg accept_reject_alg = AcceptRejectAlg {})
    : configuration (configuration_ref),
      action (action_ref),
      observables {observable_ptrs},
      parameters (run_parameters),
      iteration {0},
      accept_reject (accept_reject_alg) {}

private: /* functions */
  void Thermalisation()
  {
    while (iteration < parameters.thermalisation_steps)
      ThermalisationStep();
  }

  void Measure()
  {
    auto total_steps = parameters.thermalisation_steps 
       + parameters.time_between_measurements * parameters.number_of_measurements;

    while (iteration < total_steps)
      MeasurementStep();
  }

  void ThermalisationStep()
  {
    MonteCarloStep();
    ++iteration;
  }

  void MeasurementStep()
  {
    MonteCarloStep();

    auto therm_step_mod = parameters.thermalisation_steps % parameters.time_between_measurements;

    if (iteration % parameters.time_between_measurements == therm_step_mod)
      MeasureObservables();

    ++iteration;
  }

  virtual void MonteCarloStep()
  {
    configuration.Update();
    auto weight = action.CalculateWeight();

    if (accept_reject.Test(weight)) {
      configuration.Accept();
      ++accept_count;
    } else {
      configuration.Reject();
      ++reject_count;
    }
  }

  void MeasureObservables()
  {
    for (auto & obs : observables)
      obs->Calculate();
  }

private: /* Variables */
  Monte_Carlo_Update & configuration;
  Action & action;
  std::vector< std::shared_ptr<Observable> > observables;

  RunParameters parameters;
  std::size_t iteration;

  AcceptRejectAlg accept_reject;

  std::size_t accept_count = 0, reject_count = 0;
};

}; //namespace simskel

#endif /* SIMSKEL_METROPOLIS_INT_HPP */
