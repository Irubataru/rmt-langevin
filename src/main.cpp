
/*
 * Created: 07-04-2016
 * Modified: Wed 06 Sep 2017 13:58:03 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <builders/register_all.hpp>
#include <builders/simulation_builder.hpp>
#include <utilities/program_options.hpp>
#include <chrono>

using nlohmann::json;
using namespace rmt;

int main(int argc, char **argv)
{
  auto cfg_file = build::get_config_filename(argc, argv);

  json main_tree;
  std::ifstream ifs(cfg_file);

  if (!ifs) {
    throw std::runtime_error{"init error: unable to open config file \"" +
                             cfg_file + "\""};
  }

  ifs >> main_tree;

  auto model_params = build::parse_model_parameters(main_tree);

  build::core::register_all();
  build::Simulation_Builder simulation_builder{main_tree};

  std::cout << "Running with " << M_size << "x" << M_size << " matrices, "
            << "model: " << parse::to_string(model_params.model)
            << ", storage: " << parse::to_string(model_params.storage) << "\n"
            << std::endl;

  for (auto mass : model_params.mass_scan) {
    for (auto mu : model_params.mu_scan) {
      std::cout << "m = " << mass << ", mu = " << mu << std::endl;

      Physical_Parameters params{mu, mass, model_params.number_of_flavours};
      auto simulation = simulation_builder.build(params);

      auto start_time = std::chrono::high_resolution_clock::now();

      for (auto const &stage : simulation)
        stage->run();

      auto runtime = std::chrono::duration_cast<std::chrono::milliseconds>(
          std::chrono::high_resolution_clock::now() - start_time);

      std::cout << "\nTime: " << runtime.count() << " ms\n";
    }
  }
}
