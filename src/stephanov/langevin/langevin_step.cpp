
/*
 * Created: 16-06-2016
 * Modified: Fri 01 Sep 2017 11:23:52 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "langevin_step.hpp"

namespace rmt {
namespace stephanov {

namespace cartesian {

void Langevin_Step::DoUpdate()
{
  using cart_name = ::rmt::stephanov::cartesian::M_name;
  using numeric::sqrt;

  force->update();

  if (storage->get_storage_revision() == 0) {
    force->update();
  } else if (storage->get_storage_revision() != force->get_force_revision()) {
    force->update();
  }

  auto step_length = stepsize->value;
  noise->set_std_dev(sqrt(numeric::number_type{2.} * step_length));

  auto &a = storage->read_write(cart_name::A);
  auto &b = storage->read_write(cart_name::B);

  a += step_length * force->get_force_a();
  a = blaze::forEach(a, [this](const rmt::numeric::complex_type &x) {
    return x + noise_weight * noise->noise();
  });

  b += step_length * force->get_force_b();
  b = blaze::forEach(b, [this](const rmt::numeric::complex_type &x) {
    return x + noise_weight * noise->noise();
  });

  storage->update();
}

} // namespace cartesian

namespace polar {

void Langevin_Step::DoUpdate()
{
  using polr_name = ::rmt::stephanov::polar::M_name;

  if (storage->get_storage_revision() != force->get_force_revision())
    force->update();

  auto step_length = stepsize->value;
  noise->set_std_dev(sqrt(numeric::number_type{2.} * step_length));

  auto &r = storage->read_write(polr_name::R);
  auto &p = storage->read_write(polr_name::PHI);

  r += step_length * force->get_force_r();
  r = blaze::forEach(r, [this](const rmt::numeric::complex_type &x) {
    return x + noise_weight * noise->noise();
  });

  p += step_length * force->get_force_phi();
  p = blaze::forEach(p, [this](const rmt::numeric::complex_type &x) {
    return x + noise_weight * noise->noise();
  });

  storage->update();
}

} // namespace polar

} // namespace stephanov
} // namespace rmt
