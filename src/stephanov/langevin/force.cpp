/*
 * Created: 08-05-2016
 * Modified: Mon 11 Sep 2017 12:07:21 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "force.hpp"
#include <utilities/matrix/componentwise_mult.hpp>

using ::rmt::numeric::number_type;
using ::rmt::numeric::complex_type;
using ::rmt::numeric::imag_i;

namespace rmt {
namespace stephanov {

namespace cartesian {

Force::Force(std::shared_ptr<const Matrix_Storage> m_storage)
    : storage{m_storage},
      a_force{matrix::Matrix()},
      b_force{matrix::Matrix()}
{
}

Force::Force(std::shared_ptr<const Matrix_Storage> m_storage,
             Physical_Parameters pars)
    : storage{m_storage},
      a_force{matrix::Matrix()},
      b_force{matrix::Matrix()},
      par(pars)
{
}

void Force::do_update()
{
  using base_name = ::rmt::stephanov::M_name;
  using cart_name = ::rmt::stephanov::cartesian::M_name;

  const auto &a = storage->read(cart_name::A);
  const auto &b = storage->read(cart_name::B);
  const auto &g = storage->read(base_name::G);
  const auto &x = storage->read(base_name::X);
  const auto &xc = storage->read(base_name::XC);
  const auto &mu_mat = storage->read(base_name::MU);

  auto nf_d = static_cast<number_type>(par.number_of_flavours);

  a_force = static_cast<number_type>(-2. * M_size) * a +
            nf_d * (x * g + blaze::trans(g * xc) -
                    imag_i * mu_mat * (g + blaze::trans(g)));
  b_force = static_cast<number_type>(-2. * M_size) * b -
            imag_i * nf_d * (x * g - blaze::trans(g * xc) +
                             imag_i * mu_mat * (blaze::trans(g) - g));
}

namespace shifted {

Force::Force(std::shared_ptr<const Matrix_Storage> m_storage)
    : storage{m_storage},
      a_force{matrix::Matrix()},
      b_force{matrix::Matrix()}
{
}

Force::Force(std::shared_ptr<const Matrix_Storage> m_storage,
             Physical_Parameters pars)
    : storage{m_storage},
      a_force{matrix::Matrix()},
      b_force{matrix::Matrix()},
      par(pars)
{
}

void Force::do_update()
{
  using base_name = ::rmt::stephanov::M_name;
  using cart_name = ::rmt::stephanov::cartesian::M_name;

  const auto &a = storage->read(cart_name::A);
  const auto &b = storage->read(cart_name::B);
  const auto &g = storage->read(base_name::G);

  auto mu = par.chemical_potential;
  auto nf_d = static_cast<number_type>(par.number_of_flavours);

  a_force = static_cast<number_type>(-2. * M_size) *
                (a + matrix::Identity_Matrix(imag_i * mu)) +
            nf_d * ((a + imag_i * b) * g +
                    blaze::trans(g * blaze::trans(a - imag_i * b)));

  b_force = static_cast<number_type>(-2. * M_size) * b -
            imag_i * nf_d * ((a + imag_i * b) * g -
                             blaze::trans(g * blaze::trans(a - imag_i * b)));
}


} // namespace shifted 

} // namespace cartesian

namespace polar {

Force::Force(std::shared_ptr<const Matrix_Storage> m_storage)
    : storage{m_storage},
      r_force{matrix::Matrix()},
      phi_force{matrix::Matrix()},
      force_revision{0}
{
}

Force::Force(std::shared_ptr<const Matrix_Storage> m_storage,
             Physical_Parameters pars)
    : storage{m_storage},
      r_force{matrix::Matrix()},
      phi_force{matrix::Matrix()},
      par(pars),
      force_revision{0}
{
}

void Force::conditional_update()
{
  if (force_revision != storage->get_storage_revision())
    update();
}

void Force::update()
{
  using base_name = ::rmt::stephanov::M_name;
  using cart_name = ::rmt::stephanov::polar::M_name;

  const auto &r = storage->read(cart_name::R);
  const auto &phi = storage->read(cart_name::PHI);

  auto exp_phi = M_type{blaze::forEach(
      phi, [](const complex_type &ang) { return numeric::exp(imag_i * ang); })};

  auto minus_exp_phi = M_type{blaze::forEach(phi, [](const complex_type &ang) {
    return numeric::exp(-imag_i * ang);
  })};

  auto x_star = matrix::Componentwise_Multiplication(r, minus_exp_phi);

  auto r_inv = M_type{blaze::forEach(
      r, [](const complex_type &x) { return number_type{1.} / x; })};

  const auto &g = storage->read(base_name::G);
  const auto &x = storage->read(base_name::X);

  auto mu = par.chemical_potential;
  auto nf_d = static_cast<number_type>(par.number_of_flavours);

  r_force =
      static_cast<number_type>(-2. * M_size) * r + r_inv +
      nf_d *
          (matrix::Componentwise_Multiplication(minus_exp_phi, x * g) +
           matrix::Componentwise_Multiplication(exp_phi,
                                                x_star * blaze::trans(g)) -
           imag_i * mu *
               (matrix::Componentwise_Multiplication(exp_phi, blaze::trans(g)) +
                matrix::Componentwise_Multiplication(minus_exp_phi, g)));

  phi_force =
      nf_d * (imag_i * matrix::Componentwise_Multiplication(
                           x, x_star * blaze::trans(g)) -
              imag_i * matrix::Componentwise_Multiplication(x_star, x * g) +
              mu * (matrix::Componentwise_Multiplication(x, blaze::trans(g)) -
                    matrix::Componentwise_Multiplication(x_star, g)));

  force_revision = storage->get_storage_revision();
}

} // namespace polar

} // namespace stephanov
} // namespace rmt
