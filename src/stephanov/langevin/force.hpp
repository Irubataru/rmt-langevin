/*
 * Created: 08-05-2016
 * Modified: Fri 01 Sep 2017 12:29:19 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_FORCE_HPP
#define STEPHANOV_FORCE_HPP

#include <compile_configuration.hpp>
#include <memory>
#include <stephanov/resource/resource.hpp>

namespace rmt {
namespace stephanov {

namespace cartesian {

class Force_Base : public Simulation_Force
{
public:
  Force_Base() : force_revision_{0}
  {
  }

  virtual const M_type &get_force_a() const = 0;
  virtual const M_type &get_force_b() const = 0;
  unsigned long get_force_revision() const
  {
    return force_revision_;
  }

  void update()
  {
    do_update();
    force_revision_ = get_storge_revision();
  }

  void conditional_update()
  {
    if (force_revision_ != get_storge_revision())
      update();
  }

  virtual ~Force_Base()
  {
  }

private:
  virtual unsigned long get_storge_revision() const = 0;
  virtual void do_update() = 0;

private:
  unsigned long force_revision_;
};

class Force : public Force_Base,
              public builder::builder_core::requires_resource_type
{
public:
  using storage_type = ::rmt::stephanov::cartesian::Matrix_Storage;

  explicit Force(std::shared_ptr<const Matrix_Storage> m_storage);
  explicit Force(std::shared_ptr<const Matrix_Storage> m_storage,
                 Physical_Parameters pars);

  virtual const M_type &get_force_a() const override
  {
    return a_force;
  }

  virtual const M_type &get_force_b() const override
  {
    return b_force;
  }

  virtual ~Force()
  {
  }

private:
  virtual unsigned long get_storge_revision() const override
  {
    return storage->get_storage_revision();
  }

  virtual void do_update() override;

private:
  std::shared_ptr<const storage_type> storage;

  M_type a_force, b_force;
  Physical_Parameters par;
};

namespace shifted {

class Force : public Force_Base,
              public builder::builder_core::requires_resource_type
{
public:
  using storage_type = ::rmt::stephanov::cartesian::shifted::Matrix_Storage;

  explicit Force(std::shared_ptr<const Matrix_Storage> m_storage);
  explicit Force(std::shared_ptr<const Matrix_Storage> m_storage,
                 Physical_Parameters pars);

  virtual const M_type &get_force_a() const override
  {
    return a_force;
  }

  virtual const M_type &get_force_b() const override
  {
    return b_force;
  }

  virtual ~Force()
  {
  }

private:
  virtual unsigned long get_storge_revision() const override
  {
    return storage->get_storage_revision();
  }

  virtual void do_update() override;

private:
  std::shared_ptr<const Matrix_Storage> storage;

  M_type a_force, b_force;
  Physical_Parameters par;
};

} // namespace shifted

} // namespace cartesian

namespace polar {

class Force : public Simulation_Force,
              public builder::builder_core::requires_resource_type
{
public:
  explicit Force(std::shared_ptr<const Matrix_Storage> m_storage);
  explicit Force(std::shared_ptr<const Matrix_Storage> m_storage,
                 Physical_Parameters pars);

  virtual const M_type &get_force_r() const
  {
    return r_force;
  }

  virtual const M_type &get_force_phi() const
  {
    return phi_force;
  }

  virtual unsigned long get_force_revision() const
  {
    return force_revision;
  }

  virtual void update();
  virtual void conditional_update();

private:
  std::shared_ptr<const Matrix_Storage> storage;

  M_type r_force, phi_force;
  Physical_Parameters par;

  unsigned long force_revision;
};

} // namespace polar

namespace builder {
namespace builder_core {

struct requires_force_type
{
  char *requirements_requires_force;
};

} // namespace builder
} // namespace builder_core

} // namespace stephanov
} // namespace rmt

#endif /* STEPHANOV_FORCE_HPP */
