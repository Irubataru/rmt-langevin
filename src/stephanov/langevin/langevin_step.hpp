/*
 * Created: 08-05-2016
 * Modified: Thu 18 May 2017 15:44:04 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_LANGEVIN_STEP_HPP
#define STEPHANOV_LANGEVIN_STEP_HPP

#include "force.hpp"
#include <simskel/update/update_int.hpp>
#include <stephanov/resource/resource.hpp>
#include <utilities/stepsize/stepsize_keeper.hpp>
#include <utilities/white_noise.hpp>

namespace rmt {
namespace stephanov {

namespace cartesian {

class Langevin_Step : public ::simskel::Simple_Update,
                      public builder::builder_core::requires_resource_type,
                      public builder::builder_core::requires_force_type
{
public:
  Langevin_Step(std::shared_ptr<Matrix_Storage> storage_ptr,
                std::shared_ptr<Force_Base> force_ptr,
                std::shared_ptr<const stepsize::Stepsize> stepsize_ptr,
                std::unique_ptr<White_Noise> noise)
      : storage{storage_ptr},
        force{force_ptr},
        stepsize{std::move(stepsize_ptr)},
        noise{std::move(noise)},
        noise_weight{1.}
  {
  }

  virtual ~Langevin_Step()
  {
  }

  void set_noise_weight(numeric::number_type new_weight) noexcept
  {
    noise_weight = new_weight;
  }

private:
  void DoUpdate() override;

private:
  std::shared_ptr<Matrix_Storage> storage;
  std::shared_ptr<Force_Base> force;

  std::shared_ptr<const stepsize::Stepsize> stepsize;
  std::unique_ptr<White_Noise> noise;
  numeric::number_type noise_weight;
};

} // namespace cartesian

namespace polar {

class Langevin_Step : public ::simskel::Simple_Update,
                      public builder::builder_core::requires_resource_type,
                      public builder::builder_core::requires_force_type
{
public:
  Langevin_Step(std::shared_ptr<Matrix_Storage> storage_ptr,
                std::shared_ptr<Force> force_ptr,
                std::shared_ptr<const stepsize::Stepsize> stepsize_ptr,
                std::unique_ptr<White_Noise> noise)
      : storage{storage_ptr},
        force{force_ptr},
        stepsize{std::move(stepsize_ptr)},
        noise{std::move(noise)},
        noise_weight{1.}
  {
  }

  void set_noise_weight(numeric::number_type new_weight) noexcept
  {
    noise_weight = new_weight;
  }

  virtual ~Langevin_Step()
  {
  }

private:
  void DoUpdate() override;

private:
  std::shared_ptr<Matrix_Storage> storage;
  std::shared_ptr<Force> force;

  std::shared_ptr<const stepsize::Stepsize> stepsize;
  std::unique_ptr<White_Noise> noise;
  numeric::number_type noise_weight;
};

} // namespace polar

} // namespace stephanov
} // namespace rmt

#endif /* STEPHANOV_LANGEVIN_STEP_HPP */
