
/*
 * Created: 27-04-2017
 * Modified: Tue 02 May 2017 10:57:21 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_CONFIGURATION_PAIR_HPP
#define STEPHANOV_CONFIGURATION_PAIR_HPP

#include <types.hpp>

namespace rmt {
namespace stephanov {

struct Configuration_Pair
{
  M_type X;
  M_type XC;
};

namespace cartesian {

struct Storage_Pair
{
  M_type a;
  M_type b;
};

} // namespace cartesian 

namespace polar {

struct Storage_Pair
{
  M_type r;
  M_type phi;
};

} // namespace polar 

} // namespace stephanov
} // namespace rmt

#endif /* STEPHANOV_CONFIGURATION_PAIR_HPP */
