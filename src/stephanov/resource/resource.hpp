/*
 * Created: 08-04-2016
 * Modified: Mon 11 Sep 2017 12:02:40 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_RESOURCE_HPP
#define STEPHANOV_RESOURCE_HPP

#include <types.hpp>
#include <utilities/matrix/identity_constructor.hpp>

namespace rmt {
namespace stephanov {

extern const std::string model_name;

enum class M_name
{
  X,
  XC,
  M,
  MU,
  G
};

class Matrix_Storage_Base : public Simulation_Config
{
public:
  using value_type = numeric::complex_type;

  Matrix_Storage_Base();

  virtual const M_type &read(M_name) const;

  const value_type &read_mass() const
  {
    return mass_val;
  }

  const value_type &read_mu() const
  {
    return mu_val;
  }

  virtual void set_mass(value_type mass)
  {
    mass_val = mass;
    matrix::Identity_Matrix(mf_mat, mass * mass);
    set_mass_internal(mass);
  }

  virtual void set_mu(value_type chemical_potential)
  {
    mu_val = chemical_potential;
    mu_mat = matrix::Zero_Matrix();

    for (auto i = 0; i < M_size/2; ++i)
      mu_mat(i,i) = mu_val;

    for (auto i = M_size/2; i < M_size; ++i)
      mu_mat(i,i) = std::conj(mu_val);

    mu_sqr_mat = mu_mat * mu_mat;
    set_mu_internal(chemical_potential);
  }

  unsigned long get_storage_revision() const
  {
    return storage_revision;
  }

  virtual void update();

  virtual ~Matrix_Storage_Base()
  {
  }

private:
  virtual M_type construct_X() const = 0;
  virtual M_type construct_X_conjugate() const = 0;
  virtual void set_mu_internal(const value_type &mu) {}
  virtual void set_mass_internal(const value_type &mass) {}

private:
  M_type mf_mat, mu_mat, mu_sqr_mat, x, xc, g;
  value_type mu_val, mass_val;
  unsigned long storage_revision;
};

namespace cartesian {

enum class M_name
{
  A,
  B
};

class Matrix_Storage : public Matrix_Storage_Base
{
public:
  using Matrix_Storage_Base::read;

  Matrix_Storage();

  virtual const M_type &read(M_name) const;
  virtual M_type &read_write(M_name);

  virtual ~Matrix_Storage()
  {
  }

private:
  virtual M_type construct_X() const override;
  virtual M_type construct_X_conjugate() const override;

protected:
  M_type a, b;
};

extern const std::string representation_name;

namespace shifted {

class Matrix_Storage : public ::rmt::stephanov::cartesian::Matrix_Storage
{
public:
  using Matrix_Storage_Base::value_type;

private:
  virtual M_type construct_X() const override;
  virtual M_type construct_X_conjugate() const override;

  virtual void set_mu_internal(const value_type &mu) override
  {
    imu_ = matrix::Identity_Matrix(numeric::imag_i * mu);
  }

private:
  M_type imu_;
};

extern const std::string representation_name;

} // namespace shifted 

} // namespace cartesian

namespace polar {

enum class M_name
{
  R,
  PHI
};

class Matrix_Storage : public Matrix_Storage_Base
{
public:
  using Matrix_Storage_Base::read;

  Matrix_Storage();

  virtual const M_type &read(M_name) const;
  virtual M_type &read_write(M_name);

  virtual ~Matrix_Storage()
  {
  }

private:
  virtual M_type construct_X() const override;
  virtual M_type construct_X_conjugate() const override;

private:
  M_type r, phi;
};

extern const std::string representation_name;

} // namespace polar

namespace builder {
namespace builder_core {

struct requires_resource_type
{
  char *requirements_requires_resource;
};

} // namespace builder_core
} // namespace builder

} // namespace stephanov
} // namespace rmt

#endif /* STEPHANOV_RESOURCE_HPP */
