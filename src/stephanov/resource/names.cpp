/*
 * Created: 10-05-2016
 * Modified: Thu 18 May 2017 12:06:59 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "resource.hpp"

namespace rmt {
namespace stephanov {

const std::string model_name = "stephanov";

namespace cartesian {

const std::string representation_name = {"cartesian"};

namespace shifted {
const std::string representation_name = {"cartesian (shifted)"};
} // namespace shifted 

} // namespace cartesian

namespace polar {
const std::string representation_name = {"polar"};
} // namespace polar

} // namespace stephanov
} // namespace rmt
