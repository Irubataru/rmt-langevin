/*
 * Created: 08-04-2016
 * Modified: Wed 27 Sep 2017 16:13:20 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "resource.hpp"
#include <utilities/matrix/componentwise_mult.hpp>

using rmt::numeric::imag_i;

namespace rmt {
namespace stephanov {

Matrix_Storage_Base::Matrix_Storage_Base()
    : mf_mat{matrix::Identity_Matrix()},
      mu_mat{matrix::Identity_Matrix()},
      mu_sqr_mat{matrix::Identity_Matrix()},
      x{matrix::Matrix()},
      xc{matrix::Matrix()},
      g{matrix::Matrix()},
      storage_revision{0}
{
}

const M_type &Matrix_Storage_Base::read(M_name name) const
{
  switch (name) {
  case M_name::X:
    return x;
  case M_name::XC:
    return xc;
  case M_name::M:
    return mf_mat;
  case M_name::MU:
    return mu_mat;
  case M_name::G:
    return g;
  default:
    throw std::runtime_error{"Matrix_Storage_Base: Name out of range"};
  }
}

void Matrix_Storage_Base::update()
{
  x = construct_X();
  xc = construct_X_conjugate();

  g = blaze::inv(mf_mat - mu_sqr_mat + xc * x - imag_i * (mu_mat * x + xc * mu_mat));
  ++storage_revision;
}

namespace cartesian {

Matrix_Storage::Matrix_Storage() : a{matrix::Matrix()}, b{matrix::Matrix()}
{
}

M_type &Matrix_Storage::read_write(M_name name)
{
  switch (name) {
  case M_name::A:
    return a;
  case M_name::B:
    return b;
  default:
    throw std::runtime_error{"cartesian::Matrix_Storage: Name out of range"};
  }
}

const M_type &Matrix_Storage::read(M_name name) const
{
  switch (name) {
  case M_name::A:
    return a;
  case M_name::B:
    return b;
  default:
    throw std::runtime_error{"cartesian::Matrix_Storage: Name out of range"};
  }
}

M_type Matrix_Storage::construct_X() const
{
  return (a + imag_i * b);
}

M_type Matrix_Storage::construct_X_conjugate() const
{
  return blaze::trans(a - imag_i * b);
}

namespace shifted {

M_type Matrix_Storage::construct_X() const
{
  return (a + imu_ + imag_i * b);
}

M_type Matrix_Storage::construct_X_conjugate() const
{
  return blaze::trans(a + imu_ - imag_i * b);
}

} // namespace shifted 

} // namespace cartesian

namespace polar {

Matrix_Storage::Matrix_Storage() : r{matrix::Matrix()}, phi{matrix::Matrix()}
{
}

M_type &Matrix_Storage::read_write(M_name name)
{
  switch (name) {
  case M_name::R:
    return r;
  case M_name::PHI:
    return phi;
  default:
    throw std::runtime_error{"polar::Matrix_Storage: Name out of range"};
  }
}

const M_type &Matrix_Storage::read(M_name name) const
{
  switch (name) {
  case M_name::R:
    return r;
  case M_name::PHI:
    return phi;
  default:
    throw std::runtime_error{"cartesian::Matrix_Storage: Name out of range"};
  }
}

M_type Matrix_Storage::construct_X() const
{
  M_type exp_phi = blaze::forEach(
      phi, [](const value_type &x) { return numeric::exp(imag_i * x); });

  return matrix::Componentwise_Multiplication(r, exp_phi);
}

M_type Matrix_Storage::construct_X_conjugate() const
{
  M_type minus_exp_phi = blaze::forEach(
      phi, [](const value_type &x) { return numeric::exp(-imag_i * x); });

  return blaze::trans(matrix::Componentwise_Multiplication(r, minus_exp_phi));
}

} // namespace polar

} // namespace stephanov
} // namespace rmt
