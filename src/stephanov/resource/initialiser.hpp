/*
 * Created: 08-05-2016
 * Modified: Thu 18 May 2017 12:05:58 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_FORCE_INITIALISER_HPP
#define STEPHANOV_FORCE_INITIALISER_HPP

#include <stephanov/resource/resource.hpp>
#include <types.hpp>
#include <utilities/matrix/identity_constructor.hpp>

namespace rmt {
namespace stephanov {

namespace cartesian {

template <typename Algorithm>
void Initialise(Matrix_Storage &storage, Physical_Parameters par, Algorithm alg)
{
  alg(storage.read_write(M_name::A), storage.read_write(M_name::B));

  storage.set_mass(par.fermion_mass);
  storage.set_mu(par.chemical_potential);
  storage.update();
}

} // namespace cartesian

namespace polar {

template <typename Algorithm>
void Initialise(Matrix_Storage &storage, Physical_Parameters par, Algorithm alg)
{
  alg(storage.read_write(M_name::R), storage.read_write(M_name::PHI));

  storage.set_mass(par.fermion_mass);
  storage.set_mu(par.chemical_potential);
  storage.update();
}

} // namespace polar

} // namespace stephanov
} // namespace rmt

#endif /* STEPHANOV_FORCE_INITIALISER_HPP */
