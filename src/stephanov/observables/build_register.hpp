
/*
 * Created: 02-09-2017
 * Modified: Wed 06 Sep 2017 14:28:38 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_OBSERVABLE_BUILD_REGISTER_HPP
#define STEPHANOV_OBSERVABLE_BUILD_REGISTER_HPP

#include <builders/observable_builders/builder_factory.hpp>
#include <builders/observable_builders/stephanov/stephanov_builders.hpp>
#include <stephanov/observables/observables.hpp>
#include <utilities/json_parser_helpers.hpp>

namespace rmt {
namespace build {
namespace core {

inline void register_stephanov_observables()
{
  namespace obs = ::rmt::stephanov::observable;
  namespace cart_obs = ::rmt::stephanov::cartesian::observable;
  namespace polar_obs = ::rmt::stephanov::polar::observable;

  using parse::to_string;

  auto &dict = Factory_Dictionary::instance();

  auto model_name = to_string(Model_Type::Stephanov) + "_";
  auto model_cart_name = model_name + to_string(Storage_Type::Cartesian) + "_";
  auto model_polar_name = model_name + to_string(Storage_Type::Polar) + "_";

  dict.add({model_name + "action"},
           std::make_shared<No_Param_Obs_Build_Factory<
               stephanov::Storage_Phys_Obs_Builder<obs::Action>>>());

  dict.add({model_cart_name + "action-splits"},
           std::make_shared<No_Param_Obs_Build_Factory<
               stephanov::cartesian::Storage_Phys_Obs_Builder<
                   cart_obs::Action_Splits>>>());

  dict.add(
      {model_name + "baryon-number"},
      std::make_shared<No_Param_Obs_Build_Factory<
          stephanov::Storage_Phys_Obs_Builder<obs::Baryon_Number_Density>>>());

  dict.add({model_name + "chiral-condensate"},
           std::make_shared<No_Param_Obs_Build_Factory<
               stephanov::Storage_Phys_Obs_Builder<obs::Chiral_Condensate>>>());

  dict.add({model_name + "determinant"},
           std::make_shared<No_Param_Obs_Build_Factory<
               stephanov::Storage_Obs_Builder<obs::Determinant>>>());

  dict.add({model_name + "eigenvalues"},
           std::make_shared<No_Param_Obs_Build_Factory<
               stephanov::Storage_Obs_Builder<obs::Eigenvalues>>>());

  dict.add(
      {model_cart_name + "force-max"},
      std::make_shared<No_Param_Obs_Build_Factory<
          stephanov::cartesian::Force_Obs_Builder<cart_obs::Force_Max>>>());

  dict.add(
      {model_cart_name + "force-norm"},
      std::make_shared<No_Param_Obs_Build_Factory<
          stephanov::cartesian::Force_Obs_Builder<cart_obs::Force_Norm>>>());

  dict.add({model_polar_name + "force-max"},
           std::make_shared<No_Param_Obs_Build_Factory<
               stephanov::polar::Force_Obs_Builder<polar_obs::Force_Max>>>());

  dict.add({model_name + "configuration"},
           std::make_shared<No_Param_Obs_Build_Factory<
               stephanov::Storage_Obs_Builder<obs::Configuration>>>());

  dict.add({model_cart_name + "storage"},
           std::make_shared<No_Param_Obs_Build_Factory<
               stephanov::cartesian::Storage_Obs_Builder<
                   cart_obs::Storage>>>());

  dict.add(
      {model_polar_name + "storage"},
      std::make_shared<No_Param_Obs_Build_Factory<
          stephanov::polar::Storage_Obs_Builder<polar_obs::Storage>>>());

  dict.add({model_cart_name + "diagonal-average"},
           std::make_shared<No_Param_Obs_Build_Factory<
               stephanov::cartesian::Storage_Obs_Builder<
                   cart_obs::Diagonal_Average>>>());

  dict.add({model_name + "fermion-determinant"},
           std::make_shared<No_Param_Obs_Build_Factory<
               stephanov::Storage_Obs_Builder<obs::Fermion_Determinant>>>());

  dict.add({model_name + "fermion-eigenvalues"},
           std::make_shared<No_Param_Obs_Build_Factory<
               stephanov::Storage_Obs_Builder<obs::Fermion_Eigenvalues>>>());

  dict.add(
      {model_name + "cooling-norm"},
      std::make_shared<
          JSON_Trees_Obs_Build_Factory<stephanov::Cooling_Norm_Obs_Builder>>());
}

} // namespace core
} // namespace build
} // namespace rmt

#endif /* STEPHANOV_OBSERVABLE_BUILD_REGISTER_HPP */
