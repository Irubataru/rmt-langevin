
/*
 * Created: 08-04-2016
 * Modified: Sat 02 Sep 2017 16:03:38 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_BARYON_NUMBER_DENSITY_HPP
#define STEPHANOV_BARYON_NUMBER_DENSITY_HPP

#include <cmath>
#include <memory>
#include <simskel/named_observable.hpp>
#include <stephanov/resource/resource.hpp>
#include <types.hpp>

namespace rmt {
namespace stephanov {
namespace observable {

class Baryon_Number_Density
    : public ::rmt::observable::Named_Observable<rmt::numeric::complex_type>,
      public builder::builder_core::requires_resource_type
{
public:
  using value_type = rmt::numeric::complex_type;
  using number_type = rmt::numeric::number_type;
  using size_type =
      ::rmt::observable::Object_With_Record<value_type>::size_type;

  Baryon_Number_Density(const std::shared_ptr<const Matrix_Storage_Base> &mat,
                        Physical_Parameters par)
      : storage{mat},
        prefactor(static_cast<number_type>(par.number_of_flavours) /
                  static_cast<number_type>(M_size)),
        mu{par.chemical_potential}
  {
  }

  Baryon_Number_Density(const std::shared_ptr<const Matrix_Storage_Base> &mat,
                        Physical_Parameters par, size_type reserved_size)
      : Baryon_Number_Density{mat, par}
  {
    reserve(reserved_size);
  }

  virtual std::string name() const override
  {
    static const auto obs_name = std::string{"baryon number density"};
    return obs_name;
  }

  virtual std::string dir_name() const override
  {
    static const auto obs_dir_name = std::string{"baryon_number_density"};
    return obs_dir_name;
  }

private:
  virtual value_type calculate_value() override;

  std::shared_ptr<const Matrix_Storage_Base> storage;
  value_type prefactor;
  value_type mu;
};

} // namespace observable
} // namespace stephanov
} // namespace rmt

#endif /* STEPHANOV_BARYON_NUMBER_DENSITY_HPP */
