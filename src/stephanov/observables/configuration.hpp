
/*
 * Created: 27-04-2017
 * Modified: Fri 01 Sep 2017 10:41:31 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_OBSERVABLE_CONFIGURATION_HPP
#define STEPHANOV_OBSERVABLE_CONFIGURATION_HPP

#include <memory>
#include <simskel/named_observable.hpp>
#include <stephanov/resource/configuration_pair.hpp>
#include <stephanov/resource/resource.hpp>

namespace rmt {
namespace stephanov {
namespace observable {

class Configuration : public ::rmt::observable::Named_Observable<
                          ::rmt::stephanov::Configuration_Pair>,
                      public builder::builder_core::requires_resource_type
{
public:
  using value_type = ::rmt::stephanov::Configuration_Pair;
  using size_type =
      ::rmt::observable::Object_With_Record<value_type>::size_type;

  Configuration(const std::shared_ptr<const Matrix_Storage_Base> &mat)
      : storage{mat}
  {
  }

  Configuration(const std::shared_ptr<const Matrix_Storage_Base> &mat,
                size_type reserved_size)
      : Configuration{mat}
  {
    reserve(reserved_size);
  };

  virtual std::string name() const override
  {
    static const auto obs_name = std::string{"configuration"};
    return obs_name;
  }

private:
  virtual value_type calculate_value() override
  {
    using base_name = ::rmt::stephanov::M_name;
    return {storage->read(base_name::X), storage->read(base_name::XC)};
  }

  std::shared_ptr<const Matrix_Storage_Base> storage;
};

} // namespace observable
} // namespace stephanov
} // namespace rmt

#endif /* STEPHANOV_OBSERVABLE_CONFIGURATION_HPP */
