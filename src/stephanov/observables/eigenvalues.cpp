/*
 * Created: 08-04-2016
 * Modified: Fri 01 Sep 2017 10:55:18 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "eigenvalues.hpp"
#include <utilities/matrix/eigenvalues/eigenvalue_solver.hpp>

namespace rmt {
namespace stephanov {
namespace observable {

Eigenvalues::value_type Eigenvalues::calculate_value()
{
  using base_name = ::rmt::stephanov::M_name;

  matrix::Eigenvalue_Solver<
      matrix::precission_type<numeric::complex_type>::value,
      matrix::Symmetries::None>
      solver;

  solver.Compute(storage->read(base_name::G), true);

  return value_type{solver.Eigenvalues()};
}

} // namespace observable
} // namespace stephanov
} // namespace rmt
