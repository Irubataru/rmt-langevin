
/*
 * Created: 04-05-2017
 * Modified: Sat 02 Sep 2017 16:02:00 BST
 * STEPHANOV_OBSERVABLE_Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_OBSERVABLE_ACTION_SPLITS_HPP
#define STEPHANOV_OBSERVABLE_ACTION_SPLITS_HPP

#include "container_types.hpp"
#include <memory>
#include <simskel/named_observable.hpp>
#include <stephanov/resource/resource.hpp>

namespace rmt {
namespace stephanov {
namespace cartesian {

namespace observable {

class Action_Splits : public ::rmt::observable::Named_Observable<
                          ::rmt::stephanov::observable::Action_Splits_Storage>,
                      public builder::builder_core::requires_resource_type
{
public:
  using value_type = ::rmt::stephanov::observable::Action_Splits_Storage;
  using size_type = ::rmt::observable::Object_With_Record<value_type>::size_type;

  Action_Splits(const std::shared_ptr<const Matrix_Storage> &mat,
                Physical_Parameters par)
      : storage{mat}, number_of_flavours(par.number_of_flavours)
  {
  }

  Action_Splits(const std::shared_ptr<const Matrix_Storage> &mat,
                Physical_Parameters par, std::size_t reserved_size)
      : Action_Splits{mat, par}
  {
    reserve(reserved_size);
  };

  virtual std::string name() const override
  {
    static const auto obs_name = std::string{"complex action splits"};
    return obs_name;
  }

  virtual std::string dir_name() const override
  {
    static const auto obs_dir_name = std::string{"complex_action_splits"};
    return obs_dir_name;
  }

private:
  virtual value_type calculate_value() override;

  std::shared_ptr<const Matrix_Storage> storage;
  rmt::numeric::number_type number_of_flavours;
};

} // namespace observable

namespace shifted {
namespace observable {

class Action_Splits : public ::rmt::observable::Named_Observable<
                          ::rmt::stephanov::observable::Action_Splits_Storage>,
                      public builder::builder_core::requires_resource_type
{
public:
  using value_type = ::rmt::stephanov::observable::Action_Splits_Storage;
  using size_type = ::rmt::observable::Object_With_Record<value_type>::size_type;

  Action_Splits(const std::shared_ptr<const Matrix_Storage> &mat,
                Physical_Parameters par)
      : storage{mat}, number_of_flavours(par.number_of_flavours)
  {
  }

  Action_Splits(const std::shared_ptr<const Matrix_Storage> &mat,
                Physical_Parameters par, size_type reserved_size)
      : Action_Splits{mat, par}
  {
    reserve(reserved_size);
  };

  virtual std::string name() const override
  {
    static const auto obs_name = std::string{"complex action splits"};
    return obs_name;
  }

  virtual std::string dir_name() const override
  {
    static const auto obs_dir_name = std::string{"complex_action_splits"};
    return obs_dir_name;
  }

private:
  virtual value_type calculate_value() override;

  std::shared_ptr<const Matrix_Storage> storage;
  rmt::numeric::number_type number_of_flavours;
};

} // namespace observable 
} // namespace shifted 

} // namespace cartesian
} // namespace stephanov
} // namespace rmt

#endif /* STEPHANOV_OBSERVABLE_ACTION_SPLITS_HPP */
