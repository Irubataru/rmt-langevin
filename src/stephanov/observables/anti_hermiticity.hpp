/*
 * Created: 19-05-2016
 * Modified: Fri 01 Sep 2017 18:17:44 BST
 * STEPHANOV_Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_ANTI_HERMITICITY_OBSERVABLE_HPP
#define STEPHANOV_ANTI_HERMITICITY_OBSERVABLE_HPP

#include <simskel/named_observable.hpp>
#include <stephanov/resource/resource.hpp>
#include <utilities/matrix/trace.hpp>

namespace rmt {
namespace stephanov {
namespace cartesian {

class Anti_Hermiticity
    : public ::rmt::observable::Named_Observable<rmt::numeric::complex_type>
{
public:
  using value_type = rmt::numeric::complex_type;

  explicit Anti_Hermiticity(const Matrix_Storage_Base &s) : storage(s)
  {
  }

  virtual std::string name() const override
  {
    static const auto obs_name = std::string{"anti hermiticity"};
    return obs_name;
  }

  virtual std::string dir_name() const override
  {
    static const auto obs_dir_name = std::string{"anti_hermiticity"};
    return obs_dir_name;
  }

private:
  virtual value_type calculate_value() override
  {
    using base_name = rmt::stephanov::M_name;

    const auto &x = storage.read(base_name::X);
    const auto &xc = storage.read(base_name::XC);

    return rmt::matrix::Trace((blaze::ctrans(x) - xc) *
                              (x - blaze::ctrans(xc)));
  }

private:
  const Matrix_Storage_Base &storage;
};

} // namespace cartesian
} // namespace stephanov
} // namespace rmt

#include <builders/observable_builders/builder_factory.hpp>
#include <builders/observable_builders/stephanov/independent_observable_builders.hpp>

namespace rmt {
namespace build {
namespace core {

inline void register_stephanov_anti_hermiticity()
{
  using ::rmt::stephanov::observable::Anti_Hermiticity;
  auto &dict = Factory_Dictionary::instance();

  dict.add_factory(
      {"stephanov_anti-hermiticity"},
      std::make_shared<No_Param_Obs_Build_Factory<
          stephanov::Storage_Phys_Obs_Builder<Anti_Hermiticity>>>());
}

} // namespace core
} // namespace build
} // namespace rmt 

#endif /* STEPHANOV_ANTI_HERMITICITY_OBSERVABLE_HPP */
