/*
 * Created: 09-05-2016
 * Modified: Fri 01 Sep 2017 10:49:05 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_FORCE_MAX_HPP
#define STEPHANOV_FORCE_MAX_HPP

#include <simskel/named_observable.hpp>
#include <stephanov/langevin/force.hpp>

namespace rmt {
namespace stephanov {

namespace cartesian {
namespace observable {

class Force_Max
    : public ::rmt::observable::Named_Observable<rmt::numeric::number_type>,
      public builder::builder_core::requires_force_type
{
public:
  using value_type = rmt::numeric::number_type;
  using size_type =
      ::rmt::observable::Object_With_Record<value_type>::size_type;

  Force_Max(const std::shared_ptr<const Force_Base> &f_ref) : force{f_ref}
  {
  }

  Force_Max(const std::shared_ptr<const Force_Base> &f_ref,
            size_type reserved_size)
      : Force_Max{f_ref}
  {
    reserve(reserved_size);
  }

  virtual std::string name() const override
  {
    static const auto obs_name = std::string{"force max"};
    return obs_name;
  }

  virtual std::string dir_name() const override
  {
    static const auto obs_dir_name = std::string{"force_max"};
    return obs_dir_name;
  }

private:
  virtual value_type calculate_value() override;

  std::shared_ptr<const Force_Base> force;
};

} // namespace observable
} // namespace cartesian

namespace polar {
namespace observable {

class Force_Max
    : public ::rmt::observable::Named_Observable<rmt::numeric::number_type>,
      public builder::builder_core::requires_force_type
{
public:
  using value_type = rmt::numeric::number_type;
  using size_type =
      ::rmt::observable::Object_With_Record<value_type>::size_type;

  Force_Max(const std::shared_ptr<const Force> &f_ref) : force{f_ref}
  {
  }

  Force_Max(const std::shared_ptr<const Force> &f_ref,
            std::size_t reserved_size)
      : Force_Max{f_ref}
  {
    reserve(reserved_size);
  }

  virtual std::string name() const override
  {
    static const auto obs_name = std::string{"force max"};
    return obs_name;
  }

  virtual std::string dir_name() const override
  {
    static const auto obs_dir_name = std::string{"force_max"};
    return obs_dir_name;
  }

private:
  virtual value_type calculate_value() override;

  std::shared_ptr<const Force> force;
};

} // namespace observable
} // namespace polar

} // namespace stephanov
} // namespace rmt

#endif /* STEPHANOV_FORCE_MAX_HPP */
