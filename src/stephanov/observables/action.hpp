
/*
 * Created: 25-04-2017
 * Modified: Sat 02 Sep 2017 16:01:13 BST
 * STEPHANOV_OVBSERVABLES_Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_OVBSERVABLES_ACTION_HPP
#define STEPHANOV_OVBSERVABLES_ACTION_HPP

#include <memory>
#include <simskel/named_observable.hpp>
#include <stephanov/resource/resource.hpp>

namespace rmt {
namespace stephanov {
namespace observable {

class Action
    : public ::rmt::observable::Named_Observable<rmt::numeric::complex_type>,
      public builder::builder_core::requires_resource_type
{
public:
  using value_type = rmt::numeric::complex_type;
  using size_type =
      ::rmt::observable::Object_With_Record<value_type>::size_type;

  Action(const std::shared_ptr<const Matrix_Storage_Base> &mat,
         Physical_Parameters par)
      : storage{mat}, number_of_flavours(par.number_of_flavours)
  {
  }

  Action(const std::shared_ptr<const Matrix_Storage_Base> &mat,
         Physical_Parameters par, size_type reserved_size)
      : Action{mat, par}
  {
    reserve(reserved_size);
  };

  virtual std::string name() const override
  {
    static const auto obs_name = std::string{"complex action"};
    return obs_name;
  }

  virtual std::string dir_name() const override
  {
    static const auto obs_dir_name = std::string{"complex_action"};
    return obs_dir_name;
  }

private:
  virtual value_type calculate_value() override;

  std::shared_ptr<const Matrix_Storage_Base> storage;
  rmt::numeric::number_type number_of_flavours;
};

} // namespace observable
} // namespace stephanov
} // namespace rmt

#endif /* STEPHANOV_OVBSERVABLES_ACTION_HPP */
