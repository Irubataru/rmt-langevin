
/*
 * Created: 04-05-2017
 * Modified: Fri 05 May 2017 10:51:32 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_OBSERVABLE_CONTAINER_TYPES_HPP
#define STEPHANOV_OBSERVABLE_CONTAINER_TYPES_HPP

#include <types.hpp>

namespace rmt {
namespace stephanov {
namespace observable {

struct Action_Splits_Storage
{
  rmt::numeric::complex_type determinant;
  rmt::numeric::number_type bosonic_re_re;
  rmt::numeric::number_type bosonic_im_im;
};

struct Diagonal_Average_Storage
{
  rmt::numeric::complex_type av_diag_a;
  rmt::numeric::complex_type av_diag_b;
};

} // namespace observable
} // namespace stephanov
} // namespace rmt

#endif /* STEPHANOV_OBSERVABLE_CONTAINER_TYPES_HPP */
