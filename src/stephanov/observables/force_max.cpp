/*
 * Created: 09-05-2016
 * Modified: Fri 01 Sep 2017 10:55:41 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "force_max.hpp"

namespace rmt {
namespace stephanov {

namespace cartesian {
namespace observable {

Force_Max::value_type Force_Max::calculate_value()
{
  auto a_max = blaze::max(blaze::abs(force->get_force_a()));
  auto b_max = blaze::max(blaze::abs(force->get_force_b()));

  return (a_max > b_max) ? a_max : b_max;
}

} // namespace observable
} // namespace cartesian

namespace polar {
namespace observable {

Force_Max::value_type Force_Max::calculate_value()
{
  auto r_max = blaze::max(blaze::abs(force->get_force_r()));
  auto p_max = blaze::max(blaze::abs(force->get_force_phi()));

  return (r_max > p_max) ? r_max : p_max;
}

} // namespace observable
} // namespace polar

} // namespace stephanov
} // namespace rmt
