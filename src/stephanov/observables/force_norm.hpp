
/*
 * Created: 16-02-2017
 * Modified: Fri 01 Sep 2017 10:49:28 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_FORCE_NORM_HPP
#define STEPHANOV_FORCE_NORM_HPP

#include <simskel/named_observable.hpp>
#include <stephanov/langevin/force.hpp>

namespace rmt {
namespace stephanov {
namespace cartesian {
namespace observable {

class Force_Norm : public ::rmt::observable::Named_Observable<numeric::number_type>,
                   public builder::builder_core::requires_force_type
{
public:
  using value_type = numeric::number_type;
  using size_type =
      ::rmt::observable::Object_With_Record<value_type>::size_type;

  Force_Norm(const std::shared_ptr<const Force_Base> &f_ref) : force{f_ref}
  {
  }

  Force_Norm(const std::shared_ptr<const Force_Base> &f_ref,
             size_type reserved_size)
      : Force_Norm{f_ref}
  {
    reserve(reserved_size);
  }

  virtual std::string name() const override
  {
    static const auto obs_name = std::string{"force norm"};
    return obs_name;
  }

  virtual std::string dir_name() const override
  {
    static const auto obs_dir_name = std::string{"force_norm"};
    return obs_dir_name;
  }

private:
  virtual value_type calculate_value() override
  {
    using std::norm;
    using numeric::sqrt;
    auto norm_val = value_type{0.};

    const auto &a_ref = force->get_force_a();
    const auto &b_ref = force->get_force_b();

    for (auto i = 0ul; i < a_ref.rows(); ++i)
      for (auto j = 0ul; j < a_ref.columns(); ++j)
        norm_val += norm(a_ref(i, j)) + norm(b_ref(i, j));


    return sqrt(norm_val);
  }

  std::shared_ptr<const Force_Base> force;
};

} // namespace observable
} // namespace cartesian
} // namespace stephanov
} // namespace rmt

#endif /* STEPHANOV_FORCE_NORM_HPP */
