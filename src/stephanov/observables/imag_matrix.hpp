
/*
 * Created: 18-05-2016
 * Modified: Fri 01 Sep 2017 10:51:05 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_IMAG_MATRIX_HPP
#define STEPHANOV_IMAG_MATRIX_HPP

#include <simskel/named_observable.hpp>
#include <stephanov/resource/resource.hpp>

namespace rmt {
namespace stephanov {
namespace cartesian {
namespace observable {

class Imag_Matrix_Element
    : public ::rmt::observable::Named_Observable<
          std::pair<rmt::numeric::number_type, rmt::numeric::number_type>>
{
public:
  using number_type = rmt::numeric::number_type;
  using complex_type = rmt::numeric::complex_type;
  using value_type = std::pair<number_type, number_type>;

  Imag_Matrix_Element(const Matrix_Storage &s) : storage(s)
  {
  }

  virtual std::string name() const override
  {
    static const auto obs_name = std::string{"imag matrix element"};
    return obs_name;
  }

  virtual std::string dir_name() const override
  {
    static const auto obs_dir_name = std::string{"imag_matrix_element"};
    return obs_dir_name;
  }


private:
  virtual value_type calculate_value() override
  {
    using cart_name = rmt::stephanov::cartesian::M_name;

    auto max_a_val =
        blaze::max(blaze::abs(blaze::imag(storage.read(cart_name::A))));
    auto max_b_val =
        blaze::max(blaze::abs(blaze::imag(storage.read(cart_name::B))));

    return {max_a_val, max_b_val};
  }

private:
  const Matrix_Storage &storage;
};

} // namespace observable
} // namespace cartesian
} // namespace stephanov
} // namespace rmt

#endif /* STEPHANOV_IMAG_MATRIX_HPP */
