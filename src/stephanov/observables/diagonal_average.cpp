
/*
 * Created: 05-05-2017
 * Modified: Fri 01 Sep 2017 10:55:15 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "diagonal_average.hpp"
#include <utilities/matrix/trace.hpp>

namespace rmt {
namespace stephanov {

namespace cartesian {
namespace observable {

Diagonal_Average::value_type Diagonal_Average::calculate_value()
{
  using numeric::number_type;
  using strg_name = ::rmt::stephanov::cartesian::M_name;

  static constexpr auto one_over_n =
      number_type{1.} / static_cast<number_type>(M_size);

  return {one_over_n * matrix::Trace(storage->read(M_name::A)),
          one_over_n * matrix::Trace(storage->read(M_name::B))};
}

} // namespace observable
} // namespace cartesian

} // namespace stephanov
} // namespace rmt
