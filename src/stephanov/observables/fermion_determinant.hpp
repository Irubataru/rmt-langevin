/*
 * Created: 08-04-2016
 * Modified: Fri 01 Sep 2017 10:43:27 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_FERMION_DETERMINANT_HPP
#define STEPHANOV_FERMION_DETERMINANT_HPP

#include <memory>
#include <simskel/named_observable.hpp>
#include <stephanov/resource/resource.hpp>

namespace rmt {
namespace stephanov {
namespace observable {

class Fermion_Determinant
    : public ::rmt::observable::Named_Observable<rmt::numeric::complex_type>,
      public builder::builder_core::requires_resource_type
{
public:
  using value_type = rmt::numeric::complex_type;
  using block_matrix_type = rmt::block_matrix_type;
  using size_type =
      ::rmt::observable::Object_With_Record<value_type>::size_type;

  Fermion_Determinant(const std::shared_ptr<const Matrix_Storage_Base> &mat)
      : storage{mat}
  {
  }

  Fermion_Determinant(const std::shared_ptr<const Matrix_Storage_Base> &mat,
                      size_type reserved_size)
      : Fermion_Determinant{mat}
  {
    reserve(reserved_size);
  };

  virtual std::string name() const override
  {
    static const auto obs_name = std::string{"fermion determinant"};
    return obs_name;
  }

  virtual std::string dir_name() const override
  {
    static const auto obs_dir_name = std::string{"fermion_determinant"};
    return obs_dir_name;
  }

private:
  block_matrix_type construct_block_matrix() const;
  virtual value_type calculate_value() override;

  std::shared_ptr<const Matrix_Storage_Base> storage;
};

} // namespace observable
} // namespace stephanov
} // namespace rmt

#endif /* STEPHANOV_FERMION_DETERMINANT_HPP */
