/*
 * Created: 08-04-2016
 * Modified: Mon 11 Sep 2017 12:05:45 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "fermion_eigenvalues.hpp"
#include <utilities/matrix/eigenvalues/eigenvalue_solver.hpp>
#include <utilities/matrix/matrix_constructors.hpp>

using rmt::numeric::imag_i;

namespace rmt {
namespace stephanov {
namespace observable {

Fermion_Eigenvalues::value_type Fermion_Eigenvalues::calculate_value()
{
  matrix::Eigenvalue_Solver<
      matrix::precission_type<numeric::complex_type>::value,
      matrix::Symmetries::None>
      solver;

  solver.Compute(construct_block_matrix(), matrix::eigenvalues_only);

  return value_type{solver.Eigenvalues()};
}

Fermion_Eigenvalues::block_matrix_type
Fermion_Eigenvalues::construct_block_matrix() const
{
  using base_name = ::rmt::stephanov::M_name;

  auto block_matrix = matrix::Block_Matrix();

  blaze::submatrix(block_matrix, 0ul, 0ul, M_size, M_size) =
      storage->read_mass() * matrix::Identity_Matrix();

  blaze::submatrix(block_matrix, M_size, M_size, M_size, M_size) =
      storage->read_mass() * matrix::Identity_Matrix();

  blaze::submatrix(block_matrix, 0ul, M_size, M_size, M_size) =
      imag_i * storage->read(base_name::X) +
      storage->read(base_name::MU);

  blaze::submatrix(block_matrix, M_size, 0ul, M_size, M_size) =
      imag_i * storage->read(base_name::XC) +
      storage->read(base_name::MU);

  return block_matrix;
}

} // namespace observable
} // namespace stephanov
} // namespace rmt
