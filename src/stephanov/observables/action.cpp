
/*
 * Created: 02-05-2017
 * Modified: Fri 01 Sep 2017 10:54:01 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "action.hpp"
#include <utilities/matrix/trace.hpp>

namespace rmt {
namespace stephanov {
namespace observable {

Action::value_type Action::calculate_value()
{
  using base_name = ::rmt::stephanov::M_name;
  static const auto mat_size_d = numeric::number_type {M_size};

  const auto &x = storage->read(base_name::X);
  const auto &xc = storage->read(base_name::XC);

  return mat_size_d * rmt::matrix::Trace(xc * x) +
         number_of_flavours * numeric::log(blaze::det(storage->read(base_name::G)));
}

} // namespace observable
} // namespace stephanov
} // namespace rmt
