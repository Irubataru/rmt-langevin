/*
 * Created: 08-04-2016
 * Modified: Fri 01 Sep 2017 10:55:05 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "chiral_condensate.hpp"
#include <utilities/matrix/trace.hpp>

namespace rmt {
namespace stephanov {
namespace observable {

Chiral_Condensate::value_type Chiral_Condensate::calculate_value()
{
  using base_name = ::rmt::stephanov::M_name;
  return prefactor * rmt::matrix::Trace(storage->read(base_name::G));
}

} // namespace observable
} // namespace stephanov
} // namespace rmt
