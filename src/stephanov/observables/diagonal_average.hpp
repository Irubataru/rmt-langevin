
/*
 * Created: 05-05-2017
 * Modified: Fri 01 Sep 2017 10:42:39 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_OBSERVABLE_DIAGONAL_AVERAGE_HPP
#define STEPHANOV_OBSERVABLE_DIAGONAL_AVERAGE_HPP

#include "container_types.hpp"
#include <memory>
#include <simskel/named_observable.hpp>
#include <stephanov/resource/resource.hpp>

namespace rmt {
namespace stephanov {

namespace cartesian {
namespace observable {

class Diagonal_Average
    : public ::rmt::observable::Named_Observable<
          ::rmt::stephanov::observable::Diagonal_Average_Storage>,
      public builder::builder_core::requires_resource_type
{
public:
  using value_type = ::rmt::stephanov::observable::Diagonal_Average_Storage;
  using size_type =
      ::rmt::observable::Object_With_Record<value_type>::size_type;

  Diagonal_Average(const std::shared_ptr<const Matrix_Storage> &mat)
      : storage{mat}
  {
  }

  Diagonal_Average(const std::shared_ptr<const Matrix_Storage> &mat,
                   size_type reserved_size)
      : Diagonal_Average{mat}
  {
    reserve(reserved_size);
  };

  virtual std::string name() const override
  {
    static const auto obs_name = std::string{"diagonal averages cartesian"};
    return obs_name;
  }

  virtual std::string dir_name() const override
  {
    static const auto obs_dir_name = std::string{"diagonal_averages_cartesian"};
    return obs_dir_name;
  }

private:
  virtual value_type calculate_value() override;

  std::shared_ptr<const Matrix_Storage> storage;
};

} // namespace observable
} // namespace cartesian

} // namespace stephanov
} // namespace rmt

#endif /* STEPHANOV_OBSERVABLE_DIAGONAL_AVERAGE_HPP */
