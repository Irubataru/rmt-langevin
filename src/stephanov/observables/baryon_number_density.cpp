/*
 * Created: 08-04-2016
 * Modified: Wed 13 Sep 2017 16:38:26 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "baryon_number_density.hpp"
#include <utilities/matrix/identity_constructor.hpp>
#include <utilities/matrix/trace.hpp>

using rmt::numeric::imag_i;

namespace rmt {
namespace stephanov {
namespace observable {

Baryon_Number_Density::value_type Baryon_Number_Density::calculate_value()
{
  using n = rmt::numeric::number_type;
  using base_name = ::rmt::stephanov::M_name;
  using rmt::matrix::Identity_Matrix;

  const auto &g_ref = storage->read(base_name::G);
  const auto &x_ref = storage->read(base_name::X);
  const auto &xc_ref = storage->read(base_name::XC);
  const auto &mu_ref = storage->read(base_name::MU);

  return -prefactor *
         rmt::matrix::Trace(g_ref * (n{2.} * mu_ref +
                                     imag_i * (x_ref + xc_ref)));
}

} // namespace observable
} // namespace stephanov
} // namespace rmt
