/*
 * Created: 08-04-2016
 * Modified: Fri 01 Sep 2017 10:55:11 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "determinant.hpp"

namespace rmt {
namespace stephanov {
namespace observable {

Determinant::value_type Determinant::calculate_value()
{
  using base_name = ::rmt::stephanov::M_name;
  return rmt::numeric::number_type{1.} /
         blaze::det(storage->read(base_name::G));
}

} // namespace observable
} // namespace splittorff
} // namespace rmt
