
/*
 * Created: 02-05-2017
 * Modified: Fri 01 Sep 2017 10:53:39 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_OBSERVABLE_STORAGE_HPP
#define STEPHANOV_OBSERVABLE_STORAGE_HPP

#include <memory>
#include <simskel/named_observable.hpp>
#include <stephanov/resource/configuration_pair.hpp>
#include <stephanov/resource/resource.hpp>

namespace rmt {
namespace stephanov {

namespace cartesian {
namespace observable {

class Storage : public ::rmt::observable::Named_Observable<Storage_Pair>,
                public builder::builder_core::requires_resource_type
{
public:
  using value_type = ::rmt::stephanov::cartesian::Storage_Pair;
  using resource_type = ::rmt::stephanov::cartesian::Matrix_Storage;
  using size_type =
      ::rmt::observable::Object_With_Record<value_type>::size_type;

  Storage(const std::shared_ptr<const resource_type> &mat) : storage{mat}
  {
  }

  Storage(const std::shared_ptr<const resource_type> &mat,
          size_type reserved_size)
      : Storage{mat}
  {
    reserve(reserved_size);
  };

  virtual std::string name() const override
  {
    static const auto obs_name = std::string{"configuration"};
    return obs_name;
  }

private:
  virtual value_type calculate_value() override
  {
    using storage_name = M_name;
    return {storage->read(storage_name::A), storage->read(storage_name::B)};
  }

  std::shared_ptr<const resource_type> storage;
};

} // namespace observable
} // namespace cartesian

namespace polar {
namespace observable {

class Storage : public ::rmt::observable::Named_Observable<Storage_Pair>,
                public builder::builder_core::requires_resource_type
{
public:
  using value_type = ::rmt::stephanov::polar::Storage_Pair;
  using resource_type = ::rmt::stephanov::polar::Matrix_Storage;
  using size_type =
      ::rmt::observable::Object_With_Record<value_type>::size_type;

  Storage(const std::shared_ptr<const resource_type> &mat) : storage{mat}
  {
  }

  Storage(const std::shared_ptr<const resource_type> &mat,
          std::size_t reserved_size)
      : Storage{mat}
  {
    reserve(reserved_size);
  };

  virtual std::string name() const override
  {
    static const auto obs_name = std::string{"configuration"};
    return obs_name;
  }

private:
  virtual value_type calculate_value() override
  {
    using storage_name = M_name;
    return {storage->read(storage_name::R), storage->read(storage_name::PHI)};
  }

  std::shared_ptr<const resource_type> storage;
};

} // namespace observable
} // namespace polar

} // namespace stephanov
} // namespace rmt

#endif /* STEPHANOV_OBSERVABLE_STORAGE_HPP */
