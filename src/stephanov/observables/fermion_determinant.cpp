/*
 * Created: 08-04-2016
 * Modified: Mon 11 Sep 2017 12:05:23 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "fermion_determinant.hpp"
#include <utilities/matrix/matrix_constructors.hpp>

using rmt::numeric::imag_i;

namespace rmt {
namespace stephanov {
namespace observable {

Fermion_Determinant::value_type Fermion_Determinant::calculate_value()
{
  return blaze::det(construct_block_matrix());
}

Fermion_Determinant::block_matrix_type
Fermion_Determinant::construct_block_matrix() const
{
  using base_name = ::rmt::stephanov::M_name;

  auto block_matrix = matrix::Block_Matrix();

  blaze::submatrix(block_matrix, 0ul, 0ul, M_size, M_size) =
      storage->read_mass() * matrix::Identity_Matrix();

  blaze::submatrix(block_matrix, M_size, M_size, M_size, M_size) =
      storage->read_mass() * matrix::Identity_Matrix();

  blaze::submatrix(block_matrix, 0ul, M_size, M_size, M_size) =
      imag_i * storage->read(base_name::X) +
      storage->read(base_name::MU);

  blaze::submatrix(block_matrix, M_size, 0ul, M_size, M_size) =
      imag_i * storage->read(base_name::XC) +
      storage->read(base_name::MU);

  return block_matrix;
}

} // namespace observable
} // namespace splittorff
} // namespace rmt
