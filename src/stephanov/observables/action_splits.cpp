
/*
 * Created: 04-05-2017
 * Modified: Fri 01 Sep 2017 10:54:25 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "action_splits.hpp"
#include <utilities/matrix/trace.hpp>

namespace rmt {
namespace stephanov {

namespace cartesian {
namespace observable {

Action_Splits::value_type Action_Splits::calculate_value()
{
  using base_name = ::rmt::stephanov::M_name;
  using strg_name = ::rmt::stephanov::cartesian::M_name;
  using blaze::trans;
  using blaze::imag;
  using blaze::real;

  static const auto mat_size_d = numeric::number_type{M_size};

  const auto &a = storage->read(strg_name::A);
  const auto &b = storage->read(strg_name::B);

  return {
      number_of_flavours *
          numeric::log(blaze::det(storage->read(base_name::G))),
      mat_size_d *
          (rmt::matrix::Trace(blaze::trans(blaze::real(a)) * blaze::real(a)) +
           rmt::matrix::Trace(blaze::trans(blaze::real(b)) * blaze::real(b))),
      -mat_size_d *
          (rmt::matrix::Trace(blaze::trans(blaze::imag(a)) * blaze::imag(a)) +
           rmt::matrix::Trace(blaze::trans(blaze::imag(b)) * blaze::imag(b)))};
}


} // namespace observable

namespace shifted {
namespace observable {

Action_Splits::value_type Action_Splits::calculate_value()
{
  using base_name = ::rmt::stephanov::M_name;
  using strg_name = ::rmt::stephanov::cartesian::M_name;
  using blaze::trans;
  using blaze::imag;
  using blaze::real;

  static const auto mat_size_d = numeric::number_type{M_size};

  const auto &a = storage->read(strg_name::A);
  const auto &b = storage->read(strg_name::B);

  return {
      number_of_flavours *
          numeric::log(blaze::det(storage->read(base_name::G))),
      mat_size_d *
          (rmt::matrix::Trace(blaze::trans(blaze::real(a)) * blaze::real(a)) +
           rmt::matrix::Trace(blaze::trans(blaze::real(b)) * blaze::real(b))),
      -mat_size_d *
          (rmt::matrix::Trace(
               blaze::trans(blaze::imag(
                   a +
                   numeric::imag_i *
                       matrix::Identity_Matrix(storage->read_mu()))) *
               blaze::imag(a +
                           numeric::imag_i *
                               matrix::Identity_Matrix(storage->read_mu()))) +
           rmt::matrix::Trace(blaze::trans(blaze::imag(b)) * blaze::imag(b)))};
}

} // namespace observable 
} // namespace shifted

} // namespace cartesian

} // namespace stephanov
} // namespace rmt
