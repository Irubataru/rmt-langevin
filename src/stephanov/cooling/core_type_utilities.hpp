
/*
 * Created: 24-02-2017
 * Modified: Thu 18 May 2017 16:35:47 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_CORE_TYPE_UTILITIES_HPP
#define STEPHANOV_CORE_TYPE_UTILITIES_HPP

#include "core_types.hpp"

namespace rmt {
namespace stephanov {
namespace cartesian {
namespace cooling {

template <typename Norm_Type, typename... Args>
inline std::shared_ptr<Simple_Cooling_Step>
Make_Simple_Cooling_Step(std::shared_ptr<Matrix_Storage> s,
                         const Simple_Core_Parameters &cp, Args &&... norm_args)
{
  using core_type = Simple_Cooling_Step::core_type;
  using index_alg_type = core_type::index_alg_type;
  using index_conv_alg_type = core_type::index_conv_alg_type;
  using min_alg_type = core_type::min_alg_type;

  auto norm_ptr = std::make_shared<Norm_Type>(std::forward<Args>(norm_args)...);

  return std::make_shared<Simple_Cooling_Step>(
      s, core_type{index_alg_type{norm_ptr, cp.derivative_epsilon},
                   index_conv_alg_type{},
                   min_alg_type{norm_ptr, cp.alpha_step, cp.max_alpha,
                                cp.max_alpha_step_iterations}});
}

template <typename Norm_Type, typename... Args>
inline std::shared_ptr<Fixed_Length_Cooling_Step>
Make_Fixed_Length_Cooling_Step(std::shared_ptr<Matrix_Storage> s,
                               const Fixed_Length_Core_Parameters &cp,
                               Args &&... norm_args)
{
  using core_type = Fixed_Length_Cooling_Step::core_type;
  using index_alg_type = core_type::index_alg_type;
  using index_conv_alg_type = core_type::index_conv_alg_type;
  using min_alg_type = core_type::min_alg_type;

  auto norm_ptr = std::make_shared<Norm_Type>(std::forward<Args>(norm_args)...);

  return std::make_shared<Fixed_Length_Cooling_Step>(
      s, core_type{index_alg_type{norm_ptr, cp.derivative_epsilon},
                   index_conv_alg_type{}, min_alg_type{cp.stepsize}});
}

template <typename Norm_Type, typename... Args>
inline std::shared_ptr<Fixed_Length_Analytic_Cooling_Step>
Make_Fixed_Length_Analytic_Cooling_Step(
    std::shared_ptr<Matrix_Storage> s,
    const Fixed_Length_Analytic_Core_Parameters &cp, Args &&... norm_args)
{
  using core_type = Fixed_Length_Analytic_Cooling_Step::core_type;
  using index_alg_type = core_type::index_alg_type;
  using index_conv_alg_type = core_type::index_conv_alg_type;
  using min_alg_type = core_type::min_alg_type;

  auto norm_ptr = std::make_shared<Norm_Type>(std::forward<Args>(norm_args)...);

  return std::make_shared<Fixed_Length_Analytic_Cooling_Step>(
      s, core_type{index_alg_type{norm_ptr}, index_conv_alg_type{},
                   min_alg_type{cp.stepsize}});
}

template <typename Norm_Type, typename... Args>
inline std::shared_ptr<Analytic_Minimise_Cooling_Step>
Make_Analytic_Minimise_Cooling_Step(std::shared_ptr<Matrix_Storage> s,
                                    const Analytic_Minimise_Core_Parameters &cp,
                                    Args &&... norm_args)
{
  using core_type = Analytic_Minimise_Cooling_Step::core_type;
  using index_alg_type = core_type::index_alg_type;
  using index_conv_alg_type = core_type::index_conv_alg_type;
  using min_alg_type = core_type::min_alg_type;

  auto norm_ptr = std::make_shared<Norm_Type>(std::forward<Args>(norm_args)...);

  return std::make_shared<Analytic_Minimise_Cooling_Step>(
      s, core_type{index_alg_type{norm_ptr}, index_conv_alg_type{},
                   min_alg_type{norm_ptr, cp.alpha_step, cp.max_alpha,
                                cp.max_alpha_step_iterations}});
}

} // namespace cooling
} // namespace cartesian

} // namespace stephanov
} // namespace rmt

#endif /* STEPHANOV_CORE_TYPE_UTILITIES_HPP */
