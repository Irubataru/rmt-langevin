/*
 * STEPHANOV_Created: 05-06-2016
 * Modified: Thu 16 Feb 2017 16:24:51 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_COOLING_TYPES_HPP
#define STEPHANOV_COOLING_TYPES_HPP

#include <types.hpp>

namespace rmt {
namespace stephanov {
namespace cooling {

namespace norm {

struct Norm_Arguments
{
  const M_type &x;
  const M_type &xc;
  const rmt::numeric::complex_type &mu;
  const rmt::numeric::complex_type &mass;
};

} // namespace norm

namespace core {

using Generator_Indices = std::vector<numeric::number_type>;
using Generator_Matrix = M_type;

struct Transformation_Matrices
{
  M_type g, g_inverse;
};

} // namespace core 

} // namespace cooling
} // namespace stephanov
} // namespace rmt

#endif /* STEPHANOV_COOLING_TYPES_HPP */
