/*
 * Created: 05-06-2016
 * Modified: Thu 16 Feb 2017 19:18:03 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_COOLING_NORMS_HPP
#define STEPHANOV_COOLING_NORMS_HPP

#include "cooling_norm/aggregate_norm.hpp"
#include "cooling_norm/eigenvalue_norm.hpp"
#include "cooling_norm/quartic_norm.hpp"
#include "cooling_norm/hermiticity_norm.hpp"

namespace rmt {
namespace stephanov {
namespace cooling {
namespace norm {

using Anti_Hermiticity_Norm = Quartic_Norm;

} // namespace norm
} // namespace cooling
} // namespace stephanov
} // namespace rmt

#endif /* STEPHANOV_COOLING_NORMS_HPP */
