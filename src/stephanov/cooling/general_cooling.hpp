/*
 * Created: 05-06-2016
 * Modified: Mon 06 Nov 2017 09:43:43 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_GENERAL_COOLING_HPP
#define STEPHANOV_GENERAL_COOLING_HPP

#include "cooling_types.hpp"
#include <simskel/update/update_int.hpp>
#include <stephanov/resource/resource.hpp>

namespace rmt {
namespace stephanov {
namespace cooling {

namespace core {

template <typename Index_Algorithm, typename Convertion_Algorithm,
          typename Minimise_Algorithm>
class Cooling_Core
{
public:
  using number_type = rmt::numeric::number_type;
  using norm_arg_type = norm::Norm_Arguments;

  using index_alg_type = Index_Algorithm;
  using index_conv_alg_type = Convertion_Algorithm;
  using min_alg_type = Minimise_Algorithm;

  Cooling_Core(const Index_Algorithm &index_algorithm,
               const Convertion_Algorithm &conversion_algorithm,
               const Minimise_Algorithm &minimise_algorithm)
      : idx_alg(index_algorithm),
        conversion_alg(conversion_algorithm),
        min_alg(minimise_algorithm)
  {
  }

  Transformation_Matrices
  get_transformation(const std::shared_ptr<Matrix_Storage_Base> &storage)
  {
    auto norm_args = construct_norm_args(storage);

    auto idx = compute_indices(norm_args);
    auto g_matrix = compute_matrix(idx);
    return minimise_norm(norm_args, g_matrix);
  }

private: /* functions */
  norm_arg_type
  construct_norm_args(const std::shared_ptr<Matrix_Storage_Base> &storage)
  {
    using base_name = ::rmt::stephanov::M_name;

    return {storage->read(base_name::X), storage->read(base_name::XC),
            storage->read_mu(), storage->read_mass()};
  }

  Generator_Indices compute_indices(const norm_arg_type &norm_args)
  {
    return idx_alg(norm_args);
  }

  Generator_Matrix compute_matrix(const Generator_Indices &idx)
  {
    return conversion_alg(idx);
  }

  Transformation_Matrices minimise_norm(const norm_arg_type &norm_args,
                                        const Generator_Matrix &g_matrix)
  {
    return min_alg(norm_args, g_matrix);
  }

private: /* variables */
  Index_Algorithm idx_alg;
  Convertion_Algorithm conversion_alg;
  Minimise_Algorithm min_alg;
};

namespace cartesian {

template <typename Core>
class General_Cartesian_Cooling_Step
    : public ::simskel::Simple_Update,
      public builder::builder_core::requires_resource_type
{
private:
  using storage_type = ::rmt::stephanov::cartesian::Matrix_Storage;
  using number_type = rmt::numeric::number_type;

public:
  explicit General_Cartesian_Cooling_Step(
      const std::shared_ptr<storage_type> &s, const Core &c)
      : storage{s}, core{c}
  {
  }

  virtual ~General_Cartesian_Cooling_Step()
  {
  }

private: /* functions */
  virtual void DoUpdate() override
  {
    using cart_name = ::rmt::stephanov::cartesian::M_name;
    using n = number_type;
    using rmt::numeric::imag_i;

    auto transforms = core.get_transformation(storage);

    const auto &g = transforms.g;
    const auto &gi = transforms.g_inverse;

    auto &a = storage->read_write(cart_name::A);
    auto &b = storage->read_write(cart_name::B);

    auto a_copy = static_cast<M_type>(a);
    auto b_copy = static_cast<M_type>(b);

    a = n{0.5} *
        (g * a_copy * gi + blaze::trans(gi) * a_copy * blaze::trans(g) +
         imag_i *
             (g * b_copy * gi - blaze::trans(gi) * b_copy * blaze::trans(g)));
    b = n{0.5} *
        (g * b_copy * gi + blaze::trans(gi) * b_copy * blaze::trans(g) -
         imag_i *
             (g * a_copy * gi - blaze::trans(gi) * a_copy * blaze::trans(g)));

    storage->update();
  }

private: /* variables */
  std::shared_ptr<storage_type> storage;
  Core core;
};

} // namespace cartesian

} // namespace core

} // namespace cooling
} // namespace stephanov
} // namespace rmt

#endif /* STEPHANOV_GENERAL_COOLING_HPP */
