
/*
 * STEPHANOV_COOLING_Created: 05-06-2016
 * Modified: Fri 24 Feb 2017 14:12:08 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_COOLING_CORE_TYPES_HPP
#define STEPHANOV_COOLING_CORE_TYPES_HPP

#include "core/index_computation_algorithms.hpp"
#include "core/index_conversion_algorithm.hpp"
#include "core/transformation_conversion_algorithms.hpp"

#include "general_cooling.hpp"

namespace rmt {
namespace stephanov {

namespace cooling {
namespace core {

using Simple_Core = Cooling_Core<Numerical_Derivative_IdxCmpAlg,
                                 Advanced_Index_Conversion_Algorithm,
                                 Brent_Minimisation_Algorithm>;

using Fixed_Length_Core = Cooling_Core<Numerical_Derivative_IdxCmpAlg,
                                       Advanced_Index_Conversion_Algorithm,
                                       Fixed_Minimisation_Algorithm>;

using Fixed_Length_Analytic_Core =
    Cooling_Core<Analytic_Derivative_IdxCmpAlg,
                 Advanced_Index_Conversion_Algorithm,
                 Fixed_Minimisation_Algorithm>;

using Analytic_Minimise_Core = Cooling_Core<Analytic_Derivative_IdxCmpAlg,
                                            Advanced_Index_Conversion_Algorithm,
                                            Brent_Minimisation_Algorithm>;

} // namespace core
} // namespace cooling

namespace cartesian {
namespace cooling {

struct Simple_Core_Parameters
{
  rmt::numeric::number_type derivative_epsilon;
  rmt::numeric::number_type alpha_step;
  rmt::numeric::number_type max_alpha;
  std::size_t max_alpha_step_iterations;
};

class Simple_Cooling_Step final
    : public ::rmt::stephanov::cooling::core::cartesian::
          General_Cartesian_Cooling_Step<
              ::rmt::stephanov::cooling::core::Simple_Core>
{
public:
  using core_type = ::rmt::stephanov::cooling::core::Simple_Core;
  using parent_type = ::rmt::stephanov::cooling::core::cartesian::
      General_Cartesian_Cooling_Step<core_type>;
  using storage_type = ::rmt::stephanov::cartesian::Matrix_Storage;

  explicit Simple_Cooling_Step(std::shared_ptr<storage_type> &s, core_type core)
      : parent_type{s, std::move(core)}
  {
  }
};

struct Fixed_Length_Core_Parameters
{
  rmt::numeric::number_type derivative_epsilon;
  rmt::numeric::number_type stepsize;
};

class Fixed_Length_Cooling_Step final
    : public ::rmt::stephanov::cooling::core::cartesian::
          General_Cartesian_Cooling_Step<
              ::rmt::stephanov::cooling::core::Fixed_Length_Core>
{
public:
  using core_type = ::rmt::stephanov::cooling::core::Fixed_Length_Core;
  using parent_type = ::rmt::stephanov::cooling::core::cartesian::
      General_Cartesian_Cooling_Step<core_type>;
  using storage_type = ::rmt::stephanov::cartesian::Matrix_Storage;

  explicit Fixed_Length_Cooling_Step(std::shared_ptr<storage_type> &s,
                                     core_type core)
      : parent_type{s, std::move(core)}
  {
  }
};

struct Fixed_Length_Analytic_Core_Parameters
{
  rmt::numeric::number_type stepsize;
};

class Fixed_Length_Analytic_Cooling_Step final
    : public ::rmt::stephanov::cooling::core::cartesian::
          General_Cartesian_Cooling_Step<
              ::rmt::stephanov::cooling::core::Fixed_Length_Analytic_Core>
{
public:
  using core_type = ::rmt::stephanov::cooling::core::Fixed_Length_Analytic_Core;
  using parent_type = ::rmt::stephanov::cooling::core::cartesian::
      General_Cartesian_Cooling_Step<core_type>;
  using storage_type = ::rmt::stephanov::cartesian::Matrix_Storage;

  explicit Fixed_Length_Analytic_Cooling_Step(std::shared_ptr<storage_type> &s,
                                              core_type core)
      : parent_type{s, std::move(core)}
  {
  }
};

struct Analytic_Minimise_Core_Parameters
{
  rmt::numeric::number_type alpha_step;
  rmt::numeric::number_type max_alpha;
  std::size_t max_alpha_step_iterations;
};

class Analytic_Minimise_Cooling_Step final
    : public ::rmt::stephanov::cooling::core::cartesian::
          General_Cartesian_Cooling_Step<
              ::rmt::stephanov::cooling::core::Analytic_Minimise_Core>
{
public:
  using core_type = ::rmt::stephanov::cooling::core::Analytic_Minimise_Core;
  using parent_type = ::rmt::stephanov::cooling::core::cartesian::
      General_Cartesian_Cooling_Step<core_type>;
  using storage_type = ::rmt::stephanov::cartesian::Matrix_Storage;

  explicit Analytic_Minimise_Cooling_Step(std::shared_ptr<storage_type> &s, core_type core)
      : parent_type{s, std::move(core)}
  {
  }
};


} // namespace cooling
} // namespace cartesian

} // namespace stephanov
} // namespace rmt

#endif /* STEPHANOV_COOLING_CORE_TYPES_HPP */
