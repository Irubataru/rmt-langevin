
/*
 * Created: 16-02-2017
 * Modified: Sat 02 Sep 2017 14:56:29 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_NORM_INTERFACE_HPP
#define STEPHANOV_NORM_INTERFACE_HPP

#include "../cooling_types.hpp"
#include <types.hpp>
#include <blaze/math/CompressedMatrix.h>

namespace rmt {
namespace stephanov {
namespace cooling {
namespace norm {

class Norm : public Simulation_Cooling_Norm
{
public:
  using number_type = rmt::numeric::number_type;
  using arg_type = Norm_Arguments;

  number_type operator()(const arg_type &args)
  {
    return compute(args);
  }

  virtual number_type compute(const arg_type &args) = 0;

  virtual ~Norm()
  {
  }
};

// TODO: Move to a separate header file to minimise include dependencies
class Derivative_Norm : public Norm
{
public:
  using number_type = Norm::number_type;
  using complex_type = rmt::numeric::complex_type;
  using arg_type = Norm_Arguments;
  using sparse_gen_type = blaze::CompressedMatrix<complex_type>;

  virtual number_type derivative(const arg_type &args,
                                 const M_type &generator) = 0;

  virtual number_type derivative(const arg_type &args,
                                 const sparse_gen_type &generator) = 0;

  virtual ~Derivative_Norm()
  {
  }
};

} // namespace norm
} // namespace cooling
} // namespace stephanov
} // namespace rmt

#endif /* STEPHANOV_NORM_INTERFACE_HPP */
