
/*
 * Created: 02-09-2017
 * Modified: Sat 02 Sep 2017 16:44:07 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_NORM_BUILD_REGISTER_HPP
#define STEPHANOV_NORM_BUILD_REGISTER_HPP

#include <builders/norm_builders/norm_builder.hpp>
#include <builders/norm_builders/specific_builders.hpp>
#include <stephanov/cooling/cooling_norms.hpp>
#include <utilities/json_parser_helpers.hpp>

namespace rmt {
namespace build {
namespace core {

void register_stephanov_cooling_norms()
{
  using ::rmt::stephanov::cooling::norm::Quartic_Norm;
  using ::rmt::stephanov::cooling::norm::Anti_Hermiticity_Norm;
  using ::rmt::stephanov::cooling::norm::Hermiticity_Norm;
  using ::rmt::stephanov::cooling::norm::Aggregate_Norm;

  auto model_name = std::string{parse::to_string(Model_Type::Stephanov) + "_"};
  auto &dict = Norm_Builder_Dictionary::instance();

  dict.add(model_name + "hermiticity",
           std::make_shared<No_Argument_Norm_Builder<Hermiticity_Norm>>());

  dict.add(model_name + "quartic",
           std::make_shared<No_Argument_Norm_Builder<Quartic_Norm>>());

  dict.add(model_name + "anti-hermiticity",
           std::make_shared<No_Argument_Norm_Builder<Anti_Hermiticity_Norm>>());

  dict.add(model_name + "eigenvalue",
           std::make_shared<stephanov::Eigenvalue_Norm_Builder>());

  dict.add(model_name + "aggregate",
           std::make_shared<stephanov::Aggregate_Norm_Builder>());
}

} // namespace core
} // namespace build
} // namespace rmt

#endif /* STEPHANOV_NORM_BUILD_REGISTER_HPP */
