
/*
 * Created: 20-01-2017
 * Modified: Fri 24 Feb 2017 12:59:13 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_HERMITICITY_NORM_HPP
#define STEPHANOV_HERMITICITY_NORM_HPP

#include "norm_interface.hpp"
#include <utilities/matrix/trace.hpp>

namespace rmt {
namespace stephanov {
namespace cooling {
namespace norm {

class Hermiticity_Norm : public Derivative_Norm
{
public:
  using number_type = Norm::number_type;
  using arg_type = Norm::arg_type;
  using sparse_gen_type = Derivative_Norm::sparse_gen_type;

  virtual number_type compute(const arg_type &args) override
  {
    return matrix::Trace(blaze::ctrans(args.xc - blaze::ctrans(args.x)) *
                         (args.xc - blaze::ctrans(args.x)))
               .real() /
           number_type{M_size};
  }

  virtual number_type derivative(const arg_type &args,
                                 const M_type &gen) override
  {
    return derivative_impl(args, gen);
  }

  virtual number_type derivative(const arg_type &args,
                                 const sparse_gen_type &gen) override
  {
    return derivative_impl(args, gen);
  }

private:
  template <typename Mat_Type>
  number_type derivative_impl(const arg_type &args, const Mat_Type &gen)
  {
    // Fully reduced
    return matrix::Trace(
               blaze::ctrans(args.xc) * (gen * args.xc - args.xc * gen) +
               blaze::ctrans(args.x) * (gen * args.x - args.x * gen))
               .real() *
           number_type{2.} / number_type{M_size};
  }
};

} // namespace norm
} // namespace cooling
} // namespace stephanov
} // namespace rmt

#endif /* STEPHANOV_HERMITICITY_NORM_HPP */
