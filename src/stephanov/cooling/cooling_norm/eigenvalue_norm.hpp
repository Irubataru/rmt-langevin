/*
 * Created: 07-06-2016
 * Modified: Thu 16 Feb 2017 14:47:41 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_COOLING_EIGENVALUE_NORM_HPP
#define STEPHANOV_COOLING_EIGENVALUE_NORM_HPP

#include "norm_interface.hpp"
#include <functional>
#include <vector>

namespace rmt {
namespace stephanov {
namespace cooling {
namespace norm {

class Eigenvalue_Norm : public Norm
{
public:
  using number_type = Norm::number_type;
  using arg_type = Norm::arg_type;
  using block_matrix_type = rmt::block_matrix_type;


  struct param_type
  {
    number_type eigenvalue_weight;
    std::size_t number_of_eigenvalues;
  };

  template <typename Function>
  Eigenvalue_Norm(param_type params, Function &&eig_func)
      : xi{params.eigenvalue_weight},
        number_of_eigenvalues{params.number_of_eigenvalues},
        compute_eigenvalues{std::forward<Function>(eig_func)}
  {
  }

  template <typename Function>
  Eigenvalue_Norm(number_type eig_weight, std::size_t num_eigs,
                  Function &&eig_func)
      : xi{eig_weight},
        number_of_eigenvalues{num_eigs},
        compute_eigenvalues{std::forward<Function>(eig_func)}
  {
  }

  virtual number_type compute(const arg_type &args) override;

private:
  block_matrix_type construct_block_diagonal(const Norm_Arguments &args);

private:
  number_type xi;
  std::size_t number_of_eigenvalues;

  std::function<std::vector<number_type>(const block_matrix_type &,
                                         std::size_t)>
      compute_eigenvalues;
};

} // namespace norm
} // namespace cooling
} // namespace stephanov
} // namespace rmt

#endif /* STEPHANOV_COOLING_EIGENVALUE_NORM_HPP */
