/*
 * Created: 05-06-2016
 * Modified: Tue 30 May 2017 12:15:42 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_COOLING_QUARTIC_NORM_HPP
#define STEPHANOV_COOLING_QUARTIC_NORM_HPP

#include "norm_interface.hpp"
#include <utilities/matrix/identity_constructor.hpp>
#include <utilities/matrix/trace.hpp>

namespace rmt {
namespace stephanov {
namespace cooling {
namespace norm {

class Quartic_Norm : public Norm
{
public:
  using number_type = Norm::number_type;
  using arg_type = Norm::arg_type;

  virtual number_type compute(const arg_type &args) override
  {
    using numeric::imag_i;

    auto X = static_cast<M_type>(imag_i * args.x +
                                 args.mu * matrix::Identity_Matrix());
    auto Y = static_cast<M_type>(imag_i * args.xc +
                                 args.mu * matrix::Identity_Matrix());

    auto XYd = static_cast<M_type>(X + blaze::ctrans(Y));
    auto XYd2 = static_cast<M_type>(blaze::ctrans(XYd) * XYd);

    return matrix::Trace(XYd2 * XYd2).real() / static_cast<number_type>(M_size);
  }
};

} // namespace norm
} // namespace cooling
} // namespace stephanov
} // namespace rmt

#endif /* STEPHANOV_COOLING_QUARTIC_NORM_HPP */
