/*
 * Created: 14-06-2016
 * Modified: Mon 06 Nov 2017 15:20:55 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "eigenvalue_norm.hpp"
#include <utilities/matrix/block_constructors.hpp>
#include <utilities/matrix/identity_constructor.hpp>

using rmt::numeric::imag_i;

namespace rmt {
namespace stephanov {
namespace cooling {
namespace norm {

Eigenvalue_Norm::number_type
Eigenvalue_Norm::compute(const Eigenvalue_Norm::arg_type &args)
{
  using rmt::numeric::exp;

  auto eigen_values = compute_eigenvalues(construct_block_diagonal(args),
                                          number_of_eigenvalues);
  number_type norm = number_type{0.};

  for (auto eig : eigen_values)
    norm += exp(-xi * eig);

  return norm;
}

Eigenvalue_Norm::block_matrix_type
Eigenvalue_Norm::construct_block_diagonal(const Norm_Arguments &args)
{
  auto result = matrix::Block_Zero_Matrix();

  auto X = static_cast<M_type>(imag_i * args.x +
                               args.mu * matrix::Identity_Matrix());
  auto Y = static_cast<M_type>(imag_i * args.xc +
                               args.mu * matrix::Identity_Matrix());

  auto Xd = static_cast<complex_column_matrix_type>(blaze::ctrans(X));
  auto Yd = static_cast<complex_column_matrix_type>(blaze::ctrans(Y));

  blaze::submatrix(result, 0ul, 0ul, M_size, M_size) =
      std::norm(args.mass) *
          blaze::IdentityMatrix<numeric::complex_type>(M_size) +
      Yd * Y;

  blaze::submatrix(result, M_size, M_size, M_size, M_size) =
      std::norm(args.mass) *
          blaze::IdentityMatrix<numeric::complex_type>(M_size) +
      Xd * X;

  blaze::submatrix(result, 0ul, M_size, M_size, M_size) =
      std::conj(args.mass) * X + args.mass * blaze::ctrans(Y);

  blaze::submatrix(result, M_size, 0ul, M_size, M_size) =
      args.mass * blaze::ctrans(X) + std::conj(args.mass) * Y;

  return result;
}

} // namespace norm
} // namespace cooling
} // namespace stephanov
} // namespace rmt
