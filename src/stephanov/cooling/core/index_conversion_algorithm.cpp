
/*
 * Created: 16-02-2017
 * Modified: Fri 17 Feb 2017 13:13:34 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "index_conversion_algorithm.hpp"
#include <utilities/matrix/default_constructor.hpp>

namespace rmt {
namespace stephanov {
namespace cooling {
namespace core {

using number_type = Advanced_Index_Conversion_Algorithm::number_type;
using arg_type = Advanced_Index_Conversion_Algorithm::arg_type;
using return_type = Advanced_Index_Conversion_Algorithm::return_type;
using numeric::imag_i;

return_type Advanced_Index_Conversion_Algorithm::operator()(const arg_type &idx)
{
  using n = number_type;
  using rmt::numeric::sqrt;

  if (idx.size() != (M_size * M_size))
    throw std::runtime_error{"In Advanced_Index_Conversion_Algorithm: Wrong "
                             "number of generator indices"};

  auto result = matrix::Zero_Matrix();
  auto idx_it = idx.begin();

  // U(1) matrices

  auto v = sqrt(2./static_cast<n>(M_size)) * (*idx_it);

  for (auto i = 0ul; i < M_size; ++i)
    result(i,i) += v;

  ++idx_it;

  // SU(N) matrices

  // Compute the diagonal elements
  for (auto i = 1u; i < M_size; ++i) {
    auto d = static_cast<number_type>(i + 1);
    auto f = sqrt(n{2.} / (d * (d - n{1.})));
    auto v = *idx_it;

    for (auto j = 0u; j < i; ++j)
      result(j, j) += f * v;

    result(i, i) += f * (n{1.} - d) * v;

    ++idx_it;
  }

  // Compute off-diagonal elements
  for (auto i = 0u; i < M_size; ++i) {
    for (auto j = i + 1; j < M_size; ++j) {
      result(i, j) = *idx_it;
      result(j, i) = *idx_it;
      ++idx_it;

      result(i, j) += -(*idx_it) * imag_i;
      result(j, i) += +(*idx_it) * imag_i;
      ++idx_it;
    }
  }

  return result;
}

} // namespace core
} // namespace cooling
} // namespace stephanov
} // namespace rmt
