
/*
 * Created: 16-02-2017
 * Modified: Thu 16 Feb 2017 16:26:19 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_INDEX_CONVERSION_ALGORITHM_HPP
#define STEPHANOV_INDEX_CONVERSION_ALGORITHM_HPP

#include "../cooling_types.hpp"

namespace rmt {
namespace stephanov {
namespace cooling {
namespace core {

struct Advanced_Index_Conversion_Algorithm
{
  using number_type = numeric::number_type;
  using arg_type = Generator_Indices;
  using return_type = Generator_Matrix;

  return_type operator()(const arg_type &);
};

} // namespace core
} // namespace cooling
} // namespace stephanov
} // namespace rmt

#endif /* STEPHANOV_INDEX_CONVERSION_ALGORITHM_HPP */
