
/*
 * Created: 16-02-2017
 * Modified: Tue 07 Mar 2017 16:13:10 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "transformation_conversion_algorithms.hpp"
#include <utilities/matrix/default_constructor.hpp>
#include <utilities/matrix/eigenvalues/eigenvalue_solver.hpp>
#include <utilities/minimum/minimisers.hpp>

#include <iostream>

namespace rmt {
namespace stephanov {
namespace cooling {
namespace core {

using eig_vals_type = real_vector_type;
using eig_vecs_type = M_type;

namespace {

std::pair<M_type, M_type>
Exponensiate_Matrix(const eig_vecs_type &eigen_vectors,
                    const eig_vals_type &eigen_values,
                    const numeric::number_type &exp_parameter)
{
  using rmt::numeric::exp;

  auto diagonal = matrix::Zero_Matrix();
  auto diagonal_i = matrix::Zero_Matrix();

  for (auto i = 0u; i < M_size; ++i) {
    diagonal(i, i) = exp(-exp_parameter * eigen_values[i]);
    diagonal_i(i, i) = exp(exp_parameter * eigen_values[i]);
  }

  return {static_cast<M_type>(eigen_vectors * diagonal *
                              blaze::ctrans(eigen_vectors)),
          static_cast<M_type>(eigen_vectors * diagonal_i *
                              blaze::ctrans(eigen_vectors))};
}
}

using brent_norm_arg_type = Brent_Minimisation_Algorithm::norm_arg_type;
using brent_arg_type = Brent_Minimisation_Algorithm::arg_type;
using brent_return_type = Brent_Minimisation_Algorithm::return_type;

brent_return_type Brent_Minimisation_Algorithm::
operator()(const brent_norm_arg_type &args, const brent_arg_type &alg_mat)
{
  matrix::Eigenvalue_Solver<
      matrix::precission_type<numeric::complex_type>::value,
      matrix::Symmetries::Hermitian>
      solver;

  solver.Compute(alg_mat, matrix::eigenvalues_and_eigenvectors);

  auto eigen_values = solver.Eigenvalues();
  auto eigen_vectors = solver.Eigenvectors();

  auto min_func = [&eigen_values, &eigen_vectors, &args,
                   this](number_type alpha) {
    auto trans_mat = Exponensiate_Matrix(eigen_vectors, eigen_values, alpha);

    auto x_temp = M_type{trans_mat.first * args.x * trans_mat.second};
    auto xc_temp = M_type{trans_mat.first * args.xc * trans_mat.second};

    auto norm_args = norm_arg_type{x_temp, xc_temp, args.mu, args.mass};

    return norm_ptr->compute(norm_args);
  };

  minimise::Brent_Minimisation min_alg{{}};

  auto result = min_alg.minimise({0., alpha, max_it, max_alpha}, {1e-3, max_it},
                                 min_func);

  auto trafo_pair =
      Exponensiate_Matrix(eigen_vectors, eigen_values, result.min);

  return {std::move(trafo_pair.first), std::move(trafo_pair.second)};
}

using fixed_norm_arg_type = Brent_Minimisation_Algorithm::norm_arg_type;
using fixed_arg_type = Brent_Minimisation_Algorithm::arg_type;
using fixed_return_type = Brent_Minimisation_Algorithm::return_type;

fixed_return_type Fixed_Minimisation_Algorithm::
operator()(const fixed_norm_arg_type &args, const fixed_arg_type &alg_mat)
{
  matrix::Eigenvalue_Solver<
      matrix::precission_type<numeric::complex_type>::value,
      matrix::Symmetries::Hermitian>
      solver;

  solver.Compute(alg_mat, matrix::eigenvalues_and_eigenvectors);

  auto eigen_values = solver.Eigenvalues();
  auto eigen_vectors = solver.Eigenvectors();

  auto trafo_pair =
      Exponensiate_Matrix(eigen_vectors, eigen_values, delta);

  return {std::move(trafo_pair.first), std::move(trafo_pair.second)};
}

} // namespace core
} // namespace cooling
} // namespace stephanov
} // namespace rmt
