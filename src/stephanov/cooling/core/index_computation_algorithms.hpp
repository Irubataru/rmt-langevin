
/*
 * Created: 16-02-2017
 * Modified: Thu 02 Nov 2017 13:48:21 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_INDEX_COMPUTATION_ALGORITHMS_HPP
#define STEPHANOV_INDEX_COMPUTATION_ALGORITHMS_HPP

#include "../cooling_norm/norm_interface.hpp"
#include <memory>

namespace rmt {
namespace stephanov {
namespace cooling {
namespace core {

struct Numerical_Derivative_IdxCmpAlg
{
  using number_type = numeric::number_type;
  using arg_type = norm::Norm_Arguments;
  using storage_type = Generator_Indices;

  explicit Numerical_Derivative_IdxCmpAlg(
      const std::shared_ptr<norm::Norm> &nptr, const number_type &eps)
      : norm_ptr{nptr}, epsilon{eps}
  {
  }

  Numerical_Derivative_IdxCmpAlg(Numerical_Derivative_IdxCmpAlg const &) =
      default;

  storage_type operator()(const arg_type &args);

private: /* functions */
  number_type compute_derivative(const arg_type &args, const number_type &norm0,
                                 const number_type &eps)
  {
    return (norm_ptr->compute(args) - norm0) / eps;
  }

  storage_type compute_indices(const arg_type &args, const number_type &eps);

private:
  std::shared_ptr<norm::Norm> norm_ptr;
  number_type epsilon;
};

struct Normalised_Numerical_Derivative_IdxCmpAlg
    : public Numerical_Derivative_IdxCmpAlg
{
  using parent_type = Numerical_Derivative_IdxCmpAlg;
  using parent_type::parent_type;

  using arg_type = parent_type::arg_type;
  using storage_type = parent_type::storage_type;

  storage_type operator()(const arg_type &);
};

struct Analytic_Derivative_IdxCmpAlg
{
  using number_type = numeric::number_type;
  using arg_type = norm::Norm_Arguments;
  using storage_type = Generator_Indices;

  explicit Analytic_Derivative_IdxCmpAlg(
      const std::shared_ptr<norm::Derivative_Norm> &nptr)
      : norm_ptr{nptr}
  {
  }

  Analytic_Derivative_IdxCmpAlg(Analytic_Derivative_IdxCmpAlg const &) =
      default;

  storage_type operator()(const arg_type &args);

private:
  std::shared_ptr<norm::Derivative_Norm> norm_ptr;
};

} // namespace core
} // namespace cooling
} // namespace stephanov
} // namespace rmt

#endif /* STEPHANOV_INDEX_COMPUTATION_ALGORITHMS_HPP */
