
/*
 * Created: 16-02-2017
 * Modified: Thu 23 Feb 2017 12:06:31 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_TRANSFORMATION_CONVERSION_ALGORITHMS_HPP
#define STEPHANOV_TRANSFORMATION_CONVERSION_ALGORITHMS_HPP

#include "../cooling_norm/norm_interface.hpp"
#include <memory>

namespace rmt {
namespace stephanov {
namespace cooling {
namespace core {

struct Brent_Minimisation_Algorithm
{
public:
  using number_type = rmt::numeric::number_type;
  using norm_arg_type = norm::Norm_Arguments;
  using arg_type = Generator_Matrix;
  using return_type = Transformation_Matrices;

  explicit Brent_Minimisation_Algorithm(
      const std::shared_ptr<norm::Norm> &nptr, number_type aa,
      number_type max_aa, std::size_t max_iterations = 10)
      : norm_ptr{nptr}, alpha{aa}, max_alpha{max_aa}, max_it{max_iterations}
  {
  }

  return_type operator()(const norm_arg_type &args, const arg_type &g);

private:
  std::shared_ptr<norm::Norm> norm_ptr;
  number_type alpha;
  number_type max_alpha;
  std::size_t max_it;
};

struct Fixed_Minimisation_Algorithm
{
public:
  using number_type = rmt::numeric::number_type;
  using norm_arg_type = norm::Norm_Arguments;
  using arg_type = Generator_Matrix;
  using return_type = Transformation_Matrices;

  explicit Fixed_Minimisation_Algorithm(number_type dd)
      : delta{dd}
  {
  }

  return_type operator()(const norm_arg_type &args, const arg_type &g);

private:
  number_type delta;
};

} // namespace core
} // namespace cooling
} // namespace stephanov
} // namespace rmt

#endif /* STEPHANOV_TRANSFORMATION_CONVERSION_ALGORITHMS_HPP */
