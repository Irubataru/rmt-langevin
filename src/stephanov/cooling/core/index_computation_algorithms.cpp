
/*
 * Created: 16-02-2017
 * Modified: Fri 24 Feb 2017 14:06:54 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "index_computation_algorithms.hpp"
#include <blaze/math/CompressedMatrix.h>
#include <utilities/matrix/default_constructor.hpp>


#include <iostream>

namespace rmt {
namespace stephanov {
namespace cooling {
namespace core {

namespace {

std::vector<numeric::number_type> Normalise(std::vector<numeric::number_type> v)
{
  using numeric::number_type;
  using numeric::abs;
  using numeric::sqrt;

  auto norm =
      sqrt(std::accumulate(v.begin(), v.end(), number_type{0.},
                           [](const number_type &val, const number_type &c) {
                             return val + c * c;
                           }));

  std::transform(v.begin(), v.end(), v.begin(),
                 [norm](const number_type &d) { return d / (sqrt(norm) + number_type{1.}); });

  return v;
}

}

using storage_type = Numerical_Derivative_IdxCmpAlg::storage_type;
using arg_type = Numerical_Derivative_IdxCmpAlg::arg_type;
using number_type = Numerical_Derivative_IdxCmpAlg::number_type;

storage_type Numerical_Derivative_IdxCmpAlg::
operator()(const arg_type &args)
{
  return compute_indices(args, epsilon);
};

// TODO: Add a compiler switch between using sparse and dense matrices for the
// generator depending on M_size

storage_type
Numerical_Derivative_IdxCmpAlg::compute_indices(const arg_type &args,
                                                    const number_type &eps)
{
  using rmt::numeric::sqrt;
  using numeric::imag_i;
  using n = number_type;

  auto g_indices = storage_type{};
  g_indices.reserve(M_size * M_size);

  auto norm_temporary = number_type{};

  try {
    norm_temporary = norm_ptr->compute(args);
  } catch (std::runtime_error &run_err) {
    std::ostringstream err_msg;
    err_msg << "In Numerical_Derivative_IdxCmpAlg: Error constructing "
               "norm0, source:\n";
    err_msg << run_err.what();

    throw std::runtime_error{err_msg.str()};
  }

  const auto norm0 = norm_temporary;

  M_type x_temp = matrix::Matrix();
  M_type xc_temp = matrix::Matrix();

  auto temp_args = arg_type{x_temp, xc_temp, args.mu, args.mass};

  // U(1) matrix

  {
    blaze::CompressedMatrix<numeric::complex_type> generator(M_size, M_size);
    generator.reserve(M_size);

    for (auto i = 0ul; i < M_size; ++i) {
      generator.append(i, i, sqrt(2./static_cast<n>(M_size)) * eps);
      generator.finalize(i);
    }

    x_temp =
        static_cast<M_type>(args.x + generator * args.x - args.x * generator);
    xc_temp = static_cast<M_type>(args.xc + generator * args.xc -
                                  args.xc * generator);

    g_indices.push_back(compute_derivative(temp_args, norm0, eps));
  }

  // SU(N) matrices

  // - Diagonal matrices

  for (auto i = 1u; i < M_size; ++i) {
    blaze::CompressedMatrix<numeric::complex_type> generator(M_size, M_size);
    generator.reserve(i + 1);

    auto d = static_cast<number_type>(i + 1u);
    auto f = sqrt(n{2.} / (d * (d - n{1.})));

    for (auto j = 0u; j < i; ++j) {
      generator.append(j, j, f * eps);
      generator.finalize(j);
    }

    generator.append(i, i, f * (n{1.} - d) * eps);
    generator.finalize(i);

    x_temp =
        static_cast<M_type>(args.x + generator * args.x - args.x * generator);
    xc_temp = static_cast<M_type>(args.xc + generator * args.xc -
                                  args.xc * generator);

    g_indices.push_back(compute_derivative(temp_args, norm0, eps));
  }

  // - Off-diagonal matrices

  for (auto i = 0u; i < M_size; ++i) {
    for (auto j = i + 1; j < M_size; ++j) {
      blaze::CompressedMatrix<numeric::complex_type> generator(M_size, M_size);
      generator.reserve(2);

      generator.insert(i, j, eps);
      generator.finalize(i);
      generator.insert(j, i, eps);
      generator.finalize(j);

      x_temp =
          static_cast<M_type>(args.x + generator * args.x - args.x * generator);
      xc_temp = static_cast<M_type>(args.xc + generator * args.xc -
                                    args.xc * generator);
      g_indices.push_back(compute_derivative(temp_args, norm0, eps));

      generator(i, j) = -eps * imag_i;
      generator(j, i) = +eps * imag_i;
      x_temp =
          static_cast<M_type>(args.x + generator * args.x - args.x * generator);
      xc_temp = static_cast<M_type>(args.xc + generator * args.xc -
                                    args.xc * generator);
      g_indices.push_back(compute_derivative(temp_args, norm0, eps));
    }
  }

  //for (auto ind : g_indices)
    //std::cout << ind << ", ";
  //std::cout << std::endl;

  return g_indices;
}

storage_type Normalised_Numerical_Derivative_IdxCmpAlg::
operator()(const arg_type &args)
{
  return Normalise(parent_type::operator()(args));
};


storage_type Analytic_Derivative_IdxCmpAlg::
operator()(const arg_type &args)
{
  using rmt::numeric::sqrt;
  using numeric::imag_i;
  using n = number_type;

  auto g_indices = storage_type{};
  g_indices.reserve(M_size * M_size);

  // U(1) matrix

  {
    blaze::CompressedMatrix<numeric::complex_type> generator(M_size, M_size);
    generator.reserve(M_size);

    for (auto i = 0ul; i < M_size; ++i) {
      generator.append(i, i, sqrt(2./static_cast<n>(M_size)));
      generator.finalize(i);
    }

    g_indices.push_back(norm_ptr->derivative(args, generator));
  }

  // SU(N) matrices

  // - Diagonal matrices

  for (auto i = 1u; i < M_size; ++i) {
    blaze::CompressedMatrix<numeric::complex_type> generator(M_size, M_size);
    generator.reserve(i + 1);

    auto d = static_cast<number_type>(i + 1u);
    auto f = sqrt(n{2.} / (d * (d - n{1.})));

    for (auto j = 0u; j < i; ++j) {
      generator.append(j, j, f);
      generator.finalize(j);
    }

    generator.append(i, i, f * (n{1.} - d));
    generator.finalize(i);

    g_indices.push_back(norm_ptr->derivative(args, generator));
  }

  // - Off-diagonal matrices

  for (auto i = 0u; i < M_size; ++i) {
    for (auto j = i + 1; j < M_size; ++j) {
      blaze::CompressedMatrix<numeric::complex_type> generator(M_size, M_size);
      generator.reserve(2);

      generator.insert(i, j, n{1.});
      generator.finalize(i);
      generator.insert(j, i, n{1.});
      generator.finalize(j);

      g_indices.push_back(norm_ptr->derivative(args, generator));

      generator(i, j) = -imag_i;
      generator(j, i) = +imag_i;

      g_indices.push_back(norm_ptr->derivative(args, generator));
    }
  }

  //for (auto ind : g_indices)
    //std::cout << ind << ", ";
  //std::cout << std::endl;

  return g_indices;
};

} // namespace core
} // namespace cooling
} // namespace stephanov
} // namespace rmt
