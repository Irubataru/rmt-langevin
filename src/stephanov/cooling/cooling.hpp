
/*
 * Created: 16-02-2017
 * Modified: Fri 24 Feb 2017 14:10:43 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_COOLING_COLLECTION_HPP
#define STEPHANOV_COOLING_COLLECTION_HPP

#include "core_types.hpp"
#include "core_type_utilities.hpp"
#include "cooling_norms.hpp"


#endif /* STEPHANOV_COOLING_COLLECTION_HPP */
