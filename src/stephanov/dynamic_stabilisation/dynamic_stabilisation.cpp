
/*
 * Created: 20-09-2017
 * Modified: Wed 20 Sep 2017 16:48:27 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "dynamic_stabilisation.hpp"

namespace rmt {
namespace stephanov {
namespace cartesian {

void Dynamic_Stabilisation::DoUpdate()
{
  using cart_name = ::rmt::stephanov::cartesian::M_name;

  auto step_length = stepsize_->value;

  auto & a_mat = storage_->read_write(cart_name::A);
  auto & b_mat = storage_->read_write(cart_name::B);

  a_mat -= step_length * alpha_ * numeric::imag_i * blaze::pow(blaze::imag(a_mat), power_);
  b_mat -= step_length * alpha_ * numeric::imag_i * blaze::pow(blaze::imag(b_mat), power_);
}

} // namespace cartesian
} // namespace stephanov
} // namespace rmt
