
/*
 * Created: 20-09-2017
 * Modified: Wed 20 Sep 2017 16:28:36 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_DYNAMIC_STABILISATION_HPP
#define STEPHANOV_DYNAMIC_STABILISATION_HPP

#include <simskel/update/update_int.hpp>
#include <stephanov/resource/resource.hpp>
#include <utilities/stepsize/stepsize_keeper.hpp>

namespace rmt {
namespace stephanov {
namespace cartesian {

class Dynamic_Stabilisation : public ::simskel::Simple_Update
{
public:
  Dynamic_Stabilisation(std::shared_ptr<Matrix_Storage> storage,
                        std::shared_ptr<const stepsize::Stepsize> stepsize,
                        numeric::number_type alpha, unsigned power)
      : storage_{storage},
        power_{power},
        alpha_{alpha},
        stepsize_{stepsize} {};

  virtual ~Dynamic_Stabilisation() {};

private:
  void DoUpdate() override;

private:
  std::shared_ptr<Matrix_Storage> storage_;

  unsigned power_;
  numeric::number_type alpha_;
  std::shared_ptr<const stepsize::Stepsize> stepsize_;
};

} // namespace cartesian
} // namespace stephanov
} // namespace rmt

#endif /* STEPHANOV_DYNAMIC_STABILISATION_HPP */
