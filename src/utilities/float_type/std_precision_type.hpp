/*
 * Created: 16-06-2016
 * Modified: Tue 01 Nov 2016 16:00:38 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STD_PRECISION_TYPE_HPP
#define STD_PRECISION_TYPE_HPP

namespace rmt {
namespace numeric {

using singleprecision_type = double;

} // namespace numeric
} // namespace rmt

#endif /* STD_PRECISION_TYPE_HPP */
