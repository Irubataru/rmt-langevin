/*
 * Created: 10-06-2016
 * Modified: Tue 14 Feb 2017 10:57:03 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef MULTIPRECISION_TYPE_HPP
#define MULTIPRECISION_TYPE_HPP

#include <limits>

//#include<boost/multiprecision/cpp_dec_float.hpp>
#include <boost/multiprecision/gmp.hpp>
//#include<boost/multiprecision/mpfr.hpp>

namespace rmt {
namespace numeric {

// using multiprecision_type
//= boost::multiprecision::number<
// boost::multiprecision::cpp_dec_float<500>,
// boost::multiprecision::et_off>;

using multiprecision_type =
    boost::multiprecision::number<boost::multiprecision::gmp_float<500>,
                                  boost::multiprecision::et_off>;

// using multiprecision_type
//= boost::multiprecision::number<
// boost::multiprecision::mpfr_float_backend<500,
// boost::multiprecision::allocate_dynamic>,
// boost::multiprecision::et_off>;

} // namespace numeric
} // namespace rmt

#endif /* MULTIPRECISION_TYPE_HPP */
