/*
 * Created: 16-06-2016
 * Modified: Thu 16 Jun 2016 14:22:49 CEST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STD_TO_STRING_HPP
#define STD_TO_STRING_HPP

#include <boost/lexical_cast.hpp>
#include <utilities/float_type/std_precision_type.hpp>

namespace rmt {
namespace numeric {

inline singleprecision_type from_string(const std::string &str)
{
  return boost::lexical_cast<singleprecision_type>(str);
}

inline singleprecision_type from_string(const char *str)
{
  return boost::lexical_cast<singleprecision_type>(str);
}

} // namespace numeric
} // namespace rmt

#endif /* STD_TO_STRING_HPP */
