/*
 * Created: 16-06-2016
 * Modified: Thu 16 Jun 2016 14:23:00 CEST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef BOOST_TO_STRING_HPP
#define BOOST_TO_STRING_HPP

#include <utilities/float_type/multiprecision_type.hpp>

namespace rmt {
namespace numeric {

inline multiprecision_type from_string(const std::string &str)
{
  return multiprecision_type{str};
}

inline multiprecision_type from_string(const char *str)
{
  return multiprecision_type{str};
}

} // namespace numeric
} // namespace rmt

#endif /* BOOST_TO_STRING_HPP */
