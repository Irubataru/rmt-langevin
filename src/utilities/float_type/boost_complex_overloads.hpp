/*
 * Created: 14-06-2016
 * Modified: Sun 03 Jul 2016 22:01:27 CEST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef BOOST_COMPLEX_OVERLOADS_HPP
#define BOOST_COMPLEX_OVERLOADS_HPP

#include <complex>
#include <utilities/float_type/multiprecision_type.hpp>

namespace rmt {
namespace numeric {

inline multiprecision_type abs(const std::complex<multiprecision_type> &n)
{
  using boost::multiprecision::abs;
  return abs(n);
}

inline multiprecision_type fabs(const std::complex<multiprecision_type> &n)
{
  using boost::multiprecision::fabs;
  return fabs(n);
}

inline std::complex<multiprecision_type>
sqrt(const std::complex<multiprecision_type> &n)
{
  using boost::multiprecision::sqrt;
  return sqrt(n);
}

inline std::complex<multiprecision_type>
exp(const std::complex<multiprecision_type> &n)
{
  using boost::multiprecision::exp;
  return exp(n);
}

inline std::complex<multiprecision_type>
log(const std::complex<multiprecision_type> &n)
{
  using boost::multiprecision::log;
  return log(n);
}

inline std::complex<multiprecision_type>
pow(const std::complex<multiprecision_type> &n, const multiprecision_type &p)
{
  using boost::multiprecision::pow;
  return pow(n, p);
}

inline std::complex<multiprecision_type>
pow(const multiprecision_type &n, const std::complex<multiprecision_type> &p)
{
  using boost::multiprecision::pow;
  return pow(n, p);
}

inline std::complex<multiprecision_type>
pow(const std::complex<multiprecision_type> &n,
    const std::complex<multiprecision_type> &p)
{
  using boost::multiprecision::pow;
  return pow(n, p);
}

inline std::complex<multiprecision_type>
cosh(const std::complex<multiprecision_type> &n)
{
  using boost::multiprecision::cosh;
  return cosh(n);
}

inline std::complex<multiprecision_type>
sinh(const std::complex<multiprecision_type> &n)
{
  using boost::multiprecision::sinh;
  return sinh(n);
}

} // namespace numeric
} // namespace rmt

#endif /* BOOST_COMPLEX_OVERLOADS_HPP */
