/*
 * Created: 14-06-2016
 * Modified: Tue 28 Jun 2016 21:41:37 CEST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef BOOST_REAL_OVERLOADS_HPP
#define BOOST_REAL_OVERLOADS_HPP

#include <utilities/float_type/multiprecision_type.hpp>

namespace rmt {
namespace numeric {

inline multiprecision_type abs(const multiprecision_type &n)
{
  using boost::multiprecision::abs;
  return abs(n);
}

inline multiprecision_type fabs(const multiprecision_type &n)
{
  using boost::multiprecision::fabs;
  return fabs(n);
}

inline multiprecision_type sqrt(const multiprecision_type &n)
{
  using boost::multiprecision::sqrt;
  return sqrt(n);
}

inline multiprecision_type exp(const multiprecision_type &n)
{
  using boost::multiprecision::exp;
  return exp(n);
}

inline multiprecision_type log(const multiprecision_type &n)
{
  using boost::multiprecision::log;
  return log(n);
}

inline multiprecision_type pow(const multiprecision_type &n,
                               const multiprecision_type &p)
{
  using boost::multiprecision::pow;
  return pow(n, p);
}

inline multiprecision_type cosh(const multiprecision_type &n)
{
  using boost::multiprecision::cosh;
  return cosh(n);
}

inline multiprecision_type sinh(const multiprecision_type &n)
{
  using boost::multiprecision::sinh;
  return sinh(n);
}

} // namespace numeric
} // namespace rmt

#endif /* BOOST_REAL_OVERLOADS_HPP */
