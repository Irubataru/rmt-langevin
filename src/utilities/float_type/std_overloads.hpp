/*
 * Created: 14-06-2016
 * Modified: Mon 05 Dec 2016 14:36:01 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef BOOST_REAL_OVERLOADS_HPP
#define BOOST_REAL_OVERLOADS_HPP

#include <cmath>
#include <complex>
#include <utilities/float_type/std_precision_type.hpp>

namespace rmt {
namespace numeric {

template <typename Numeric>
inline auto abs(const Numeric &n) -> decltype(std::abs(n))
{
  using std::abs;
  return abs(n);
}

template <typename Numeric>
inline auto fabs(const Numeric &n) -> decltype(std::fabs(n))
{
  using std::fabs;
  return fabs(n);
}

template <typename NL, typename NR>
inline auto max(const NL &lhs, const NR &rhs) -> decltype(std::max(lhs, rhs))
{
  using std::max;
  return max(lhs, rhs);
}

template <typename Numeric>
inline auto sqrt(const Numeric &n) -> decltype(std::sqrt(n))
{
  using std::sqrt;
  return sqrt(n);
}

template <typename Numeric>
inline auto exp(const Numeric &n) -> decltype(std::exp(n))
{
  using std::exp;
  return exp(n);
}

template <typename Numeric>
inline auto log(const Numeric &n) -> decltype(std::log(n))
{
  using std::log;
  return log(n);
}

template <typename Numeric, typename Exponent>
inline auto pow(const Numeric &n, const Exponent &p) -> decltype(std::pow(n, p))
{
  using std::pow;
  return pow(n, p);
}

template <typename Numeric>
inline auto cosh(const Numeric &n) -> decltype(std::cosh(n))
{
  using std::cosh;
  return cosh(n);
}

template <typename Numeric>
inline auto sinh(const Numeric &n) -> decltype(std::sinh(n))
{
  using std::sinh;
  return sinh(n);
}

template <typename Numeric>
inline auto cos(const Numeric &n) -> decltype(std::cos(n))
{
  using std::cos;
  return cos(n);
}

template <typename Numeric>
inline auto sin(const Numeric &n) -> decltype(std::sin(n))
{
  using std::sin;
  return sin(n);
}

} // namespace numeric
} // namespace rmt

#endif /* BOOST_REAL_OVERLOADS_HPP */
