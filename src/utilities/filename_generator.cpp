
/*
 * Created: 31-08-2017
 * Modified: Thu 31 Aug 2017 19:22:31 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "filename_generator.hpp"
#include "filename_parser.hpp"

namespace rmt {
namespace build {

bool Filename_Generator::is_valid() const
{
  Model_Parameters mparm;
  mparm.model = Model_Type::Stephanov;
  mparm.storage = Storage_Type::Cartesian;
  mparm.mu_scan = {0.0};
  mparm.mass_scan = {0.0};

  Physical_Parameters phparm;
  phparm.chemical_potential = 0.0;
  phparm.fermion_mass = 0.0;

  return is_valid(phparm, mparm);
}

bool Filename_Generator::is_valid(Physical_Parameters const &phparm,
                                  Model_Parameters const &mparm) const
{
  filename::parameter_symbols symbols{phparm, mparm};
  symbols.add_dummy("obs");

  std::string file_name;

  auto last_slash = pattern_.find_last_of("/");
  if(last_slash == std::string::npos)
    file_name = pattern_;
  else
   file_name = pattern_.substr(last_slash);

  if (file_name.find(std::string{"@{obs}"}) == std::string::npos)
    return false;

  return filename::parse(pattern_, symbols).second;
}

std::string Filename_Generator::filename(Physical_Parameters const &phparm,
                                         Model_Parameters const &mparm,
                                         std::string obs_name) const
{
  if (!is_valid(phparm, mparm))
    throw std::runtime_error{"Filename_Generator::filename - invalid pattern"};

  filename::parameter_symbols symbols{phparm, mparm};
  symbols.add("obs", obs_name);

  return filename::parse(pattern_, symbols).first;
}

std::string Filename_Generator::directory(Physical_Parameters const &phparm,
                                          Model_Parameters const &mparm) const
{
  if (!is_valid(phparm, mparm))
    throw std::runtime_error{"Filename_Generator::directory - invalid pattern"};

  filename::parameter_symbols symbols{phparm, mparm};
  symbols.add_dummy("obs");

  auto parsed = filename::parse(pattern_, symbols).first;
  return parsed.substr(0, parsed.find_last_of("/"));
}

} // namespace build
} // namespace rmt
