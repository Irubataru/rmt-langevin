/*
 * Created: 22-04-2016
 * Modified: Sun 12 Jun 2016 17:30:53 CEST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "filename_grammar.hpp"
#include <boost/lexical_cast.hpp>
#include <boost/spirit/include/phoenix_bind.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <utilities/converters.hpp>

namespace rmt {
namespace filename {
namespace grammar {
namespace phx = boost::phoenix;

using qi::lit;
using qi::char_;
using qi::double_;
using qi::_1;
using qi::_val;
using rmt::numeric::number_type;

template <typename Iterator>
simple_surrounded_arithmetic<Iterator>::simple_surrounded_arithmetic(
    const qi::symbols<char, std::string> &obs)
    : simple_surrounded_arithmetic::base_type(covered)
{
  covered %= "[" >> qi::skip(skipper)[expression] >> "]";

  expression =
      qi::skip(skipper)[term[_val = _1] >> *((('+') >> term[_val += _1]) |
                                             (('-') >> term[_val -= _1]))];

  term = qi::skip(skipper)[factor[_val = _1] >> *(('*' >> factor[_val *= _1]) |
                                                  ('/' >> factor[_val /= _1]))];

  factor = qi::skip(skipper)
      [double_[_val = _1] |
       qi::lexeme[("@{" >>
                   obs[_val = phx::bind(&::string_to_float<double>, _1)] >>
                   "}")] |
       qi::lexeme[obs[_val = phx::bind(&::string_to_float<double>, _1)]] |
       ('(' >> expression[_val = _1] >> ')') | ('-' >> factor[_val = -_1]) |
       ('+' >> factor[_val = _1])];
}

template <typename Iterator>
expression_parser<Iterator>::expression_parser(
    const qi::symbols<char, std::string> &obs)
    : expression_parser::base_type(start), arithmetic(obs)
{
  start = lit('@') >>
          ((arithmetic[_val = phx::bind(&::float_to_string<double>, _1)]) |
           (var[_val = _1]));

  var %= lit("{") >> obs >> lit("}");
}

template <typename Iterator>
variable_replacer<Iterator>::variable_replacer(
    const qi::symbols<char, std::string> &obs)
    : variable_replacer::base_type(start), expr(obs)
{
  start = +(+(char_ - '@')[_val += _1] | expr[_val += _1]);
}

template struct simple_surrounded_arithmetic<std::string::const_iterator>;
template struct expression_parser<std::string::const_iterator>;
template struct variable_replacer<std::string::const_iterator>;

} // namespace grammar
} // namespace filename
} // namespace rmt
