/*
 * Created: 22-04-2016
 * Modified: Fri 22 Apr 2016 12:28:12 CEST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef SCAN_GRAMMAR_HPP
#define SCAN_GRAMMAR_HPP

#include <boost/spirit/include/qi.hpp>
#include <vector>

namespace rmt {
namespace scan {
namespace grammar {
namespace qi = boost::spirit::qi;
namespace ascii = boost::spirit::ascii;
namespace phx = boost::phoenix;

template <typename Iterator>
struct double_vector_grammar
    : qi::grammar<Iterator, std::vector<double>(), ascii::space_type>
{
  double_vector_grammar();

  qi::rule<Iterator, std::vector<double>(), ascii::space_type> start;
};

} // namespace grammar
} // namespace scan
} // namespace rmt

#endif /* SCAN_GRAMMAR_HPP */
