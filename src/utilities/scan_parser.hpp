/*
 * Created: 22-04-2016
 * Modified: Thu 31 Aug 2017 16:47:27 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef SCAN_PARSER_HPP
#define SCAN_PARSER_HPP

#include <complex>
#include <types.hpp>
#include <vector>

namespace rmt {
namespace scan {

std::vector<rmt::numeric::complex_type> parse(const std::string &, bool imag_q);
std::vector<rmt::numeric::number_type> parse_real(const std::string &);

} // namespace scan
} // namespace rmt

#endif /* SCAN_PARSER_HPP */
