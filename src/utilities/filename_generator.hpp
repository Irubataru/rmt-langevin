
/*
 * Created: 31-08-2017
 * Modified: Thu 31 Aug 2017 18:34:31 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef FILENAME_GENERATOR_HPP
#define FILENAME_GENERATOR_HPP

#include <string>
#include <types.hpp>

namespace rmt {
namespace build {

class Filename_Generator
{
public:
  Filename_Generator() = default;
  Filename_Generator(Filename_Generator const &) = default;
  Filename_Generator(Filename_Generator &&) = default;

  Filename_Generator(std::string pattern) : pattern_{pattern}
  {
  }

  bool is_valid() const;
  bool is_valid(Physical_Parameters const &, Model_Parameters const &) const;

  std::string filename(Physical_Parameters const &, Model_Parameters const &,
                       std::string) const;

  std::string directory(Physical_Parameters const &,
                        Model_Parameters const &) const;

  std::string get_pattern() const
  {
    return pattern_;
  }

  void set_pattern(std::string new_pattern)
  {
    pattern_ = new_pattern;
  }

private:
  std::string pattern_;
};

} // namespace build
} // namespace rmt

#endif /* FILENAME_GENERATOR_HPP */
