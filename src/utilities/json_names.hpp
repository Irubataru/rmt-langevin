
/*
 * Created: 31-08-2017
 * Modified: Mon 06 Nov 2017 09:48:36 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef JSON_NAMES_HPP
#define JSON_NAMES_HPP

#include <string>

namespace rmt {
namespace build {

extern std::string const default_config_filename;

struct JSON_Categories
{
  static std::string const file;
  static std::string const scan;
  static std::string const parameters;
  static std::string const observables;
  static std::string const steps;
  static std::string const simulation;
};

struct JSON_Generic
{
  static std::string const name;
  static std::string const ref_name;
  static std::string const frequency;
  static std::string const parameters;
  static std::string const repeat;
};

struct JSON_File
{
  static std::string const save_file;
};

struct JSON_Scan
{
  static std::string const therm_steps;
  static std::string const therm_time;
  static std::string const measure_skips;
  static std::string const final_time;
  static std::string const total_steps;
  static std::string const delta_t;
  static std::string const update_type;
};

struct JSON_Parameters
{
  static std::string const model;
  static std::string const storage;
  static std::string const nf;
  static std::string const mu_real;
  static std::string const mu_imag;
  static std::string const mass_real;
  static std::string const mass_imag;
};

} // namespace build 
} // namespace rmt 

#endif /* JSON_NAMES_HPP */
