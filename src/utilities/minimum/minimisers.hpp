/*
 * Created: 18-11-2016
 * Modified: Fri 18 Nov 2016 16:28:59 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef MINIMISERS_HPP
#define MINIMISERS_HPP

#include "core/bracket_min.hpp"
#include "core/brent_core.hpp"
#include "general_minimisation.hpp"

namespace rmt {
namespace minimise {

using Brent_Minimisation = Bracket_Minimisation<
    core::Bracket_Minimisation_Core<core::Bracketing, core::Brent>>;

} // namespace minimise
} // namespace rmt

#endif /* MINIMISERS_HPP */
