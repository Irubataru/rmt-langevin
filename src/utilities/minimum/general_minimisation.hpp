
/*
 * Created: 18-11-2016
 * Modified: Thu 23 Feb 2017 11:01:04 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef GENERAL_MINIMISATION_HPP
#define GENERAL_MINIMISATION_HPP

#include <sstream>
#include <utilities/minimum/core/minimisation_types.hpp>

#include <iostream>

namespace rmt {
namespace minimise {

namespace core {

template <typename Bracket_Alg, typename Min_Alg>
class Bracket_Minimisation_Core
{
public:
  using bracket_input_param_type = core::Bracket_Input_Type;
  using minimise_input_param_type = core::Minimisation_Input_Type;

  template <typename F>
  using bracket_return_type = core::Bracket_Return_Type<F>;

  template <typename F>
  using minimise_return_type = core::Minimisation_Return_Type<F>;

  Bracket_Minimisation_Core() = default;
  Bracket_Minimisation_Core(Bracket_Minimisation_Core &&) = default;
  Bracket_Minimisation_Core(const Bracket_Minimisation_Core &) = default;

  Bracket_Minimisation_Core(Bracket_Alg br_alg, Min_Alg m_alg)
      : bracket_alg{br_alg}, minimum_alg{m_alg}
  {
  }

  template <typename F>
  bracket_return_type<F> bracket(bracket_input_param_type par, F f)
  {
    return bracket_alg(par, f);
  }

  template <typename F>
  minimise_return_type<F> minimise(bracket_return_type<F> br_ret,
                                   minimise_input_param_type min_par)
  {
    return minimum_alg(br_ret, min_par);
  }

private:
  Bracket_Alg bracket_alg;
  Min_Alg minimum_alg;
};

} // namespace core

template <typename Core>
class Bracket_Minimisation
{
public:
  template <typename f>
  using return_type = core::Minimisation_Return_Type<f>;

  explicit Bracket_Minimisation(Core c) : alg_core{c}
  {
  }

  template <typename Function_Type>
  return_type<Function_Type>
  minimise(core::Bracket_Input_Type bracket_alg_param,
           core::Minimisation_Input_Type min_alg_param, Function_Type f)
  {
    auto bracket_res = alg_core.bracket(std::move(bracket_alg_param), f);

    if (bracket_res.exit_code == core::Bracket_Exit_Code::Max_Value) {
      return {bracket_alg_param.max_param,
              bracket_res.f(bracket_alg_param.max_param), bracket_res.f};
    } else if (bracket_res.exit_code ==
               core::Bracket_Exit_Code::Max_Itterations) {
      std::ostringstream err_stream;
      err_stream << "In Bracket_Minimisation.minimise(...): Max bracket "
                 << "itterations reached and no bracket found.\n"
                 << "Current bracket: {" << bracket_res.left << ", "
                 << bracket_res.mid << ", " << bracket_res.right << "}";
      throw std::runtime_error{err_stream.str()};
    }

    auto ret_val = alg_core.minimise(bracket_res, min_alg_param);

    // If the minimum is at a larger than the max parameter we reset
    if (ret_val.min > bracket_alg_param.max_param) {
      ret_val.min = bracket_alg_param.max_param;
      ret_val.f_min = ret_val.f(ret_val.min);
    }

    return ret_val;
  }

private:
  Core alg_core;
};

} // namespace minimise
} // namespace rmt

#endif /* GENERAL_MINIMISATION_HPP */
