/*
 * Created: 21-07-2016
 * Modified: Fri 18 Nov 2016 14:52:20 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef UTILITY_MINIMUM_BRACKET_MIN_HPP
#define UTILITY_MINIMUM_BRACKET_MIN_HPP

#include <utilities/minimum/core/minimisation_types.hpp>

namespace rmt {
namespace minimise {
namespace core {

class Bracketing
{
public:
  using number_type = numeric::number_type;
  using input_parameters_type = Bracket_Input_Type;

  template <typename f>
  using return_type = Bracket_Return_Type<f>;

  template <typename Function_Type>
  static return_type<Function_Type> bracket(input_parameters_type,
                                            Function_Type);

  template <typename Function_Type>
  return_type<Function_Type> operator()(input_parameters_type input_parameters,
                                        Function_Type f)
  {
    return bracket(input_parameters, std::move(f));
  }

private:
  static void shift_3(number_type &a, number_type &b, number_type &c,
                      number_type d)
  {
    a = b;
    b = c;
    c = d;
  }
};

} // namespace core
} // namespace minimise
} // namespace rmt

#include "bracket_min_impl.hpp"

#endif /* UTILITY_MINIMUM_BRACKET_MIN_HPP */
