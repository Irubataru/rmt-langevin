/*
 * Created: 18-11-2016
 * Modified: Tue 21 Feb 2017 15:16:20 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef BRACKET_MIN_IMPL_HPP
#define BRACKET_MIN_IMPL_HPP

#include "bracket_min.hpp"
#include <cmath>

namespace rmt {
namespace minimise {
namespace core {

namespace bracket {

// Numerical Recipes utility functions
namespace {

template <typename T>
T nr_sign(const T &x, const T &y)
{
  return rmt::numeric::abs(x) * (T{1.} - T{2.} * (y < T{0}));
}
}

} // namespace bracket

template <typename Function_Type>
inline Bracketing::return_type<Function_Type>
Bracketing::bracket(Bracketing::input_parameters_type input_parameters,
                    Function_Type f)
{
  using numeric::abs;
  using numeric::sqrt;
  const auto golden_ratio =
      number_type{0.5} * (number_type{1.} + sqrt(number_type{5.}));
  constexpr auto small_number =
      100. * std::numeric_limits<number_type>::epsilon();
  constexpr auto furthest_step = number_type{100.};

  auto left = input_parameters.left;
  auto mid = input_parameters.right;

  auto f_left = f(left);
  auto f_mid = f(mid);
  auto f_u = number_type{0.};

  // Rerverse search direction if initial points are in the wrong order
  if (f_mid > f_left) {
    std::swap(left, mid);
    std::swap(f_left, f_mid);
  }

  // First guess for a rightmost point
  auto right = mid + golden_ratio * (mid - left);
  auto f_right = f(right);

  auto step = std::size_t{0ul};

  // Loop until a bracket is found
  while (f_mid > f_right) {

    // Test if we went too far, in which case no bracket is found and max_param
    // should be used
    if (left > input_parameters.max_param) {
      return {left, mid, right, f_left, f_mid, f_right, std::move(f), Bracket_Exit_Code::Max_Value};
    }

    if (step == input_parameters.max_steps)
      return {left, mid, right, f_left, f_mid, f_right, std::move(f), Bracket_Exit_Code::Max_Itterations};

    // Estimate next point through parabolic extrapolation of left, mid and
    // right
    auto r = (mid - left) * (f_mid - f_right);
    auto q = (mid - right) * (f_mid - f_left);
    auto u = mid -
             ((mid - right) * q - (mid - left) * r) /
                 (number_type{2.} *
                  bracket::nr_sign(std::max(abs(q - r), small_number), q - r));

    // A limit on the furthest we will move using this extrapolation
    auto u_limit = mid + furthest_step * (right - mid);

    // If the parabolic point is between mid and right
    if ((mid - u) * (u - right) > number_type{0.}) {
      f_u = f(u);

      // Check if a minimum between mid and right exists
      if (f_u < f_right)
        return {mid, u, right, f_mid, f_u, f_right, std::move(f), Bracket_Exit_Code::Bracket_Found};

      // Check if a minumum between left and u
      else if (f_u > f_mid)
        return {left, mid, u, f_left, f_mid, f_u, std::move(f), Bracket_Exit_Code::Bracket_Found};

      // Use magnification instead
      u = right + golden_ratio * (right - mid);
      f_u = f(u);

      // The parabolic point is between right and the upper limit
    } else if ((right - u) * (u - u_limit) > number_type{0.}) {
      f_u = f(u);
      if (f_u < f_right) {
        shift_3(mid, right, u, u + golden_ratio * (u - right));
        shift_3(f_mid, f_right, f_u, f(u));
      }

      // The parabolic point is outside the limited range, use the limit instead
    } else if ((u - u_limit) * (u_limit - right) >= number_type{0.}) {
      u = u_limit;
      f_u = f(u);

      // Reject the parabolic alltogether, use magnification
    } else {
      u = right + golden_ratio * (right - mid);
      f_u = f(u);
    }

    // Shift the three points to the right
    shift_3(left, mid, right, u);
    shift_3(f_left, f_mid, f_right, f_u);
    ++step;
  }

  return {left, mid, right, f_left, f_mid, f_right, std::move(f), Bracket_Exit_Code::Bracket_Found};
}

} // namespace core
} // namespace minimise
} // namespace rmt

#endif /* BRACKET_MIN_IMPL_HPP */
