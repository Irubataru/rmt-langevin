
/*
 * Created: 18-11-2016
 * Modified: Tue 21 Feb 2017 15:02:26 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef MINIMISATION_TYPES_HPP
#define MINIMISATION_TYPES_HPP

#include <number_type.hpp>

namespace rmt {
namespace minimise {
namespace core {

struct Bracket_Input_Type
{
  numeric::number_type left, right;
  std::size_t max_steps = 100;
  numeric::number_type max_param =
      std::numeric_limits<numeric::number_type>::max();
};

enum class Bracket_Exit_Code
{
  Bracket_Found,
  Max_Value,
  Max_Itterations
};

template <typename Function_Type>
struct Bracket_Return_Type
{
  numeric::number_type left, mid, right;
  numeric::number_type f_left, f_mid, f_right;
  Function_Type f;
  Bracket_Exit_Code exit_code;
};

struct Minimisation_Input_Type
{
  numeric::number_type tolerance =
      100. * std::numeric_limits<numeric::number_type>::epsilon();

  std::size_t max_steps = 100;
};

template <typename Function_Type>
struct Minimisation_Return_Type
{
  numeric::number_type min;
  numeric::number_type f_min;
  Function_Type f;
};

} // namespace core
} // namespace minimise
} // namespace rmt

#endif /* MINIMISATION_TYPES_HPP */
