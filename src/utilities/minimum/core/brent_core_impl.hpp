
/*
 * Created: 18-11-2016
 * Modified: Tue 06 Dec 2016 14:19:30 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef BRENT_CORE_IMPL_HPP
#define BRENT_CORE_IMPL_HPP

#include "brent_core.hpp"

namespace rmt {
namespace minimise {
namespace core {

namespace brent {

namespace {

template <typename T>
T nr_sign(const T &x, const T &y)
{
  return rmt::numeric::abs(x) * (T{1} - T{2} * (y < T{0}));
}
}

} // namespace brent

template <typename Function_Type>
Brent::return_type<Function_Type>
Brent::minimise(Brent::bracket_parameters_type<Function_Type> bracket_param,
                Brent::input_parameters_type input_param)
{
  using numeric::abs;
  using numeric::sqrt;
  const auto golden_section =
      number_type{0.5} * (number_type{3.} - sqrt(number_type{5.}));
  constexpr auto small_number =
      100. * std::numeric_limits<number_type>::epsilon();

  auto d_step = number_type{0.};
  auto e_step = number_type{0.}; // Distance moved the step before last

  auto left = bracket_param.left;
  auto right = bracket_param.right;

  auto para_pos = number_type{0.};
  auto f_para = number_type{0.};

  auto x = bracket_param.mid;
  auto w = bracket_param.mid;
  auto v = bracket_param.mid;
  auto f_x = bracket_param.f_mid;
  auto f_w = bracket_param.f_mid;
  auto f_v = bracket_param.f_mid;

  // Extract the function from the bracket return type
  auto f = std::move(bracket_param.f);

  // The main loop, if this exits the function throws
  for (auto step = 0ul; step < input_param.max_steps; ++step) {
    auto half_point = number_type{0.5} * (left + right);

    auto mid_tol = input_param.tolerance * abs(x);
    auto twice_mid_tol = number_type{2.} * mid_tol + small_number;

    // Test whether the minimum is within the accepted tolerance
    if (abs(x - half_point) <=
        (twice_mid_tol - number_type{0.5} * (right - left)))
      return {x, f_x, std::move(f)};

    // Construct a trial parabolic fit
    if (abs(e_step) > mid_tol) {
      auto r = (x - w) * (f_x - f_v);
      auto q = (x - v) * (f_x - f_w);
      auto p = (x - v) * q - (x - w) * r;
      q = number_type{2.} * (q - r);

      if (q > number_type{0.})
        p = -p;

      q = abs(q);
      auto e_temp = e_step;
      e_step = d_step;

      if (abs(p) >= abs(number_type{0.5} * q * e_temp) or p <= q * (left - x) or
          p >= q * (right - x)) {
        e_step = (x >= half_point ? left - x : right - x);
        d_step = golden_section * e_step;
      } else {
        d_step = p / q;
        para_pos = x + d_step; // Do the parabolic step

        if ((para_pos - left) < twice_mid_tol or
            (right - para_pos) < twice_mid_tol)
          d_step = brent::nr_sign(mid_tol, half_point - x);
      }
    } else {
      e_step = (x >= half_point ? left - x : right - x);
      d_step = golden_section * e_step;
    }

    // The one function evaluation per iteration
    para_pos = (abs(d_step) >= mid_tol ? x + d_step
                                       : x + brent::nr_sign(mid_tol, d_step));
    f_para = f(para_pos);

    // Final bit of housekeeping
    if (f_para <= f_x) {
      if (para_pos >= x)
        left = x;
      else
        right = x;

      shift(v, w, x, para_pos);
      shift(f_v, f_w, f_x, f_para);
    } else {
      if (para_pos < x)
        left = para_pos;
      else
        right = para_pos;

      if (f_para <= f_w or w == x) {
        v = w;
        w = para_pos;
        f_v = f_w;
        f_w = f_para;
      } else if (f_para <= f_v or v == x or v == w) {
        v = para_pos;
        f_v = f_para;
      }
    }
  }

  return {x, f_x, std::move(f)};
}

} // namespace core
} // namespace minimise
} // namespace rmt

#endif /* BRENT_CORE_IMPL_HPP */
