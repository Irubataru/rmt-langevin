/*
 * Created: 18-11-2016
 * Modified: Tue 06 Dec 2016 12:35:29 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef BRENT_CORE_HPP
#define BRENT_CORE_HPP

#include <utilities/minimum/core/minimisation_types.hpp>

namespace rmt {
namespace minimise {
namespace core {

class Brent
{
public:
  using number_type = numeric::number_type;
  using input_parameters_type = Minimisation_Input_Type;

  template <typename f>
  using bracket_parameters_type = Bracket_Return_Type<f>;

  template <typename f>
  using return_type = Minimisation_Return_Type<f>;

  template <typename Function_Type>
  static return_type<Function_Type>
      minimise(bracket_parameters_type<Function_Type>, input_parameters_type);

  template <typename Function_Type>
  return_type<Function_Type>
  operator()(bracket_parameters_type<Function_Type> bpar,
             input_parameters_type ipar) const
  {
    return minimise(std::move(bpar), std::move(ipar));
  }

private:
  template <typename T>
  static void shift(T &a, T &b, T &c)
  {
    a = std::move(b);
    b = std::move(c);
  }

  template <typename T>
  static void shift(T &a, T &b, T &c, T &d)
  {
    a = std::move(b);
    b = std::move(c);
    c = std::move(d);
  }
};

} // namespace core
} // namespace minimise
} // namespace rmt

#include "brent_core_impl.hpp"

#endif /* BRENT_CORE_HPP */
