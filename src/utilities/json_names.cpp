
/*
 * Created: 31-08-2017
 * Modified: Mon 06 Nov 2017 09:48:54 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "json_names.hpp"

namespace rmt {
namespace build {

const std::string default_config_filename = "configuration.json";

std::string const JSON_Categories::file = "file";
std::string const JSON_Categories::scan = "scan";
std::string const JSON_Categories::parameters = "parameters";
std::string const JSON_Categories::observables = "observables";
std::string const JSON_Categories::steps = "steps";
std::string const JSON_Categories::simulation = "simulation";

std::string const JSON_Generic::name = "name";
std::string const JSON_Generic::ref_name = "ref-name";
std::string const JSON_Generic::frequency = "frequency";
std::string const JSON_Generic::parameters = "parameters";
std::string const JSON_Generic::repeat = "repeat";

std::string const JSON_File::save_file = "save-file";

std::string const JSON_Scan::therm_steps = "thermalisation-steps";
std::string const JSON_Scan::therm_time = "thermalisation-time";
std::string const JSON_Scan::measure_skips = "measure-skips";
std::string const JSON_Scan::final_time = "final-time";
std::string const JSON_Scan::total_steps = "total-steps";
std::string const JSON_Scan::delta_t = "delta-t";
std::string const JSON_Scan::update_type = "update-type";

std::string const JSON_Parameters::model = "model";
std::string const JSON_Parameters::storage = "storage";
std::string const JSON_Parameters::nf = "nf";
std::string const JSON_Parameters::mu_real = "mu-real";
std::string const JSON_Parameters::mu_imag = "mu-imag";
std::string const JSON_Parameters::mass_real = "mass-real";
std::string const JSON_Parameters::mass_imag = "mass-imag";


} // namespace build 
} // namespace rmt 
