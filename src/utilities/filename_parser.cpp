/*
 * Created: 22-04-2016
 * Modified: Thu 31 Aug 2017 18:55:28 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "filename_parser.hpp"
#include <boost/lexical_cast.hpp>
#include <compile_configuration.hpp>
#include <utilities/converters.hpp>
#include <utilities/filename_grammar.hpp>
#include <utilities/json_parser_helpers.hpp>

namespace rmt {
namespace filename {

parameter_symbols::parameter_symbols(Physical_Parameters const &cfg,
                                     Model_Parameters const &mparm)
{
  add("mu", float_to_string(cfg.chemical_potential.real()));
  add("mu_re", float_to_string(cfg.chemical_potential.real()));
  add("mu_im", float_to_string(cfg.chemical_potential.imag()));

  auto find_it = std::find(mparm.mu_scan.begin(), mparm.mu_scan.end(), cfg.chemical_potential);

  if (find_it == mparm.mu_scan.end())
    throw std::runtime_error{"error setting \"mu_n\": mu value not in scan params"};

  add("mu_n", boost::lexical_cast<std::string>(std::distance(mparm.mu_scan.begin(), find_it)));

  add("mass", float_to_string(cfg.fermion_mass.real()));
  add("mass_re", float_to_string(cfg.fermion_mass.real()));
  add("mass_im", float_to_string(cfg.fermion_mass.imag()));

  find_it = std::find(mparm.mass_scan.begin(), mparm.mass_scan.end(), cfg.fermion_mass);

  if (find_it == mparm.mass_scan.end())
    throw std::runtime_error{"error setting \"mass_n\": mass value not in scan params"};

  add("mass_n", boost::lexical_cast<std::string>(std::distance(mparm.mass_scan.begin(), find_it)));

  add("nf", boost::lexical_cast<std::string>(cfg.number_of_flavours));
  add("mat_size", boost::lexical_cast<std::string>(M_size));

  add("model", parse::to_string(mparm.model));
  add("storage", parse::to_string(mparm.storage));
}

void parameter_symbols::add_dummy(std::string dummy)
{
  add(dummy, "@{" + dummy + "}");
}

std::pair<std::string,bool> parse(const std::string &s, const parameter_symbols &symb)
{
  using string_iterator_type = std::string::const_iterator;
  using parse_grammar = grammar::variable_replacer<string_iterator_type>;

  auto parsed_string = std::string{};
  string_iterator_type iter = s.cbegin();
  string_iterator_type end = s.cend();

  parse_grammar gr{symb};
  bool success = boost::spirit::qi::parse(iter, end, gr, parsed_string);

  if (!success or (iter != end))
    return {std::string{iter, end}, false};

  return {parsed_string, true};
}

} // namespace filename
} // namespace rmt
