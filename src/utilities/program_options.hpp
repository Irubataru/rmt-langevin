/*
 * Created: 16-04-2016
 * Modified: Thu 31 Aug 2017 19:52:19 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef PROGRAM_OPTIONS_HPP
#define PROGRAM_OPTIONS_HPP

#include <nlohmann/json.hpp>
#include <types.hpp>
#include "filename_generator.hpp"

namespace rmt {
namespace build {

std::string get_config_filename(int argc, char *argv[]);

Filename_Generator parse_save_pattern(nlohmann::json const &);
Scan_Parameters parse_scan_parameters(nlohmann::json const &);
Model_Parameters parse_model_parameters(nlohmann::json const &);

} // namespace build 
};

#endif /* PROGRAM_OPTIONS_HPP */
