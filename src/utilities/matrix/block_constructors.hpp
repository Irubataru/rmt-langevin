/*
 * Created: 03-11-2016
 * Modified: Thu 03 Nov 2016 17:20:37 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef MATRIX_BLOCK_CONSTRUCTORS_HPP
#define MATRIX_BLOCK_CONSTRUCTORS_HPP

#include <types.hpp>

namespace rmt {

namespace matrix {

#ifdef DYNAMIC

inline block_matrix_type Block_Matrix()
{
  return block_matrix_type(2 * M_size, 2 * M_size);
}

inline block_matrix_type Block_Matrix(numeric::complex_type init_value)
{
  return block_matrix_type(2 * M_size, 2 * M_size, init_value);
}

#else

inline block_matrix_type Block_Matrix()
{
  return block_matrix_type();
}

inline block_matrix_type Block_Matrix(numeric::complex_type init_value)
{
  return block_matrix_type(init_value);
}

#endif

inline block_matrix_type Block_Zero_Matrix()
{
  return Block_Matrix(numeric::complex_type{0.});
}

} // namespace matrix

} // namespace rmt

#endif /* MATRIX_BLOCK_CONSTRUCTORS_HPP */
