/*
 * Created: 01-11-2016
 * Modified: Thu 04 May 2017 18:15:35 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef MATRIX_TRACE_HPP
#define MATRIX_TRACE_HPP

#include <blaze/math/DenseMatrix.h>
#include <number_type.hpp>

namespace rmt {
namespace matrix {

//TODO: Disable for non-arithmetic types
template <typename MT, // Type of the dense matrix
          bool OT>     // Order type of the matrix
inline blaze::ElementType_<MT>
Trace(const blaze::DenseMatrix<MT, OT> &mat)
{
  using CT = blaze::CompositeType_<MT>;
  using element_type = blaze::ElementType_<MT>;

  auto result = element_type{0.};

  CT A(~mat); // Evaluation of the dense matrix operand

  auto end = (A.rows() > A.columns()) ? A.columns() : A.rows();
  for (auto i = 0ul; i < end; ++i)
    result += A(i, i);

  return result;
}

} // namespace matrix
} // namespace rmt

#endif /* MATRIX_TRACE_HPP */
