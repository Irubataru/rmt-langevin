/*
 * Created: 07-11-2016
 * Modified: Mon 07 Nov 2016 19:37:14 GMT
 * Author: Jonas MATRIX_R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef MATRIX_RANDOM_CONSTRUCTOR_HPP
#define MATRIX_RANDOM_CONSTRUCTOR_HPP

#include <random>
#include <types.hpp>
#include <utilities/matrix/default_constructor.hpp>

namespace rmt {
namespace matrix {

inline M_type Random_Matrix()
{
  std::random_device rd{};
  std::mt19937 rng{rd()};
  std::uniform_real_distribution<double> dist{-1., 1.};

  auto random_matrix = Matrix(numeric::complex_type{0.});

  for (auto i = 0; i < M_size; ++i)
    for (auto j = 0; j < M_size; ++j)
      random_matrix(i, j) = dist(rng);

  return random_matrix;
}

template <typename Dist>
inline M_type Random_Matrix(Dist &dist)
{
  std::random_device rd{};
  std::mt19937 rng{rd()};

  auto random_matrix = Matrix(numeric::complex_type{0.});

  for (auto i = 0; i < M_size; ++i)
    for (auto j = 0; j < M_size; ++j)
      random_matrix(i, j) = dist(rng);

  return random_matrix;
}

template <typename Dist, typename Rng>
inline M_type Random_Matrix(Dist &dist, Rng &rng)
{
  auto random_matrix = Matrix(numeric::complex_type{0.});

  for (auto i = 0; i < M_size; ++i)
    for (auto j = 0; j < M_size; ++j)
      random_matrix(i, j) = dist(rng);

  return random_matrix;
}

} // namespace matrix
} // namespace rmt

#endif /* MATRIX_RANDOM_CONSTRUCTOR_HPP */
