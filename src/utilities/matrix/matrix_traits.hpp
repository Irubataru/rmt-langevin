/*
 * Created: 02-11-2016
 * Modified: Tue 08 Nov 2016 11:54:38 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef MATRIX_TRAITS_HPP
#define MATRIX_TRAITS_HPP

#include <complex>

namespace rmt {
namespace matrix {

enum class Precission
{
  Single,
  Double,
  Complex_Single,
  Complex_Double,
  NonStandard
};

enum class Symmetries
{
  None,
  Symmetric,
  Hermitian
};

template <typename value_type>
struct precission_type
{
  static constexpr Precission value = Precission::NonStandard;
};

template <>
struct precission_type<float>
{
  static constexpr Precission value = Precission::Single;
};

template <>
struct precission_type<double>
{
  static constexpr Precission value = Precission::Double;
};

template <>
struct precission_type<std::complex<float>>
{
  static constexpr Precission value = Precission::Complex_Single;
};

template <>
struct precission_type<std::complex<double>>
{
  static constexpr Precission value = Precission::Complex_Double;
};

template <typename Type>
struct real_type
{
  using value = Type;
};

template <typename Type>
struct real_type<std::complex<Type>>
{
  using value = Type;
};

} // namespace matrix
} // namespace rmt

#endif /* MATRIX_TRAITS_HPP */
