/*
 * Created: 03-11-2016
 * Modified: Thu 23 Feb 2017 13:01:54 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef EIGENVALUE_SOLVER_IMPL_HPP
#define EIGENVALUE_SOLVER_IMPL_HPP

#include "eigenvalue_solver.hpp"
#include "lapacke_eigenvalue_wrappers.hpp"

namespace rmt {
namespace matrix {

template <typename MT, bool OT>
inline void
Eigenvalue_Solver<Precission::Complex_Double, Symmetries::None>::Compute(
    const blaze::DenseMatrix<MT, OT> &mat, bool evals_flag)
{
  if (evals_flag == eigenvalues_only) {
    Compute_Eigenvalues_Only(mat);
  } else {
    Compute_Eigenvalues_and_Eigenvectors(mat);
  }
}

/*
 * Compute the eigenvalus of a general non-symmetrix complex matrix by calling
 * the following LAPACKE functions
 * zgebal -> zgehrd -> zhseqr
 *
 * Throws a runtime error if any of these methods fail.
 */
template <typename MT, bool OT>
inline void Eigenvalue_Solver<Precission::Complex_Double, Symmetries::None>::
    Compute_Eigenvalues_Only(const blaze::DenseMatrix<MT, OT> &mat)
{
  BLAZE_CONSTRAINT_MUST_BE_SAME_TYPE(blaze::ElementType_<MT>,
                                     std::complex<double>);

  if (!blaze::isSquare(~mat))
    throw std::invalid_argument{
        "Eigenvalue_Solver<Complex_Double, None>: The matrix must be square"};

  auto working_copy = blaze::DynamicMatrix<std::complex<double>, OT>{mat};

  eigen_values = {};
  eigen_vectors = {};

  auto scales = blaze::DynamicVector<double>{};
  auto tau = blaze::DynamicVector<std::complex<double>>{};
  lapack_int ilo, ihi;

  auto info = internal::zgebal(working_copy, 'N', ilo, ihi, scales);

  if (info < 0)
    throw std::runtime_error{"Eigenvalue_Solver<Complex_Double, None>: Invalid "
                             "arguments for zgebal"};

  info = internal::zgehrd(working_copy, ilo, ihi, tau);

  if (info < 0)
    throw std::runtime_error{"Eigenvalue_Solver<Complex_Double, None>: Invalid "
                             "arguments for zgehrd"};

  auto q_mat = blaze::DynamicMatrix<std::complex<double>>{};

  info =
      internal::zhseqr(working_copy, 'E', 'N', ilo, ihi, q_mat, eigen_values);

  if (info < 0)
    throw std::runtime_error{"Eigenvalue_Solver<Complex_Double, None>: Invalid "
                             "arguments for zhseqr"};
  else if (info > 0)
    throw std::runtime_error{"Eigenvalue_Solver<Complex_Double, None>: Failed "
                             "to compute the eigenvalues of the matrix"};
}

template <typename MT, bool OT>
inline void Eigenvalue_Solver<Precission::Complex_Double, Symmetries::None>::
    Compute_Eigenvalues_and_Eigenvectors(const blaze::DenseMatrix<MT, OT> &mat)
{
  throw std::invalid_argument{"Eigenvalue_Solver<Complex_Double, None>: The "
                              "eigenvector solver hasn't been implemented"};
}

/*
 * Compute the eigenvalus (and eigenvectors) of a hermitian matrix by calling
 * the following LAPACKE functions
 * zhetrd -> (zungtr) -> zsteqr
 *
 * Throws a runtime error if any of these methods fail.
 */
template <typename MT, bool OT>
inline void
Eigenvalue_Solver<Precission::Complex_Double, Symmetries::Hermitian>::Compute(
    const blaze::DenseMatrix<MT, OT> &mat, bool evals_flag)
{
  BLAZE_CONSTRAINT_MUST_BE_SAME_TYPE(blaze::ElementType_<MT>,
                                     std::complex<double>);

  if (!blaze::isHermitian(~mat))
    throw std::invalid_argument{"Eigenvalue_Solver<Complex_Double, Hermitian>: "
                                "The matrix must be Hermitian"};

  eigen_values = {};
  eigen_vectors = blaze::DynamicMatrix<std::complex<double>, OT>{mat};

  auto offdiag = blaze::DynamicVector<double>{};
  auto tau = blaze::DynamicVector<std::complex<double>>{};

  auto info = internal::zhetrd(eigen_vectors, 'U', eigen_values, offdiag, tau);

  if (info < 0)
    throw std::runtime_error{
        "Eigenvalue_Solver<Complex_Double, Hermitian>: Invalid "
        "arguments for zhetrd"};

  if (evals_flag != eigenvalues_only) {
    info = internal::zungtr(eigen_vectors, 'U', tau);

    if (info < 0)
      throw std::runtime_error{"Eigenvalue_Solver<Complex_Double, Hermitian>: "
                               "Invalid arguments for zungtr"};
  }

  auto job_type = (evals_flag == eigenvalues_only) ? 'N' : 'V';

  info = internal::zsteqr(eigen_vectors, job_type, eigen_values, offdiag);

  if (info < 0)
    throw std::runtime_error{
        "Eigenvalue_Solver<Complex_Double, Hermitian>: Invalid "
        "arguments for zhseqr"};
  else if (info > 0)
    throw std::runtime_error{
        "Eigenvalue_Solver<Complex_Double, Hermitian>: Failed "
        "to compute the eigenvalues of the matrix"};

  if (evals_flag == eigenvalues_only)
    eigen_vectors = {};
}

} // namespace matrix
} // namespace rmt

#endif /* EIGENVALUE_SOLVER_IMPL_HPP */
