/*
 * Created: 02-11-2016
 * Modified: Thu 15 Dec 2016 16:50:21 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef MATRIX_EIGENVALUE_SOLVER_HPP
#define MATRIX_EIGENVALUE_SOLVER_HPP

#include <blaze/math/DynamicMatrix.h>
#include <utilities/matrix/matrix_traits.hpp>

namespace rmt {
namespace matrix {

template <Precission P, Symmetries S>
class Eigenvalue_Solver
{
};

constexpr bool eigenvalues_only = true;
constexpr bool eigenvalues_and_eigenvectors = !eigenvalues_only;

template <>
class Eigenvalue_Solver<Precission::Complex_Double, Symmetries::None>
{
public:
  using eigenvalue_type = std::complex<double>;
  using eigenvalue_container_type =
      blaze::DynamicVector<eigenvalue_type, blaze::columnVector>;
  using eigenvector_container_type =
      blaze::DynamicMatrix<std::complex<double>, blaze::rowMajor>;

  template <typename MT, bool OT>
  void Compute(const blaze::DenseMatrix<MT, OT> &,
               bool eigenvalues_only = true);

  const eigenvalue_container_type Eigenvalues() const
  {
    return eigen_values;
  }

  const eigenvector_container_type Eigenvectors() const
  {
    return eigen_vectors;
  }

public: /* member functions */
  template <typename MT, bool OT>
  void Compute_Eigenvalues_Only(const blaze::DenseMatrix<MT, OT> &);

  template <typename MT, bool OT>
  void Compute_Eigenvalues_and_Eigenvectors(const blaze::DenseMatrix<MT, OT> &);

private: /* member variables */
  eigenvalue_container_type eigen_values;
  eigenvector_container_type eigen_vectors;
};

template <>
class Eigenvalue_Solver<Precission::Complex_Double, Symmetries::Hermitian>
{
public:
  using eigenvalue_type = double;
  using eigenvalue_container_type = blaze::DynamicVector<eigenvalue_type>;
  using eigenvector_container_type =
      blaze::DynamicMatrix<std::complex<double>, blaze::rowMajor>;

  template <typename MT, bool OT>
  void Compute(const blaze::DenseMatrix<MT, OT> &, bool = eigenvalues_only);

  const eigenvalue_container_type Eigenvalues() const
  {
    return eigen_values;
  }

  const eigenvector_container_type Eigenvectors() const
  {
    return eigen_vectors;
  }

private: /* member variables */
  eigenvalue_container_type eigen_values;
  eigenvector_container_type eigen_vectors;
};

} // namespace matrix
} // namespace rmt

#include "eigenvalue_solver_impl.hpp"

#endif /* MATRIX_EIGENVALUE_SOLVER_HPP */
