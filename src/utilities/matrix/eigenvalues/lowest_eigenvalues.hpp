/*
 * Created: 08-11-2016
 * Modified: Tue 08 Nov 2016 13:31:49 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef MATRIX_LOWEST_EIGENVALUES_HPP
#define MATRIX_LOWEST_EIGENVALUES_HPP

#include <blaze/math/DynamicMatrix.h>
#include <blaze/math/DynamicVector.h>
#include <number_type.hpp>
#include <utilities/matrix/matrix_traits.hpp>
#include <vector>

namespace rmt {
namespace matrix {

class Lowest_Eigenvalues
{
public:
  template <typename MT, bool OT>
  std::vector<typename real_type<blaze::ElementType_<MT>>::value>
  operator()(const blaze::DenseMatrix<MT, OT> &, std::size_t);
};

} // namespace matrix
} // namespace rmt

#include "lowest_eigenvalues_impl.hpp"

#endif /* MATRIX_LOWEST_EIGENVALUES_HPP */
