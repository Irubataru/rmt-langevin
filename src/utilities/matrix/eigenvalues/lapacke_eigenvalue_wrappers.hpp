/*
 * Created: 02-11-2016
 * Modified: Mon 07 Nov 2016 12:19:52 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef LAPACKE_EIGENVALUE_WRAPPERS_HPP
#define LAPACKE_EIGENVALUE_WRAPPERS_HPP

#include <blaze/math/DenseMatrix.h>
#include <blaze/math/DynamicVector.h>
#include <boost/cast.hpp>
#include <memory>
#include <mkl_lapacke.h>

namespace rmt {
namespace matrix {
namespace internal {

/*
 * Balances the input matrix by performing a similarity transformation.
 *
 * Parameters:
 *  [in/out] mat - The matrix to be balance, the balancing happens in place
 *  [in] job_type ({'N'|'P'|'S'|'B'}) - Various working modes of the algorithm
 * depending on the preconditions of 'mat'
 *                                       'N': mat is neither permuted nor
 * scaled,  'P': mat is permuted, but not scaled
 *                                       'S': mat is scaled, but not permuted
 * 'B': mat is both scaled and permuted
 *  [out] ilo - The lower index of the block matrix part of the matrix after
 * balancing (=1 if the matrix cannot be simplified)
 *  [out] ihi - The upper index of the block matrix part of the matrix after
 * balancing (=n if the matrix cannot be simplified)
 *  [out] scales - Scaling factors of the balanced matrix
 *
 * Return value:
 *   0 if the execution was a success
 *  -1 if the input parameters had an illegal value
*/
template <typename MT, // Type of the dense matrix
          bool OT,     // Order type of the matrix
          bool TF>     // Transpose flag of the scaling result matrix
inline lapack_int
zgebal(blaze::DenseMatrix<MT, OT> &mat, char job_type, lapack_int &ilo,
       lapack_int &ihi, blaze::DynamicVector<double, TF> &scales)
{
  using boost::numeric_cast;

  BLAZE_CONSTRAINT_MUST_NOT_BE_ADAPTOR_TYPE(MT);
  BLAZE_CONSTRAINT_MUST_NOT_BE_COMPUTATION_TYPE(MT);
  BLAZE_CONSTRAINT_MUST_HAVE_MUTABLE_DATA_ACCESS(MT);
  BLAZE_CONSTRAINT_MUST_BE_BLAS_COMPATIBLE_TYPE(blaze::ElementType_<MT>);
  BLAZE_CONSTRAINT_MUST_BE_SAME_TYPE(blaze::ElementType_<MT>,
                                     std::complex<double>);

  const auto n = numeric_cast<int>((~mat).rows());
  const auto lda = numeric_cast<int>((~mat).spacing());
  constexpr auto matrix_layout =
      blaze::IsRowMajorMatrix<MT>::value ? LAPACK_ROW_MAJOR : LAPACK_COL_MAJOR;

  scales.resize(n, false);

  return LAPACKE_zgebal(matrix_layout, job_type, n, (~mat).data(), lda, &ilo,
                        &ihi, scales.data());
}

/*
 * Reduces the input matrix to its upper Hessenberg form
 *
 * Parameters:
 *  [in/out] mat - The matrix to be reduced to its upper Hessenberg form, the
 * reduction happens in place
 *  [in] ilo/ihi - Indices of the block matrix of a reduced matrix (returned
 * from ?gebal).
 *                 If the matrix hasn't been balanced, they take the values
 * ilo=1, ihi=n
 *  [out] tau - The elementaru reflectors of the transformation, can be used to
 * construct the Q-matrix of the transformation
 *
 * Return values:
 *   0 if the execution was a success
 *  -1 if the input parameters had an illegal value
 */
template <typename MT, // Type of the dense matrix
          bool OT,     // Order type of the matrix
          bool TF>     // Transpose flag of the storage vector
inline lapack_int
zgehrd(blaze::DenseMatrix<MT, OT> &mat, lapack_int ilo, lapack_int ihi,
       blaze::DynamicVector<std::complex<double>, TF> &tau)
{
  using boost::numeric_cast;

  BLAZE_CONSTRAINT_MUST_NOT_BE_ADAPTOR_TYPE(MT);
  BLAZE_CONSTRAINT_MUST_NOT_BE_COMPUTATION_TYPE(MT);
  BLAZE_CONSTRAINT_MUST_HAVE_MUTABLE_DATA_ACCESS(MT);
  BLAZE_CONSTRAINT_MUST_BE_BLAS_COMPATIBLE_TYPE(blaze::ElementType_<MT>);
  BLAZE_CONSTRAINT_MUST_BE_SAME_TYPE(blaze::ElementType_<MT>,
                                     std::complex<double>);

  const auto n = numeric_cast<int>((~mat).rows());
  const auto lda = numeric_cast<int>((~mat).spacing());
  constexpr auto matrix_layout =
      blaze::IsRowMajorMatrix<MT>::value ? LAPACK_ROW_MAJOR : LAPACK_COL_MAJOR;

  tau.resize(n - 1, false);

  return LAPACKE_zgehrd(matrix_layout, n, ilo, ihi, (~mat).data(), lda,
                        tau.data());
}

/*
 * Computes the eigenvalues, and optionally the Schur factorisation, of an upper
 * Hessenberg matrix
 *
 * Parameters:
 *  [in/out] hess - The upper Hessenberg matrix to compute the eigenvalues of.
 * If job_type='E' then the matrix is unspecified on
 *                  exit, if job_type='S' (and the function exits successfully),
 * then the matrix contains the upper
 *                  quasi-triangular form of the Schur decomposition
 *  [in] job_type ({'E'|'S'}) - Specify which job to perform. 'E': compute
 * eigenvalues only. 'S': Compute the Schur form
 *  [in] computation_type ({'N'|'I'|'S'}) - Which Schur form to compute (if any)
 *                                           'N': No schur form is computed (the
 * matrix Q is not referenced)
 *                                           'I': The Schur vectors of H are
 * computed
 *                                           'V': The Schur vectors of A are
 * computed
 *  [in/out] Q - The Q-matrix of the upper Hessenberg reduction, computed by
 * ?gehrd & ?unghr. Will not be referenced if job_type='E'
 *  [out] evals - The resulting eigenvalues
 *
 * Return values:
 *   0 if the execution was a success
 *  <0 if the one or more of the input parameters had an illegal value
 *  >0 if the method failed to compute all of the eigenvalues
 */
template <typename MT1, // Type of the upper Hessenberg matrix
          typename MT2, // Type of the corresponding Q-matrix
          bool OT, // Order type of the upper Hessenberg matrix and the Q-matrix
          bool TF> // Transpose flag of the eigenvalue vector
inline lapack_int
zhseqr(blaze::DenseMatrix<MT1, OT> &hess, char job_type, char computation_type,
       lapack_int ilo, lapack_int ihi, blaze::DenseMatrix<MT2, OT> &Q,
       blaze::DynamicVector<std::complex<double>, TF> &evals)
{
  using boost::numeric_cast;

  BLAZE_CONSTRAINT_MUST_NOT_BE_ADAPTOR_TYPE(MT1);
  BLAZE_CONSTRAINT_MUST_NOT_BE_ADAPTOR_TYPE(MT2);
  BLAZE_CONSTRAINT_MUST_NOT_BE_COMPUTATION_TYPE(MT1);
  BLAZE_CONSTRAINT_MUST_NOT_BE_COMPUTATION_TYPE(MT2);
  BLAZE_CONSTRAINT_MUST_HAVE_MUTABLE_DATA_ACCESS(MT1);
  BLAZE_CONSTRAINT_MUST_HAVE_MUTABLE_DATA_ACCESS(MT2);
  BLAZE_CONSTRAINT_MUST_BE_SAME_TYPE(blaze::ElementType_<MT1>,
                                     std::complex<double>);
  BLAZE_CONSTRAINT_MUST_BE_SAME_TYPE(blaze::ElementType_<MT2>,
                                     std::complex<double>);

  const auto n = numeric_cast<int>((~hess).rows());
  const auto lda = numeric_cast<int>((~hess).spacing());
  constexpr auto matrix_layout =
      blaze::IsRowMajorMatrix<MT1>::value ? LAPACK_ROW_MAJOR : LAPACK_COL_MAJOR;

  evals.resize(n, false);

  return LAPACKE_zhseqr(matrix_layout, job_type, computation_type, n, ilo, ihi,
                        (~hess).data(), lda, evals.data(), (~Q).data(), lda);
}

/*
 * Reduce a complex Hermitian matrix to tridiagonal form
 *
 * Parameters:
 *  [in/out] mat - the matrix to convert to tridiagonal form, diagonal and
 * super(/sub)diagonal overwritten by corresponding values
 *                 of the tridiagonal, the remainder by the array tau,
 * representing the Q-matrix
 *  [in] storage_type ({'U'|'L'}) - whether the matrix elements are stored in
 * the Upper or Lower triangular part of the input matrix
 *  [out] d - the diagonal elements of the tridiagonal matrix
 *  [out] e - the off-diagonal elements of the tridiagonal matrix
 *  [out tau - the elementary reflectors of the unitary Q-matrix
 *
 * Return values:
 *   0 if the execution was a success
 *  <0 if the one or more of the input parameters had an illegal value
 */
template <typename MT, // Matrix type of the input matrix
          bool OT,     // Order type of the input matrix
          bool TF1,    // Transpose flag of the "d" output matrix
          bool TF2,    // Transpose flag of the "e" output matrix
          bool TF3>    // Transpose flag of the "tau" output matrix
inline lapack_int
zhetrd(blaze::DenseMatrix<MT, OT> &mat, char storage_type,
       blaze::DynamicVector<double, TF1> &d,
       blaze::DynamicVector<double, TF2> &e,
       blaze::DynamicVector<std::complex<double>, TF3> &tau)
{
  using boost::numeric_cast;

  BLAZE_CONSTRAINT_MUST_NOT_BE_ADAPTOR_TYPE(MT);
  BLAZE_CONSTRAINT_MUST_NOT_BE_COMPUTATION_TYPE(MT);
  BLAZE_CONSTRAINT_MUST_HAVE_MUTABLE_DATA_ACCESS(MT);
  BLAZE_CONSTRAINT_MUST_BE_SAME_TYPE(blaze::ElementType_<MT>,
                                     std::complex<double>);

  const auto n = numeric_cast<int>((~mat).rows());
  const auto lda = numeric_cast<int>((~mat).spacing());
  constexpr auto matrix_layout =
      blaze::IsRowMajorMatrix<MT>::value ? LAPACK_ROW_MAJOR : LAPACK_COL_MAJOR;

  d.resize(n, false);
  e.resize(std::max(1, n - 1), false);
  tau.resize(std::max(1, n - 1), false);

  return LAPACKE_zhetrd(matrix_layout, storage_type, n, (~mat).data(), lda,
                        d.data(), e.data(), tau.data());
}

/*
 * Explicitly generate the Q-matrix which determines the transformation in
 * zhetrd
 *
 * Parameters:
 *  [in/out] mat - the array returned by zhetrd, it will be overwritten by the
 * Q-matrix
 *  [in] storage_type ({'U'|'L'}) - whether the matrix elements are stored in
 * the Upper or Lower triangular part of the input matrix
 *  [in] tau - the tau array returned by zhetrd
 *
 * Return values:
 *   0 if the execution was a success
 *  <0 if the one or more of the input parameters had an illegal value
 */
template <typename MT, // Matrix type of the input matrix
          bool OT,     // Order type of the input matrix
          typename VT, // Vector type of the tau vector
          bool TF>     // Transpose flag of the "d" output matrix
inline lapack_int
zungtr(blaze::DenseMatrix<MT, OT> &mat, char storage_type,
       const blaze::DenseVector<VT, TF> &tau)
{
  using boost::numeric_cast;

  BLAZE_CONSTRAINT_MUST_NOT_BE_ADAPTOR_TYPE(MT);
  BLAZE_CONSTRAINT_MUST_NOT_BE_ADAPTOR_TYPE(VT);
  BLAZE_CONSTRAINT_MUST_HAVE_MUTABLE_DATA_ACCESS(MT);
  BLAZE_CONSTRAINT_MUST_HAVE_MUTABLE_DATA_ACCESS(VT);
  BLAZE_CONSTRAINT_MUST_NOT_BE_COMPUTATION_TYPE(MT);
  BLAZE_CONSTRAINT_MUST_NOT_BE_COMPUTATION_TYPE(VT);
  BLAZE_CONSTRAINT_MUST_BE_SAME_TYPE(blaze::ElementType_<MT>,
                                     std::complex<double>);
  BLAZE_CONSTRAINT_MUST_BE_SAME_TYPE(blaze::ElementType_<VT>,
                                     std::complex<double>);

  const auto n = numeric_cast<int>((~mat).rows());
  const auto lda = numeric_cast<int>((~mat).spacing());
  constexpr auto matrix_layout =
      blaze::IsRowMajorMatrix<MT>::value ? LAPACK_ROW_MAJOR : LAPACK_COL_MAJOR;

  if (std::max(1, n - 1) < (~tau).size())
    throw std::runtime_error{"zungtr: the tau matrix is on the incorrect form"};

  return LAPACKE_zungtr(matrix_layout, storage_type, n, (~mat).data(), lda,
                        (~tau).data());
}

/*
 * Compute the eigenvalues and eigenvectors of a Hermitian matrix reduced to
 * tridiagonal form
 *
 * Parameters:
 *  [in/out] mat - if job_type='V' then mat has to be the Q-matrix for the
 * tridiagonal reduction, otherwise it will not be referenced
 *                 if job_type!='N' it will contain an n-by-n matrix with the
 * orthonormal eigenvectors
 *  [in] job_type ({'N'|'I'|'V'}) - specified what the algorithm computes
 *                                    'N': compute eigenvalues only
 *                                    'I': compute eigenvectors of of the
 * tridiagonal matrix
 *                                    'V': compute eigenvectors of the original
 * hermitian matrix
 *  [in/out] eigen_values - when called must contain the diagonal of the
 * tridiagonal matrix, vector 'd' from zhetrd
 *                          will contain the eigenvalues if the function call is
 * a success
 *  [in/out] e - the off-diagonal elements of the trigiagonal matrix, vector 'e'
 * from zhetrd, will be overwritten
 *
 * Return values:
 *   0 if the execution was a success
 *  <0 if the one or more of the input parameters had an illegal value
 *  >0 if the method failed to compute all of the eigenvalues
 */
template <typename MT, // Matrix type of the input matrix
          bool OT,     // Order type of the input matrix
          bool TF1,    // Transpose flag of the eigen_values input matrix
          bool TF2>    // Transpose flag of the "e" input matrix
inline lapack_int
zsteqr(blaze::DenseMatrix<MT, OT> &mat, char job_type,
       blaze::DynamicVector<double, TF1> &eigen_values,
       blaze::DynamicVector<double, TF2> &e)
{
  using boost::numeric_cast;

  BLAZE_CONSTRAINT_MUST_NOT_BE_ADAPTOR_TYPE(MT);
  BLAZE_CONSTRAINT_MUST_HAVE_MUTABLE_DATA_ACCESS(MT);
  BLAZE_CONSTRAINT_MUST_NOT_BE_COMPUTATION_TYPE(MT);
  BLAZE_CONSTRAINT_MUST_BE_SAME_TYPE(blaze::ElementType_<MT>,
                                     std::complex<double>);

  const auto n = numeric_cast<int>(eigen_values.size());
  const auto ldz = std::max(1, numeric_cast<int>((~mat).spacing()));
  constexpr auto matrix_layout =
      blaze::IsRowMajorMatrix<MT>::value ? LAPACK_ROW_MAJOR : LAPACK_COL_MAJOR;

  if (std::max(1, n - 1) < e.size())
    throw std::runtime_error{
        "zsteqr: the input matrices have the wrong dimensions"};

  return LAPACKE_zsteqr(matrix_layout, job_type, n, eigen_values.data(),
                        e.data(), (~mat).data(), ldz);
}

} // namespace internal
} // namespace matrix
} // namespace rmt

#endif /* LAPACKE_EIGENVALUE_WRAPPERS_HPP */
