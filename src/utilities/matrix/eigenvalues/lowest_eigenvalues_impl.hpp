/*
 * Created: 08-11-2016
 * Modified: Tue 14 Mar 2017 13:51:22 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef MATRIX_LOWEST_EIGENVALUES_IMPL_HPP
#define MATRIX_LOWEST_EIGENVALUES_IMPL_HPP

#include "lowest_eigenvalues.hpp"
#include <algorithm>
#include <sstream>
#include <utilities/matrix/eigenvalues/eigenvalue_solver.hpp>

namespace rmt {
namespace matrix {

template <typename MT, bool OT>
inline std::vector<typename real_type<blaze::ElementType_<MT>>::value>
Lowest_Eigenvalues::operator()(const blaze::DenseMatrix<MT, OT> &input_matrix,
                               std::size_t num_eigvals)
{
  using element_type = blaze::ElementType_<MT>;
  using value_type = typename real_type<element_type>::value;

  if (!blaze::isHermitian(~input_matrix)) {
    std::ostringstream oss;
    oss << "Lowest_Eigenvalues: The input-matrix is not Hermitian\n"
        << "Matrix content:\n"
        << input_matrix;

    throw std::runtime_error{oss.str()};
  }

  if (num_eigvals > (~input_matrix).rows())
    throw std::runtime_error{"Lowest_Eigenvalues: Too many eigenvalues"};

  matrix::Eigenvalue_Solver<matrix::precission_type<element_type>::value,
                            matrix::Symmetries::Hermitian>
      solver;

  solver.Compute(input_matrix, eigenvalues_only);

  auto evals = solver.Eigenvalues();

  std::sort(evals.begin(), evals.end());

  auto result = std::vector<value_type>(num_eigvals);
  std::copy_n(evals.begin(), num_eigvals, result.begin());

  return result;
}

} // namespace matrix
} // namespace rmt

#endif /* MATRIX_LOWEST_EIGENVALUES_IMPL_HPP */
