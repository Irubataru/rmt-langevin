/*
 * Created: 01-11-2016
 * Modified: Wed 02 Nov 2016 12:11:55 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef MATRIX_IS_EQUAL_HPP
#define MATRIX_IS_EQUAL_HPP

#include <blaze/math/DenseMatrix.h>
#include <number_type.hpp>

namespace {

inline bool equal(const rmt::numeric::complex_type &lhs,
                  const rmt::numeric::complex_type &rhs,
                  rmt::numeric::number_type error)
{
  return rmt::numeric::fabs(lhs - rhs) <=
         rmt::numeric::max(error, error * rmt::numeric::fabs(lhs));
}
}

namespace rmt {
namespace matrix {

template <typename T1, // Type of the left-hand side dense matrix
          bool O1,     // Order type of the left-hand side dense matrix
          typename T2, // Type of the right-hand side dense matrix
          bool O2>     // Order type of the right-hand side dense matrix
inline bool
Is_Equal(const blaze::DenseMatrix<T1, O1> &lhs,
         const blaze::DenseMatrix<T2, O2> &rhs)
{
  return lhs == rhs;
}

template <typename T1, // Type of the left-hand side dense matrix
          typename T2> // Type of the right-hand side dense matrix
inline bool
Is_Equal(const blaze::DenseMatrix<T1, blaze::rowMajor> &lhs,
         const blaze::DenseMatrix<T2, blaze::rowMajor> &rhs,
         numeric::number_type error)
{
  using CT1 = blaze::CompositeType_<T1>;
  using CT2 = blaze::CompositeType_<T2>;

  if ((~lhs).rows() != (~rhs).rows() or (~lhs).columns() != (~rhs).columns())
    return false;

  auto A = CT1{~lhs};
  auto B = CT2{~rhs};

  for (auto i = 0ul; i < A.rows(); ++i)
    for (auto j = 0ul; j < A.columns(); ++j)
      if (!equal(A(i, j), B(i, j), rmt::numeric::fabs(error)))
        return false;

  return true;
}

} // namespace matrix
} // namespace rmt

#endif /* MATRIX_IS_EQUAL_HPP */
