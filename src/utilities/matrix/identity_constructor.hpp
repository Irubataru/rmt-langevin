/*
 * Created: 01-11-2016
 * Modified: Tue 01 Nov 2016 14:34:48 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef IDENTITY_CONSTRUCTOR_HPP
#define IDENTITY_CONSTRUCTOR_HPP

#include <types.hpp>
#include <utilities/matrix/default_constructor.hpp>

namespace rmt {
namespace matrix {

inline M_type
Identity_Matrix(numeric::complex_type value = numeric::complex_type{1.0})
{
  auto identity_matrix = Matrix(numeric::complex_type{0.});

  for (auto i = 0; i < M_size; ++i)
    identity_matrix(i, i) = value;

  return identity_matrix;
}

inline void Identity_Matrix(M_type &mat,
                            numeric::complex_type value = numeric::complex_type{
                                1.0})
{
  for (auto i = 0; i < M_size; ++i) {
    for (auto j = 0; j < M_size; ++j)
      mat(i, j) = numeric::complex_type{0.};

    mat(i, i) = value;
  }
}

} // namespace matrix
} // namespace rmt

#endif /* IDENTITY_CONSTRUCTOR_HPP */
