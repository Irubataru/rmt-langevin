/*
 * Created: 01-11-2016
 * Modified: Tue 01 Nov 2016 15:47:18 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef MATRIX_DEFAULT_CONSTRUCTOR_HPP
#define MATRIX_DEFAULT_CONSTRUCTOR_HPP

#include <types.hpp>

namespace rmt {

namespace matrix {

#ifdef DYNAMIC

inline M_type Matrix()
{
  return M_type(M_size, M_size);
}

inline M_type Matrix(numeric::complex_type init_value)
{
  return M_type(M_size, M_size, init_value);
}

#else

inline M_type Matrix()
{
  return M_type();
}

inline M_type Matrix(numeric::complex_type init_value)
{
  return M_type(init_value);
}

#endif

inline M_type Zero_Matrix()
{
  return Matrix(numeric::complex_type{0.});
}

} // namespace matrix

} // namespace rmt

#endif /* MATRIX_DEFAULT_CONSTRUCTOR_HPP */
