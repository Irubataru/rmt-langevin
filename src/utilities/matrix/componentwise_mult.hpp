/*
 * Created: 01-11-2016
 * Modified: Mon 07 Nov 2016 17:57:02 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef MATRIX_COMPONENTWISE_MULT_HPP
#define MATRIX_COMPONENTWISE_MULT_HPP

#include <types.hpp>
#include <utilities/matrix/default_constructor.hpp>

namespace rmt {
namespace matrix {

template <typename T1, // Type of the left-hand side dense matrix
          typename T2> // Type of the right-hand side dense matrix
inline M_type
Componentwise_Multiplication(const blaze::DenseMatrix<T1, blaze::rowMajor> &lhs,
                             const blaze::DenseMatrix<T2, blaze::rowMajor> &rhs)
{
  typedef blaze::CompositeType_<T1> CT1;
  typedef blaze::CompositeType_<T2> CT2;

  if ((~lhs).rows() != (~rhs).rows() or (~lhs).columns() != (~rhs).columns())
    throw std::runtime_error{"rmt::matrix::Componentwise_Multiplication: "
                             "Cannot multiply matrices of different "
                             "dimensions"};

  auto result = Matrix();

  CT1 A(~lhs);
  CT2 B(~rhs);

  for (auto i = 0ul; i < A.rows(); ++i)
    for (auto j = 0ul; j < A.columns(); ++j)
      result(i, j) = A(i, j) * B(i, j);

  return result;
}

template <typename T1, // Type of the left-hand side dense matrix
          typename T2> // Type of the right-hand side dense matrix
inline M_type
Componentwise_Multiplication(
    const blaze::DenseMatrix<T1, blaze::columnMajor> &lhs,
    const blaze::DenseMatrix<T2, blaze::columnMajor> &rhs)
{
  typedef blaze::CompositeType_<T1> CT1;
  typedef blaze::CompositeType_<T2> CT2;

  if ((~lhs).rows() != (~rhs).rows() or (~lhs).columns() != (~rhs).columns())
    throw std::runtime_error{"rmt::matrix::Componentwise_Multiplication: "
                             "Cannot multiply matrices of different "
                             "dimensions"};

  auto result = Matrix();

  CT1 A(~lhs);
  CT2 B(~rhs);

  for (auto j = 0ul; j < A.columns(); ++j)
    for (auto i = 0ul; i < A.rows(); ++i)
      result(i, j) = A(i, j) * B(i, j);

  return result;
}

template <
    typename T1, // Type of the left-hand side dense matrix
    typename T2, // Type of the right-hand side dense matrix
    bool OT> // Orientation type of the lhs matrix, oposite of the rhs matrix
inline M_type
Componentwise_Multiplication(const blaze::DenseMatrix<T1, OT> &lhs,
                             const blaze::DenseMatrix<T2, !OT> &rhs)
{
  typedef blaze::CompositeType_<T1> CT1;
  typedef blaze::CompositeType_<T2> CT2;

  if ((~lhs).rows() != (~rhs).rows() or (~lhs).columns() != (~rhs).columns())
    throw std::runtime_error{"rmt::matrix::Componentwise_Multiplication: "
                             "Cannot multiply matrices of different "
                             "dimensions"};

  auto result = Matrix();

  CT1 A(~lhs);
  CT2 B(~rhs);

  for (auto i = 0ul; i < A.rows(); ++i)
    for (auto j = 0ul; j < A.columns(); ++j)
      result(i, j) = A(i, j) * B(i, j);

  return result;
}

} // namespace matrix
} // namespace rmt

#endif /* MATRIX_COMPONENTWISE_MULT_HPP */
