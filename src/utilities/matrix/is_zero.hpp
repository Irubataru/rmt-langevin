/*
 * Created: 01-11-2016
 * Modified: Wed 02 Nov 2016 11:33:43 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef MATRIX_IS_ZERO_HPP
#define MATRIX_IS_ZERO_HPP

#include <blaze/math/DenseMatrix.h>
#include <number_type.hpp>

namespace rmt {
namespace matrix {

template <typename MT, // Type of the dense matrix
          bool OT>     // Order type of the matrix
inline bool
Is_Zero(const blaze::DenseMatrix<MT, OT> &mat)
{
  using CT = blaze::CompositeType_<MT>;

  auto A = CT{~mat};

  for (auto i = 0ul; i < A.rows(); ++i)
    for (auto j = 0ul; j < A.columns(); ++j)
      if (A(i, j) != numeric::complex_type{0.})
        return false;

  return true;
}

template <typename MT, // Type of the dense matrix
          bool OT>     // Order type of the matrix
inline bool
Is_Zero(const blaze::DenseMatrix<MT, OT> &mat, numeric::number_type error)
{
  using CT = blaze::CompositeType_<MT>;

  auto A = CT{~mat};

  for (auto i = 0ul; i < A.rows(); ++i)
    for (auto j = 0ul; j < A.columns(); ++j)
      if (numeric::abs(A(i, j)) > error)
        return false;

  return true;
}

} // namespace matrix
} // namespace rmt

#endif /* MATRIX_IS_ZERO_HPP */
