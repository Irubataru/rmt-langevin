/*
 * Created: 07-11-2016
 * Modified: Mon 07 Nov 2016 19:39:27 GMT
 * Author: Jonas R. MATRIX_Glesaaen (jonas@glesaaen.com)
 */

#ifndef MATRIX_GENERATE_CONSTRUCTOR_HPP
#define MATRIX_GENERATE_CONSTRUCTOR_HPP

#include <types.hpp>
#include <utilities/matrix/default_constructor.hpp>

namespace rmt {
namespace matrix {

template <typename Function>
inline M_type Random_Matrix(Function &&dist)
{
  auto generated_matrix = Matrix(numeric::complex_type{0.});

  for (auto i = 0; i < M_size; ++i)
    for (auto j = 0; j < M_size; ++j)
      generated_matrix(i, j) = function();

  return generated_matrix;
}

} // namespace matrix
} // namespace rmt

#endif /* MATRIX_GENERATE_CONSTRUCTOR_HPP */
