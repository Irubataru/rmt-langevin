
/*
 * Created: 01-09-2017
 * Modified: Fri 01 Sep 2017 12:07:07 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef ENUM_HELPERS_HPP
#define ENUM_HELPERS_HPP

#include <cstdlib>

namespace rmt {
namespace utility {

template <typename Enum>
inline constexpr std::size_t enum_value(Enum e)
{
  return static_cast<std::size_t>(e);
}

template <typename Enum>
inline constexpr std::size_t enum_size()
{
  return static_cast<std::size_t>(Enum::End_);
}

template <typename Enum1, typename Enum2>
struct Combine_Enums
{
  constexpr std::size_t operator()(Enum1 e1, Enum2 e2)
  {
    return enum_value(e1) * enum_size<Enum2>() + enum_value(e2);
  }
};

} // namespace utility 
} // namespace rmt 


#endif /* ENUM_HELPERS_HPP */
