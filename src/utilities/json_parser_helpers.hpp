
/*
 * Created: 31-08-2017
 * Modified: Mon 06 Nov 2017 10:00:34 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef JSON_PARSER_HELPERS_HPP
#define JSON_PARSER_HELPERS_HPP

#include <algorithm>
#include <builders/dictionary.hpp>
#include <nlohmann/json.hpp>
#include <stdexcept>
#include <types.hpp>
#include <utilities/enum_helpers.hpp>
#include <utilities/json_names.hpp>

namespace rmt {
namespace parse {

class invalid_parse_exception : public std::runtime_error
{
public:
  invalid_parse_exception(std::string json_path, std::string json_key)
      : std::runtime_error{"error while parsing \"" + json_path + "\": key \"" +
                           json_key + "\" is required but not provided"}
  {
  }

  invalid_parse_exception() = default;
};

inline std::string to_string(Model_Type val)
{
  switch (val) {
  case Model_Type::Stephanov:
    return "stephanov";
  case Model_Type::Osborn:
    return "osborn";
  case Model_Type::Begin_:
    return "Model_Type";
  default:
    throw std::runtime_error{"to_string<Model_Type> invalid enum value"};
  }
}

inline std::string to_string(Storage_Type val)
{
  switch (val) {
  case Storage_Type::Cartesian:
    return "cartesian";
  case Storage_Type::Polar:
    return "polar";
  case Storage_Type::Begin_:
    return "Storage_Type";
  default:
    throw std::runtime_error{"to_string<Storage_Type> invalid enum value"};
  }
}

inline std::string to_string(Stepsize_Type val)
{
  switch (val) {
  case Stepsize_Type::Constant:
    return "constant";
  case Stepsize_Type::Adaptive:
    return "adaptive";
  case Stepsize_Type::Begin_:
    return "Stepsize_Type";
  default:
    throw std::runtime_error{"to_string<Stepsize_Type> invalid enum value"};
  }
}

inline std::string to_string(Minimise_Alg val)
{
  switch (val) {
  case Minimise_Alg::Brent:
    return "brent";
  case Minimise_Alg::Fixed:
    return "fixed";
  case Minimise_Alg::Begin_:
    return "Minimise_Alg";
  default:
    throw std::runtime_error{"to_string<Minimise_Alg> invalid enum value"};
  }
}

inline std::string to_string(Derivative_Alg val)
{
  switch (val) {
  case Derivative_Alg::Numerical:
    return "numerical";
  case Derivative_Alg::Analytic:
    return "analytic";
  case Derivative_Alg::Begin_:
    return "Derivative_Alg";
  default:
    throw std::runtime_error{"to_string<Derivative_Alg> invalid enum value"};
  }
}

template <typename Enum>
inline std::string enum_key_string() = delete;

template <>
inline std::string enum_key_string<Model_Type>()
{
  return build::JSON_Parameters::model;
}

template <>
inline std::string enum_key_string<Storage_Type>()
{
  return build::JSON_Parameters::storage;
}

template <typename Enum>
inline Enum find_enum(std::string str)
{
  using utility::enum_value;

  for (auto x = enum_value(Enum::Begin_) + 1; x != enum_value(Enum::End_);
       ++x) {
    if (to_string(static_cast<Enum>(x)) == str)
      return static_cast<Enum>(x);
  }

  return Enum::End_;
}

inline nlohmann::json parse_required_key(nlohmann::json const &main_tree,
                                         std::string key, std::string path = "")
{
  if (main_tree.find(key) == main_tree.end())
    throw invalid_parse_exception{path, key};

  return main_tree[key];
}

template <typename Enum>
inline Enum parse_enum(nlohmann::json const &tree, std::string key,
                       std::string path = "")
{
  std::string val = parse_required_key(tree, key, path);
  std::transform(val.begin(), val.end(), val.begin(), ::tolower);

  auto e_val = find_enum<Enum>(val);

  if (e_val == Enum::End_)
    throw std::runtime_error{"error while parsing enum with key \"" + key +
                             "\": \"" + val + "\" is not a valid " +
                             to_string(Enum::Begin_)};

  return e_val;
}

template <typename Enum>
inline Enum parse_enum(nlohmann::json const &tree)
{
  return parse_enum<Enum>(tree, enum_key_string<Enum>());
}

template <typename Enum>
inline Enum global_parse(nlohmann::json const &tree)
{
  return parse_enum<Enum>(tree.at(build::JSON_Categories::parameters));
}

inline bool has_key(nlohmann::json const &tree, std::string key)
{
  return tree.find(key) != tree.end();
}

inline bool has_keys(nlohmann::json const &tree)
{
  return true;
}

template <typename... Args>
inline bool has_keys(nlohmann::json const &tree, std::string key, Args ...tail)
{
  return has_key(tree, key) and has_keys(tree, tail...);
}

template <typename Entry_Type>
inline auto
check_dictionary_by_key(std::string key, Model_Type model, Storage_Type storage,
                        build::core::Dictionary<Entry_Type> const &dict)
    -> decltype(dict.get(key))
{
  auto model_storage_key =
      parse::to_string(model) + "_" + parse::to_string(storage) + "_" + key;

  auto model_key = parse::to_string(model) + "_" + key;

  if (dict.has_key(model_storage_key)) {
    return dict.get(model_storage_key);
  } else if (dict.has_key(model_key)) {
    return dict.get(model_key);
  } else if (dict.has_key(key)) {
    return dict.get(key);
  } else {
    throw std::runtime_error{"dictionary lookup error: no key by name \"" +
                             key + "\" for " + parse::to_string(model) +
                             "::" + parse::to_string(storage)};
  }
}

} // namespace parse
} // namespace rmt

#endif /* JSON_PARSER_HELPERS_HPP */
