/*
 * Created: 22-04-2016
 * Modified: Sun 12 Jun 2016 16:57:56 CEST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "scan_grammar.hpp"
#include <boost/spirit/include/phoenix_bind.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <string>
#include <utilities/converters.hpp>

namespace rmt {
namespace scan {
namespace grammar {

template <typename Iterator>
double_vector_grammar<Iterator>::double_vector_grammar()
    : double_vector_grammar::base_type(start)
{
  using qi::lit;
  using qi::double_;
  using qi::_val;
  using qi::_1;
  using qi::_2;
  using qi::_3;

  start = (lit("seq") >> double_ >> double_ >>
           double_)[_val = phx::bind(&::seq<double>, _1, _2, _3)] |
          (+(double_))[_val = _1];
}

template struct double_vector_grammar<std::string::const_iterator>;

} // namespace grammar
} // namespace scan
} // namespace rmt
