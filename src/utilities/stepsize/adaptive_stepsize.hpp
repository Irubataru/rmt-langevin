
/*
 * Created: 15-12-2016
 * Modified: Tue 10 Jan 2017 15:15:24 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef ADAPTIVE_STEPSIZE_HPP
#define ADAPTIVE_STEPSIZE_HPP

#include <functional>
#include <memory>
#include <simskel/update/update_int.hpp>
#include <utilities/stepsize/stepsize_keeper.hpp>

namespace rmt {
namespace stepsize {

class Adaptive_Stepsize : public ::simskel::Simple_Update
{
public:
  using value_type = Stepsize::value_type;

  Adaptive_Stepsize(value_type base_step, std::shared_ptr<Stepsize> step_ptr)
      : base_stepsize{base_step}, stepsize{std::move(step_ptr)}
  {
  }

private:
  virtual void DoUpdate() override
  {
    stepsize->value = base_stepsize * weight();
  }

  virtual value_type weight() = 0;

  value_type base_stepsize;
  std::shared_ptr<Stepsize> stepsize;
};

} // namespace stepsize
} // namespace rmt

#endif /* ADAPTIVE_STEPSIZE_HPP */
