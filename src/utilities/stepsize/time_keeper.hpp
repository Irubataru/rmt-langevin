
/*
 * Created: 15-12-2016
 * Modified: Tue 05 Sep 2017 15:44:33 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef TIME_KEEPER_HPP
#define TIME_KEEPER_HPP

#include <memory>
#include <simskel/update/update_int.hpp>
#include <utilities/stepsize/stepsize_keeper.hpp>

namespace rmt {
namespace stepsize {

class Time_Keeper : public ::simskel::Simple_Update
{
public:
  using value_type = Stepsize::value_type;

  explicit Time_Keeper(value_type start_time,
                       std::shared_ptr<const Stepsize> stepsize_ptr)
      : langevin_time{start_time}, stepsize{std::move(stepsize_ptr)}
  {
  }

  explicit Time_Keeper(std::shared_ptr<const Stepsize> stepsize_ptr)
      : Time_Keeper{value_type{0.}, stepsize_ptr}
  {
  }

  value_type time() const noexcept
  {
    return langevin_time;
  }

private:
  virtual void DoUpdate() override
  {
    langevin_time += stepsize->value;
  }

  value_type langevin_time;
  std::shared_ptr<const Stepsize> stepsize;
};

} // namespace stepsize
} // namespace rmt

#endif /* TIME_KEEPER_HPP */
