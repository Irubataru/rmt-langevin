
/*
 * Created: 15-12-2016
 * Modified: Thu 15 Dec 2016 16:21:37 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPSIZE_OBSERVABLE_HPP
#define STEPSIZE_OBSERVABLE_HPP

#include <memory>
#include <simskel/observable/observable_interface.hpp>
#include <utilities/stepsize/stepsize_keeper.hpp>

namespace rmt {
namespace stepsize {

class Stepsize_Observable
    : public ::simskel::ObservableWRecord<numeric::number_type>
{
public:
  using value_type = numeric::number_type;

  explicit Stepsize_Observable(std::shared_ptr<const Stepsize> stepsize_ptr)
      : stepsize{std::move(stepsize_ptr)}
  {
  }

  virtual ~Stepsize_Observable()
  {
  }

private:
  virtual value_type CalculateValue() override
  {
    return stepsize->value;
  }

  std::shared_ptr<const Stepsize> stepsize;
};

} // namespace stepsize
} // namespace rmt

#endif /* STEPSIZE_OBSERVABLE_HPP */
