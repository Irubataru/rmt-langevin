
/*
 * Created: 15-12-2016
 * Modified: Thu 15 Dec 2016 16:21:07 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPSIZE_KEEPER_HPP
#define STEPSIZE_KEEPER_HPP

#include <number_type.hpp>

namespace rmt {
namespace stepsize {

struct Stepsize
{
  using value_type = numeric::number_type;
  Stepsize() = delete;

  explicit Stepsize(value_type init_step) noexcept : value{init_step}
  {
  }

  value_type value;
};

} // namespace stepsize
} // namespace rmt

#endif /* STEPSIZE_KEEPER_HPP */
