TARGETS := gitversion.cpp gitversion.o

gitversion.cpp_DEPS := $(TOP)/.git/HEAD $(TOP)/.git/index
gitversion.cpp_CMD  = @echo "\\\#include<utilities/git/gitversion.hpp>" > $@ && \
	echo "namespace git {" >> $@ && \
  echo "const std::string git_version {\"$(shell git rev-parse HEAD)\"};" >> $@ && \
  echo "}" >> $@

gitversion.o_DEPS := $(OBJDIR)/gitversion.cpp
