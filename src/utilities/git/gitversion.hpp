/*
 * Created: 28-04-2016
 * Modified: Thu 28 Apr 2016 13:39:34 CEST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef GITVERSION_HPP
#define GITVERSION_HPP

#include <string>

namespace git {

extern const std::string git_version;

} // namespace git

#endif /* GITVERSION_HPP */
