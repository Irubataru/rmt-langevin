
/*
 * Created: 02-05-2017
 * Modified: Tue 02 May 2017 14:49:34 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef ARCHIVE_WRAPPER_HPP
#define ARCHIVE_WRAPPER_HPP

#include <ostream>
#include <types.hpp>
#include <blaze/util/Serialization.h>
#include <stephanov/resource/configuration_pair.hpp>

namespace rmt {
namespace output {

class archive_wrapper
{
  template <typename P>
  friend archive_wrapper &operator<<(archive_wrapper &, const P &);

public:
  explicit archive_wrapper(std::ostream &os_ref) : archive{os_ref}
  {
  }

  void print(const stephanov::Configuration_Pair &);
  void print(const stephanov::cartesian::Storage_Pair &);
  void print(const stephanov::polar::Storage_Pair &);

  void next()
  {
  }

  static bool parameter_header_q()
  {
    return false;
  }

private:
  blaze::Archive<std::ostream> archive;
};

// Default print behaviour for every type combination
template <typename T>
inline archive_wrapper &operator<<(archive_wrapper &aw, const T &val)
{
  aw.print(val);
  return aw;
}

} // namespace output
} // namespace rmt

#endif /* ARCHIVE_WRAPPER_HPP */
