
/*
 * Created: 02-05-2017
 * Modified: Tue 02 May 2017 14:33:04 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef VALUE_PRINTERS_HPP
#define VALUE_PRINTERS_HPP

#include "stream_wrapper.hpp"
#include "archive_wrapper.hpp"

namespace rmt {
namespace output {

template <typename T>
struct wrapper_selector
{
  using value = stream_wrapper;
};

template <>
struct wrapper_selector<stephanov::Configuration_Pair>
{
  using value = archive_wrapper;
};

template <>
struct wrapper_selector<stephanov::cartesian::Storage_Pair>
{
  using value = archive_wrapper;
};

template <>
struct wrapper_selector<stephanov::polar::Storage_Pair>
{
  using value = archive_wrapper;
};


} // namespace output 
} // namespace rmt 

#endif /* VALUE_PRINTERS_HPP */
