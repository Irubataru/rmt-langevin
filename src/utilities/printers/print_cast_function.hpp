
/*
 * Created: 06-03-2017
 * Modified: Thu 06 Apr 2017 16:54:32 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef PRINT_CAST_FUNCTION_HPP
#define PRINT_CAST_FUNCTION_HPP

namespace rmt {

namespace simskel {

template <typename Type>
class Named_Observable;

} // namespace simskel

namespace output {

class Output_Visitor;

template <typename Obs_Type>
void cast_and_print(Output_Visitor *visitor,
                    const observable::Named_Observable<Obs_Type> &obs);

template <typename Obs_Type>
void cast_and_print_and_empty(Output_Visitor *visitor,
                              observable::Named_Observable<Obs_Type> &obs);

} // namespace output
} // namespace rmt

#include "file_printer.hpp"

namespace rmt {
namespace output {

template <typename Obs_Type>
inline void cast_and_print(Output_Visitor *visitor,
                           const observable::Named_Observable<Obs_Type> &obs)
{
  if (auto printer = dynamic_cast<File_Printer *>(visitor))
    printer->print(obs);
  else
    throw std::runtime_error{
        "Cannot cast Output_Visitor* to an appropriate printer"};
}

template <typename Obs_Type>
inline void
cast_and_print_and_empty(Output_Visitor *visitor,
                         observable::Named_Observable<Obs_Type> &obs)
{
  if (auto printer = dynamic_cast<File_Printer *>(visitor))
    printer->print_append(obs);
  else
    throw std::runtime_error{
        "Cannot cast Output_Visitor* to an appropriate printer"};
}

} // namespace output
} // namespace rmt

#endif /* PRINT_CAST_FUNCTION_HPP */
