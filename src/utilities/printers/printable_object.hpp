
/*
 * Created: 06-03-2017
 * Modified: Thu 06 Apr 2017 16:45:29 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef PRINTABLE_OBJECT_HPP
#define PRINTABLE_OBJECT_HPP

namespace rmt {
namespace output {

class Output_Visitor
{
public:
  virtual ~Output_Visitor()
  {
  }
};

class Printable
{
public:
  virtual void print(Output_Visitor *) = 0;
  virtual void print_and_empty(Output_Visitor *) = 0;
};

} // namespace output
} // namespace rmt

#endif /* PRINTABLE_OBJECT_HPP */
