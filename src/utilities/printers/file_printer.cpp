
/*
 * Created: 06-03-2017
 * Modified: Thu 31 Aug 2017 19:07:19 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "file_printer.hpp"
#include <boost/filesystem.hpp>
#include <utilities/git/gitversion.hpp>

namespace fs = boost::filesystem;

namespace rmt {
namespace output {

void File_Printer::create_folder()
{
  auto save_dir = fname_gen.directory(phys_params, model_parms);

  if (!fs::is_directory(fs::path{save_dir}))
    fs::create_directories(fs::path{save_dir});
}

bool File_Printer::file_exists(const std::string &filename)
{
  return fs::is_regular_file(fs::path{filename});
}

} // namespace output
} // namespace rmt
