
/*
 * Created: 02-05-2017
 * Modified: Tue 02 May 2017 14:30:57 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "archive_wrapper.hpp"
#include <blaze/math/serialization/MatrixSerializer.h>

namespace rmt {
namespace output {

void archive_wrapper::print(const stephanov::Configuration_Pair &val)
{
  archive << val.X << val.XC;
}

void archive_wrapper::print(const stephanov::cartesian::Storage_Pair &val)
{
  archive << val.a << val.b;
}

void archive_wrapper::print(const stephanov::polar::Storage_Pair &val)
{
  archive << val.r << val.phi;
}

} // namespace output
} // namespace rmt
