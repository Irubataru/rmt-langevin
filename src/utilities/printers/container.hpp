
/*
 * Created: 06-03-2017
 * Modified: Fri 01 Sep 2017 16:21:56 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef PRINTER_CONTAINER_HPP
#define PRINTER_CONTAINER_HPP

#include "printable_object.hpp"
#include <list>
#include <memory>
#include <simskel/runner/langevin.hpp>

namespace rmt {
namespace output {

using Printable_List = std::list<std::shared_ptr<Printable>>;

template <typename Observable, typename... Args>
void register_new_observable(::simskel::langevin::LangevinRunner &runner,
                             Printable_List &obs_list, Args &&... args);

template <typename Observable, typename... Args>
void register_every_n_observable(::simskel::langevin::LangevinRunner &runner,
                                 Printable_List &obs_list, std::size_t n,
                                 Args &&... args);

} // namespace output
} // namespace rmt

#include <simskel/utility_observables.hpp>

namespace rmt {
namespace output {

template <typename Observable, typename... Args>
inline void register_new_observable(::simskel::langevin::LangevinRunner &runner,
                                    Printable_List &obs_list, Args &&... args)
{
  auto obs = std::make_shared<Observable>(std::forward<Args>(args)...);
  runner.AddObservable(obs);
  obs_list.push_back(obs);
}

template <typename Observable, typename... Args>
inline void
register_every_n_observable(::simskel::langevin::LangevinRunner &runner,
                            Printable_List &obs_list, std::size_t n,
                            Args &&... args)
{
  auto obs = std::make_shared<Observable>(std::forward<Args>(args)...);
  runner.AddObservable(std::make_shared<observable::Measure_Every_N>(obs, n));
  obs_list.push_back(obs);
}

} // namespace output
} // namespace rmt

#endif /* PRINTER_CONTAINER_HPP */
