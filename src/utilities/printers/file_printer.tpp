
/*
 * Created: 06-03-2017
 * Modified: 
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef FILE_PRINTER_TPP
#define FILE_PRINTER_TPP

#include "value_printers.hpp"
#include <simskel/named_observable.hpp>
#include <iomanip>

namespace rmt {
namespace output {

template <typename Type>
inline void
File_Printer::print(const observable::Named_Observable<Type> &obs)
{
  using value_type = typename observable::Named_Observable<Type>::value_type;
  using wrapper_type = typename wrapper_selector<Type>::value;
  create_folder();

  auto filename = fname_gen.filename(phys_params, model_parms, obs.dir_name());

  std::ofstream ofs{filename};

  if (!ofs)
    throw std::runtime_error{"Cannot open file \"" + filename +
                             "\" to print observable"};

  ofs << std::setprecision(10);

  if (wrapper_type::parameter_header_q())
    print_parameter_header(ofs);

  wrapper_type vp{ofs};

  for (const auto& val : obs.data()) {
    vp << val;
    vp.next();
  }

  obs.set_printed();
}

template <typename Type>
inline void
File_Printer::print_append(observable::Named_Observable<Type> &obs)
{
  using value_type = typename observable::Named_Observable<Type>::value_type;
  using wrapper_type = typename wrapper_selector<Type>::value;
  create_folder();

  auto filename = fname_gen.filename(phys_params, model_parms, obs.dir_name());

  std::ofstream ofs{};

  if (!obs.has_been_printed())
    ofs.open(filename, std::fstream::out);
  else
    ofs.open(filename, std::fstream::out | std::fstream::app);

  if (!ofs)
    throw std::runtime_error{"Cannot open file \"" + filename +
                             "\" to print observable"};

  ofs << std::setprecision(10);

  if (!obs.has_been_printed() and wrapper_type::parameter_header_q())
    print_parameter_header(ofs);

  wrapper_type vp{ofs};

  for (auto const& val : obs.data()) {
    vp << val;
    vp.next();
  }

  obs.clear();
  obs.set_printed();
}

} // namespace output
} // namespace rmt

#endif /* FILE_PRINTER_TPP */
