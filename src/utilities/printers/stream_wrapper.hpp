
/*
 * Created: 06-03-2017
 * Modified: Fri 05 May 2017 10:55:01 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STREAM_WRAPPER_HPP
#define STREAM_WRAPPER_HPP

#include <ostream>
#include <stephanov/observables/container_types.hpp>
#include <types.hpp>

namespace rmt {
namespace output {

class stream_wrapper
{
  template <typename P>
  friend stream_wrapper &operator<<(stream_wrapper &, const P &);

public:
  explicit stream_wrapper(std::ostream &os_ref) : os{os_ref}
  {
  }

  template <typename T>
  void print(const T &val)
  {
    os << val;
  }

  void space()
  {
    os << " ";
  }

  void next()
  {
    os << "\n";
  }

  static bool parameter_header_q()
  {
    return true;
  }

private:
  std::ostream &os;
};

// Default print behaviour for every type combination
template <typename T>
inline stream_wrapper &operator<<(stream_wrapper &sw, const T &val)
{
  sw.print(val);
  return sw;
}

// Specialisations for various types
template <>
void stream_wrapper::print(const rmt::numeric::complex_type &val);

template <>
void stream_wrapper::print(const rmt::real_vector_type &val);

template <>
void stream_wrapper::print(const rmt::complex_vector_type &val);

#ifndef DYNAMIC

template <>
void stream_wrapper::print(const rmt::block_real_vector_type &val);

template <>
void stream_wrapper::print(const rmt::block_vector_type &val);

#endif

// Observable specialisations

// -- Stephanov model

template <>
void stream_wrapper::print(
    const rmt::stephanov::observable::Action_Splits_Storage &val);

template <>
void stream_wrapper::print(
    const rmt::stephanov::observable::Diagonal_Average_Storage &val);

} // namespace output
} // namespace rmt

#endif /* STREAM_WRAPPER_HPP */
