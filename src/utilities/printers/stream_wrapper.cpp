
/*
 * Created: 06-03-2017
 * Modified: Fri 05 May 2017 10:55:39 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "stream_wrapper.hpp"
#include <types.hpp>

namespace rmt {
namespace output {

template <>
void stream_wrapper::print(
                           const rmt::numeric::complex_type &val)
{
  os << val.real() << " " << val.imag();
}

template <>
void stream_wrapper::print(const rmt::real_vector_type &val)
{
  for (auto i = 0ul; i < val.size(); ++i) {
    print(val[i]);
    next();
  }
}

template <>
void stream_wrapper::print(const rmt::complex_vector_type &val)
{
  for (auto i = 0ul; i < val.size(); ++i) {
    print(val[i]);
    next();
  }
}

#ifndef DYNAMIC

template <>
void stream_wrapper::print(const rmt::block_real_vector_type &val)
{
  for (auto i = 0ul; i < val.size(); ++i) {
    print(val[i]);
    next();
  }
}

template <>
void stream_wrapper::print(const rmt::block_vector_type &val)
{
  for (auto i = 0ul; i < val.size(); ++i) {
    print(val[i]);
    next();
  }
}

#endif

// Observable specialisations

// -- Stephanov model

template <>
void stream_wrapper::print(const rmt::stephanov::observable::Action_Splits_Storage &val)
{
  print(val.determinant);

  space();
  print(val.bosonic_re_re);

  space();
  print(val.bosonic_im_im);
}

template <>
void stream_wrapper::print(const rmt::stephanov::observable::Diagonal_Average_Storage &val)
{
  print(val.av_diag_a);

  space();
  print(val.av_diag_b);
}

} // namespace output
} // namespace rmt
