
/*
 * Created: 06-03-2017
 * Modified: Thu 31 Aug 2017 19:07:28 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef FILE_PRINTER_HPP
#define FILE_PRINTER_HPP

#include "printable_object.hpp"
#include <fstream>
#include <memory>
#include <utilities/filename_generator.hpp>

namespace rmt {

namespace observable {

template <typename Type>
class Named_Observable;

} // namespace observable

namespace output {

class File_Printer : public Output_Visitor
{
public:
  explicit File_Printer(build::Filename_Generator file_gen) : fname_gen{file_gen}
  {
  }

  File_Printer(build::Filename_Generator file_gen,
               Physical_Parameters const &phparms,
               Model_Parameters const &mparms)
      : fname_gen{file_gen}, phys_params(phparms), model_parms{mparms}
  {
  }

  virtual ~File_Printer()
  {
  }

  template <typename Type>
  void print(const observable::Named_Observable<Type> &obs);

  template <typename Type>
  void print_append(observable::Named_Observable<Type> &obs);

  void create_folder();

  bool file_exists(const std::string &filename);

  void print_parameter_header(std::ostream &os)
  {
    os << "#mass: " << phys_params.fermion_mass << "\n"
       << "#mu: " << phys_params.chemical_potential << "\n"
       << "#nf: " << phys_params.number_of_flavours << std::endl;
  }

  void set_physical_parameters(const Physical_Parameters &new_pp)
  {
    phys_params = new_pp;
  }

  void set_model_parameters(Model_Parameters const &new_mp)
  {
    model_parms = new_mp;
  }

private:
  build::Filename_Generator fname_gen;

  Physical_Parameters phys_params;
  Model_Parameters model_parms;
};

} // namespace output
} // namespace rmt

#include "file_printer.tpp"

#endif /* FILE_PRINTER_HPP */
