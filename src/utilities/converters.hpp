/*
 * Created: 22-04-2016
 * Modified: Thu 31 Aug 2017 16:59:04 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef CONVERTERS_HPP
#define CONVERTERS_HPP

#include <boost/lexical_cast.hpp>
#include <complex>
#include <functional>
#include <iomanip>
#include <number_type.hpp>
#include <string>
#include <vector>

template <typename N>
inline std::string float_to_string(const N &d, unsigned precision)
{
  std::ostringstream oss;
  oss << std::setprecision(precision) << d;

  return oss.str();
}

template <typename N>
inline std::string float_to_string(const N &d)
{
  return float_to_string(d, 4);
}

template <typename N>
inline std::string float_to_string_sc(const N &d, unsigned precision)
{
  std::ostringstream oss;
  oss << std::setprecision(precision) << std::scientific << d;

  return oss.str();
}

template <typename N>
inline std::complex<N> float_to_complex(const N &d, bool imag_q)
{
  if (!imag_q)
    return {d, 0.};
  else
    return {0., d};
}

template <typename N>
inline N string_to_float(std::string str)
{
  return boost::lexical_cast<N>(str);
}

template <typename N>
inline std::vector<N> seq(N begin, const N &end, const N &step)
{
  auto result = std::vector<N>{};

  while (begin < (end + 0.5 * step)) {
    result.push_back(begin);
    begin += step;
  }

  return result;
};

template <typename N>
inline std::vector<std::complex<N>>
float_vec_to_cmpx_vec(const std::vector<N> &vec, bool imag_q)
{
  using namespace std::placeholders;
  auto converted_vec = std::vector<std::complex<N>>(vec.size());

  std::transform(vec.begin(), vec.end(), converted_vec.begin(),
                 std::bind(&float_to_complex<N>, _1, imag_q));

  return converted_vec;
}

template <typename N>
inline std::vector<std::complex<N>>
real_real_cartesian_product(std::vector<N> const &real_vec,
                            std::vector<N> const &imag_vec)
{
  std::vector<std::complex<N>> result;
  result.reserve(real_vec.size() * imag_vec.size());

  for (auto real_val : real_vec)
    for (auto imag_val : imag_vec)
      result.emplace_back(real_val, imag_val);

  return result;
}

#endif /* CONVERTERS_HPP */
