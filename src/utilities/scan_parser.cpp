/*
 * Created: 22-04-2016
 * Modified: Thu 31 Aug 2017 16:52:42 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "scan_parser.hpp"
#include <number_type.hpp>
#include <utilities/converters.hpp>
#include <utilities/scan_grammar.hpp>

namespace rmt {
namespace scan {

std::vector<rmt::numeric::complex_type>
parse(const std::string &str, bool imag_q)
{
  using string_iterator = std::string::const_iterator;
  using parse_grammar = grammar::double_vector_grammar<string_iterator>;
  using boost::spirit::ascii::space;

  std::vector<double> parsed_vector;
  string_iterator iter = str.begin();
  string_iterator end = str.end();

  parse_grammar gr{};

  bool success =
      boost::spirit::qi::phrase_parse(iter, end, gr, space, parsed_vector);

  if (!success or (iter != end))
    throw std::runtime_error{"Unable to parse scan range\n"
                             "remainder: " +
                             std::string{iter, end}};

  auto float_vec = std::vector<rmt::numeric::number_type>(parsed_vector.size());
  std::transform(parsed_vector.begin(), parsed_vector.end(), float_vec.begin(),
                 [](double d) { return rmt::numeric::number_type{d}; });

  return float_vec_to_cmpx_vec(float_vec, imag_q);
}

std::vector<rmt::numeric::number_type>
parse_real(const std::string &str)
{
  using string_iterator = std::string::const_iterator;
  using parse_grammar = grammar::double_vector_grammar<string_iterator>;
  using boost::spirit::ascii::space;

  std::vector<double> parsed_vector;
  string_iterator iter = str.begin();
  string_iterator end = str.end();

  parse_grammar gr{};

  bool success =
      boost::spirit::qi::phrase_parse(iter, end, gr, space, parsed_vector);

  if (!success or (iter != end))
    throw std::runtime_error{"Unable to parse scan range\n"
                             "remainder: " +
                             std::string{iter, end}};

  auto float_vec = std::vector<rmt::numeric::number_type>(parsed_vector.size());
  std::transform(parsed_vector.begin(), parsed_vector.end(), float_vec.begin(),
                 [](double d) { return rmt::numeric::number_type{d}; });

  return float_vec;
}

} // namespace scan
} // namespace rmt
