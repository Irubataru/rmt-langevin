/*
 * Created: 16-04-2016
 * Modified: Thu 31 Aug 2017 19:52:14 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "program_options.hpp"
#include "converters.hpp"
#include "json_names.hpp"
#include "json_parser_helpers.hpp"
#include <boost/program_options.hpp>
#include <stdexcept>
#include <utilities/filename_parser.hpp>
#include <utilities/scan_parser.hpp>

namespace po = boost::program_options;
using json = nlohmann::json;

namespace rmt {
namespace build {

std::string get_config_filename(int argc, char *argv[])
{
  po::options_description hidden{"Hidden options"};
  hidden.add_options()("input-file", po::value<std::string>()->default_value(
                                         default_config_filename),
                       "input file");

  po::positional_options_description pod;
  pod.add("input-file", 1);

  auto var_map = po::variables_map{};
  po::store(
      po::command_line_parser(argc, argv).options(hidden).positional(pod).run(),
      var_map);

  return var_map["input-file"].as<std::string>();
}

Filename_Generator parse_save_pattern(nlohmann::json const &main_tree)
{
  using parse::parse_required_key;
  const auto file_str = JSON_Categories::file;

  auto param_json = parse_required_key(main_tree, file_str);
  std::string pattern =
      parse_required_key(param_json, JSON_File::save_file, file_str);

  return Filename_Generator{pattern};
}

Scan_Parameters parse_scan_parameters(json const &main_tree)
{
  using parse::parse_required_key;
  using parse::parse_enum;
  const auto scan_str = JSON_Categories::scan;

  auto scan_json = parse_required_key(main_tree, scan_str);

  if (!scan_json.is_object())
    throw std::runtime_error{"parse error: tag \"" + scan_str +
                             "\" not a json object"};

  Scan_Parameters parsed_params;

  parsed_params.skip_steps =
      parse_required_key(scan_json, JSON_Scan::measure_skips, scan_str);

  parsed_params.base_stepsize =
      parse_required_key(scan_json, JSON_Scan::delta_t, scan_str);

  if (scan_json.find(JSON_Scan::therm_steps) != scan_json.end()) {
    parsed_params.therm_time =
        static_cast<std::size_t>(scan_json[JSON_Scan::therm_steps]) *
        parsed_params.base_stepsize;
  } else {
    if (scan_json.find(JSON_Scan::therm_time) == scan_json.end()) {
      throw std::runtime_error{
          "parse error: neither \"" + JSON_Scan::therm_steps + "\" nor "
                                                               "\"" +
          JSON_Scan::therm_time + "\" specified in \"" + scan_str + "\" block"};
    }

    parsed_params.therm_time = scan_json[JSON_Scan::therm_time];
  }

  if (scan_json.find(JSON_Scan::total_steps) != scan_json.end()) {
    parsed_params.final_time =
        static_cast<std::size_t>(scan_json[JSON_Scan::total_steps]) *
        parsed_params.base_stepsize;
  } else {
    if (scan_json.find(JSON_Scan::final_time) == scan_json.end()) {
      throw std::runtime_error{
          "parse error: neither \"" + JSON_Scan::total_steps + "\" nor "
                                                               "\"" +
          JSON_Scan::final_time + "\" specified in \"" + scan_str + "\" block"};
    }

    parsed_params.final_time = scan_json[JSON_Scan::final_time];
  }

  parsed_params.step_update =
      parse_enum<Stepsize_Type>(scan_json, JSON_Scan::update_type, scan_str);

  return parsed_params;
}

Model_Parameters parse_model_parameters(json const &main_tree)
{
  using parse::parse_required_key;
  using parse::parse_enum;
  const auto param_str = JSON_Categories::parameters;

  auto param_json = parse_required_key(main_tree, param_str);

  Model_Parameters model_params;

  model_params.model =
      parse_enum<Model_Type>(param_json, JSON_Parameters::model, param_str);

  model_params.storage =
      parse_enum<Storage_Type>(param_json, JSON_Parameters::storage, param_str);

  model_params.number_of_flavours =
      parse_required_key(param_json, JSON_Parameters::nf, param_str);

  auto mu_real_scan = scan::parse_real(
      parse_required_key(param_json, JSON_Parameters::mu_real, param_str));
  auto mu_imag_scan = scan::parse_real(
      parse_required_key(param_json, JSON_Parameters::mu_imag, param_str));

  model_params.mu_scan =
      real_real_cartesian_product(mu_real_scan, mu_imag_scan);

  auto mass_real_scan = scan::parse_real(
      parse_required_key(param_json, JSON_Parameters::mass_real, param_str));
  auto mass_imag_scan = scan::parse_real(
      parse_required_key(param_json, JSON_Parameters::mass_imag, param_str));

  model_params.mass_scan =
      real_real_cartesian_product(mass_real_scan, mass_imag_scan);

  return model_params;
}

} // namespace build
} // namespace rm
