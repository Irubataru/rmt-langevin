/*
 * Created: 22-04-2016
 * Modified: Fri 22 Apr 2016 11:22:30 CEST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef FILENAME_GRAMMAR_HPP
#define FILENAME_GRAMMAR_HPP

#include <boost/spirit/include/qi.hpp>
#include <string>

namespace rmt {
namespace filename {
namespace grammar {
namespace qi = boost::spirit::qi;
namespace ascii = boost::spirit::ascii;

template <typename Iterator>
struct simple_surrounded_arithmetic : qi::grammar<Iterator, double()>
{
  explicit simple_surrounded_arithmetic(const qi::symbols<char, std::string> &);

  qi::rule<Iterator, double()> term, factor, expression, covered;
  qi::space_type skipper;
};

template <typename Iterator>
struct expression_parser : qi::grammar<Iterator, std::string()>
{
  explicit expression_parser(const qi::symbols<char, std::string> &);

  qi::rule<Iterator, std::string()> start, var;
  simple_surrounded_arithmetic<Iterator> arithmetic;
};

template <typename Iterator>
struct variable_replacer : qi::grammar<Iterator, std::string()>
{
  explicit variable_replacer(const qi::symbols<char, std::string> &);

  qi::rule<Iterator, std::string()> start, vars;
  expression_parser<Iterator> expr;
};

} // namespace grammar
} // namespace filename
} // namespace rmt

#endif /* FILENAME_GRAMMAR_HPP */
