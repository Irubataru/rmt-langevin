/*
 * Created: 22-04-2016
 * Modified: Thu 31 Aug 2017 18:10:09 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef FILENAME_PARSER_HPP
#define FILENAME_PARSER_HPP

#include <boost/spirit/include/qi.hpp>
#include <string>
#include <types.hpp>
#include <utilities/program_options.hpp>

namespace rmt {
namespace filename {

struct parameter_symbols : boost::spirit::qi::symbols<char, std::string>
{
  parameter_symbols() = default;
  explicit parameter_symbols(const Physical_Parameters &,
                             const Model_Parameters &);

  void add_dummy(std::string);
};

std::pair<std::string,bool> parse(const std::string &, const parameter_symbols &);

} // namespace filename
} // namespace rmt

#endif /* FILENAME_PARSER_HPP */
