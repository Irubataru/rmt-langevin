
/*
 * Created: 06-04-2016
 * Modified: Thu 16 Feb 2017 19:27:45 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef WHITE_NOISE_HPP
#define WHITE_NOISE_HPP

#include <random>
#include <types.hpp>

namespace rmt {

class White_Noise
{
public:
  using value_type = numeric::number_type;

  virtual void set_std_dev(double) = 0;
  virtual double get_std_dev() = 0;
  virtual value_type noise() = 0;

  value_type operator()()
  {
    return noise();
  }

  virtual ~White_Noise()
  {
  }
};

template <typename Rng>
class Std_White_Noise : public White_Noise
{
public:
  using dist_type = std::normal_distribution<>;
  using param_type = dist_type::param_type;

  explicit Std_White_Noise(Rng &r, double standard_deviation = 1.)
      : rng(r), dist{0., standard_deviation}
  {
  }

  explicit Std_White_Noise(Rng &r, param_type p) : rng(r), dist(p)
  {
  }

  virtual void set_std_dev(double d) override
  {
    dist.param(param_type{0., d});
  }

  virtual double get_std_dev() override
  {
    return dist.stddev();
  }

  virtual value_type noise() override
  {
    return static_cast<value_type>(dist(rng));
  }

  param_type param() const
  {
    return dist.param();
  }

  void param(const param_type &params)
  {
    dist.param(params);
  }

private:
  Rng &rng;
  dist_type dist;
};

} // namespace rmt

#endif /* WHITE_NOISE_HPP */
