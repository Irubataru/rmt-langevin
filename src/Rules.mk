SRCS := *.cpp
SUBDIRS := utilities osborn stephanov simskel builders
CXXFLAGS_$(d) += -fopenmp

ifneq (,$(findstring test,$(BUILD_MODE)))
	SRCS_EXCLUDES += main.cpp test_main.cpp
else ifeq ($(BUILD_MODE),explore)
	SRCS_EXCLUDES += main.cpp
else
	SRCS_EXCLUDES += test_main.cpp
endif

INCLUDES_$(d) := $(TOP)/external/simskel/include $(TOP)/src $(ARMA_DIR)
INHERIT_DIR_VARS_$(d) := CXXFLAGS INCLUDES
