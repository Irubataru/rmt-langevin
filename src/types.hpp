/*
 * Created: 07-04-2016
 * Modified: Mon 06 Nov 2017 15:17:42 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef TYPES_HPP
#define TYPES_HPP

#include <compile_configuration.hpp>
#include <number_type.hpp>

#include <blaze/math/DynamicMatrix.h>
#include <blaze/math/DynamicVector.h>

#ifndef DYNAMIC

#include <blaze/math/StaticMatrix.h>
#include <blaze/math/StaticVector.h>

#endif

namespace rmt {

#ifdef DYNAMIC

using real_vector_type =
    blaze::DynamicVector<numeric::number_type, blaze::columnVector>;
using complex_vector_type =
    blaze::DynamicVector<numeric::complex_type, blaze::columnVector>;

using real_matrix_type = blaze::DynamicMatrix<numeric::number_type>;
using complex_matrix_type = blaze::DynamicMatrix<numeric::complex_type>;
using complex_column_matrix_type =
    blaze::DynamicMatrix<numeric::complex_type, blaze::columnMajor>;

using dynamic_matrix_type = complex_matrix_type;
using dynamic_vector_type = complex_vector_type;

using block_real_vector_type = real_vector_type;
using block_vector_type = complex_vector_type;
using block_matrix_type = complex_matrix_type;

#else

using real_vector_type =
    blaze::StaticVector<numeric::number_type, M_size, blaze::columnVector>;
using complex_vector_type =
    blaze::StaticVector<numeric::complex_type, M_size, blaze::columnVector>;

using real_matrix_type =
    blaze::StaticMatrix<numeric::number_type, M_size, M_size, blaze::rowMajor>;
using complex_matrix_type =
    blaze::StaticMatrix<numeric::complex_type, M_size, M_size, blaze::rowMajor>;

using complex_column_matrix_type =
    blaze::StaticMatrix<numeric::complex_type, M_size, M_size,
                        blaze::columnMajor>;

using dynamic_matrix_type =
    blaze::DynamicMatrix<numeric::complex_type, blaze::rowMajor>;
using dynamic_vector_type =
    blaze::DynamicMatrix<numeric::complex_type, blaze::rowMajor>;

using block_real_vector_type =
    blaze::StaticVector<numeric::number_type, 2 * M_size, blaze::columnVector>;
using block_vector_type =
    blaze::StaticVector<numeric::complex_type, 2 * M_size, blaze::columnVector>;
using block_matrix_type = blaze::StaticMatrix<numeric::complex_type, 2 * M_size,
                                              2 * M_size, blaze::rowMajor>;

#endif

using M_type = complex_matrix_type;

using Generator_Type = std::mt19937;

struct Physical_Parameters
{
  numeric::complex_type chemical_potential;
  numeric::complex_type fermion_mass;
  unsigned number_of_flavours;
};

enum class Model_Type
{
  Begin_,
  Stephanov,
  Osborn,
  End_
};

enum class Storage_Type
{
  Begin_,
  Cartesian,
  Polar,
  End_
};

enum class Stepsize_Type
{
  Begin_,
  Constant,
  Adaptive,
  End_
};

enum class Minimise_Alg
{
  Begin_,
  Brent,
  Fixed,
  End_
};

enum class Derivative_Alg
{
  Begin_,
  Numerical,
  Analytic,
  End_
};

struct Model_Parameters
{
  Model_Type model;
  Storage_Type storage;
  unsigned number_of_flavours;
  std::vector<numeric::complex_type> mu_scan;
  std::vector<numeric::complex_type> mass_scan;
};

struct Scan_Parameters
{
  std::size_t skip_steps;
  double therm_time;
  double final_time;

  Stepsize_Type step_update;
  double base_stepsize;
};

struct Simulation_Parameters
{
  std::size_t therm_steps;
  std::size_t skip_steps;
  std::size_t total_steps;
  double base_stepsize;
  double final_t;
};

class Simulation_Config
{
public:
  virtual ~Simulation_Config()
  {
  }
};

class Simulation_Force
{
public:
  virtual ~Simulation_Force()
  {
  }
};

class Simulation_Cooling_Norm
{
public:
  virtual ~Simulation_Cooling_Norm()
  {
  }
};

} // namespace rmt

#endif /* TYPES_HPP */
