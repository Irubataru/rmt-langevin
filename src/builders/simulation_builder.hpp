
/*
 * Created: 05-09-2017
 * Modified: Tue 05 Sep 2017 21:14:04 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef SIMULATION_BUILDER_HPP
#define SIMULATION_BUILDER_HPP

#include <builders/expanded_argument_package.hpp>
#include <builders/observable_builders/observable_builder.hpp>
#include <nlohmann/json.hpp>
#include <simskel/stages/stage_interface.hpp>
#include <types.hpp>
#include <utilities/stepsize/time_keeper.hpp>

namespace rmt {
namespace build {

class Simulation_Builder
{
public:
  Simulation_Builder(nlohmann::json const &tree);

  std::vector<std::unique_ptr<simulation::Stage>>
  build(Physical_Parameters const &);

private:
  std::size_t parse_seed() const;

  std::shared_ptr<Simulation_Config> build_config() const;

  std::shared_ptr<Simulation_Force>
  build_force(std::shared_ptr<Simulation_Config> cfg,
              Physical_Parameters const &params) const;

  std::unique_ptr<simulation::Stage>
  build_init_stage(core::Expanded_Argument_Package const &args) const;

  std::unique_ptr<simulation::Stage>
  parse_timed_stage(nlohmann::json const &stage_tree,
                    std::shared_ptr<stepsize::Time_Keeper> timer,
                    numeric::number_type end_time,
                    core::Expanded_Argument_Package const &args) const;

  std::unique_ptr<simulation::Stage>
  build_end_stage(core::Expanded_Argument_Package const &args,
                  std::size_t seed) const;

private:
  nlohmann::json const &main_tree_;
  Model_Parameters model_params;
  Scan_Parameters scan_params;

  std::map<std::string, std::unique_ptr<Observable_Builder>> obs_builder_map_;
};

} // namespace build
} // namespace rmt

#endif /* SIMULATION_BUILDER_HPP */
