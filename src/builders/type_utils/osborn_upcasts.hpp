
/*
 * Created: 04-09-2017
 * Modified: Mon 04 Sep 2017 15:46:11 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef BUILDER_TYPE_UTILS_OSBORN_UPCASTS_HPP
#define BUILDER_TYPE_UTILS_OSBORN_UPCASTS_HPP

#include "conditional_upcast.hpp"
#include <osborn/langevin/langevin_step.hpp>
#include <osborn/langevin/force.hpp>

namespace rmt {
namespace build {
namespace core {

inline auto osborn_config_upcast(std::shared_ptr<Simulation_Config> cfg_ptr)
{
  return conditional_upcast<::rmt::osborn::Matrix_Storage_Base>(cfg_ptr);
}

inline auto
osborn_cartesian_config_upcast(std::shared_ptr<Simulation_Config> cfg_ptr)
{
  return conditional_upcast<::rmt::osborn::cartesian::Matrix_Storage>(
      cfg_ptr);
}

inline auto
osborn_cartesian_force_upcast(std::shared_ptr<Simulation_Force> force_ptr)
{
  return conditional_upcast<::rmt::osborn::cartesian::Force>(force_ptr);
}

} // namespace core
} // namespace build
} // namespace rmt

#endif /* BUILDER_TYPE_UTILS_OSBORN_UPCASTS_HPP */
