
/*
 * Created: 04-09-2017
 * Modified: Mon 04 Sep 2017 15:43:29 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef BUILD_TYPE_UTILS_CONDITIONAL_UPCAST_HPP
#define BUILD_TYPE_UTILS_CONDITIONAL_UPCAST_HPP

#include <memory>

namespace rmt {
namespace build {
namespace core {

template <typename Target_Type, typename Input_Type>
inline auto conditional_upcast(std::shared_ptr<Input_Type> const &ptr)
{
  auto upcast = std::dynamic_pointer_cast<Target_Type>(ptr);

  if (!upcast)
    throw std::runtime_error{
        "unable to upcast from \"" + std::string{typeid(Input_Type).name()} +
        "\" to \"" + std::string{typeid(Target_Type).name()} + "\""};

  return upcast;
}

} // namespace core
} // namespace build
} // namespace rmt

#endif /* BUILD_TYPE_UTILS_CONDITIONAL_UPCAST_HPP */
