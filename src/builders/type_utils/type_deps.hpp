
/*
 * Created: 04-09-2017
 * Modified: Mon 04 Sep 2017 15:47:35 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef BUILDER_TYPE_DEPS_HPP
#define BUILDER_TYPE_DEPS_HPP

#include <stephanov/langevin/langevin_step.hpp>
#include <stephanov/langevin/force.hpp>

#include <osborn/langevin/langevin_step.hpp>
#include <osborn/langevin/force.hpp>

namespace rmt {
namespace build {
namespace core {

template <typename Target>
struct deps
{
};

template <>
struct deps<::rmt::stephanov::cartesian::Langevin_Step>
{
  using storage = ::rmt::stephanov::cartesian::Matrix_Storage;
  using force = ::rmt::stephanov::cartesian::Force_Base;
};

template <>
struct deps<::rmt::stephanov::polar::Langevin_Step>
{
  using storage = ::rmt::stephanov::polar::Matrix_Storage;
  using force = ::rmt::stephanov::polar::Force;
};

template <>
struct deps<::rmt::osborn::cartesian::Langevin_Step>
{
  using storage = ::rmt::osborn::cartesian::Matrix_Storage;
  using force = ::rmt::osborn::cartesian::Force;
};

} // namespace core 
} // namespace build 
} // namespace rmt 


#endif /* BUILDER_TYPE_DEPS_HPP */
