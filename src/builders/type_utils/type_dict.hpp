
/*
 * Created: 05-09-2017
 * Modified: Tue 05 Sep 2017 12:05:53 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef TYPE_DICT_HPP
#define TYPE_DICT_HPP

#include <stephanov/langevin/force.hpp>
#include <stephanov/langevin/langevin_step.hpp>

#include <osborn/langevin/force.hpp>
#include <osborn/langevin/langevin_step.hpp>

namespace rmt {
namespace build {
namespace core {

template <Model_Type model, Storage_Type storage>
struct type_dict
{
};

template <>
struct type_dict<Model_Type::Stephanov, Storage_Type::Cartesian>
{
  using storage_type = ::rmt::stephanov::cartesian::Matrix_Storage;
  using force_type = ::rmt::stephanov::cartesian::Force;
};

template <>
struct type_dict<Model_Type::Stephanov, Storage_Type::Polar>
{
  using storage_type = ::rmt::stephanov::polar::Matrix_Storage;
  using force_type = ::rmt::stephanov::polar::Force;
};

template <>
struct type_dict<Model_Type::Osborn, Storage_Type::Cartesian>
{
  using storage_type = ::rmt::osborn::cartesian::Matrix_Storage;
  using force_type = ::rmt::osborn::cartesian::Force;
};

} // namespace core
} // namespace Namespace
} // namespace rmt

#endif /* TYPE_DICT_HPP */
