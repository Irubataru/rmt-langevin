
/*
 * Created: 01-09-2017
 * Modified: Mon 04 Sep 2017 15:43:43 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_BUILDER_UPCASTS_HPP
#define STEPHANOV_BUILDER_UPCASTS_HPP

#include "conditional_upcast.hpp"
#include <stephanov/langevin/force.hpp>
#include <stephanov/resource/resource.hpp>

namespace rmt {
namespace build {
namespace core {

inline auto stephanov_config_upcast(std::shared_ptr<Simulation_Config> cfg_ptr)
{
  return conditional_upcast<::rmt::stephanov::Matrix_Storage_Base>(cfg_ptr);
}

inline auto
stephanov_cartesian_config_upcast(std::shared_ptr<Simulation_Config> cfg_ptr)
{
  return conditional_upcast<::rmt::stephanov::cartesian::Matrix_Storage>(
      cfg_ptr);
}

inline auto
stephanov_polar_config_upcast(std::shared_ptr<Simulation_Config> cfg_ptr)
{
  return conditional_upcast<::rmt::stephanov::polar::Matrix_Storage>(cfg_ptr);
}

inline auto
stephanov_cartesian_force_upcast(std::shared_ptr<Simulation_Force> force_ptr)
{
  return conditional_upcast<::rmt::stephanov::cartesian::Force>(force_ptr);
}

inline auto
stephanov_polar_force_upcast(std::shared_ptr<Simulation_Force> force_ptr)
{
  return conditional_upcast<::rmt::stephanov::polar::Force>(force_ptr);
}

} // namespace core
} // namespace build
} // namespace rmt

#endif /* STEPHANOV_BUILDER_UPCASTS_HPP */
