
/*
 * Created: 02-09-2017
 * Modified: Sat 02 Sep 2017 15:48:12 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef NORM_BUILDER_HPP
#define NORM_BUILDER_HPP

#include <types.hpp>
#include <nlohmann/json.hpp>
#include <builders/dictionary.hpp>

namespace rmt {
namespace build {

class Norm_Builder
{
public:
  using build_type = Simulation_Cooling_Norm;
  using ptr_type = std::unique_ptr<build_type>;

  virtual ~Norm_Builder() {}

  virtual ptr_type build(nlohmann::json const &, nlohmann::json const &) = 0;
};

template <typename Norm_Type>
class No_Argument_Norm_Builder : public Norm_Builder
{
public:
  using Norm_Builder::build_type;
  using Norm_Builder::ptr_type;

  virtual ~No_Argument_Norm_Builder() {}

  ptr_type build(nlohmann::json const &, nlohmann::json const &) override
  {
    return std::make_unique<Norm_Type>();
  }
};

namespace core {

using Norm_Builder_Dictionary = Dictionary<Norm_Builder>;

} // namespace core 

} // namespace build 
} // namespace rmt 

#endif /* NORM_BUILDER_HPP */
