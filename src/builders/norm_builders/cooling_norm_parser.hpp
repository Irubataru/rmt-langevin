
/*
 * Created: 02-09-2017
 * Modified: Sat 02 Sep 2017 16:54:35 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef COOLING_NORM_PARSER_HPP
#define COOLING_NORM_PARSER_HPP

#include "norm_builder.hpp"

namespace rmt {
namespace build {

std::unique_ptr<Simulation_Cooling_Norm>
parse_cooling_norm(nlohmann::json const &, nlohmann::json const &);

} // namespace build
} // namespace rmt

#endif /* COOLING_NORM_PARSER_HPP */
