
/*
 * Created: 02-09-2017
 * Modified: Thu 09 Nov 2017 17:38:00 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef AGGREGATE_NORM_BUILDER_HPP
#define AGGREGATE_NORM_BUILDER_HPP

#include <builders/norm_builders/norm_builder.hpp>
#include <stephanov/cooling/cooling_norm/aggregate_norm.hpp>
#include <osborn/cooling/norms/aggregate_norm.hpp>
#include <utilities/json_names.hpp>
#include <utilities/json_parser_helpers.hpp>

namespace rmt {
namespace build {

namespace stephanov {

class Aggregate_Norm_Builder : public Norm_Builder
{
public:
  using Norm_Builder::build_type;
  using Norm_Builder::ptr_type;
  using Aggregate_Norm = ::rmt::stephanov::cooling::norm::Aggregate_Norm;

  using aggr_norm_type = ::rmt::stephanov::cooling::norm::Norm;
  using aggr_norm_ptr_type = std::shared_ptr<aggr_norm_type>;

  virtual ~Aggregate_Norm_Builder()
  {
  }

  ptr_type build(nlohmann::json const &norm_tree,
                 nlohmann::json const &main_tree) override
  {
    using parse::global_parse;
    using parse::parse_required_key;

    auto model = global_parse<Model_Type>(main_tree);
    auto storage = global_parse<Storage_Type>(main_tree);

    auto aggregate_norm = std::make_unique<Aggregate_Norm>();

    auto total_weight = 0.0;

    auto param_tree =
        parse_required_key(norm_tree, JSON_Generic::parameters, "norm");

    if (param_tree.is_object()) {
      total_weight = push_back_norm(param_tree, main_tree, model, storage,
                                    *aggregate_norm);
    } else if (param_tree.is_array()) {
      for (auto const &sub_norm_tree : param_tree) {
        total_weight += push_back_norm(sub_norm_tree, main_tree, model, storage,
                                       *aggregate_norm);
      }
    } else {
      throw std::runtime_error{
          "parse error: aggregate norm parameter type not a valid json object"};
    }

    if (std::fabs(total_weight - 1.0) >
        2 * std::numeric_limits<double>::epsilon()) {

      std::ostringstream oss;
      oss << "parse error aggregate norm: the total norm is \"" << total_weight
          << "\", which is not 1.0";

      throw std::runtime_error{oss.str()};
    }

    return aggregate_norm;
  }

private:
  double push_back_norm(nlohmann::json const &norm_tree,
                        nlohmann::json const &main_tree, Model_Type model,
                        Storage_Type storage, Aggregate_Norm &aggr_norm)
  {
    using parse::parse_required_key;

    double weight =
        parse_required_key(norm_tree, "weight", "aggregate/parameters");

    aggr_norm.push_back(weight,
                        build_sub_norm(norm_tree, main_tree, model, storage));

    return weight;
  }

  aggr_norm_ptr_type build_sub_norm(nlohmann::json const &norm_tree,
                                    nlohmann::json const &main_tree,
                                    Model_Type model, Storage_Type storage)
  {
    std::string key = parse::parse_required_key(norm_tree, JSON_Generic::name,
                                                "aggregate/parameters");

    auto norm_builder = parse::check_dictionary_by_key(
        key, model, storage, core::Norm_Builder_Dictionary::instance());

    auto upcast_norm = std::dynamic_pointer_cast<aggr_norm_type>(
        std::shared_ptr<Simulation_Cooling_Norm>(
            norm_builder->build(norm_tree, main_tree)));

    if (!upcast_norm) {
      throw std::runtime_error{"bad cast: cannot upcast to stephanov cooling "
                               "norm for norm of type \"" +
                               key + "\""};
    }

    return upcast_norm;
  }
};

} // namespace stephanov

namespace osborn {

class Aggregate_Norm_Builder : public Norm_Builder
{
public:
  using Norm_Builder::build_type;
  using Norm_Builder::ptr_type;
  using Aggregate_Norm = ::rmt::osborn::cooling::norm::Aggregate_Norm;

  using aggr_norm_type = ::rmt::osborn::cooling::norm::Norm;
  using aggr_norm_ptr_type = std::shared_ptr<aggr_norm_type>;

  virtual ~Aggregate_Norm_Builder()
  {
  }

  ptr_type build(nlohmann::json const &norm_tree,
                 nlohmann::json const &main_tree) override
  {
    using parse::global_parse;
    using parse::parse_required_key;

    auto model = global_parse<Model_Type>(main_tree);
    auto storage = global_parse<Storage_Type>(main_tree);

    auto aggregate_norm = std::make_unique<Aggregate_Norm>();

    auto total_weight = 0.0;

    auto param_tree =
        parse_required_key(norm_tree, JSON_Generic::parameters, "norm");

    if (param_tree.is_object()) {
      total_weight = push_back_norm(param_tree, main_tree, model, storage,
                                    *aggregate_norm);
    } else if (param_tree.is_array()) {
      for (auto const &sub_norm_tree : param_tree) {
        total_weight += push_back_norm(sub_norm_tree, main_tree, model, storage,
                                       *aggregate_norm);
      }
    } else {
      throw std::runtime_error{
          "parse error: aggregate norm parameter type not a valid json object"};
    }

    if (std::fabs(total_weight - 1.0) >
        2 * std::numeric_limits<double>::epsilon()) {

      std::ostringstream oss;
      oss << "parse error aggregate norm: the total norm is \"" << total_weight
          << "\", which is not 1.0";

      throw std::runtime_error{oss.str()};
    }

    return aggregate_norm;
  }

private:
  double push_back_norm(nlohmann::json const &norm_tree,
                        nlohmann::json const &main_tree, Model_Type model,
                        Storage_Type storage, Aggregate_Norm aggr_norm)
  {
    using parse::parse_required_key;

    double weight =
        parse_required_key(norm_tree, "weight", "aggregate/parameters");

    aggr_norm.push_back(weight,
                        build_sub_norm(norm_tree, main_tree, model, storage));

    return weight;
  }

  aggr_norm_ptr_type build_sub_norm(nlohmann::json const &norm_tree,
                                    nlohmann::json const &main_tree,
                                    Model_Type model, Storage_Type storage)
  {
    std::string key = parse::parse_required_key(norm_tree, JSON_Generic::name,
                                                "aggregate/parameters");

    auto norm_builder = parse::check_dictionary_by_key(
        key, model, storage, core::Norm_Builder_Dictionary::instance());

    auto upcast_norm = std::dynamic_pointer_cast<aggr_norm_type>(
        std::shared_ptr<Simulation_Cooling_Norm>(
            norm_builder->build(norm_tree, main_tree)));

    if (!upcast_norm) {
      throw std::runtime_error{"bad cast: cannot upcast to osborn cooling "
                               "norm for norm of type \"" +
                               key + "\""};
    }

    return upcast_norm;
  }
};

} // namespace osborn 

} // namespace build
} // namespace rmt

#endif /* AGGREGATE_NORM_BUILDER_HPP */
