
/*
 * Created: 02-09-2017
 * Modified: Sat 02 Sep 2017 15:31:46 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_EIGENVALUE_NORM_BUILDER_HPP
#define STEPHANOV_EIGENVALUE_NORM_BUILDER_HPP

#include <builders/norm_builders/norm_builder.hpp>
#include <stephanov/cooling/cooling_norm/eigenvalue_norm.hpp>
#include <utilities/json_names.hpp>
#include <utilities/json_parser_helpers.hpp>
#include <utilities/matrix/eigenvalues/lowest_eigenvalues.hpp>

namespace rmt {
namespace build {
namespace stephanov {

class Eigenvalue_Norm_Builder : public Norm_Builder
{
public:
  using Norm_Builder::build_type;
  using Norm_Builder::ptr_type;

  virtual ~Eigenvalue_Norm_Builder()
  {
  }

  ptr_type build(nlohmann::json const &norm_tree,
                 nlohmann::json const &main_tree) override
  {
    using parse::parse_required_key;
    using ::rmt::stephanov::cooling::norm::Eigenvalue_Norm;
    using ::rmt::matrix::Lowest_Eigenvalues;

    auto param_tree =
        parse_required_key(norm_tree, JSON_Generic::parameters, "norm");

    double xi = parse_required_key(param_tree, "xi", "norm/parameters");
    std::size_t num_ev =
        parse_required_key(param_tree, "num-ev", "norm/parameters");

    return std::make_unique<Eigenvalue_Norm>(xi, num_ev, Lowest_Eigenvalues{});
  }
};

} // namespace stephanov
} // namespace build
} // namespace rmt

#endif /* STEPHANOV_EIGENVALUE_NORM_BUILDER_HPP */
