
/*
 * Created: 02-09-2017
 * Modified: Sat 02 Sep 2017 17:10:40 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "cooling_norm_parser.hpp"
#include <utilities/json_names.hpp>
#include <utilities/json_parser_helpers.hpp>

namespace rmt {
namespace build {

std::unique_ptr<Simulation_Cooling_Norm>
parse_cooling_norm(nlohmann::json const &norm_tree,
                   nlohmann::json const &main_tree)
{
  using parse::global_parse;
  using parse::parse_required_key;

  auto model = global_parse<Model_Type>(main_tree);
  auto storage = global_parse<Storage_Type>(main_tree);

  std::string key =
      parse::parse_required_key(norm_tree, JSON_Generic::name, "");

  return parse::check_dictionary_by_key(
             key, model, storage, core::Norm_Builder_Dictionary::instance())
      ->build(norm_tree, main_tree);
}

} // namespace build
} // namespace rmt
