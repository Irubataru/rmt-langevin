
/*
 * Created: 02-09-2017
 * Modified: Wed 06 Sep 2017 15:11:44 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef NORM_SPECIFIC_BUILDERS_HPP
#define NORM_SPECIFIC_BUILDERS_HPP

#include "specific_builders/aggregate_norm_builder.hpp"
#include "specific_builders/osborn_eigenvalue_norm_builder.hpp"
#include "specific_builders/stephanov_eigenvalue_norm_builder.hpp"

#endif /* NORM_SPECIFIC_BUILDERS_HPP */
