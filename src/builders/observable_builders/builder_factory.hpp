
/*
 * Created: 01-09-2017
 * Modified: Sat 02 Sep 2017 17:58:04 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef BUILDER_FACTORY_HPP
#define BUILDER_FACTORY_HPP

#include "observable_builder.hpp"
#include <nlohmann/json.hpp>
#include <builders/dictionary.hpp>

namespace rmt {
namespace build {
namespace core {

class Observable_Builder_Factory
{
public:
  using Builder_Ptr_Type = std::unique_ptr<Observable_Builder>;

  virtual ~Observable_Builder_Factory()
  {
  }

  virtual Builder_Ptr_Type create(nlohmann::json const &,
                                  nlohmann::json const &) = 0;
};

template <typename Build_Type>
class No_Param_Obs_Build_Factory : public Observable_Builder_Factory
{
public:
  using Observable_Builder_Factory::Builder_Ptr_Type;
  virtual ~No_Param_Obs_Build_Factory()
  {
  }

  virtual Builder_Ptr_Type create(nlohmann::json const &,
                                  nlohmann::json const &)
  {
    return std::make_unique<Build_Type>();
  }
};

template <typename Build_Type>
class JSON_Trees_Obs_Build_Factory : public Observable_Builder_Factory
{
public:
  using Observable_Builder_Factory::Builder_Ptr_Type;
  virtual ~JSON_Trees_Obs_Build_Factory()
  {
  }

  virtual Builder_Ptr_Type create(nlohmann::json const &sub_tree,
                                  nlohmann::json const &main_tree)
  {
    return std::make_unique<Build_Type>(sub_tree, main_tree);
  }
};

using Factory_Dictionary = Dictionary<Observable_Builder_Factory>;

} // namespace core
} // namespace build
} // namespace rmt

#endif /* BUILDER_FACTORY_HPP */
