
/*
 * Created: 02-09-2017
 * Modified: Wed 06 Sep 2017 14:58:01 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OSBORN_OBSERVABLE_COOLING_NORM_BUILDER_HPP
#define OSBORN_OBSERVABLE_COOLING_NORM_BUILDER_HPP

#include <builders/type_utils/osborn_upcasts.hpp>
#include <builders/observable_builders/observable_builder.hpp>
#include <nlohmann/json.hpp>

namespace rmt {
namespace build {
namespace osborn {

class Cooling_Norm_Obs_Builder : public Observable_Builder
{
public:
  using Observable_Builder::Observable_Ptr_Type;

  Cooling_Norm_Obs_Builder(nlohmann::json const &norm_tree,
                           nlohmann::json const &main_tree)
      : norm_tree_(norm_tree), main_tree_(main_tree)
  {
  }

  virtual ~Cooling_Norm_Obs_Builder()
  {
  }

  Observable_Ptr_Type build(core::Argument_Package const &args) override;

private:
  nlohmann::json norm_tree_;
  nlohmann::json main_tree_;
};

// Cooling_Norm_Obs_Builder

} // namespace osborn
} // namespace build
} // namespace rmt

#endif /* OSBORN_OBSERVABLE_COOLING_NORM_BUILDER_HPP */
