
/*
 * Created: 02-09-2017
 * Modified: Wed 06 Sep 2017 14:46:23 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "cooling_norm_builder.hpp"
#include <builders/norm_builders/cooling_norm_parser.hpp>
#include <osborn/observables/norm_observable.hpp>
#include <utilities/json_parser_helpers.hpp>

namespace rmt {
namespace build {
namespace osborn {

Cooling_Norm_Obs_Builder::Observable_Ptr_Type
Cooling_Norm_Obs_Builder::build(core::Argument_Package const &args)
{
  using ::rmt::osborn::cooling::norm::Norm;
  using ::rmt::osborn::observable::Cooling_Norm;

  auto params_tree = parse::parse_required_key(
      norm_tree_, JSON_Generic::parameters, "cooling-norm");

  auto upcast_norm =
      std::dynamic_pointer_cast<Norm>(std::shared_ptr<Simulation_Cooling_Norm>(
          parse_cooling_norm(params_tree, main_tree_)));

  if (!upcast_norm) {
    throw std::runtime_error{"parse error: unable to upcast cooling norm for "
                             "norm observable in model " +
                             parse::to_string(Model_Type::Stephanov)};
  }

  std::string norm_name = params_tree[JSON_Generic::name];

  if (parse::has_key(params_tree, JSON_Generic::ref_name))
    norm_name = params_tree[JSON_Generic::ref_name];

  return std::make_unique<Cooling_Norm>(
      core::osborn_config_upcast(args.config), upcast_norm, norm_name);
}

} // namespace osborn
} // namespace build
} // namespace rmt
