
/*
 * Created: 01-09-2017
 * Modified: Wed 06 Sep 2017 14:58:07 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OSBORN_INDEPENDENT_OBSERVABLE_BUILDERS_HPP
#define OSBORN_INDEPENDENT_OBSERVABLE_BUILDERS_HPP

#include <builders/type_utils/osborn_upcasts.hpp>
#include <builders/observable_builders/observable_builder.hpp>

namespace rmt {
namespace build {
namespace osborn {

template <typename Obs_Type>
class Storage_Obs_Builder : public Observable_Builder
{
public:
  using Observable_Ptr_Type = Observable_Builder::Observable_Ptr_Type;

  virtual ~Storage_Obs_Builder(){};

  Observable_Ptr_Type build(core::Argument_Package const &args) override
  {
    return std::make_unique<Obs_Type>(
        core::osborn_config_upcast(args.config));
  }
};

template <typename Obs_Type>
class Storage_Phys_Obs_Builder : public Observable_Builder
{
public:
  using Observable_Ptr_Type = Observable_Builder::Observable_Ptr_Type;

  virtual ~Storage_Phys_Obs_Builder(){};

  Observable_Ptr_Type build(core::Argument_Package const &args) override
  {
    return std::make_unique<Obs_Type>(
        core::osborn_config_upcast(args.config), args.phys_params);
  }
};

//Cooling_Norm_Obs_Builder

} // namespace osborn
} // namespace build
} // namespace rmt

#endif /* OSBORN_INDEPENDENT_OBSERVABLE_BUILDERS_HPP */
