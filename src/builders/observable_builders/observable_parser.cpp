
/*
 * Created: 01-09-2017
 * Modified: Mon 04 Sep 2017 15:33:24 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "observable_parser.hpp"
#include "builder_factory.hpp"
#include <utilities/json_names.hpp>
#include <utilities/json_parser_helpers.hpp>

namespace rmt {
namespace build {

std::map<std::string, std::unique_ptr<Observable_Builder>>
parse_observable_section(nlohmann::json const &main_tree)
{
  using parse::parse_enum;
  using parse::global_parse;
  using parse::parse_required_key;

  auto model = global_parse<Model_Type>(main_tree);
  auto storage = global_parse<Storage_Type>(main_tree);

  auto obs_tree = parse_required_key(main_tree, JSON_Categories::observables);

  std::map<std::string, std::unique_ptr<Observable_Builder>> obs_builder_map;

  if (obs_tree.is_object()) {
    obs_builder_map.insert(
        parse_observable(obs_tree, main_tree, model, storage));
  } else if (obs_tree.is_array()) {
    for (auto const &obs_json : obs_tree) {
      obs_builder_map.insert(
          parse_observable(obs_json, main_tree, model, storage));
    }
  } else {
    throw std::runtime_error{"parse error: invalid json object at \"" +
                             JSON_Categories::simulation + "\""};
  }

  return obs_builder_map;
}

std::pair<std::string, std::unique_ptr<Observable_Builder>>
parse_observable(nlohmann::json const &obs_tree,
                 nlohmann::json const &main_tree, Model_Type model,
                 Storage_Type storage)
{
  std::string name_str = parse::parse_required_key(
      obs_tree, JSON_Generic::name, JSON_Categories::observables);

  auto factory = parse::check_dictionary_by_key(
      name_str, model, storage, core::Factory_Dictionary::instance());
  auto builder_ptr = factory->create(obs_tree, main_tree);

  if (parse::has_key(obs_tree, JSON_Generic::frequency)) {
    builder_ptr = std::make_unique<Every_N_Observable_Builder>(
        std::move(builder_ptr), std::size_t{obs_tree[JSON_Generic::frequency]});
  }

  auto ref_key = name_str;

  if (parse::has_key(obs_tree, JSON_Generic::ref_name))
    ref_key = obs_tree[JSON_Generic::ref_name];

  return {ref_key, std::move(builder_ptr)};
}

} // namespace build
} // namespace rmt
