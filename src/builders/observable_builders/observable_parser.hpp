
/*
 * Created: 01-09-2017
 * Modified: Fri 01 Sep 2017 16:20:58 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OBSERVABLE_PARSER_HPP
#define OBSERVABLE_PARSER_HPP

#include "observable_builder.hpp"
#include <map>
#include <nlohmann/json.hpp>

namespace rmt {
namespace build {

std::map<std::string, std::unique_ptr<Observable_Builder>>
parse_observable_section(nlohmann::json const&);

std::pair<std::string, std::unique_ptr<Observable_Builder>> parse_observable(
    nlohmann::json const&, nlohmann::json const&, Model_Type, Storage_Type);

} // namespace build
} // namespace rmt

#endif /* OBSERVABLE_PARSER_HPP */
