
/*
 * Created: 02-09-2017
 * Modified: Fri 03 Nov 2017 16:49:55 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_OBSERVABLE_COOLING_NORM_BUILDER_HPP
#define STEPHANOV_OBSERVABLE_COOLING_NORM_BUILDER_HPP

#include <builders/type_utils/stephanov_upcasts.hpp>
#include <builders/observable_builders/observable_builder.hpp>
#include <nlohmann/json.hpp>

namespace rmt {
namespace build {
namespace stephanov {

class Cooling_Norm_Obs_Builder : public Observable_Builder
{
public:
  using Observable_Builder::Observable_Ptr_Type;

  Cooling_Norm_Obs_Builder(nlohmann::json const &norm_tree,
                           nlohmann::json const &main_tree)
      : norm_tree_(norm_tree), main_tree_(main_tree)
  {
  }

  virtual ~Cooling_Norm_Obs_Builder()
  {
  }

  Observable_Ptr_Type build(core::Argument_Package const &args) override;

private:
  nlohmann::json norm_tree_;
  nlohmann::json main_tree_;
};

// Cooling_Norm_Obs_Builder

} // namespace stephanov
} // namespace build
} // namespace rmt

#endif /* STEPHANOV_OBSERVABLE_COOLING_NORM_BUILDER_HPP */
