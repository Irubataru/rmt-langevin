
/*
 * Created: 01-09-2017
 * Modified: Mon 04 Sep 2017 21:20:54 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_INDEPENDENT_OBSERVABLE_BUILDERS_HPP
#define STEPHANOV_INDEPENDENT_OBSERVABLE_BUILDERS_HPP

#include <builders/type_utils/stephanov_upcasts.hpp>
#include <builders/observable_builders/observable_builder.hpp>

namespace rmt {
namespace build {
namespace stephanov {

template <typename Obs_Type>
class Storage_Obs_Builder : public Observable_Builder
{
public:
  using Observable_Ptr_Type = Observable_Builder::Observable_Ptr_Type;

  virtual ~Storage_Obs_Builder(){};

  Observable_Ptr_Type build(core::Argument_Package const &args) override
  {
    return std::make_unique<Obs_Type>(
        core::stephanov_config_upcast(args.config));
  }
};

template <typename Obs_Type>
class Storage_Phys_Obs_Builder : public Observable_Builder
{
public:
  using Observable_Ptr_Type = Observable_Builder::Observable_Ptr_Type;

  virtual ~Storage_Phys_Obs_Builder(){};

  Observable_Ptr_Type build(core::Argument_Package const &args) override
  {
    return std::make_unique<Obs_Type>(
        core::stephanov_config_upcast(args.config), args.phys_params);
  }
};

//Cooling_Norm_Obs_Builder

} // namespace stephanov
} // namespace build
} // namespace rmt

#endif /* STEPHANOV_INDEPENDENT_OBSERVABLE_BUILDERS_HPP */
