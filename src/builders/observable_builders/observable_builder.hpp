
/*
 * Created: 31-08-2017
 * Modified: Sat 02 Sep 2017 14:44:40 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OBSERVABLE_BUILDER_HPP
#define OBSERVABLE_BUILDER_HPP

#include <builders/core_argument_package.hpp>
#include <simskel/named_observable.hpp>
#include <simskel/utility_observables.hpp>
#include <types.hpp>

namespace rmt {
namespace build {

class Observable_Builder
{
public:
  using Observable_Ptr_Type =
      std::unique_ptr<::rmt::observable::Named_Observable_Core>;

  virtual ~Observable_Builder(){};

  virtual Observable_Ptr_Type build(core::Argument_Package const&) = 0;
};

class Every_N_Observable_Builder : public Observable_Builder
{
public:
  using Observable_Builder::Observable_Ptr_Type;

  Every_N_Observable_Builder(std::unique_ptr<Observable_Builder> builder, std::size_t n)
    : builder_{std::move(builder)}, n_{n} {}

  virtual ~Every_N_Observable_Builder() {}

  virtual Observable_Ptr_Type build(core::Argument_Package const& args) override
  {
    return std::make_unique<observable::Measure_Every_N>(builder_->build(args), n_);
  }

private:
  std::unique_ptr<Observable_Builder> builder_;
  std::size_t n_;
};

} // namespace build
} // namespace rmt

#endif /* OBSERVABLE_BUILDER_HPP */
