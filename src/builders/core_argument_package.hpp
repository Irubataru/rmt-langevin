
/*
 * Created: 01-09-2017
 * Modified: Fri 01 Sep 2017 12:46:32 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef CORE_ARGUMENT_PACKAGE_HPP
#define CORE_ARGUMENT_PACKAGE_HPP

#include <types.hpp>

namespace rmt {
namespace build {
namespace core {

struct Argument_Package
{
  std::shared_ptr<Simulation_Config> config;
  std::shared_ptr<Simulation_Force> force;
  Physical_Parameters phys_params;
};

} // namespace core 
} // namespace build 
} // namespace rmt 

#endif /* CORE_ARGUMENT_PACKAGE_HPP */
