
/*
 * Created: 05-09-2017
 * Modified: Wed 06 Sep 2017 15:12:41 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef REGISTER_ALL_HPP
#define REGISTER_ALL_HPP

#include <stephanov/observables/build_register.hpp>
#include <stephanov/cooling/cooling_norm/build_register.hpp>
#include <osborn/observables/build_register.hpp>
#include <osborn/cooling/norms/build_register.hpp>
#include <builders/step_builders/build_register.hpp>

namespace rmt {
namespace build {
namespace core {

void register_all()
{
  register_stephanov_observables();
  register_stephanov_cooling_norms();
  register_osborn_observables();
  register_osborn_cooling_norms();
  register_step_builders();
}

} // namespace core 
} // namespace build 
} // namespace rmt 

#endif /* REGISTER_ALL_HPP */
