
/*
 * Created: 05-09-2017
 * Modified: Mon 11 Dec 2017 13:22:34 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "simulation_builder.hpp"
#include <builders/observable_builders/observable_parser.hpp>
#include <builders/step_builders/specific_builders.hpp>
#include <builders/step_builders/update_step_parser.hpp>
#include <builders/type_utils/conditional_upcast.hpp>
#include <builders/type_utils/type_dict.hpp>
#include <simskel/init_update.hpp>
#include <simskel/param_save_step.hpp>
#include <simskel/progress_bar.hpp>
#include <simskel/stages/multi_step_stag.hpp>
#include <simskel/stages/timed_stage.hpp>
#include <utilities/enum_helpers.hpp>
#include <utilities/git/gitversion.hpp>
#include <utilities/json_names.hpp>
#include <utilities/json_parser_helpers.hpp>
#include <utilities/program_options.hpp>

namespace rmt {
namespace build {

Simulation_Builder::Simulation_Builder(nlohmann::json const &tree)
    : main_tree_(tree),
      model_params(parse_model_parameters(main_tree_)),
      scan_params(parse_scan_parameters(main_tree_)),
      obs_builder_map_(parse_observable_section(main_tree_))
{
}

std::vector<std::unique_ptr<simulation::Stage>>
Simulation_Builder::build(Physical_Parameters const &params)
{
  auto stepsize =
      std::make_shared<stepsize::Stepsize>(scan_params.base_stepsize);
  auto timer = std::make_shared<stepsize::Time_Keeper>(stepsize);

  auto rng_seed = parse_seed();
  auto rng = std::make_shared<Generator_Type>(rng_seed);

  auto filename = parse_save_pattern(main_tree_);

  if (!filename.is_valid())
    throw std::runtime_error{"parse error: invalid filename pattern"};

  auto simulation_tree =
      parse::parse_required_key(main_tree_, JSON_Categories::simulation);

  auto config = build_config();
  auto force = build_force(config, params);

  auto core_args = core::Argument_Package{config, force, params};

  auto obs_map = std::map<std::string,
                          std::shared_ptr<observable::Named_Observable_Core>>{};

  std::transform(
      obs_builder_map_.begin(), obs_builder_map_.end(),
      std::inserter(obs_map, obs_map.end()),
      [&core_args](auto const &build_pair) {
        return std::pair<std::string,
                         std::shared_ptr<observable::Named_Observable_Core>>{
            build_pair.first, {build_pair.second->build(core_args)}};
      });

  auto simulation = std::vector<std::unique_ptr<simulation::Stage>>{};

  auto exp_args = core::Expanded_Argument_Package{config,   force, params,
                                                  stepsize, rng,   &obs_map};

  simulation.push_back(build_init_stage(exp_args));

  if (scan_params.therm_time > numeric::number_type{0.0}) {
    if (!parse::has_key(simulation_tree, "thermalisation"))
      throw std::runtime_error{"parse error: therm-time non-zero, but no "
                               "thermalisation block specified"};

    simulation.push_back(parse_timed_stage(simulation_tree["thermalisation"],
                                           timer, scan_params.therm_time,
                                           exp_args));
  }

  if (scan_params.final_time > scan_params.therm_time) {
    if (!parse::has_key(simulation_tree, "run")) {
      throw std::runtime_error{"parse error: final-time indicates a main block "
                               "should exist, but \"run\" block not specified"};
    }

    simulation.push_back(parse_timed_stage(simulation_tree["run"], timer,
                                           scan_params.final_time, exp_args));
  }

  simulation.push_back(build_end_stage(exp_args, rng_seed));

  return simulation;
}

std::size_t Simulation_Builder::parse_seed() const
{
  std::size_t seed = 0ul;

  if (!parse::has_key(main_tree_[JSON_Categories::scan], "seed")) {
    std::random_device random_dev;
    seed = random_dev();
  } else if (main_tree_[JSON_Categories::scan]["seed"].is_number_integer()) {
    seed = main_tree_[JSON_Categories::scan]["seed"];
  } else if (main_tree_[JSON_Categories::scan]["seed"].is_string()) {
    std::string seed_string = main_tree_[JSON_Categories::scan]["seed"];

    if (seed_string == "random") {
      std::random_device random_dev;
      seed = random_dev();
    } else {
      try {
        seed = boost::lexical_cast<std::size_t>(seed_string);
      } catch (boost::bad_lexical_cast) {
        throw std::runtime_error{"parse error: invalid value for " +
                                 JSON_Categories::scan + "/seed"};
      }
    }
  } else {
    throw std::runtime_error{"parse error: invalid value for " +
                             JSON_Categories::scan + "/seed"};
  }

  return seed;
}

std::shared_ptr<Simulation_Config> Simulation_Builder::build_config() const
{
  utility::Combine_Enums<Model_Type, Storage_Type> combine;

  switch (combine(model_params.model, model_params.storage)) {
  case combine(Model_Type::Stephanov, Storage_Type::Cartesian):
    return std::make_shared<core::type_dict<
        Model_Type::Stephanov, Storage_Type::Cartesian>::storage_type>();
  case combine(Model_Type::Stephanov, Storage_Type::Polar):
    return std::make_shared<core::type_dict<
        Model_Type::Stephanov, Storage_Type::Polar>::storage_type>();
  case combine(Model_Type::Osborn, Storage_Type::Cartesian):
    return std::make_shared<core::type_dict<
        Model_Type::Osborn, Storage_Type::Cartesian>::storage_type>();
  default:
    throw std::runtime_error{
        "parse error: no implementation for model/storage combination \"" +
        parse::to_string(model_params.model) + "/" +
        parse::to_string(model_params.storage) + "\""};
  }
}

std::shared_ptr<Simulation_Force>
Simulation_Builder::build_force(std::shared_ptr<Simulation_Config> cfg,
                                Physical_Parameters const &params) const
{
  utility::Combine_Enums<Model_Type, Storage_Type> combine;

  switch (combine(model_params.model, model_params.storage)) {
  case combine(Model_Type::Stephanov, Storage_Type::Cartesian):
    return std::make_shared<core::type_dict<
        Model_Type::Stephanov, Storage_Type::Cartesian>::force_type>(
        core::conditional_upcast<core::type_dict<
            Model_Type::Stephanov, Storage_Type::Cartesian>::storage_type>(cfg),
        params);
  case combine(Model_Type::Stephanov, Storage_Type::Polar):
    return std::make_shared<core::type_dict<Model_Type::Stephanov,
                                            Storage_Type::Polar>::force_type>(
        core::conditional_upcast<core::type_dict<
            Model_Type::Stephanov, Storage_Type::Polar>::storage_type>(cfg),
        params);
  case combine(Model_Type::Osborn, Storage_Type::Cartesian):
    return std::make_shared<core::type_dict<
        Model_Type::Osborn, Storage_Type::Cartesian>::force_type>(
        core::conditional_upcast<core::type_dict<
            Model_Type::Osborn, Storage_Type::Cartesian>::storage_type>(cfg),
        params);
  default:
    throw std::runtime_error{
        "parse error: no implementation for model/storage combination \"" +
        parse::to_string(model_params.model) + "/" +
        parse::to_string(model_params.storage) + "\""};
  }
}

std::unique_ptr<simulation::Stage> Simulation_Builder::build_init_stage(
    core::Expanded_Argument_Package const &args) const
{
  auto stage = std::make_unique<simulation::Single_Step_Stage>();

  auto step_json = R"(
  {
    "name": "init",
    "parameters": {}
  }
  )"_json;

  Init_Step_Builder builder{};
  stage->push_back(builder.build(step_json, main_tree_, args));

  return stage;
}

std::unique_ptr<simulation::Stage> Simulation_Builder::parse_timed_stage(
    nlohmann::json const &stage_tree,
    std::shared_ptr<stepsize::Time_Keeper> timer, numeric::number_type end_time,
    core::Expanded_Argument_Package const &args) const
{
  auto stage = std::make_unique<simulation::Timed_Stage>(timer, end_time);

  if (stage_tree.is_object()) {
    stage->push_back(parse_update_step(stage_tree, main_tree_, args));
  } else if (stage_tree.is_array()) {
    for (auto const &step_tree : stage_tree) {
      stage->push_back(parse_update_step(step_tree, main_tree_, args));
    }
  } else {
    throw std::runtime_error{
        "parse error: invalid json object for simulation stage"};
  }

  stage->push_back(std::make_unique<utility::Every_N_Step>(
      std::make_shared<simulation::Progress_Bar>(
          timer, main_tree_[JSON_Categories::scan][JSON_Scan::final_time]),
      100));

  return stage;
}

std::unique_ptr<simulation::Stage>
Simulation_Builder::build_end_stage(core::Expanded_Argument_Package const &args,
                                    std::size_t seed) const
{
  auto stage = std::make_unique<simulation::Single_Step_Stage>();

  auto step_json = R"(
  {
    "name": "save"
  }
  )"_json;

  Save_Step_Builder builder{};
  stage->push_back(builder.build(step_json, main_tree_, args));

  std::string filename_str =
      main_tree_[JSON_Categories::file][JSON_File::save_file];

  auto filename_gen = build::Filename_Generator{filename_str};
  filename_str = filename_gen.directory(args.phys_params, model_params);

  filename_str += "/@{obs}_mu@{mu_n}_mass@{mass_n}.json";
  filename_gen.set_pattern(filename_str);

  auto tree_copy = main_tree_;
  tree_copy[JSON_Categories::scan]["seed"] = seed;

  tree_copy[JSON_Categories::parameters][JSON_Parameters::mu_real] =
      args.phys_params.chemical_potential.real();
  tree_copy[JSON_Categories::parameters][JSON_Parameters::mu_imag] =
      args.phys_params.chemical_potential.imag();
  tree_copy[JSON_Categories::parameters][JSON_Parameters::mass_real] =
      args.phys_params.fermion_mass.real();
  tree_copy[JSON_Categories::parameters][JSON_Parameters::mass_imag] =
      args.phys_params.fermion_mass.imag();

  tree_copy[JSON_Categories::parameters]["matrix-size"] = M_size;

  tree_copy["code"]["git-rev"] = git::git_version;

  stage->push_back(std::make_unique<simulation::Param_Save_Update>(
      tree_copy, filename_gen, args.phys_params, model_params));

  return stage;
}

} // namespace build
} // namespace rmt
