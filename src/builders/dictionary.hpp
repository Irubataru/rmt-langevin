
/*
 * Created: 02-09-2017
 * Modified: Sat 02 Sep 2017 17:20:27 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef BUILDER_DICTIONARY_HPP
#define BUILDER_DICTIONARY_HPP

#include <map>
#include <memory>

namespace rmt {
namespace build {
namespace core {

template <typename Entry_Type>
class Dictionary
{
public:
  using entry_type = Entry_Type;
  using ptr_type = std::shared_ptr<entry_type>;

  static Dictionary &instance()
  {
    static Dictionary instance_;
    return instance_;
  }

  bool has_key(std::string key) const
  {
    return (factories_.find(key) != factories_.end());
  }

  void add(std::string key, entry_type *&&factory)
  {
    add(key, ptr_type{factory});
  }

  void add(std::string key, ptr_type factory)
  {
    if (has_key(key))
      throw std::runtime_error{"dictionary entry with name \"" + key +
                               "\" already exists"};

    factories_[key] = std::move(factory);
  }

  ptr_type get(std::string key) const
  {
    auto find_it = factories_.find(key);

    if (find_it == factories_.end())
      return {};

    return find_it->second;
  }

  Dictionary(Dictionary const &) = delete;
  Dictionary &operator=(Dictionary const &) = delete;

private:
  Dictionary(){};
  std::map<std::string, ptr_type> factories_;
};

} // namespace core 
} // namespace build 
} // namespace rmt 

#endif /* BUILDER_DICTIONARY_HPP */
