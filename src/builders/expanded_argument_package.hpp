
/*
 * Created: 04-09-2017
 * Modified: Mon 04 Sep 2017 16:04:58 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef EXPANDED_ARGUMENT_PACKAGE_HPP
#define EXPANDED_ARGUMENT_PACKAGE_HPP

#include <map>
#include <random>
#include <simskel/named_observable.hpp>
#include <types.hpp>
#include <utilities/stepsize/stepsize_keeper.hpp>
#include <utilities/json_names.hpp>
#include <utilities/json_parser_helpers.hpp>

namespace rmt {
namespace build {
namespace core {

struct Expanded_Argument_Package
{
  std::shared_ptr<Simulation_Config> config;
  std::shared_ptr<Simulation_Force> force;
  Physical_Parameters phys_params;

  std::shared_ptr<stepsize::Stepsize> stepsize;
  std::shared_ptr<Generator_Type> rng;

  std::map<std::string,
           std::shared_ptr<::rmt::observable::Named_Observable_Core>> *obs_map;
};

} // namespace core
} // namespace build
} // namespace rmt

#endif /* EXPANDED_ARGUMENT_PACKAGE_HPP */
