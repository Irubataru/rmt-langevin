
/*
 * Created: 04-09-2017
 * Modified: Mon 06 Nov 2017 10:02:30 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "update_step_parser.hpp"
#include <simskel/utility_steps.hpp>
#include <utilities/json_names.hpp>
#include <utilities/json_parser_helpers.hpp>

namespace rmt {
namespace build {

std::unique_ptr<::simskel::Simple_Update>
parse_update_step(nlohmann::json const &step_tree,
                  nlohmann::json const &main_tree,
                  core::Expanded_Argument_Package const &args)
{
  using parse::global_parse;
  using parse::parse_required_key;

  auto model = global_parse<Model_Type>(main_tree);
  auto storage = global_parse<Storage_Type>(main_tree);

  std::string key =
      parse::parse_required_key(step_tree, JSON_Generic::name, "");

  auto step =
      parse::check_dictionary_by_key(key, model, storage,
                                     core::Step_Builder_Dictionary::instance())
          ->build(step_tree, main_tree, args);

  if (parse::has_keys(step_tree, JSON_Generic::frequency,
                      JSON_Generic::repeat)) {
    throw std::runtime_error{"Adding both frequency and repeat for the same "
                             "step currently not implemented"};
  }

  if (parse::has_key(step_tree, JSON_Generic::frequency)) {
    step = std::make_unique<utility::Every_N_Step>(
        std::shared_ptr<::simskel::Simple_Update>{std::move(step)},
        std::size_t{step_tree[JSON_Generic::frequency]});
  }

  if (parse::has_key(step_tree, JSON_Generic::repeat)) {
    step = std::make_unique<utility::Repeat_Step>(
        std::shared_ptr<::simskel::Simple_Update>(std::move(step)),
        std::size_t{step_tree[JSON_Generic::repeat]});
  }

  return step;
}

} // namespace build
} // namespace rmt
