
/*
 * Created: 04-09-2017
 * Modified: Tue 07 Nov 2017 11:17:33 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef BUILD_STEP_REGISTER_HPP
#define BUILD_STEP_REGISTER_HPP

#include "specific_builders.hpp"
#include "step_builder.hpp"

namespace rmt {
namespace build {
namespace core {

void register_step_builders()
{
  auto &dict = Step_Builder_Dictionary::instance();

  dict.add("langevin", std::make_unique<Langevin_Step_Builder>());
  dict.add("measure", std::make_unique<Measure_Step_Builder>());
  dict.add("save", std::make_unique<Save_Step_Builder>());
  dict.add("init", std::make_unique<Init_Step_Builder>());
  dict.add("dynamic-stabilisation",
           std::make_unique<Dynamic_Stabilisation_Step_Builder>());
  dict.add("cooling", std::make_unique<Cooling_Step_Builder>());
}

} // namespace core
} // namespace build
} // namespace rmt

#endif /* BUILD_STEP_REGISTER_HPP */
