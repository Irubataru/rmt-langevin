
/*
 * Created: 04-09-2017
 * Modified: Mon 04 Sep 2017 13:55:19 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEP_BUILDER_HPP
#define STEP_BUILDER_HPP

#include <builders/dictionary.hpp>
#include <builders/expanded_argument_package.hpp>
#include <nlohmann/json.hpp>
#include <simskel/update/update_int.hpp>

namespace rmt {
namespace build {

class Step_Builder
{
public:
  using build_type = ::simskel::Simple_Update;
  using ptr_type = std::unique_ptr<build_type>;

  virtual ~Step_Builder(){};

  virtual ptr_type build(nlohmann::json const &, nlohmann::json const &,
                         core::Expanded_Argument_Package const &) = 0;
};

namespace core {

using Step_Builder_Dictionary = Dictionary<Step_Builder>;

} // namespace core

} // namespace build
} // namespace rmt

#endif /* STEP_BUILDER_HPP */
