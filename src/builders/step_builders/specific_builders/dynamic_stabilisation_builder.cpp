
/*
 * Created: 20-09-2017
 * Modified: Wed 20 Sep 2017 16:30:01 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "dynamic_stabilisation_builder.hpp"
#include <builders/type_utils/stephanov_upcasts.hpp>
#include <stephanov/dynamic_stabilisation/dynamic_stabilisation.hpp>
#include <utilities/json_parser_helpers.hpp>

namespace rmt {
namespace build {

Dynamic_Stabilisation_Step_Builder::ptr_type
Dynamic_Stabilisation_Step_Builder::build(
    nlohmann::json const &step_tree, nlohmann::json const &main_tree,
    core::Expanded_Argument_Package const &args)
{
  using parse::global_parse;
  const std::string name{"dynamic-stabilisation"};

  auto model = global_parse<Model_Type>(main_tree);
  auto storage = global_parse<Storage_Type>(main_tree);

  utility::Combine_Enums<Model_Type, Storage_Type> combine;

  if (combine(model, storage) !=
      combine(Model_Type::Stephanov, Storage_Type::Cartesian)) {
    throw std::runtime_error{"parse error: invalid model-storage config for "
                             "building the dynamic stabilisation step"};
  }

  auto param_tree =
      parse::parse_required_key(step_tree, JSON_Generic::parameters, name);

  double alpha = parse::parse_required_key(
      param_tree, "alpha", name + "/" + JSON_Generic::parameters);

  unsigned power = parse::parse_required_key(
      param_tree, "power", name + "/" + JSON_Generic::parameters);

  return std::make_unique<stephanov::cartesian::Dynamic_Stabilisation>(
      core::stephanov_cartesian_config_upcast(args.config), args.stepsize,
      alpha, power);
}

} // namespace build
} // namespace rmt
