
/*
 * Created: 05-09-2017
 * Modified: Tue 05 Sep 2017 11:29:37 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef INIT_STEP_BUILDER_HPP
#define INIT_STEP_BUILDER_HPP

#include <builders/step_builders/step_builder.hpp>

namespace rmt {
namespace build {

class Init_Step_Builder : public Step_Builder
{
public:
  using Step_Builder::build_type;
  using Step_Builder::ptr_type;

  virtual ~Init_Step_Builder(){};

  ptr_type build(nlohmann::json const &step_tree,
                 nlohmann::json const &main_tree,
                 core::Expanded_Argument_Package const &args) override;

private:
  template <Model_Type model, Storage_Type storage>
  ptr_type build(core::Expanded_Argument_Package const &args);
};


} // namespace build 
} // namespace rmt 

#endif /* INIT_STEP_BUILDER_HPP */
