
/*
 * Created: 04-09-2017
 * Modified: Mon 04 Sep 2017 21:38:33 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "save_builder.hpp"
#include <simskel/save_update.hpp>
#include <utilities/program_options.hpp>

namespace rmt {
namespace build {

Save_Step_Builder::ptr_type
Save_Step_Builder::build(nlohmann::json const &step_tree,
                         nlohmann::json const &main_tree,
                         core::Expanded_Argument_Package const &args)
{
  auto filename_gen = parse_save_pattern(main_tree);
  auto model_params = parse_model_parameters(main_tree);

  if (!args.obs_map)
    throw std::runtime_error{
        "parse error: no observable map available for \"save\" step"};

  auto step = std::make_unique<simulation::Save_Update>(
      filename_gen, args.phys_params, model_params);

  if (!parse::has_key(step_tree, JSON_Generic::parameters)) {
    for (auto const &obs : *args.obs_map)
      step->push_back(obs.second);
    return step;
  }

  auto params_tree = step_tree[JSON_Generic::parameters];

  if (params_tree.is_string()) {
    if (params_tree == std::string{"all"}) {
      for (auto const &obs : *args.obs_map)
        step->push_back(obs.second);
    } else {
      step->push_back(parse_named_printable(params_tree, args));
    }
  } else if (params_tree.is_array()) {
    for (auto const &printable : params_tree) {
      step->push_back(parse_named_printable(printable, args));
    }
  } else {
    throw std::runtime_error{"invalid json object for save/parameters"};
  }

  return step;
}

std::shared_ptr<output::Printable> Save_Step_Builder::parse_named_printable(
    std::string name, core::Expanded_Argument_Package const &args)
{
  auto find_it = args.obs_map->find(name);

  if (find_it == args.obs_map->end())
    throw std::runtime_error{
        "parse error: parsing save, no observable named \"" + name +
        "\" defined"};

  return find_it->second;
}

} // namespace build
} // namespace rmt
