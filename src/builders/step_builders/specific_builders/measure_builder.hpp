
/*
 * Created: 04-09-2017
 * Modified: Tue 05 Sep 2017 17:45:02 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef MEASURE_BUILDER_HPP
#define MEASURE_BUILDER_HPP

#include <builders/step_builders/step_builder.hpp>
#include <simskel/measure_update.hpp>
#include <simskel/utility_steps.hpp>
#include <utilities/json_names.hpp>
#include <utilities/json_parser_helpers.hpp>

namespace rmt {
namespace build {

class Measure_Step_Builder : public Step_Builder
{
public:
  using Step_Builder::build_type;
  using Step_Builder::ptr_type;

  virtual ~Measure_Step_Builder(){};

  ptr_type build(nlohmann::json const &step_tree,
                 nlohmann::json const &main_tree,
                 core::Expanded_Argument_Package const &args) override
  {
    using parse::parse_required_key;

    auto step = std::make_unique<simulation::Measure>();

    if (!args.obs_map)
      throw std::runtime_error{
          "parse error: no observable map available for \"measure\" step"};

    if (!parse::has_key(step_tree, JSON_Generic::parameters)) {
      for (auto const &obs : *args.obs_map)
        step->push_back(obs.second);
      return wrap_repeat(step_tree, main_tree, std::move(step));
    }

    auto params_tree = step_tree[JSON_Generic::parameters];

    if (params_tree.is_string()) {
      if (params_tree == std::string{"all"}) {
        for (auto const &obs : *args.obs_map) {
          step->push_back(obs.second);
        }
      } else {
        step->push_back(parse_named_obs(params_tree, args));
      }
    } else if (params_tree.is_array()) {
      for (auto const &single_obs : params_tree) {
        step->push_back(parse_named_obs(single_obs, args));
      }
    } else {
      throw std::runtime_error{"invalid json object for measure/parameters"};
    }

    return wrap_repeat(step_tree, main_tree, std::move(step));
  }

private:
  std::shared_ptr<::simskel::Observable>
  parse_named_obs(std::string name, core::Expanded_Argument_Package const &args)
  {
    auto find_it = args.obs_map->find(name);

    if (find_it == args.obs_map->end())
      throw std::runtime_error{
          "parse error: parsing measure, no observable named \"" + name +
          "\" defined"};

    return find_it->second;
  }

  ptr_type wrap_repeat(nlohmann::json const &step_tree,
                       nlohmann::json const &main_tree, ptr_type step)
  {
    if (parse::has_key(step_tree, JSON_Generic::frequency))
      return step;

    std::size_t skip_steps =
        main_tree[JSON_Categories::scan][JSON_Scan::measure_skips];

    if (skip_steps == 1)
      return step;

    return std::make_unique<utility::Every_N_Step>(
        std::shared_ptr<::simskel::Simple_Update>{std::move(step)}, skip_steps);
  }
};

} // namespace build
} // namespace rmt

#endif /* MEASURE_BUILDER_HPP */
