
/*
 * Created: 05-09-2017
 * Modified: Tue 05 Sep 2017 12:06:39 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef INIT_STEP_BUILDER_CPP
#define INIT_STEP_BUILDER_CPP

#include "init_step_builder.hpp"
#include <builders/type_utils/conditional_upcast.hpp>
#include <simskel/init_update.hpp>
#include <utilities/enum_helpers.hpp>
#include <utilities/json_parser_helpers.hpp>

namespace rmt {
namespace build {

Init_Step_Builder::ptr_type
Init_Step_Builder::build(nlohmann::json const &step_tree,
                         nlohmann::json const &main_tree,
                         core::Expanded_Argument_Package const &args)
{
  using parse::global_parse;

  auto model = global_parse<Model_Type>(main_tree);
  auto storage = global_parse<Storage_Type>(main_tree);

  utility::Combine_Enums<Model_Type, Storage_Type> combine;
  switch (combine(model, storage)) {
  case combine(Model_Type::Stephanov, Storage_Type::Cartesian):
    return build<Model_Type::Stephanov, Storage_Type::Cartesian>(args);
  case combine(Model_Type::Stephanov, Storage_Type::Polar):
    return build<Model_Type::Stephanov, Storage_Type::Polar>(args);
  case combine(Model_Type::Osborn, Storage_Type::Cartesian):
    return build<Model_Type::Osborn, Storage_Type::Cartesian>(args);
  default:
    throw std::runtime_error{"parse error: invalid model-storage config for "
                             "building the init step"};
  }
}

template <Model_Type model, Storage_Type storage>
Init_Step_Builder::ptr_type
Init_Step_Builder::build(core::Expanded_Argument_Package const &args)
{
  using init_step_type = simulation::Init_Update_Step<model, storage>;
  using storage_type = typename init_step_type::storage_type;

  return std::make_unique<init_step_type>(
      core::conditional_upcast<storage_type>(args.config), args.phys_params,
      args.rng);
}

} // namespace build
} // namespace rmt

#endif /* INIT_STEP_BUILDER_CPP */
