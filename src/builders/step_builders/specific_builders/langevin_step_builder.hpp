
/*
 * Created: 04-09-2017
 * Modified: Mon 04 Sep 2017 15:25:01 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef LANGEVIN_STEP_BUILDER_HPP
#define LANGEVIN_STEP_BUILDER_HPP

#include <builders/step_builders/step_builder.hpp>

namespace rmt {
namespace build {

class Langevin_Step_Builder : public Step_Builder
{
public:
  using Step_Builder::build_type;
  using Step_Builder::ptr_type;

  virtual ~Langevin_Step_Builder(){};

  ptr_type build(nlohmann::json const &step_tree,
                 nlohmann::json const &main_tree,
                 core::Expanded_Argument_Package const &args) override;

private:
  template <typename Target>
  ptr_type build_specific_step(core::Expanded_Argument_Package const &args);
};

} // namespace build
} // namespace rmt

#endif /* LANGEVIN_STEP_BUILDER_HPP */
