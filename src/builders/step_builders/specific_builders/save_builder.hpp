
/*
 * Created: 04-09-2017
 * Modified: Mon 04 Sep 2017 21:03:57 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef SAVE_BUILDER_HPP
#define SAVE_BUILDER_HPP

#include <builders/step_builders/step_builder.hpp>
#include <simskel/measure_update.hpp>
#include <utilities/json_names.hpp>
#include <utilities/json_parser_helpers.hpp>

namespace rmt {
namespace build {

class Save_Step_Builder : public Step_Builder
{
public:
  using Step_Builder::build_type;
  using Step_Builder::ptr_type;

  virtual ~Save_Step_Builder(){};

  ptr_type build(nlohmann::json const &step_tree,
                 nlohmann::json const &main_tree,
                 core::Expanded_Argument_Package const &args) override;

private:
  std::shared_ptr<output::Printable> parse_named_printable(
      std::string name, core::Expanded_Argument_Package const &args);
};

} // namespace build
} // namespace rmt

#endif /* SAVE_BUILDER_HPP */
