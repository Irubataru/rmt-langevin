
/*
 * Created: 02-11-2017
 * Modified: Thu 02 Nov 2017 11:15:37 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef COOLING_STEP_BUILDER_HPP
#define COOLING_STEP_BUILDER_HPP

#include <builders/step_builders/step_builder.hpp>

namespace rmt {
namespace build {

class Cooling_Step_Builder : public Step_Builder
{
public:
  using Step_Builder::build_type;
  using Step_Builder::ptr_type;

  virtual ~Cooling_Step_Builder(){};

  ptr_type build(nlohmann::json const &step_tree,
                 nlohmann::json const &main_tree,
                 core::Expanded_Argument_Package const &args) override;

private:
  template <Model_Type model, Storage_Type storage>
  ptr_type build_specific(nlohmann::json const &step_tree,
                          nlohmann::json const &main_tree,
                          core::Expanded_Argument_Package const &args);
};

//template Cooling_Step_Builder::ptr_type
//Cooling_Step_Builder::build_specific<Model_Type::Stephanov,
                                     //Storage_Type::Cartesian>(
    //nlohmann::json const &step_tree, nlohmann::json const &main_tree,
    //core::Expanded_Argument_Package const &args);

//template Cooling_Step_Builder::ptr_type
//Cooling_Step_Builder::build_specific<Model_Type::Osborn,
                                     //Storage_Type::Cartesian>(
    //nlohmann::json const &step_tree, nlohmann::json const &main_tree,
    //core::Expanded_Argument_Package const &args);

} // namespace build
} // namespace rmt

#endif /* COOLING_STEP_BUILDER_HPP */
