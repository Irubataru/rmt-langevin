
/*
 * Created: 04-09-2017
 * Modified: Tue 07 Nov 2017 11:16:57 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "langevin_step_builder.hpp"
#include <builders/type_utils/conditional_upcast.hpp>
#include <builders/type_utils/type_deps.hpp>
#include <stephanov/langevin/force.hpp>
#include <stephanov/resource/resource.hpp>
#include <utilities/enum_helpers.hpp>
#include <utilities/white_noise.hpp>

namespace rmt {
namespace build {

Langevin_Step_Builder::ptr_type
Langevin_Step_Builder::build(nlohmann::json const &,
                             nlohmann::json const &main_tree,
                             core::Expanded_Argument_Package const &args)
{
  using parse::global_parse;

  auto model = global_parse<Model_Type>(main_tree);
  auto storage = global_parse<Storage_Type>(main_tree);

  utility::Combine_Enums<Model_Type, Storage_Type> combine;

  switch (combine(model, storage)) {
  case combine(Model_Type::Stephanov, Storage_Type::Cartesian):
    return build_specific_step<::rmt::stephanov::cartesian::Langevin_Step>(
        args);
  case combine(Model_Type::Stephanov, Storage_Type::Polar):
    return build_specific_step<::rmt::stephanov::polar::Langevin_Step>(args);
  case combine(Model_Type::Osborn, Storage_Type::Cartesian):
    return build_specific_step<::rmt::osborn::cartesian::Langevin_Step>(args);
  default:
    throw std::runtime_error{"parse error: invalid model-storage config for "
                             "building the langevin step"};
  }
}

template <typename Target>
Langevin_Step_Builder::ptr_type Langevin_Step_Builder::build_specific_step(
    core::Expanded_Argument_Package const &args)
{
  using core::conditional_upcast;

  using storage_type = typename core::deps<Target>::storage;
  using force_type = typename core::deps<Target>::force;

  return std::make_unique<Target>(
      conditional_upcast<storage_type>(args.config),
      conditional_upcast<force_type>(args.force), args.stepsize,
      std::make_unique<Std_White_Noise<Generator_Type>>(*args.rng));
}

} // namespace build
} // namespace rmt
