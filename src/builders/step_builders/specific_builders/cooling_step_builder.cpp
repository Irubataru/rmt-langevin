
/*
 * Created: 02-11-2017
 * Modified: Mon 06 Nov 2017 10:29:10 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "cooling_step_builder.hpp"

#include <builders/norm_builders/cooling_norm_parser.hpp>
#include <builders/norm_builders/norm_builder.hpp>
#include <builders/type_utils/conditional_upcast.hpp>
#include <builders/type_utils/type_deps.hpp>
#include <osborn/cooling/core_types.hpp>
#include <stephanov/cooling/core_types.hpp>
#include <utilities/enum_helpers.hpp>
#include <utilities/white_noise.hpp>

namespace steph_core = ::rmt::stephanov::cooling::core;

namespace rmt {
namespace build {

namespace {

template <Derivative_Alg dalg>
struct Derivative_Alg_Dictionary
{
  using type = void;
};

template <>
struct Derivative_Alg_Dictionary<Derivative_Alg::Numerical>
{
  using type = steph_core::Numerical_Derivative_IdxCmpAlg;
};

template <>
struct Derivative_Alg_Dictionary<Derivative_Alg::Analytic>
{
  using type = steph_core::Analytic_Derivative_IdxCmpAlg;
};

template <Minimise_Alg malg>
struct Minimise_Alg_Dictionary
{
  using type = void;
};

template <>
struct Minimise_Alg_Dictionary<Minimise_Alg::Brent>
{
  using type = steph_core::Brent_Minimisation_Algorithm;
};

template <>
struct Minimise_Alg_Dictionary<Minimise_Alg::Fixed>
{
  using type = steph_core::Fixed_Minimisation_Algorithm;
};

template <typename Core_Component>
Core_Component build_core_component(
    nlohmann::json const &param_tree,
    std::shared_ptr<stephanov::cooling::norm::Norm> const &norm) = delete;

/* Stephanov components */

template <>
steph_core::Numerical_Derivative_IdxCmpAlg
build_core_component<steph_core::Numerical_Derivative_IdxCmpAlg>(
    nlohmann::json const &param_tree,
    std::shared_ptr<stephanov::cooling::norm::Norm> const &norm)
{
  auto eps =
      static_cast<double>(parse::parse_required_key(param_tree, "epsilon"));

  return steph_core::Numerical_Derivative_IdxCmpAlg{norm, eps};
}

template <>
steph_core::Analytic_Derivative_IdxCmpAlg
build_core_component<steph_core::Analytic_Derivative_IdxCmpAlg>(
    nlohmann::json const &,
    std::shared_ptr<stephanov::cooling::norm::Norm> const &norm)
{
  auto upcast =
      std::dynamic_pointer_cast<stephanov::cooling::norm::Derivative_Norm>(
          norm);

  if (!upcast)
    throw std::runtime_error{
        "The chosen norm is not analytically differentiable"};

  return steph_core::Analytic_Derivative_IdxCmpAlg{upcast};
}

template <>
steph_core::Advanced_Index_Conversion_Algorithm
build_core_component<steph_core::Advanced_Index_Conversion_Algorithm>(
    nlohmann::json const &param_tree,
    std::shared_ptr<stephanov::cooling::norm::Norm> const &norm)
{
  return steph_core::Advanced_Index_Conversion_Algorithm{};
}

template <>
steph_core::Brent_Minimisation_Algorithm
build_core_component<steph_core::Brent_Minimisation_Algorithm>(
    nlohmann::json const &param_tree,
    std::shared_ptr<stephanov::cooling::norm::Norm> const &norm)
{
  using parse::parse_required_key;

  auto first_eps =
      static_cast<double>(parse_required_key(param_tree, "first-step"));

  auto max_step =
      static_cast<double>(parse_required_key(param_tree, "max-step"));

  auto max_itterations =
      static_cast<std::size_t>(parse_required_key(param_tree, "itterations"));

  return steph_core::Brent_Minimisation_Algorithm{norm, first_eps, max_step,
                                                  max_itterations};
}

template <>
steph_core::Fixed_Minimisation_Algorithm
build_core_component<steph_core::Fixed_Minimisation_Algorithm>(
    nlohmann::json const &param_tree,
    std::shared_ptr<stephanov::cooling::norm::Norm> const &norm)
{
  using parse::parse_required_key;

  auto stepsize =
      static_cast<double>(parse_required_key(param_tree, "stepsize"));

  return steph_core::Fixed_Minimisation_Algorithm{stepsize};
}

template <Derivative_Alg deriv_alg, Minimise_Alg min_alg>
auto build_stephanov_cooling_step(
    nlohmann::json const &deriv_tree, nlohmann::json const &min_tree,
    std::shared_ptr<stephanov::cooling::norm::Norm> const &norm,
    core::Expanded_Argument_Package const &args)
{
  using deriv_alg_type = typename Derivative_Alg_Dictionary<deriv_alg>::type;
  using conver_alg_type = steph_core::Advanced_Index_Conversion_Algorithm;
  using min_alg_type = typename Minimise_Alg_Dictionary<min_alg>::type;

  using core_type =
      steph_core::Cooling_Core<deriv_alg_type, conver_alg_type, min_alg_type>;

  auto upcast_config =
      core::conditional_upcast<stephanov::cartesian::Matrix_Storage>(
          args.config);

  return std::make_unique<stephanov::cooling::core::cartesian::
                              General_Cartesian_Cooling_Step<core_type>>(
      upcast_config,
      core_type{build_core_component<deriv_alg_type>(deriv_tree, norm),
                build_core_component<conver_alg_type>(deriv_tree, norm),
                build_core_component<min_alg_type>(min_tree, norm)});
}

} // namespace

template <>
Cooling_Step_Builder::ptr_type
Cooling_Step_Builder::build_specific<Model_Type::Stephanov,
                                     Storage_Type::Cartesian>(
    nlohmann::json const &step_tree, nlohmann::json const &main_tree,
    core::Expanded_Argument_Package const &args)
{
  using parse::parse_enum;
  using parse::parse_required_key;
  using stephanov::cooling::norm::Norm;

  auto param_tree = parse::parse_required_key(
      step_tree, JSON_Generic::parameters, "cooling");

  auto norm_tree = parse::parse_required_key(param_tree, "cooling-norm",
                                             "cooling/parameters");

  auto upcast_norm =
      std::dynamic_pointer_cast<Norm>(std::shared_ptr<Simulation_Cooling_Norm>(
          parse_cooling_norm(norm_tree, main_tree)));

  if (!upcast_norm) {
    throw std::runtime_error{"parse error: unable to upcast cooling norm for "
                             "norm observable in model " +
                             parse::to_string(Model_Type::Stephanov)};
  }

  auto deriv_tree =
      parse_required_key(param_tree, "derivative", "cooling/parameters");
  auto min_tree =
      parse_required_key(param_tree, "minimise", "cooling/parameters");

  auto deriv_alg = parse_enum<Derivative_Alg>(deriv_tree, "name",
                                              "cooling/parameters/derivative");
  auto min_alg =
      parse_enum<Minimise_Alg>(min_tree, "name", "cooling/parameters/minimise");

  utility::Combine_Enums<Derivative_Alg, Minimise_Alg> combine;

  switch (combine(deriv_alg, min_alg)) {
  case combine(Derivative_Alg::Numerical, Minimise_Alg::Fixed):
    return build_stephanov_cooling_step<Derivative_Alg::Numerical,
                                        Minimise_Alg::Fixed>(
        deriv_tree, min_tree, upcast_norm, args);
  case combine(Derivative_Alg::Numerical, Minimise_Alg::Brent):
    return build_stephanov_cooling_step<Derivative_Alg::Numerical,
                                        Minimise_Alg::Brent>(
        deriv_tree, min_tree, upcast_norm, args);
  case combine(Derivative_Alg::Analytic, Minimise_Alg::Fixed):
    return build_stephanov_cooling_step<Derivative_Alg::Analytic,
                                        Minimise_Alg::Fixed>(
        deriv_tree, min_tree, upcast_norm, args);
  case combine(Derivative_Alg::Analytic, Minimise_Alg::Brent):
    return build_stephanov_cooling_step<Derivative_Alg::Analytic,
                                        Minimise_Alg::Brent>(
        deriv_tree, min_tree, upcast_norm, args);
  default:
    throw std::runtime_error{
        "Invalid derivative and minimsation combination for the cooling step"};
  }
}

template <>
Cooling_Step_Builder::ptr_type
Cooling_Step_Builder::build_specific<Model_Type::Osborn,
                                     Storage_Type::Cartesian>(
    nlohmann::json const &step_tree, nlohmann::json const &main_tree,
    core::Expanded_Argument_Package const &args)
{
  throw std::runtime_error{"Cooling for the Osborn model not implemented yet"};
}

Cooling_Step_Builder::ptr_type
Cooling_Step_Builder::build(nlohmann::json const &step_tree,
                            nlohmann::json const &main_tree,
                            core::Expanded_Argument_Package const &args)
{
  using parse::global_parse;

  auto model = global_parse<Model_Type>(main_tree);
  auto storage = global_parse<Storage_Type>(main_tree);

  utility::Combine_Enums<Model_Type, Storage_Type> combine;

  switch (combine(model, storage)) {
  case combine(Model_Type::Stephanov, Storage_Type::Cartesian):
    return build_specific<Model_Type::Stephanov, Storage_Type::Cartesian>(
        step_tree, main_tree, args);
  case combine(Model_Type::Osborn, Storage_Type::Cartesian):
    return build_specific<Model_Type::Osborn, Storage_Type::Cartesian>(
        step_tree, main_tree, args);
  default:
    throw std::runtime_error{"parse error: invalid model-storage config for "
                             "building the langevin step"};
  }
}

} // namespace build
} // namespace rmt
