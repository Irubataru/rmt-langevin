
/*
 * Created: 04-09-2017
 * Modified: Mon 04 Sep 2017 21:16:47 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef UPDATE_STEP_PARSER_HPP
#define UPDATE_STEP_PARSER_HPP

#include "step_builder.hpp"

namespace rmt {
namespace build {

std::unique_ptr<::simskel::Simple_Update>
parse_update_step(nlohmann::json const &, nlohmann::json const &,
                  core::Expanded_Argument_Package const &);

} // namespace build
} // namespace rmt

#endif /* UPDATE_STEP_PARSER_HPP */
