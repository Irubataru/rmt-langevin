/*
 * Created: 06-04-2016
 * Modified: Fri 01 Sep 2017 12:28:04 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OSBORN_RMT_RESOURCES_HPP
#define OSBORN_RMT_RESOURCES_HPP

#include <types.hpp>
#include <utilities/matrix/identity_constructor.hpp>

namespace rmt {
namespace osborn {

enum class M_name
{
  PHI,
  PSI,
  PHIADJ,
  PSIADJ,
  X,
  Y,
  XY,
  M,
  G,
  R,
  T
};

class Matrix_Storage_Base : public Simulation_Config
{
public:
  using value_type = numeric::complex_type;

  Matrix_Storage_Base();

  const M_type &read(M_name) const;

  const value_type &read_mass() const
  {
    return mass_val;
  }

  const value_type &read_mu() const
  {
    return mu_val;
  }

  void set_mass(value_type mass)
  {
    mass_val = mass;
    matrix::Identity_Matrix(mf_mat, mass * mass);
  }

  void set_mu(value_type chemical_potential)
  {
    using numeric::cosh;
    using numeric::sinh;

    mu_val = chemical_potential;
    c_mu = cosh(chemical_potential);
    s_mu = sinh(chemical_potential);
  }

  unsigned long get_storage_revision() const
  {
    return storage_revision;
  }

  void update();

  virtual ~Matrix_Storage_Base()
  {
  }

private:
  virtual M_type construct_Phi() const = 0;
  virtual M_type construct_PhiAdj() const = 0;
  virtual M_type construct_Psi() const = 0;
  virtual M_type construct_PsiAdj() const = 0;

  M_type phi, psi, phi_adj, psi_adj, x, y, xy, mf_mat, g, r, t;

  value_type mu_val, mass_val;
  value_type c_mu, s_mu;

  unsigned long storage_revision;
};

extern const std::string model_name;

namespace cartesian {

enum class M_name
{
  A,
  B,
  ALPH,
  BETA
};

class Matrix_Storage : public Matrix_Storage_Base
{
public:
  using Matrix_Storage_Base::read;

  Matrix_Storage();

  virtual const M_type &read(M_name) const;
  virtual M_type &read_write(M_name);

  virtual ~Matrix_Storage()
  {
  }

private:
  virtual M_type construct_Phi() const override;
  virtual M_type construct_PhiAdj() const override;
  virtual M_type construct_Psi() const override;
  virtual M_type construct_PsiAdj() const override;

private:
  M_type a, b, alph, beta;
};

extern const std::string representation_name;

} // namespace cartesian

namespace builder {
namespace builder_core {

struct requires_resource_type
{
  char *requirements_requires_resource;
};

} // namespace builder_core
} // namespace builder

} // namespace osborn
} // namespace RMT

#endif /* OSBORN_RMT_RESOURCES_HPP */
