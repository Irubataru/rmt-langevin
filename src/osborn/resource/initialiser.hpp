/*
 * Created: 08-05-2016
 * Modified: Tue 28 Jun 2016 22:27:10 CEST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OSBORN_FORCE_INITIALISER_HPP
#define OSBORN_FORCE_INITIALISER_HPP

#include <osborn/resource/resource.hpp>
#include <types.hpp>

namespace rmt {
namespace osborn {
namespace cartesian {

template <typename Algorithm>
void Initialise(Matrix_Storage &storage, Physical_Parameters par, Algorithm alg)
{
  alg(storage.read_write(M_name::A), storage.read_write(M_name::B),
      storage.read_write(M_name::ALPH), storage.read_write(M_name::BETA));

  storage.set_mass(par.fermion_mass);
  storage.set_mu(par.chemical_potential);
  storage.update();
}

} // namespace cartesian
} // namespace osborn
} // namespace rmt

#endif /* OSBORN_FORCE_INITIALISER_HPP */
