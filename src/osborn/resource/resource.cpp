/*
 * Created: 06-04-2016
 * Modified: Tue 14 Feb 2017 10:57:44 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "resource.hpp"

namespace rmt {
namespace osborn {

Matrix_Storage_Base::Matrix_Storage_Base()
    : phi{matrix::Matrix()},
      psi{matrix::Matrix()},
      phi_adj{matrix::Matrix()},
      psi_adj{matrix::Matrix()},
      x{matrix::Matrix()},
      y{matrix::Matrix()},
      xy{matrix::Matrix()},
      mf_mat{matrix::Identity_Matrix()},
      g{matrix::Matrix()},
      r{matrix::Matrix()},
      t{matrix::Matrix()},
      storage_revision{0}
{
}

const M_type &Matrix_Storage_Base::read(M_name name) const
{
  switch (name) {
  case M_name::PHI:
    return phi;
  case M_name::PSI:
    return psi;
  case M_name::PHIADJ:
    return phi_adj;
  case M_name::PSIADJ:
    return psi_adj;
  case M_name::X:
    return x;
  case M_name::Y:
    return y;
  case M_name::XY:
    return xy;
  case M_name::M:
    return mf_mat;
  case M_name::G:
    return g;
  case M_name::R:
    return r;
  case M_name::T:
    return t;
  default:
    throw std::runtime_error{"Matrix_Storage_Base: Name out of range"};
  }
}

void Matrix_Storage_Base::update()
{
  using numeric::imag_i;

  phi = construct_Phi();
  psi = construct_Psi();
  phi_adj = construct_PhiAdj();
  psi_adj = construct_PsiAdj();

  x = imag_i * c_mu * phi + s_mu * psi;
  y = imag_i * c_mu * phi_adj + s_mu * psi_adj;
  xy = x * y;

  g = blaze::inv(mf_mat - xy);
  r = blaze::trans(y * g);
  t = g * x;

  ++storage_revision;
}

namespace cartesian {

Matrix_Storage::Matrix_Storage()
    : a{matrix::Matrix()},
      b{matrix::Matrix()},
      alph{matrix::Matrix()},
      beta{matrix::Matrix()}
{
}

M_type &Matrix_Storage::read_write(M_name name)
{
  switch (name) {
  case M_name::A:
    return a;
  case M_name::B:
    return b;
  case M_name::ALPH:
    return alph;
  case M_name::BETA:
    return beta;
  default:
    throw std::runtime_error{"cartesian::Matrix_Storage: Name out of range"};
  }
}

const M_type &Matrix_Storage::read(M_name name) const
{
  switch (name) {
  case M_name::A:
    return a;
  case M_name::B:
    return b;
  case M_name::ALPH:
    return alph;
  case M_name::BETA:
    return beta;
  default:
    throw std::runtime_error{"cartesian::Matrix_Storage: Name out of range"};
  }
}

M_type Matrix_Storage::construct_Phi() const
{
  using numeric::imag_i;
  return (a + imag_i * b);
}

M_type Matrix_Storage::construct_PhiAdj() const
{
  using numeric::imag_i;
  return blaze::trans(a - imag_i * b);
}

M_type Matrix_Storage::construct_Psi() const
{
  using numeric::imag_i;
  return (alph + imag_i * beta);
}

M_type Matrix_Storage::construct_PsiAdj() const
{
  using numeric::imag_i;
  return blaze::trans(alph - imag_i * beta);
}

} // namespace cartesian

} // namespace osborn
} // namespace RMT
