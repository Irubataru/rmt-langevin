/*
 * Created: 10-05-2016
 * Modified: Sat 02 Jul 2016 17:06:27 CEST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "resource.hpp"

namespace rmt {
namespace osborn {

const std::string model_name = "osborn";

namespace cartesian {
const std::string representation_name = {"cartesian"};
} // namespace cartesian

} // namespace osborn
} // namespace rmt
