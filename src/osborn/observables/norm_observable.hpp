/*
 * Created: 08-06-2016
 * Modified: Fri 01 Sep 2017 11:37:48 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OSBORN_NORM_OBSERVABLE_HPP
#define OSBORN_NORM_OBSERVABLE_HPP

#include <complex>
#include <memory>
#include <osborn/cooling/cooling_types.hpp>
#include <osborn/cooling/norms/norm_interface.hpp>
#include <osborn/resource/resource.hpp>
#include <simskel/named_observable.hpp>
#include <types.hpp>

namespace rmt {
namespace osborn {
namespace observable {

class Cooling_Norm
    : public ::rmt::observable::Named_Observable<rmt::numeric::number_type>,
      public builder::builder_core::requires_resource_type
{
public:
  using value_type = rmt::numeric::number_type;
  using norm_args_type = ::rmt::osborn::cooling::norm::Norm_Arguments;
  using size_type =
      ::rmt::observable::Object_With_Record<value_type>::size_type;

  Cooling_Norm(const std::shared_ptr<const Matrix_Storage_Base> &mat,
               const std::shared_ptr<cooling::norm::Norm> &nptr,
               const std::string &norm_name, const std::string &norm_dir_name)
      : storage{mat},
        norm_ptr{nptr},
        obs_name_{norm_name},
        obs_dir_name_{norm_dir_name}
  {
  }

  Cooling_Norm(const std::shared_ptr<const Matrix_Storage_Base> &mat,
               const std::shared_ptr<cooling::norm::Norm> &nptr,
               const std::string &norm_name)
      : Cooling_Norm{mat, nptr, norm_name, norm_name}
  {
    std::replace(obs_dir_name_.begin(), obs_dir_name_.end(), ' ', '_');
  }

  Cooling_Norm(const std::shared_ptr<const Matrix_Storage_Base> &mat,
               const std::shared_ptr<cooling::norm::Norm> &nptr)
      : Cooling_Norm{mat, nptr, "cooling norm"}
  {
  }

  virtual std::string name() const override
  {
    return obs_name_;
  }

  virtual std::string dir_name() const override
  {
    return obs_dir_name_;
  }

private:
  norm_args_type construct_norm_args() const
  {
    using base_name = ::rmt::osborn::M_name;

    return {storage->read(base_name::X),
            storage->read(base_name::Y),
            storage->read(base_name::PHI),
            storage->read(base_name::PHIADJ),
            storage->read(base_name::PSI),
            storage->read(base_name::PSIADJ),
            storage->read_mu(),
            storage->read_mass()};
  }

private:
  virtual value_type calculate_value() override
  {
    return norm_ptr->compute(construct_norm_args());
  }

  std::shared_ptr<const Matrix_Storage_Base> storage;
  std::shared_ptr<cooling::norm::Norm> norm_ptr;

  std::string obs_name_;
  std::string obs_dir_name_;
};

} // namespace observable
} // namespace osborn
} // namespace rmt

#endif /* OSBORN_NORM_OBSERVABLE_HPP */
