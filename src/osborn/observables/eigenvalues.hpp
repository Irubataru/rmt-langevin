/*
 * Created: 07-04-2016
 * Modified: Fri 01 Sep 2017 11:36:24 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OSBORN_EIGENVALUES_HPP
#define OSBORN_EIGENVALUES_HPP

#include <memory>
#include <osborn/resource/resource.hpp>
#include <simskel/named_observable.hpp>

namespace rmt {
namespace osborn {
namespace observable {

class Eigenvalues
    : public ::rmt::observable::Named_Observable<rmt::complex_vector_type>,
      public builder::builder_core::requires_resource_type
{
public:
  using value_type = rmt::complex_vector_type;
  using size_type =
      ::rmt::observable::Object_With_Record<value_type>::size_type;

  Eigenvalues(const std::shared_ptr<const Matrix_Storage_Base> &mat)
      : storage{mat}
  {
  }

  Eigenvalues(const std::shared_ptr<const Matrix_Storage_Base> &mat,
              size_type reserved_size)
      : Eigenvalues{mat}
  {
    reserve(reserved_size);
  };

  virtual std::string name() const override
  {
    static const auto obs_name = std::string{"inverse matrix eigenvalues"};
    return obs_name;
  }

  virtual std::string dir_name() const override
  {
    static const auto obs_dir_name = std::string{"inverse_matrix_eigenvalues"};
    return obs_dir_name;
  }

private:
  virtual value_type calculate_value() override;
  std::shared_ptr<const Matrix_Storage_Base> storage;
};

} // namespace observable
} // namespace osborn
} // namespace rmt

#endif /* OSBORN_EIGENVALUES_HPP */
