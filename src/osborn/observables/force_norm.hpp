
/*
 * Created: 12-12-2016
 * Modified: Fri 01 Sep 2017 11:37:13 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef FORCE_NORM_HPP
#define FORCE_NORM_HPP

#include <osborn/langevin/force.hpp>
#include <simskel/named_observable.hpp>

namespace rmt {
namespace osborn {
namespace cartesian {
namespace observable {

class Force_Norm : public ::rmt::observable::Named_Observable<rmt::numeric::number_type>,
                   public builder::builder_core::requires_force_type
{
public:
  using value_type = rmt::numeric::number_type;
  using size_type =
      ::rmt::observable::Object_With_Record<value_type>::size_type;

  Force_Norm(const std::shared_ptr<const Force> &f_ref) : force{f_ref}
  {
  }

  Force_Norm(const std::shared_ptr<const Force> &f_ref,
             size_type reserved_size)
      : Force_Norm{f_ref}
  {
    reserve(reserved_size);
  }

  virtual std::string name() const override
  {
    static const auto obs_name = std::string{"force norm pair"};
    return obs_name;
  }

  virtual std::string dir_name() const override
  {
    static const auto obs_dir_name = std::string{"force_norm_pair"};
    return obs_dir_name;
  }

private:
  virtual value_type calculate_value() override
  {
    using std::norm;
    using numeric::sqrt;

    auto phi_force_norm = value_type{0.};

    const auto &a_ref = force->get_force_a();
    const auto &b_ref = force->get_force_b();

    for (auto i = 0ul; i < a_ref.rows(); ++i)
      for (auto j = 0ul; j < a_ref.columns(); ++j)
        phi_force_norm += norm(a_ref(i, j)) + norm(b_ref(i, j));

    auto psi_force_norm = value_type{0.};

    const auto &al_ref = force->get_force_alpha();
    const auto &be_ref = force->get_force_beta();

    for (auto i = 0ul; i < al_ref.rows(); ++i)
      for (auto j = 0ul; j < al_ref.columns(); ++j)
        psi_force_norm += norm(al_ref(i, j)) + norm(be_ref(i, j));

    return sqrt(phi_force_norm + psi_force_norm);
  }

  std::shared_ptr<const Force> force;
};

} // namespace observable
} // namespace cartesian
} // namespace osborn
} // namespace rmt

#endif /* FORCE_NORM_HPP */
