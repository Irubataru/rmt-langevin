/*
 * Created: 07-04-2016
 * Modified: Fri 01 Sep 2017 11:38:17 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "complex_determinant.hpp"

namespace rmt {
namespace osborn {
namespace observable {

Determinant::value_type Determinant::calculate_value()
{
  using base_name = ::rmt::osborn::M_name;

  const auto &g_ref = storage->read(base_name::G);
  return rmt::numeric::number_type{1.} / blaze::det(g_ref);
}

} // namespace observable
} // namespace osborn
} // namespace rmt
