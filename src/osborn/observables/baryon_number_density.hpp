/*
 * Created: 07-04-2016
 * Modified: Fri 01 Sep 2017 11:35:04 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OSBORN_BARYON_NUMBER_DENSITY_HPP
#define OSBORN_BARYON_NUMBER_DENSITY_HPP

#include <memory>
#include <osborn/resource/resource.hpp>
#include <simskel/named_observable.hpp>
#include <types.hpp>

namespace rmt {
namespace osborn {
namespace observable {

class Baryon_Number_Density
    : public ::rmt::observable::Named_Observable<rmt::numeric::complex_type>,
      public builder::builder_core::requires_resource_type
{
public:
  using value_type = rmt::numeric::complex_type;
  using number_type = rmt::numeric::number_type;
  using size_type =
      ::rmt::observable::Object_With_Record<value_type>::size_type;

  Baryon_Number_Density(const std::shared_ptr<const Matrix_Storage_Base> &mat,
                        Physical_Parameters par)
      : storage{mat},
        prefactor(static_cast<double>(par.number_of_flavours) / M_size),
        c_mu(rmt::numeric::cosh(par.chemical_potential)),
        s_mu(rmt::numeric::sinh(par.chemical_potential))
  {
  }

  Baryon_Number_Density(const std::shared_ptr<const Matrix_Storage_Base> &mat,
                        Physical_Parameters par, size_type reserved_size)
      : Baryon_Number_Density{mat, par}
  {
    reserve(reserved_size);
  };

  virtual std::string name() const override
  {
    static const auto obs_name = std::string{"baryon number density"};
    return obs_name;
  }

  virtual std::string dir_name() const override
  {
    static const auto obs_dir_name = std::string{"baryon_number_density"};
    return obs_dir_name;
  }

private:
  virtual value_type calculate_value() override;

  std::shared_ptr<const Matrix_Storage_Base> storage;
  value_type prefactor;
  value_type c_mu, s_mu;
};

} // namespace observable
} // namespace osborn
} // namespace rmt

#endif /* OSBORN_BARYON_NUMBER_DENSITY_HPP */
