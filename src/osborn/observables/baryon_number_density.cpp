
/*
 * Created: 07-04-2016
 * Modified: Fri 01 Sep 2017 11:38:01 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "baryon_number_density.hpp"
#include <utilities/matrix/trace.hpp>

using rmt::numeric::imag_i;

namespace rmt {
namespace osborn {
namespace observable {

Baryon_Number_Density::value_type Baryon_Number_Density::calculate_value()
{
  using base_name = ::rmt::osborn::M_name;

  const auto &phi = storage->read(base_name::PHI);
  const auto &psi = storage->read(base_name::PSI);
  const auto &phi_adj = storage->read(base_name::PHIADJ);
  const auto &psi_adj = storage->read(base_name::PSIADJ);

  const auto &g = storage->read(base_name::G);
  const auto &x = storage->read(base_name::X);
  const auto &y = storage->read(base_name::Y);

  return -prefactor *
         matrix::Trace(g * ((imag_i * s_mu * phi + c_mu * psi) * y +
                            x * (imag_i * s_mu * phi_adj + c_mu * psi_adj)));
}

} // namespace observable
} // namespace osborn
} // namespace rmt
