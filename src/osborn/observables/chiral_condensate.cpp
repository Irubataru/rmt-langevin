/*
 * Created: 07-04-2016
 * Modified: Fri 01 Sep 2017 11:38:13 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "chiral_condensate.hpp"
#include <utilities/matrix/trace.hpp>

namespace rmt {
namespace osborn {
namespace observable {

Chiral_Condensate::value_type Chiral_Condensate::calculate_value()
{
  using base_name = ::rmt::osborn::M_name;

  const auto &g_ref = storage->read(base_name::G);
  return prefactor * matrix::Trace(g_ref);
}

} // namespace observable
} // namespace osborn
} // namespace rmt
