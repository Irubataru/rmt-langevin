
/*
 * Created: 07-04-2016
 * Modified: Fri 01 Sep 2017 11:35:44 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OSBORN_CHIRAL_CONDENSATE_HPP
#define OSBORN_CHIRAL_CONDENSATE_HPP

#include <memory>
#include <osborn/resource/resource.hpp>
#include <simskel/named_observable.hpp>
#include <types.hpp>

namespace rmt {
namespace osborn {
namespace observable {

class Chiral_Condensate
    : public ::rmt::observable::Named_Observable<rmt::numeric::complex_type>,
      public builder::builder_core::requires_resource_type
{
public:
  using value_type = rmt::numeric::complex_type;
  using number_type = rmt::numeric::number_type;
  using size_type =
      ::rmt::observable::Object_With_Record<value_type>::size_type;

  Chiral_Condensate(const std::shared_ptr<const Matrix_Storage_Base> &mat,
                    Physical_Parameters par)
      : storage{mat}, prefactor{0.}
  {
    using n = number_type;
    prefactor = n{2.} * par.fermion_mass *
                static_cast<n>(par.number_of_flavours) / static_cast<n>(M_size);
  }

  Chiral_Condensate(const std::shared_ptr<const Matrix_Storage_Base> &mat,
                    Physical_Parameters par, size_type reserved_size)
      : Chiral_Condensate{mat, par}
  {
    reserve(reserved_size);
  };

  virtual std::string name() const override
  {
    static const auto obs_name = std::string{"chiral condensate"};
    return obs_name;
  }

  virtual std::string dir_name() const override
  {
    static const auto obs_dir_name = std::string{"chiral_condensate"};
    return obs_dir_name;
  }

private:
  virtual value_type calculate_value() override;

  std::shared_ptr<const Matrix_Storage_Base> storage;
  value_type prefactor;
};

} // namespace observable
} // namespace osborn
} // namespace rmt

#endif /* OSBORN_CHIRAL_CONDENSATE_HPP */
