
/*
 * Created: 14-03-2017
 * Modified: Fri 01 Sep 2017 11:38:26 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "fermion_eigenvalues.hpp"
#include <utilities/matrix/eigenvalues/eigenvalue_solver.hpp>
#include <utilities/matrix/matrix_constructors.hpp>

using rmt::numeric::imag_i;

namespace rmt {
namespace osborn {
namespace observable {

Fermion_Eigenvalues::value_type Fermion_Eigenvalues::calculate_value()
{
  matrix::Eigenvalue_Solver<
      matrix::precission_type<numeric::complex_type>::value,
      matrix::Symmetries::None>
      solver;

  solver.Compute(construct_block_matrix(), matrix::eigenvalues_only);

  return value_type{solver.Eigenvalues()};
}

Fermion_Eigenvalues::block_matrix_type
Fermion_Eigenvalues::construct_block_matrix() const
{
  using base_name = ::rmt::osborn::M_name;

  auto block_matrix = matrix::Block_Matrix();

  blaze::submatrix(block_matrix, 0ul, 0ul, M_size, M_size) =
      storage->read_mass() * matrix::Identity_Matrix();

  blaze::submatrix(block_matrix, M_size, M_size, M_size, M_size) =
      storage->read_mass() * matrix::Identity_Matrix();

  blaze::submatrix(block_matrix, 0ul, M_size, M_size, M_size) =
      storage->read(base_name::X);

  blaze::submatrix(block_matrix, M_size, 0ul, M_size, M_size) =
      storage->read(base_name::Y);

  return block_matrix;
}

} // namespace observable
} // namespace osborn
} // namespace rmt
