
/*
 * Created: 02-09-2017
 * Modified: Wed 06 Sep 2017 14:58:21 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OSBORN_OBSERVABLE_BUILD_REGISTER_HPP
#define OSBORN_OBSERVABLE_BUILD_REGISTER_HPP

#include "observables_all.hpp"
#include <builders/observable_builders/builder_factory.hpp>
#include <builders/observable_builders/osborn/osborn_builders.hpp>
#include <utilities/json_parser_helpers.hpp>

namespace rmt {
namespace build {
namespace core {

inline void register_osborn_observables()
{
  namespace obs = ::rmt::osborn::observable;
  namespace cart_obs = ::rmt::osborn::cartesian::observable;

  using parse::to_string;

  auto &dict = Factory_Dictionary::instance();

  auto model_name = to_string(Model_Type::Osborn) + "_";
  auto model_cart_name = model_name + to_string(Storage_Type::Cartesian) + "_";

  dict.add({model_name + "baryon-number"},
           std::make_shared<No_Param_Obs_Build_Factory<
               osborn::Storage_Phys_Obs_Builder<obs::Baryon_Number_Density>>>());

  dict.add({model_name + "chiral-condensate"},
           std::make_shared<No_Param_Obs_Build_Factory<
               osborn::Storage_Phys_Obs_Builder<obs::Chiral_Condensate>>>());

  dict.add({model_name + "determinant"},
           std::make_shared<No_Param_Obs_Build_Factory<
               osborn::Storage_Obs_Builder<obs::Determinant>>>());

  dict.add({model_name + "eigenvalues"},
           std::make_shared<No_Param_Obs_Build_Factory<
               osborn::Storage_Obs_Builder<obs::Eigenvalues>>>());

  dict.add({model_name + "fermion-eigenvalues"},
           std::make_shared<No_Param_Obs_Build_Factory<
               osborn::Storage_Obs_Builder<obs::Fermion_Eigenvalues>>>());

  dict.add({model_cart_name + "force-norm"},
           std::make_shared<No_Param_Obs_Build_Factory<
               osborn::cartesian::Force_Obs_Builder<cart_obs::Force_Norm>>>());

  dict.add(
      {model_name + "cooling-norm"},
      std::make_shared<
          JSON_Trees_Obs_Build_Factory<osborn::Cooling_Norm_Obs_Builder>>());
}

} // namespace core
} // namespace build
} // namespace rmt

#endif /* OSBORN_OBSERVABLE_BUILD_REGISTER_HPP */
