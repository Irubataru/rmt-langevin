
/*
 * Created: 14-03-2017
 * Modified: Fri 01 Sep 2017 11:36:45 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OSBORN_FERMION_EIGENVALUES_HPP
#define OSBORN_FERMION_EIGENVALUES_HPP

#include <memory>
#include <simskel/named_observable.hpp>
#include <osborn/resource/resource.hpp>

namespace rmt {
namespace osborn {
namespace observable {

class Fermion_Eigenvalues
    : public ::rmt::observable::Named_Observable<rmt::block_vector_type>,
      public builder::builder_core::requires_resource_type
{
public:
  using value_type = rmt::block_vector_type;
  using block_matrix_type = rmt::block_matrix_type;
  using size_type =
      ::rmt::observable::Object_With_Record<value_type>::size_type;

  Fermion_Eigenvalues(const std::shared_ptr<const Matrix_Storage_Base> &mat)
      : storage{mat}
  {
  }

  Fermion_Eigenvalues(const std::shared_ptr<const Matrix_Storage_Base> &mat,
                      size_type reserved_size)
      : Fermion_Eigenvalues{mat}
  {
    reserve(reserved_size);
  };

  virtual std::string name() const override
  {
    static const auto obs_name = std::string{"fermion eigenvalues"};
    return obs_name;
  }

  virtual std::string dir_name() const override
  {
    static const auto obs_dir_name = std::string{"fermion_eigenvalues"};
    return obs_dir_name;
  }

private:
  block_matrix_type construct_block_matrix() const;

private:
  virtual value_type calculate_value() override;

  std::shared_ptr<const Matrix_Storage_Base> storage;
};

} // namespace observable
} // namespace osborn
} // namespace rmt


#endif /* OSBORN_FERMION_EIGENVALUES_HPP */
