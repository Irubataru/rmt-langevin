
/*
 * Created: 08-05-2016
 * Modified: Tue 14 Feb 2017 12:35:10 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OSBORN_LANGEVIN_STEP_HPP
#define OSBORN_LANGEVIN_STEP_HPP

#include "force.hpp"
#include <osborn/resource/resource.hpp>
#include <simskel/update/update_int.hpp>
#include <utilities/stepsize/stepsize_keeper.hpp>
#include <utilities/white_noise.hpp>

namespace rmt {
namespace osborn {

namespace cartesian {

class Langevin_Step : public ::simskel::Simple_Update,
                      public builder::builder_core::requires_resource_type,
                      public builder::builder_core::requires_force_type
{
public:
  Langevin_Step(std::shared_ptr<Matrix_Storage> storage_ptr,
                std::shared_ptr<Force> force_ptr,
                std::shared_ptr<const stepsize::Stepsize> stepsize_ptr,
                std::unique_ptr<White_Noise> noise)
      : storage{storage_ptr},
        force{force_ptr},
        stepsize{std::move(stepsize_ptr)},
        noise{std::move(noise)}
  {
  }

  virtual ~Langevin_Step()
  {
  }

private:
  void DoUpdate() override;

private:
  std::shared_ptr<Matrix_Storage> storage;
  std::shared_ptr<Force> force;

  std::shared_ptr<const stepsize::Stepsize> stepsize;
  std::unique_ptr<White_Noise> noise;
};

} // namespace cartesian

} // namespace osborn
} // namespace rmt

#endif /* OSBORN_LANGEVIN_STEP_HPP */
