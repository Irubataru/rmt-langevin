
/*
 * Created: 16-06-2016
 * Modified: Thu 15 Dec 2016 15:53:01 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "langevin_step.hpp"

namespace rmt {
namespace osborn {

namespace cartesian {

void Langevin_Step::DoUpdate()
{
  using cart_name = ::rmt::osborn::cartesian::M_name;
  using numeric::sqrt;

  if (storage->get_storage_revision() != force->get_force_revision())
    force->update();

  auto step_length = stepsize->value;
  noise->set_std_dev(sqrt(numeric::number_type{2.} * step_length));

  auto &a = storage->read_write(cart_name::A);
  auto &b = storage->read_write(cart_name::B);
  auto &al = storage->read_write(cart_name::ALPH);
  auto &be = storage->read_write(cart_name::BETA);

  a += step_length * force->get_force_a();
  a = blaze::forEach(a, [this](const rmt::numeric::complex_type &x) {
    return x + noise->noise();
  });

  b += step_length * force->get_force_b();
  b = blaze::forEach(b, [this](const rmt::numeric::complex_type &x) {
    return x + noise->noise();
  });

  al += step_length * force->get_force_alpha();
  al = blaze::forEach(al, [this](const rmt::numeric::complex_type &x) {
    return x + noise->noise();
  });

  be += step_length * force->get_force_beta();
  be = blaze::forEach(be, [this](const rmt::numeric::complex_type &x) {
    return x + noise->noise();
  });

  storage->update();
}

} // namespace cartesian

} // namespace osborn
} // namespace rmt
