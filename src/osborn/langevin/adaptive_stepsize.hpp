
/*
 * Created: 10-01-2017
 * Modified: Tue 14 Feb 2017 12:59:49 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OSBORN_ADAPTIVE_STEPSIZE_HPP
#define OSBORN_ADAPTIVE_STEPSIZE_HPP

#include "force.hpp"
#include <utilities/stepsize/adaptive_stepsize.hpp>

namespace rmt {
namespace osborn {
namespace cartesian {

class Adaptive_Stepsize : public ::rmt::stepsize::Adaptive_Stepsize,
                          public builder::builder_core::requires_force_type
{
public:
  Adaptive_Stepsize(std::shared_ptr<Force> force, value_type base_stepsize,
                    std::shared_ptr<::rmt::stepsize::Stepsize> step_ptr,
                    value_type base_norm)
      : ::rmt::stepsize::Adaptive_Stepsize{base_stepsize, step_ptr},
        force_ptr{force},
        base_norm{base_norm}
  {
  }

private:
  virtual value_type weight() override
  {
    using std::norm;
    using numeric::sqrt;
    auto norm_val = value_type{0.};

    force_ptr->conditional_update();

    const auto &a_ref = force_ptr->get_force_a();
    const auto &b_ref = force_ptr->get_force_b();
    const auto &al_ref = force_ptr->get_force_alpha();
    const auto &be_ref = force_ptr->get_force_beta();

    for (auto i = 0ul; i < a_ref.rows(); ++i)
      for (auto j = 0ul; j < a_ref.columns(); ++j)
        norm_val += norm(a_ref(i, j)) + norm(b_ref(i, j)) + norm(al_ref(i, j)) +
                    norm(be_ref(i, j));

    return base_norm / sqrt(norm_val);
  }

  std::shared_ptr<Force> force_ptr;
  value_type base_norm;
};

} // namespace cartesian
} // namespace osborn
} // namespace rmt

#endif /* OSBORN_ADAPTIVE_STEPSIZE_HPP */
