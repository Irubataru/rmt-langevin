/*
 * Created: 08-05-2016
 * Modified: Fri 01 Sep 2017 12:28:35 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OSBORN_FORCE_HPP
#define OSBORN_FORCE_HPP

#include <compile_configuration.hpp>
#include <memory>
#include <osborn/resource/resource.hpp>

namespace rmt {
namespace osborn {

namespace cartesian {

class Force : public Simulation_Force,
              public builder::builder_core::requires_resource_type
{
public:
  explicit Force(std::shared_ptr<const Matrix_Storage> m_storage);
  explicit Force(std::shared_ptr<const Matrix_Storage> m_storage,
                 Physical_Parameters pars);

  virtual const M_type &get_force_a() const
  {
    return a_force;
  }

  virtual const M_type &get_force_b() const
  {
    return b_force;
  }

  virtual const M_type &get_force_alpha() const
  {
    return alpha_force;
  }

  virtual const M_type &get_force_beta() const
  {
    return beta_force;
  }

  unsigned long get_force_revision() const
  {
    return force_revision;
  }

  virtual void update();
  virtual void conditional_update();

  virtual ~Force()
  {
  }

private:
  std::shared_ptr<const Matrix_Storage> storage;

  M_type a_force, b_force, alpha_force, beta_force;
  Physical_Parameters par;

  unsigned long force_revision;
};

} // namespace cartesian

namespace builder {
namespace builder_core {

struct requires_force_type
{
  char *requirements_requires_force;
};

} // namespace builder
} // namespace builder_core

} // namespace osborn
} // namespace rmt

#endif /* OSBORN_FORCE_HPP */
