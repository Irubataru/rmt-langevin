
/*
 * Created: 10-01-2017
 * Modified: Tue 14 Feb 2017 12:34:50 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OSBORN_FORCE_NORM_HPP
#define OSBORN_FORCE_NORM_HPP

#include "force.hpp"

namespace rmt {
namespace osborn {
namespace cartesian {

class Force_Norm : public builder::builder_core::requires_force_type
{
public:
  using value_type = numeric::number_type;

  explicit Force_Norm(std::shared_ptr<const Force> force) : force_ptr{force}
  {
  }

  Force_Norm(const Force_Norm &) = default;
  Force_Norm(Force_Norm &&) = default;

  Force_Norm &operator=(const Force_Norm &) = default;
  Force_Norm &operator=(Force_Norm &&) = default;

  value_type norm() const
  {
    using std::norm;
    using numeric::sqrt;
    auto norm_val = value_type{0.};

    const auto &a_ref = force_ptr->get_force_a();
    const auto &b_ref = force_ptr->get_force_b();
    const auto &al_ref = force_ptr->get_force_alpha();
    const auto &be_ref = force_ptr->get_force_beta();

    for (auto i = 0ul; i < a_ref.rows(); ++i)
      for (auto j = 0ul; j < a_ref.columns(); ++j)
        norm_val += norm(a_ref(i, j)) + norm(b_ref(i, j)) + norm(al_ref(i, j)) +
                    norm(be_ref(i, j));

    return norm_val;
  }

  value_type operator()() const
  {
    return norm();
  }

private:
  std::shared_ptr<const Force> force_ptr;
};

} // namespace cartesian
} // namespace osborn
} // namespace rmt

#endif /* OSBORN_FORCE_NORM_HPP */
