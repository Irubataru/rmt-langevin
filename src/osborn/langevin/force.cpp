/*
 * Created: 08-05-2016
 * Modified: Fri 16 Dec 2016 11:29:51 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "force.hpp"
#include <utilities/matrix/default_constructor.hpp>

using ::rmt::numeric::number_type;
using ::rmt::numeric::complex_type;
using ::rmt::numeric::imag_i;

using ::rmt::numeric::cosh;
using ::rmt::numeric::sinh;

namespace rmt {
namespace osborn {

namespace cartesian {

Force::Force(std::shared_ptr<const Matrix_Storage> m_storage)
    : storage{m_storage},
      a_force{matrix::Matrix()},
      b_force{matrix::Matrix()},
      alpha_force{matrix::Matrix()},
      beta_force{matrix::Matrix()},
      force_revision{0}
{
}

Force::Force(std::shared_ptr<const Matrix_Storage> m_storage,
             Physical_Parameters pars)
    : storage{m_storage},
      a_force{matrix::Matrix()},
      b_force{matrix::Matrix()},
      alpha_force{matrix::Matrix()},
      beta_force{matrix::Matrix()},
      par(pars),
      force_revision{0}
{
}

void Force::conditional_update()
{
  if (force_revision != storage->get_storage_revision())
    update();
}

void Force::update()
{
  using base_name = ::rmt::osborn::M_name;
  using cart_name = ::rmt::osborn::cartesian::M_name;

  const auto &a = storage->read(cart_name::A);
  const auto &b = storage->read(cart_name::B);
  const auto &al = storage->read(cart_name::ALPH);
  const auto &be = storage->read(cart_name::BETA);

  const auto &r = storage->read(base_name::R);
  const auto &t = storage->read(base_name::T);

  auto c_mu = cosh(par.chemical_potential);
  auto s_mu = sinh(par.chemical_potential);
  auto nf_d = static_cast<number_type>(par.number_of_flavours);

  a_force = -2. * M_size * a - imag_i * nf_d * c_mu * (r + t);
  b_force = -2. * M_size * b + nf_d * c_mu * (r - t);
  alpha_force = -2. * M_size * al + nf_d * s_mu * (r + t);
  beta_force = -2. * M_size * be - imag_i * nf_d * s_mu * (r - t);

  force_revision = storage->get_storage_revision();
}

} // namespace cartesian

} // namespace osborn
} // namespace rmt
