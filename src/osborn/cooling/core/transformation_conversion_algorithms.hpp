
/*
 * Created: 14-11-2016
 * Modified: Wed 15 Feb 2017 11:48:36 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OSBORN_TRANSFORMATION_CONVERSION_ALGORITHMS_HPP
#define OSBORN_TRANSFORMATION_CONVERSION_ALGORITHMS_HPP

#include "../cooling_types.hpp"
#include "../norms/norm_interface.hpp"
#include <fstream>
#include <memory>
#include <types.hpp>

namespace rmt {
namespace osborn {
namespace cooling {
namespace core {

struct Direct_Minimalisation_Algorithm
{
public:
  using number_type = rmt::numeric::number_type;
  using eig_vals_type = rmt::real_vector_type;

  explicit Direct_Minimalisation_Algorithm(
      const std::shared_ptr<norm::Norm> &nptr, number_type al,
      number_type al_max, std::size_t max_iterations = 10)
      : norm_ptr{nptr}, alpha{al}, max_alpha{al_max}, max_it{max_iterations}
  {
  }

  Transformation_Matrices operator()(const norm::Norm_Arguments &,
                                     const Generator_Matrices &);

private:
  std::shared_ptr<norm::Norm> norm_ptr;
  number_type alpha;
  number_type max_alpha;
  std::size_t max_it;
};

struct Fixed_Step_Transformation
{
public:
  using number_type = rmt::numeric::number_type;
  using eig_vals_type = rmt::real_vector_type;

  explicit Fixed_Step_Transformation(number_type aa) : alpha{aa}
  {
  }

  Transformation_Matrices operator()(const norm::Norm_Arguments &,
                                     const Generator_Matrices &);

private:
  number_type alpha;
};

struct Fixed_Step_Transformation_Log
{
public:
  using number_type = rmt::numeric::number_type;
  using eig_vals_type = rmt::real_vector_type;

  explicit Fixed_Step_Transformation_Log(
      const std::shared_ptr<norm::Norm> &nptr, number_type alpha,
      std::string filename)
      : norm_ptr{nptr}, trafo(alpha), ofs{filename}
  {
    if (!ofs)
      throw std::runtime_error{"Cannot open file \"" + filename +
                               "\" for writing"};
  }

  Transformation_Matrices operator()(const norm::Norm_Arguments &,
                                     const Generator_Matrices &);

private:
  std::shared_ptr<norm::Norm> norm_ptr;
  Fixed_Step_Transformation trafo;
  std::ofstream ofs;
};

} // namespace core
} // namespace cooling
} // namespace osborn
} // namespace rmt

#endif /* OSBORN_TRANSFORMATION_CONVERSION_ALGORITHMS_HPP */
