
/*
 * Created: 14-11-2016
 * Modified: Tue 14 Feb 2017 18:10:21 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "transformation_conversion_algorithms.hpp"
#include <utilities/matrix/default_constructor.hpp>
#include <utilities/matrix/eigenvalues/eigenvalue_solver.hpp>
#include <utilities/matrix/identity_constructor.hpp>
#include <utilities/minimum/minimisers.hpp>

#include <iostream>

namespace rmt {
namespace osborn {
namespace cooling {
namespace core {

using norm::Norm_Arguments;

namespace {

std::pair<M_type, M_type>
Exponensiate_Matrix(const M_type &eigen_vectors,
                    const real_vector_type &eigen_values,
                    const numeric::number_type &exp_parameter)
{
  using rmt::numeric::exp;

  auto diagonal = matrix::Zero_Matrix();
  auto diagonal_i = matrix::Zero_Matrix();

  for (auto i = 0u; i < M_size; ++i) {
    diagonal(i, i) = exp(-exp_parameter * eigen_values[i]);
    diagonal_i(i, i) = exp(exp_parameter * eigen_values[i]);
  }

  return {static_cast<M_type>(eigen_vectors * diagonal *
                              blaze::ctrans(eigen_vectors)),
          static_cast<M_type>(eigen_vectors * diagonal_i *
                              blaze::ctrans(eigen_vectors))};
}
}

Transformation_Matrices Direct_Minimalisation_Algorithm::
operator()(const Norm_Arguments &args, const Generator_Matrices &gen_mat)
{
  matrix::Eigenvalue_Solver<
      matrix::precission_type<numeric::complex_type>::value,
      matrix::Symmetries::Hermitian>
      solver;

  solver.Compute(gen_mat.g_gen, matrix::eigenvalues_and_eigenvectors);

  auto g_eigen_values = solver.Eigenvalues();
  auto g_eigen_vectors = solver.Eigenvectors();

  solver.Compute(gen_mat.h_gen, matrix::eigenvalues_and_eigenvectors);

  auto h_eigen_values = solver.Eigenvalues();
  auto h_eigen_vectors = solver.Eigenvectors();

  auto min_func = [&g_eigen_values, &g_eigen_vectors, &h_eigen_values,
                   &h_eigen_vectors, &args, this](number_type alpha) {
    auto g_pair = Exponensiate_Matrix(g_eigen_vectors, g_eigen_values, alpha);
    auto h_pair = Exponensiate_Matrix(h_eigen_vectors, h_eigen_values, alpha);

    auto X_temp = M_type{g_pair.first * args.X * h_pair.second};
    auto Y_temp = M_type{h_pair.first * args.Y * g_pair.second};

    M_type phi_temp, psi_temp, phi_adj_temp, psi_adj_temp;

    if (norm_ptr->needs_phi_psi()) {
      phi_temp = g_pair.first * args.phi * h_pair.second;
      psi_temp = g_pair.first * args.psi * h_pair.second;

      phi_adj_temp = h_pair.first * args.phi_adj * g_pair.second;
      psi_adj_temp = h_pair.first * args.psi_adj * g_pair.second;
    }

    auto norm_args =
        Norm_Arguments{X_temp,   Y_temp,       phi_temp, phi_adj_temp,
                       psi_temp, psi_adj_temp, args.mu,  args.mass};

    return norm_ptr->compute(norm_args);
  };

  minimise::Brent_Minimisation min_alg{{}};

  auto result = min_alg.minimise({0., alpha, max_it, max_alpha}, {1e-3, max_it},
                                 min_func);

  auto g_pair =
      Exponensiate_Matrix(g_eigen_vectors, g_eigen_values, result.min);
  auto h_pair =
      Exponensiate_Matrix(h_eigen_vectors, h_eigen_values, result.min);

  return {std::move(g_pair.first), std::move(g_pair.second),
          std::move(h_pair.first), std::move(h_pair.second)};
}

Transformation_Matrices Fixed_Step_Transformation::
operator()(const Norm_Arguments &, const Generator_Matrices &gen_mat)
{
  matrix::Eigenvalue_Solver<
      matrix::precission_type<numeric::complex_type>::value,
      matrix::Symmetries::Hermitian>
      solver;

  solver.Compute(gen_mat.g_gen, matrix::eigenvalues_and_eigenvectors);

  auto g_eigen_values = solver.Eigenvalues();
  auto g_eigen_vectors = solver.Eigenvectors();

  solver.Compute(gen_mat.h_gen, matrix::eigenvalues_and_eigenvectors);

  auto h_eigen_values = solver.Eigenvalues();
  auto h_eigen_vectors = solver.Eigenvectors();

  auto g_pair = Exponensiate_Matrix(g_eigen_vectors, g_eigen_values, alpha);
  auto h_pair = Exponensiate_Matrix(h_eigen_vectors, h_eigen_values, alpha);

  return {std::move(g_pair.first), std::move(g_pair.second),
          std::move(h_pair.first), std::move(h_pair.second)};
}

} // namespace core
} // namespace cooling
} // namespace osborn
} // namespace rmt
