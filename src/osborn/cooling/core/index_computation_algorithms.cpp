
/*
 * Created: 14-11-2016
 * Modified: Tue 14 Feb 2017 17:58:10 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "index_computation_algorithms.hpp"
#include <blaze/math/CompressedMatrix.h>
#include <utilities/matrix/default_constructor.hpp>

#include <algorithm>
#include <iostream>

namespace rmt {
namespace osborn {
namespace cooling {
namespace core {

using norm::Norm_Arguments;

namespace {

void Normalise(std::vector<numeric::number_type> &v)
{
  using numeric::number_type;
  using numeric::abs;

  auto norm =
      sqrt(std::accumulate(v.begin(), v.end(), number_type{0.},
                           [](const number_type &val, const number_type &c) {
                             return val + c * c;
                           }));
  if (norm > numeric::number_type{1.})
    std::transform(v.begin(), v.end(), v.begin(),
                   [norm](const number_type &d) { return d / norm; });
}

void TransformLeft(
    const M_type &input_mat, M_type &output_mat,
    const blaze::CompressedMatrix<numeric::complex_type> &generator)
{
  output_mat = input_mat + generator * input_mat;
}

void TransformRight(
    const M_type &input_mat, M_type &output_mat,
    const blaze::CompressedMatrix<numeric::complex_type> &generator)
{
  output_mat = input_mat - input_mat * generator;
}

struct Matrix_Reference_Container
{
  M_type &X;
  M_type &Y;

  M_type &phi;
  M_type &phi_adj;
  M_type &psi;
  M_type &psi_adj;
};

void TransformG(const Norm_Arguments &input_args,
                Matrix_Reference_Container &temp_args,
                const blaze::CompressedMatrix<numeric::complex_type> &generator,
                bool compute_phi_psi)
{
  TransformLeft(input_args.X, temp_args.X, generator);
  TransformRight(input_args.Y, temp_args.Y, generator);

  if (compute_phi_psi) {
    TransformRight(input_args.phi_adj, temp_args.phi_adj, generator);
    TransformRight(input_args.psi_adj, temp_args.psi_adj, generator);

    TransformLeft(input_args.phi, temp_args.phi, generator);
    TransformLeft(input_args.psi, temp_args.psi, generator);
  }
}

void TransformH(const Norm_Arguments &input_args,
                Matrix_Reference_Container &temp_args,
                const blaze::CompressedMatrix<numeric::complex_type> &generator,
                bool compute_phi_psi)
{
  TransformRight(input_args.X, temp_args.X, generator);
  TransformLeft(input_args.Y, temp_args.Y, generator);

  if (compute_phi_psi) {
    TransformRight(input_args.phi, temp_args.phi, generator);
    TransformRight(input_args.psi, temp_args.psi, generator);

    TransformLeft(input_args.phi_adj, temp_args.phi_adj, generator);
    TransformLeft(input_args.psi_adj, temp_args.psi_adj, generator);
  }
}

} // namespace

Transformation_Indices Numerical_Derivative_Index_Computation_Algorithm::
operator()(const Norm_Arguments &args)
{
  return compute_indices(args, epsilon);
};

Transformation_Indices
Numerical_Derivative_Index_Computation_Algorithm::compute_indices(
    const Norm_Arguments &args,
    const Numerical_Derivative_Index_Computation_Algorithm::number_type &eps)
{
  using rmt::numeric::sqrt;
  using n = number_type;
  using rmt::numeric::imag_i;

  const auto needs_phi_psi = norm->needs_phi_psi();

  auto indices = Transformation_Indices();
  indices.g_indices.reserve(M_size * M_size);
  indices.h_indices.reserve(M_size * M_size);

  auto norm_temporary = number_type{};

  try {
    norm_temporary = norm->compute(args);
  } catch (std::runtime_error &run_err) {
    std::ostringstream err_msg;
    err_msg << "In Numerical_Derivative_Index_Computation_Algorithm: Error "
               "constructing norm0, source:\n";
    err_msg << run_err.what();

    throw std::runtime_error{err_msg.str()};
  }

  const auto norm0 = norm_temporary;

  M_type X_temp = matrix::Matrix(), Y_temp = matrix::Matrix();
  M_type phi_temp, phi_adj_temp, psi_temp, psi_adj_temp;

  if (needs_phi_psi) {
    phi_temp = matrix::Matrix();
    phi_adj_temp = matrix::Matrix();
    psi_temp = matrix::Matrix();
    psi_adj_temp = matrix::Matrix();
  }

  auto temp_args =
      Norm_Arguments{X_temp,   Y_temp,       phi_temp, phi_adj_temp,
                     psi_temp, psi_adj_temp, args.mu,  args.mass};

  // TODO: find a more elegant solution to this
  auto nonconst_temp_args = Matrix_Reference_Container{
      X_temp, Y_temp, phi_temp, phi_adj_temp, psi_temp, psi_adj_temp};

  // U(1) matrix

  {
    blaze::CompressedMatrix<numeric::complex_type> generator(M_size, M_size);
    generator.reserve(1);

    generator.append(0, 0, sqrt(2.) * eps);
    generator.finalize(0);

    TransformG(args, nonconst_temp_args, generator, needs_phi_psi);
    indices.g_indices.push_back(numerical_derivative(temp_args, norm0, eps));

    TransformH(args, nonconst_temp_args, generator, needs_phi_psi);
    indices.h_indices.push_back(numerical_derivative(temp_args, norm0, eps));
  }

  // SU(N) matrices

  // - Diagonal matrices

  for (auto i = 1u; i < M_size; ++i) {
    blaze::CompressedMatrix<numeric::complex_type> generator(M_size, M_size);
    generator.reserve(i + 1);

    auto d = static_cast<number_type>(i + 1u);
    auto f = sqrt(n{2.} / (d * (d - n{1.})));

    for (auto j = 0u; j < i; ++j) {
      generator.append(j, j, f * eps);
      generator.finalize(j);
    }

    generator.append(i, i, f * (n{1.} - d) * eps);
    generator.finalize(i);

    TransformG(args, nonconst_temp_args, generator, needs_phi_psi);
    indices.g_indices.push_back(numerical_derivative(temp_args, norm0, eps));

    TransformH(args, nonconst_temp_args, generator, needs_phi_psi);
    indices.h_indices.push_back(numerical_derivative(temp_args, norm0, eps));
  }

  // - Off-diagonal matrices

  for (auto i = 0u; i < M_size; ++i) {
    for (auto j = i + 1; j < M_size; ++j) {
      blaze::CompressedMatrix<numeric::complex_type> generator(M_size, M_size);
      generator.reserve(2);

      generator.insert(i, j, eps);
      generator.finalize(i);
      generator.insert(j, i, eps);
      generator.finalize(j);

      TransformG(args, nonconst_temp_args, generator, needs_phi_psi);
      indices.g_indices.push_back(numerical_derivative(temp_args, norm0, eps));

      TransformH(args, nonconst_temp_args, generator, needs_phi_psi);
      indices.h_indices.push_back(numerical_derivative(temp_args, norm0, eps));

      generator(i, j) = -eps * imag_i;
      generator(j, i) = +eps * imag_i;

      TransformG(args, nonconst_temp_args, generator, needs_phi_psi);
      indices.g_indices.push_back(numerical_derivative(temp_args, norm0, eps));

      TransformH(args, nonconst_temp_args, generator, needs_phi_psi);
      indices.h_indices.push_back(numerical_derivative(temp_args, norm0, eps));
    }
  }

  return indices;
}

Transformation_Indices
Normalised_Numerical_Derivative_Index_Computation_Algorithm::
operator()(const Norm_Arguments &args)
{
  using numeric::abs;
  using numeric::sqrt;
  auto indices = parent_type::operator()(args);

  Normalise(indices.g_indices);
  Normalise(indices.h_indices);

  return indices;
}

} // namespace core
} // namespace cooling
} // namespace osborn
} // namespace rmt
