/*
 * Created: 14-11-2016
 * Modified: Wed 08 Feb 2017 15:33:47 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "index_conversion_algorithm.hpp"
#include <utilities/matrix/default_constructor.hpp>

namespace rmt {
namespace osborn {
namespace cooling {
namespace core {

Generator_Matrices Direct_Index_Conversion_Algorithm::
operator()(const Transformation_Indices &idx)
{
  using n = number_type;
  using rmt::numeric::sqrt;
  using rmt::numeric::imag_i;

  if (idx.g_indices.size() != (M_size * M_size) or
      idx.h_indices.size() != (M_size * M_size))
    throw std::runtime_error{"In Advanced_Index_Conversion_Algorithm: Wrong "
                             "number of generator indices"};

  auto result =
      Generator_Matrices{matrix::Zero_Matrix(), matrix::Zero_Matrix()};

  auto g_idx_it = idx.g_indices.begin();
  auto h_idx_it = idx.h_indices.begin();

  // U(1) matrices

  auto gv = sqrt(2.) * (*g_idx_it);
  auto hv = sqrt(2.) * (*h_idx_it);

  result.g_gen(0, 0) += gv;
  result.h_gen(0, 0) += hv;

  ++g_idx_it;
  ++h_idx_it;

  // SU(N) matrices

  // Compute the diagonal elements
  for (auto i = 1u; i < M_size; ++i) {
    auto d = static_cast<number_type>(i + 1);
    auto f = sqrt(n{2.} / (d * (d - n{1.})));

    auto gv = *g_idx_it;
    auto hv = *h_idx_it;

    for (auto j = 0u; j < i; ++j) {
      result.g_gen(j, j) += f * gv;
      result.h_gen(j, j) += f * hv;
    }

    result.g_gen(i, i) += f * (n{1.} - d) * gv;
    result.h_gen(i, i) += f * (n{1.} - d) * hv;

    ++g_idx_it;
    ++h_idx_it;
  }

  // Compute off-diagonal elements
  for (auto i = 0u; i < M_size; ++i) {
    for (auto j = i + 1; j < M_size; ++j) {
      result.g_gen(i, j) = *g_idx_it;
      result.g_gen(j, i) = *g_idx_it;
      ++g_idx_it;

      result.h_gen(i, j) = *h_idx_it;
      result.h_gen(j, i) = *h_idx_it;
      ++h_idx_it;

      result.g_gen(i, j) += -(*g_idx_it) * imag_i;
      result.g_gen(j, i) += +(*g_idx_it) * imag_i;
      ++g_idx_it;

      result.h_gen(i, j) += -(*h_idx_it) * imag_i;
      result.h_gen(j, i) += +(*h_idx_it) * imag_i;
      ++h_idx_it;
    }
  }

  return result;
}

} // namespace core
} // namespace cooling
} // namespace osborn
} // namespace rmt
