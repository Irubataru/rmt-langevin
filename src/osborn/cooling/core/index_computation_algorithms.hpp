/*
 * Created: 14-11-2016
 * Modified: Wed 15 Feb 2017 11:46:28 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OSBORN_INDEX_COMPUTATION_ALGORITHMS_HPP
#define OSBORN_INDEX_COMPUTATION_ALGORITHMS_HPP

#include "../cooling_types.hpp"
#include "../norms/norm_interface.hpp"
#include <memory>
#include <types.hpp>

namespace rmt {
namespace osborn {
namespace cooling {
namespace core {

struct Numerical_Derivative_Index_Computation_Algorithm
{
  using number_type = rmt::numeric::number_type;
  using arg_type = norm::Norm_Arguments;

  explicit Numerical_Derivative_Index_Computation_Algorithm(
      const std::shared_ptr<norm::Norm> &the_norm, const number_type &eps)
      : norm{the_norm}, epsilon{eps}
  {
  }

  Transformation_Indices operator()(const arg_type &args);

private: /* functions */
  number_type numerical_derivative(const arg_type &args,
                                   const number_type &norm0,
                                   const number_type &eps)
  {
    return (norm->compute(args) - norm0) / eps;
  }

  Transformation_Indices compute_indices(const arg_type &args,
                                         const number_type &eps);

private:
  std::shared_ptr<norm::Norm> norm;
  number_type epsilon;
};

struct Normalised_Numerical_Derivative_Index_Computation_Algorithm
    : public Numerical_Derivative_Index_Computation_Algorithm
{
  using parent_type = Numerical_Derivative_Index_Computation_Algorithm;
  using parent_type::parent_type;

  using number_type = parent_type::arg_type;
  using arg_type = parent_type::arg_type;

  Transformation_Indices operator()(const arg_type &);
};

} // namespace core
} // namespace cooling
} // namespace osborn
} // namespace rmt

#endif /* OSBORN_INDEX_COMPUTATION_ALGORITHMS_HPP */
