/*
 * Created: 14-11-2016
 * Modified: Mon 14 Nov 2016 12:37:14 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OSBORN_INDEX_CONVERSION_ALGORITHM_HPP
#define OSBORN_INDEX_CONVERSION_ALGORITHM_HPP

#include "../cooling_types.hpp"
#include <types.hpp>

namespace rmt {
namespace osborn {
namespace cooling {
namespace core {

struct Direct_Index_Conversion_Algorithm
{
  using number_type = rmt::numeric::number_type;

  Generator_Matrices operator()(const Transformation_Indices &idx);
};

} // namespace core
} // namespace cooling
} // namespace osborn
} // namespace rmt

#endif /* OSBORN_INDEX_CONVERSION_ALGORITHM_HPP */
