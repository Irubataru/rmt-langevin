/*
 * OSBORN_Created: 05-06-2016
 * Modified: Tue 14 Feb 2017 16:58:14 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OSBORN_COOLING_TYPES_HPP
#define OSBORN_COOLING_TYPES_HPP

#include <types.hpp>
#include <vector>

namespace rmt {
namespace osborn {
namespace cooling {

namespace norm {

struct Norm_Arguments
{
  const M_type &X;
  const M_type &Y;

  const M_type &phi;
  const M_type &phi_adj;
  const M_type &psi;
  const M_type &psi_adj;

  const rmt::numeric::complex_type &mu;
  const rmt::numeric::complex_type &mass;
};

} // namespace norm

namespace core {

struct Transformation_Indices
{
  std::vector<numeric::number_type> g_indices;
  std::vector<numeric::number_type> h_indices;
};

struct Generator_Matrices
{
  M_type g_gen, h_gen;
};

struct Transformation_Matrices
{
  M_type g, g_inv;
  M_type h, h_inv;
};

} // namespace core

} // namespace cooling
} // namespace osborn
} // namespace rmt

#endif /* OSBORN_COOLING_TYPES_HPP */
