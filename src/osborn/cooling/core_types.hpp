/*
 * OSBORN_COOLING_Created: 05-06-2016
 * Modified: Thu 16 Feb 2017 17:30:31 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OSBORN_COOLING_CORE_TYPES_HPP
#define OSBORN_COOLING_CORE_TYPES_HPP

#include "core/index_computation_algorithms.hpp"
#include "core/index_conversion_algorithm.hpp"
#include "core/transformation_conversion_algorithms.hpp"
#include "general_cooling.hpp"
#include "norms/norm_interface.hpp"

namespace rmt {
namespace osborn {
namespace cooling {
namespace core {

using Simple_Core =
    Cooling_Core<Numerical_Derivative_Index_Computation_Algorithm,
                 Direct_Index_Conversion_Algorithm,
                 Direct_Minimalisation_Algorithm>;

using Fixed_Core =
    Cooling_Core<Numerical_Derivative_Index_Computation_Algorithm,
                 Direct_Index_Conversion_Algorithm, Fixed_Step_Transformation>;

using Fixed_Log_Core =
    Cooling_Core<Normalised_Numerical_Derivative_Index_Computation_Algorithm,
                 Direct_Index_Conversion_Algorithm,
                 Fixed_Step_Transformation_Log>;

} // namespace core
} // namespace cooling

namespace cartesian {
namespace cooling {

struct Simple_Core_Parameters
{
  rmt::numeric::number_type derivative_epsilon;
  rmt::numeric::number_type alpha_step;
  rmt::numeric::number_type max_alpha;
  std::size_t max_alpha_step_iterations;
};

class Simple_Cooling_Step : public ::rmt::osborn::cooling::core::cartesian::
                                General_Cartesian_Cooling_Step<
                                    ::rmt::osborn::cooling::core::Simple_Core>
{
public:
  using core_type = ::rmt::osborn::cooling::core::Simple_Core;
  using parent_type =
      ::rmt::osborn::cooling::core::cartesian::General_Cartesian_Cooling_Step<
          core_type>;
  using storage_type = ::rmt::osborn::cartesian::Matrix_Storage;

  using index_alg_type = ::rmt::osborn::cooling::core::
      Normalised_Numerical_Derivative_Index_Computation_Algorithm;
  using index_conv_alg_type =
      ::rmt::osborn::cooling::core::Direct_Index_Conversion_Algorithm;
  using min_alg_type =
      ::rmt::osborn::cooling::core::Direct_Minimalisation_Algorithm;

  explicit Simple_Cooling_Step(std::shared_ptr<storage_type> &s, core_type core)
      : parent_type{s, std::move(core)}
  {
  }

  Simple_Cooling_Step(const Simple_Cooling_Step &) = default;
  Simple_Cooling_Step(Simple_Cooling_Step &&) = default;
};

template <typename Norm_Type, typename... Args>
inline std::shared_ptr<Simple_Cooling_Step>
Make_Simple_Cooling_Step(std::shared_ptr<Matrix_Storage> &s,
                         const Simple_Core_Parameters &cp, Args &&... norm_args)
{
  using core_type = Simple_Cooling_Step::core_type;
  using index_alg_type = Simple_Cooling_Step::index_alg_type;
  using index_conv_alg_type = Simple_Cooling_Step::index_conv_alg_type;
  using min_alg_type = Simple_Cooling_Step::min_alg_type;

  auto norm_ptr = std::make_shared<Norm_Type>(std::forward<Args>(norm_args)...);

  return std::make_shared<Simple_Cooling_Step>(
      s, core_type{index_alg_type{norm_ptr, cp.derivative_epsilon},
                   index_conv_alg_type{},
                   min_alg_type{norm_ptr, cp.alpha_step, cp.max_alpha,
                                cp.max_alpha_step_iterations}});
}

struct Fixed_Core_Parameters
{
  rmt::numeric::number_type derivative_epsilon;
  rmt::numeric::number_type alpha;
};

class Fixed_Cooling_Step : public ::rmt::osborn::cooling::core::cartesian::
                               General_Cartesian_Cooling_Step<
                                   ::rmt::osborn::cooling::core::Fixed_Core>
{
public:
  using core_type = ::rmt::osborn::cooling::core::Fixed_Core;
  using parent_type =
      ::rmt::osborn::cooling::core::cartesian::General_Cartesian_Cooling_Step<
          core_type>;
  using storage_type = ::rmt::osborn::cartesian::Matrix_Storage;

  using index_alg_type = ::rmt::osborn::cooling::core::
      Numerical_Derivative_Index_Computation_Algorithm;
  using index_conv_alg_type =
      ::rmt::osborn::cooling::core::Direct_Index_Conversion_Algorithm;
  using min_alg_type = ::rmt::osborn::cooling::core::Fixed_Step_Transformation;

  explicit Fixed_Cooling_Step(std::shared_ptr<storage_type> &s, core_type core)
      : parent_type{s, std::move(core)}
  {
  }

  Fixed_Cooling_Step(const Fixed_Cooling_Step &) = default;
  Fixed_Cooling_Step(Fixed_Cooling_Step &&) = default;
};

template <typename Norm_Type, typename... Args>
inline std::shared_ptr<Fixed_Cooling_Step>
Make_Fixed_Cooling_Step(std::shared_ptr<Matrix_Storage> &s,
                        const Fixed_Core_Parameters &cp, Args &&... norm_args)
{
  using core_type = Fixed_Cooling_Step::core_type;
  using index_alg_type = Fixed_Cooling_Step::index_alg_type;
  using index_conv_alg_type = Fixed_Cooling_Step::index_conv_alg_type;
  using min_alg_type = Fixed_Cooling_Step::min_alg_type;

  auto norm_ptr = std::make_shared<Norm_Type>(std::forward<Args>(norm_args)...);

  return std::make_shared<Fixed_Cooling_Step>(
      s, core_type{index_alg_type{norm_ptr, cp.derivative_epsilon},
                   index_conv_alg_type{}, min_alg_type{cp.alpha}});
}

} // namespace cooling
} // namespace cartesian

} // namespace osborn
} // namespace rmt

#endif /* OSBORN_COOLING_CORE_TYPES_HPP */
