/*
 * Created: 05-06-2016
 * Modified: Fri 20 Jan 2017 17:08:27 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OSBORN_COOLING_NORMS_HPP
#define OSBORN_COOLING_NORMS_HPP

#include "norms/aggregate_norm.hpp"
#include "norms/anti_hermiticity_norm.hpp"
#include "norms/eigenvalue_norm.hpp"
#include "norms/hermiticity_norm.hpp"

#endif /* OSBORN_COOLING_NORMS_HPP */
