
/*
 * Created: 05-06-2016
 * Modified: Wed 15 Feb 2017 11:34:51 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OSBORN_GENERAL_COOLING_HPP
#define OSBORN_GENERAL_COOLING_HPP

#include "cooling_types.hpp"
#include <osborn/resource/resource.hpp>
#include <simskel/update/update_int.hpp>

namespace {

rmt::numeric::number_type Matrix_Norm(const rmt::M_type &mat)
{
  using std::norm;
  using rmt::numeric::sqrt;
  using rmt::numeric::complex_type;

  auto norm_val = rmt::numeric::number_type{0.};

  for (auto i = 0ul; i < mat.rows(); ++i)
    for (auto j = 0ul; j < mat.columns(); ++j)
      if (i == j)
        norm_val += norm(mat(i, j) - complex_type{1.});
      else
        norm_val += norm(mat(i, j));

  return sqrt(norm_val);
}
}

namespace rmt {
namespace osborn {
namespace cooling {

namespace core {

template <typename Index_Algorithm, typename Convertion_Algorithm,
          typename Minimise_Algorithm>
class Cooling_Core
{
public:
  using number_type = rmt::numeric::number_type;

  Cooling_Core(const Index_Algorithm &index_algorithm,
               const Convertion_Algorithm &conversion_algorithm,
               const Minimise_Algorithm &minimise_algorithm)
      : idx_alg(index_algorithm),
        conversion_alg(conversion_algorithm),
        min_alg(minimise_algorithm)
  {
  }

private: /* functions */
  norm::Norm_Arguments
  construct_norm_args(const std::shared_ptr<Matrix_Storage_Base> &storage)
  {
    using base_name = ::rmt::osborn::M_name;

    return {storage->read(base_name::X),
            storage->read(base_name::Y),
            storage->read(base_name::PHI),
            storage->read(base_name::PHIADJ),
            storage->read(base_name::PSI),
            storage->read(base_name::PSIADJ),
            storage->read_mu(),
            storage->read_mass()};
  }

  Transformation_Indices compute_indices(const norm::Norm_Arguments &norm_args)
  {
    return idx_alg(norm_args);
  }

  Generator_Matrices compute_matrix(const Transformation_Indices &idx)
  {
    return conversion_alg(idx);
  }

  Transformation_Matrices minimise_norm(const norm::Norm_Arguments &norm_args,
                                        const Generator_Matrices &gen_mat)
  {
    return min_alg(norm_args, gen_mat);
  }

public: /* functions */
  Transformation_Matrices
  get_transformation(const std::shared_ptr<Matrix_Storage_Base> &storage)
  {
    auto norm_args = construct_norm_args(storage);

    auto idx = compute_indices(norm_args);
    // std::cout << idx.g_indices.front() << " " << idx.h_indices.front() <<
    // std::endl;

    auto g_matrix = compute_matrix(idx);
    return minimise_norm(norm_args, g_matrix);
  }

private: /* variables */
  Index_Algorithm idx_alg;
  Convertion_Algorithm conversion_alg;
  Minimise_Algorithm min_alg;
};

namespace cartesian {

template <typename Core>
class General_Cartesian_Cooling_Step
    : public ::simskel::Update_Chain,
      public builder::builder_core::requires_resource_type
{
private:
  using storage_type = ::rmt::osborn::cartesian::Matrix_Storage;
  using number_type = rmt::numeric::number_type;

public:
  explicit General_Cartesian_Cooling_Step(
      const std::shared_ptr<storage_type> &s, const Core &c)
      : storage{s}, core{c}
  {
  }

  virtual ~General_Cartesian_Cooling_Step()
  {
  }

private: /* functions */
  virtual void SelfUpdate() override
  {
    using cart_name = ::rmt::osborn::cartesian::M_name;
    using n = number_type;
    using rmt::numeric::imag_i;

    auto transforms = core.get_transformation(storage);

    const auto &g = transforms.g;
    const auto &gi = transforms.g_inv;
    const auto &h = transforms.h;
    const auto &hi = transforms.h_inv;

    auto &a = storage->read_write(cart_name::A);
    auto &b = storage->read_write(cart_name::B);
    auto &al = storage->read_write(cart_name::ALPH);
    auto &bt = storage->read_write(cart_name::BETA);

    const auto a_copy = static_cast<M_type>(a);
    const auto b_copy = static_cast<M_type>(b);

    a = n{0.5} *
        (g * a_copy * hi + blaze::trans(gi) * a_copy * blaze::trans(h) +
         imag_i *
             (g * b_copy * hi - blaze::trans(gi) * b_copy * blaze::trans(h)));
    b = n{0.5} *
        (g * b_copy * hi + blaze::trans(gi) * b_copy * blaze::trans(h) -
         imag_i *
             (g * a_copy * hi - blaze::trans(gi) * a_copy * blaze::trans(h)));

    const auto al_copy = static_cast<M_type>(al);
    const auto bt_copy = static_cast<M_type>(bt);

    al = n{0.5} *
         (g * al_copy * hi + blaze::trans(gi) * al_copy * blaze::trans(h) +
          imag_i * (g * bt_copy * hi -
                    blaze::trans(gi) * bt_copy * blaze::trans(h)));
    bt = n{0.5} *
         (g * bt_copy * hi + blaze::trans(gi) * bt_copy * blaze::trans(h) -
          imag_i * (g * al_copy * hi -
                    blaze::trans(gi) * al_copy * blaze::trans(h)));

    storage->update();
  }

private: /* variables */
  std::shared_ptr<storage_type> storage;
  Core core;
};

} // namespace cartesian

} // namespace core

} // namespace cooling
} // namespace osborn
} // namespace rmt

#endif /* OSBORN_GENERAL_COOLING_HPP */
