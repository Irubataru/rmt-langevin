
/*
 * Created: 20-01-2017
 * Modified: Wed 06 Sep 2017 15:13:48 BST
 * OSBORN_Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OSBORN_AGGREGATE_NORM_HPP
#define OSBORN_AGGREGATE_NORM_HPP

#include "norm_interface.hpp"

namespace rmt {
namespace osborn {
namespace cooling {
namespace norm {

class Aggregate_Norm : public Norm
{
public:
  using number_type = Norm::number_type;
  using arg_type = Norm::arg_type;

  using norm_type = std::shared_ptr<Norm>;
  using weight_norm_pair = std::pair<number_type, norm_type>;

  Aggregate_Norm() = default;

  template <typename... Norms>
  explicit Aggregate_Norm(Norms... norms)
  {
    recursive_constructor(std::forward<Norms>(norms)...);
  }

  Aggregate_Norm(const Aggregate_Norm &) = default;
  Aggregate_Norm(Aggregate_Norm &&) = default;

  Aggregate_Norm &operator=(const Aggregate_Norm &) = default;
  Aggregate_Norm &operator=(Aggregate_Norm &&) = default;

  virtual number_type compute(const arg_type &args) override
  {
    if (!check_ok) {
      check_weight_total();
    }

    return std::accumulate(norms.begin(), norms.end(), number_type{0.},
                           [&args](number_type val, weight_norm_pair &wn) {
                             return val + wn.first * wn.second->compute(args);
                           });
  }

  virtual bool needs_phi_psi() const override
  {
    return std::accumulate(norms.cbegin(), norms.cend(), false,
                           [](bool statement, const weight_norm_pair &wn) {
                             return statement or wn.second->needs_phi_psi();
                           });
  }

  void push_back(number_type w, const norm_type nptr)
  {
    norms.push_back({w, nptr});
  }

  template <typename Norm_Type, typename... Args>
  void emplace_back(number_type w, Args... norm_args)
  {
    norms.push_back(
        {w, std::make_shared<Norm_Type>(std::forward<Args>(norm_args)...)});
  }

private:
  void recursive_constructor(const weight_norm_pair &norm_weight)
  {
    norms.push_back(weight_norm_pair{norm_weight.first, norm_weight.second});
  }

  template <typename... Rest_Norms>
  void recursive_constructor(const weight_norm_pair &norm_weight,
                             Rest_Norms... rest)
  {
    recursive_constructor(norm_weight);
    recursive_constructor(std::forward<Rest_Norms>(rest)...);
  }

  void check_weight_total()
  {
    auto sum = std::accumulate(norms.begin(), norms.end(), number_type{0.},
                               [](number_type val, const weight_norm_pair &wn) {
                                 return val + wn.first;
                               });

    if (sum != number_type{1.}) {
      std::ostringstream oss;
      oss << "Aggregate_Norm: The total norm is \"" << sum
          << "\", which is not one\n";
      oss << "norms.size() = " << norms.size();
      throw std::runtime_error{oss.str()};
    }

    check_ok = true;
  }

private:
  std::vector<weight_norm_pair> norms;
  bool check_ok = false;
};

} // namespace norm
} // namespace cooling
} // namespace osborn
} // namespace rmt

#endif /* OSBORN_AGGREGATE_NORM_HPP */
