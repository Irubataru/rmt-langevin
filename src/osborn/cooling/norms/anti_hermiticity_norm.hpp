/*
 * Created: 05-06-2016
 * Modified: Tue 14 Feb 2017 16:54:16 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OSBORN_ANTI_HERMITICITY_NORM_HPP
#define OSBORN_ANTI_HERMITICITY_NORM_HPP

#include "norm_interface.hpp"
#include <utilities/matrix/trace.hpp>

namespace rmt {
namespace osborn {
namespace cooling {
namespace norm {

class Anti_Hermiticity_Norm : public Norm
{
public:
  using number_type = Norm::number_type;
  using arg_type = Norm::arg_type;

  virtual number_type compute(const arg_type &args) override
  {
    auto XYd = static_cast<M_type>(args.X + blaze::ctrans(args.Y));
    return matrix::Trace(blaze::ctrans(XYd) * XYd).real() / number_type{M_size};
  }

  virtual bool needs_phi_psi() const override
  {
    return false;
  }
};

} // namespace norm
} // namespace cooling
} // namespace osborn
} // namespace rmt

#endif /* OSBORN_ANTI_HERMITICITY_NORM_HPP */
