
/*
 * Created: 20-01-2017
 * Modified: Tue 14 Feb 2017 16:55:16 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OSBORN_HERMITICITY_NORM_HPP
#define OSBORN_HERMITICITY_NORM_HPP

#include "norm_interface.hpp"
#include <utilities/matrix/trace.hpp>

namespace rmt {
namespace osborn {
namespace cooling {
namespace norm {

class Hermiticity_Norm : public Norm
{
public:
  using number_type = Norm::number_type;
  using arg_type = Norm::arg_type;

  virtual number_type compute(const arg_type &args) override
  {
    auto phi_herm =
        matrix::Trace(blaze::ctrans(args.phi_adj - blaze::ctrans(args.phi)) *
                      (args.phi_adj - blaze::ctrans(args.phi)))
            .real();

    auto psi_herm =
        matrix::Trace(blaze::ctrans(args.psi_adj - blaze::ctrans(args.psi)) *
                      (args.psi_adj - blaze::ctrans(args.psi)))
            .real();

    return (phi_herm + psi_herm) / number_type{M_size};
  }

  virtual bool needs_phi_psi() const override
  {
    return false;
  }
};

} // namespace norm
} // namespace cooling
} // namespace osborn
} // namespace rmt

#endif /* OSBORN_HERMITICITY_NORM_HPP */
