
/*
 * Created: 07-06-2016
 * Modified: Tue 14 Feb 2017 16:45:23 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OSBORN_COOLING_EIGENVALUE_NORM_HPP
#define OSBORN_COOLING_EIGENVALUE_NORM_HPP

#include "norm_interface.hpp"
#include <functional>

namespace rmt {
namespace osborn {
namespace cooling {
namespace norm {

class Eigenvalue_Norm : public Norm
{
public:
  using number_type = Norm::number_type;
  using arg_type = Norm::arg_type;
  using block_matrix_type = rmt::block_matrix_type;

  struct param_type
  {
    number_type eigenvalue_weight;
    std::size_t number_of_eigenvalues;
  };

  virtual number_type compute(const arg_type &args) override;

  virtual bool needs_phi_psi() const override
  {
    return false;
  }

  template <typename Function>
  Eigenvalue_Norm(param_type params, Function &&eig_func)
      : xi{params.eigenvalue_weight},
        number_of_eigenvalues{params.number_of_eigenvalues},
        compute_eigenvalues{std::forward<Function>(eig_func)}
  {
  }

  template <typename Function>
  Eigenvalue_Norm(number_type eig_weight, std::size_t num_eigs,
                  Function &&eig_func)
      : xi{eig_weight},
        number_of_eigenvalues{num_eigs},
        compute_eigenvalues{std::forward<Function>(eig_func)}
  {
  }

private:
  block_matrix_type construct_block_diagonal(const arg_type &args);

private:
  number_type xi;
  std::size_t number_of_eigenvalues;

  std::function<std::vector<number_type>(const block_matrix_type &,
                                         std::size_t)>
      compute_eigenvalues;
};

} // namespace norm
} // namespace cooling
} // namespace osborn
} // namespace rmt

#endif /* OSBORN_COOLING_EIGENVALUE_NORM_HPP */
