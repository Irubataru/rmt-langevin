/*
 * Created: 14-06-2016
 * Modified: Tue 14 Feb 2017 16:44:43 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "eigenvalue_norm.hpp"
#include <utilities/matrix/block_constructors.hpp>
#include <utilities/matrix/identity_constructor.hpp>

using rmt::numeric::imag_i;

namespace rmt {
namespace osborn {
namespace cooling {
namespace norm {

Eigenvalue_Norm::number_type
Eigenvalue_Norm::compute(const Eigenvalue_Norm::arg_type &args)
{
  using rmt::numeric::exp;

  auto eigen_values = compute_eigenvalues(construct_block_diagonal(args),
                                          number_of_eigenvalues);
  number_type norm = number_type{0.};

  for (auto eig : eigen_values)
    norm += exp(-xi * eig);

  return norm;
}

Eigenvalue_Norm::block_matrix_type
Eigenvalue_Norm::construct_block_diagonal(const Eigenvalue_Norm::arg_type &args)
{
  auto result = matrix::Block_Zero_Matrix();

  auto xdx = static_cast<M_type>(blaze::ctrans(args.X) * args.X);
  auto ydy = static_cast<M_type>(blaze::ctrans(args.Y) * args.Y);

  blaze::submatrix(result, 0ul, 0ul, M_size, M_size) =
      std::norm(args.mass) * matrix::Identity_Matrix() + ydy;

  blaze::submatrix(result, M_size, M_size, M_size, M_size) =
      std::norm(args.mass) * matrix::Identity_Matrix() + xdx;

  blaze::submatrix(result, 0ul, M_size, M_size, M_size) =
      std::conj(args.mass) * args.X + args.mass * blaze::ctrans(args.Y);

  blaze::submatrix(result, M_size, 0ul, M_size, M_size) =
      args.mass * blaze::ctrans(args.X) + std::conj(args.mass) * args.Y;

  return result;
}

} // namespace norm
} // namespace cooling
} // namespace osborn
} // namespace rmt
