
/*
 * Created: 14-02-2017
 * Modified: Wed 06 Sep 2017 15:09:46 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OSBORN_NORM_INTERFACE_HPP
#define OSBORN_NORM_INTERFACE_HPP

#include "../cooling_types.hpp"

namespace rmt {
namespace osborn {
namespace cooling {
namespace norm {

class Norm : public Simulation_Cooling_Norm
{
public:
  using number_type = rmt::numeric::number_type;
  using arg_type = Norm_Arguments;

  number_type operator()(const arg_type &args)
  {
    return compute(args);
  }

  virtual number_type compute(const arg_type &args) = 0;
  virtual bool needs_phi_psi() const = 0;

  virtual ~Norm()
  {
  }
};

} // namespace norm
} // namespace cooling
} // namespace osborn
} // namespace rmt

#endif /* OSBORN_NORM_INTERFACE_HPP */
