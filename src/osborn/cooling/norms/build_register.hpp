
/*
 * Created: 06-09-2017
 * Modified: Wed 06 Sep 2017 15:12:33 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OSBORN_COOLING_NORM_BUILD_REGISTER_HPP
#define OSBORN_COOLING_NORM_BUILD_REGISTER_HPP

#include <builders/norm_builders/norm_builder.hpp>
#include <builders/norm_builders/specific_builders.hpp>
#include <osborn/cooling/norms.hpp>
#include <utilities/json_parser_helpers.hpp>

namespace rmt {
namespace build {
namespace core {

void register_osborn_cooling_norms()
{
  namespace norm = ::rmt::osborn::cooling::norm;

  auto model_name = std::string{parse::to_string(Model_Type::Osborn) + "_"};
  auto &dict = Norm_Builder_Dictionary::instance();

  dict.add(model_name + "hermiticity",
           std::make_shared<No_Argument_Norm_Builder<norm::Hermiticity_Norm>>());

  dict.add(model_name + "anti-hermiticity",
           std::make_shared<No_Argument_Norm_Builder<norm::Anti_Hermiticity_Norm>>());

  dict.add(model_name + "eigenvalue",
           std::make_shared<osborn::Eigenvalue_Norm_Builder>());

  dict.add(model_name + "aggregate",
           std::make_shared<osborn::Aggregate_Norm_Builder>());
}

} // namespace core
} // namespace build
} // namespace rmt


#endif /* OSBORN_COOLING_NORM_BUILD_REGISTER_HPP */
