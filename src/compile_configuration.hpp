/*
 * Created: 06-04-2016
 * Modified: Tue 01 Nov 2016 14:17:14 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef COMPILE_CONFIGURATION_HPP
#define COMPILE_CONFIGURATION_HPP

#include <cstdlib>

#ifdef MATSIZE
constexpr auto M_size = std::size_t{MATSIZE};
#else
constexpr auto M_size = std::size_t{6};
#endif

#ifdef DYNAMIC
constexpr auto M_dynamic = bool{true};
#else
constexpr auto M_dynamic = bool{false};
#endif

#endif /* COMPILE_CONFIGURATION_HPP */
