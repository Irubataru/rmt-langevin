/*
 * Created: 10-06-2016
 * Modified: Fri 18 Nov 2016 10:58:37 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef NUMBER_TYPE_HPP
#define NUMBER_TYPE_HPP

#include <complex>

//#include<utilities/float_type/multiprecision_type.hpp>
//#include<utilities/float_type/boost_real_overloads.hpp>
//#include<utilities/float_type/boost_complex_overloads.hpp>
//#include<utilities/float_type/boost_to_string.hpp>

#include <utilities/float_type/std_overloads.hpp>
#include <utilities/float_type/std_precision_type.hpp>
#include <utilities/float_type/std_to_string.hpp>

namespace rmt {
namespace numeric {

// using number_type = multiprecision_type;
using number_type = singleprecision_type;
using complex_type = std::complex<number_type>;

const auto imag_i = complex_type{0., 1.};

} // namespace numeric
} // namespace rmt

#endif /* NUMBER_TYPE_HPP */
