
/*
 * Created: 04-09-2017
 * Modified: Mon 04 Sep 2017 16:30:33 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef MEASURE_UPDATE_HPP
#define MEASURE_UPDATE_HPP

#include <simskel/update/update_int.hpp>
#include <simskel/observable/observable_interface.hpp>

namespace rmt {
namespace simulation {

class Measure : public ::simskel::Simple_Update
{
public:
  using obs_type = ::simskel::Observable;
  using obs_ptr_type = std::shared_ptr<obs_type>;

  Measure() = default;

  virtual ~Measure() {};

  void push_back(obs_ptr_type const& new_obs)
  {
    observables.push_back(new_obs);
  }

  void push_back(obs_type* &&new_obs)
  {
    observables.emplace_back(new_obs);
  }

private:
  void DoUpdate() override
  {
    for (auto const& obs : observables)
      obs->Calculate();
  }

  std::vector<obs_ptr_type> observables;
};

} // namespace simulation 
} // namespace rmt 

#endif /* MEASURE_UPDATE_HPP */
