
/*
 * Created: 05-09-2017
 * Modified: Tue 05 Sep 2017 14:41:21 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef MULTI_STEP_STAG_HPP
#define MULTI_STEP_STAG_HPP

#include "stage_interface.hpp"

namespace rmt {
namespace simulation {

class Multi_Step_Stage : public Stage
{
public:
  Multi_Step_Stage(std::size_t total_steps)
    : total_number_of_steps{total_steps}, current_step{0} {};

  virtual ~Multi_Step_Stage() {};

private:
  virtual bool is_done()
  {
    return current_step >= total_number_of_steps;
  }

  virtual void post_execute_update()
  {
    ++current_step;
  }

  std::size_t total_number_of_steps;
  std::size_t current_step;
};

class Single_Step_Stage : public Multi_Step_Stage
{
public:
  Single_Step_Stage()
    : Multi_Step_Stage{1} {};

  virtual ~Single_Step_Stage() {};
};

} // namespace simulation 
} // namespace rmt 

#endif /* MULTI_STEP_STAG_HPP */
