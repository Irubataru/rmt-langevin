
/*
 * Created: 05-09-2017
 * Modified: Tue 05 Sep 2017 15:44:22 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STAGE_INTERFACE_HPP
#define STAGE_INTERFACE_HPP

#include <simskel/update/update_int.hpp>
#include <vector>

namespace rmt {
namespace simulation {

class Stage
{
public:
  using update_type = ::simskel::Simple_Update;
  using ptr_type = std::shared_ptr<update_type>;

  virtual ~Stage(){};

  void run()
  {
    while(!is_done())
      execute();
  }

  void execute()
  {
    for (auto const& step : steps_)
      step->Update();

    post_execute_update();
  }

  void push_back(ptr_type const& step)
  {
    steps_.push_back(step);
  }

  void push_back(update_type * &&step)
  {
    push_back(ptr_type{step});
  }

private:
  virtual bool is_done()
  {
    return true;
  }

  virtual void post_execute_update(){};

  std::vector<ptr_type> steps_;
};

} // namespace simulation
} // namespace rmt

#endif /* STAGE_INTERFACE_HPP */
