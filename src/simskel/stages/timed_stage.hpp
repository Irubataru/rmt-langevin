
/*
 * Created: 05-09-2017
 * Modified: Tue 05 Sep 2017 18:03:03 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef TIMED_STAGE_HPP
#define TIMED_STAGE_HPP

#include "stage_interface.hpp"
#include <utilities/stepsize/time_keeper.hpp>

namespace rmt {
namespace simulation {

class Timed_Stage : public Stage
{
public:
  using time_value_type = stepsize::Time_Keeper::value_type;

  Timed_Stage(std::shared_ptr<stepsize::Time_Keeper> timer,
              time_value_type end_time)
      : final_time{end_time}, time_keeper{timer} {};

  virtual ~Timed_Stage() {};

private:
  virtual bool is_done()
  {
    return time_keeper->time() >= final_time;
  }

  virtual void post_execute_update()
  {
    time_keeper->Update();
  }

  const time_value_type final_time;
  std::shared_ptr<stepsize::Time_Keeper> time_keeper;
};

} // namespace simulation
} // namespace rmt

#endif /* TIMED_STAGE_HPP */
