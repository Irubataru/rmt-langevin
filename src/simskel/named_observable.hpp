
/*
 * Created: 06-03-2017
 * Modified: Sat 02 Sep 2017 15:47:23 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef NAMED_OBSERVABLE_HPP
#define NAMED_OBSERVABLE_HPP

#include <simskel/object_w_record.hpp>
#include <simskel/observable/observable_interface.hpp>
#include <string>
#include <utilities/printers/printable_object.hpp>

namespace rmt {
namespace observable {

class Named_Observable_Core : public ::simskel::Observable,
                              public output::Printable
{
public:
  virtual ::std::string name() const = 0;

  virtual ::std::string dir_name() const
  {
    return name();
  }

  virtual ~Named_Observable_Core(){};

  bool has_been_printed() const
  {
    return printed_q;
  }

  void set_printed() const
  {
    printed_q = true;
  }

private:
  mutable bool printed_q = false;
};

template <typename Type>
class Named_Observable : public Named_Observable_Core,
                         public Object_With_Record<Type>
{
public:
  using Object_With_Record<Type>::new_element;

  virtual ~Named_Observable()
  {
  }

  virtual void print(output::Output_Visitor *visitor) override;
  virtual void print_and_empty(output::Output_Visitor *visitor) override;

private:
  void DoCalculation() override
  {
    new_element();
  };
};

} // namespace observable
} // namespace rmt

#include <utilities/printers/print_cast_function.hpp>

namespace rmt {
namespace observable {

template <typename Type>
inline void Named_Observable<Type>::print(output::Output_Visitor *visitor)
{
  output::cast_and_print(visitor, *this);
}

template <typename Type>
inline void
Named_Observable<Type>::print_and_empty(output::Output_Visitor *visitor)
{
  output::cast_and_print_and_empty(visitor, *this);
}

} // namespace observable
} // namespace rmt

#endif /* NAMED_OBSERVABLE_HPP */
