
/*
 * Created: 07-03-2017
 * Modified: Sat 02 Sep 2017 15:46:59 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef SIMSKEL_UTILITY_OBSERVABLES_HPP
#define SIMSKEL_UTILITY_OBSERVABLES_HPP

#include <memory>
#include <simskel/named_observable.hpp>

namespace rmt {
namespace observable {

class Measure_Every_N : public observable::Named_Observable_Core
{
public:
  Measure_Every_N(std::unique_ptr<observable::Named_Observable_Core> obs, std::size_t n)
    : obs_{std::move(obs)}, n_{n}, current_{0ul} {}

  Measure_Every_N(std::shared_ptr<Named_Observable_Core> const&obs,
                  std::size_t n)
      : obs_{obs}, n_{n}, current_{0ul}
  {
  }

  Measure_Every_N(Named_Observable_Core *obs, std::size_t n)
      : obs_{obs}, n_{n}, current_{0ul}
  {
  }

  std::string name() const override
  {
    return obs_->name();
  }

  std::string dir_name() const override
  {
    return obs_->dir_name();
  }

  virtual void print(output::Output_Visitor *visitor) override
  {
    return obs_->print(visitor);
  }

  virtual void print_and_empty(output::Output_Visitor *visitor) override
  {
    return obs_->print_and_empty(visitor);
  }

  virtual ~Measure_Every_N()
  {
  }

private:
  void DoCalculation() override
  {
    ++current_;

    if (current_ % n_ == 0)
      obs_->Calculate();
  }

  std::shared_ptr<observable::Named_Observable_Core> obs_;
  std::size_t n_;
  std::size_t current_;
};

} // namespace observable 
} // namespace rmt

/*
 *#include <utilities/printers/container.hpp>
 *#include <utilities/printers/file_printer.hpp>
 *
 *namespace rmt {
 *namespace utility {
 *
 *class Print_Measurements : public ::simskel::Observable
 *{
 *public:
 *  Print_Measurements(const std::shared_ptr<output::Printable_List> &observables,
 *                     std::shared_ptr<Program_Options> po,
 *                     const Physical_Parameters &params)
 *      : obs_list{observables}, file_printer{po, params}
 *  {
 *  }
 *
 *  virtual ~Print_Measurements()
 *  {
 *  }
 *
 *private:
 *  void DoCalculation() override
 *  {
 *    for (auto obs : *obs_list)
 *      obs->print_and_empty(&file_printer);
 *  }
 *
 *  std::shared_ptr<output::Printable_List> obs_list;
 *  output::File_Printer file_printer;
 *};
 *
 *} // namespace utility
 *} // namespace rmt
 */

#endif /* SIMSKEL_UTILITY_OBSERVABLES_HPP */
