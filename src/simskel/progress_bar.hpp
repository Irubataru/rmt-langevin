
/*
 * Created: 05-09-2017
 * Modified: Tue 05 Sep 2017 18:10:00 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef PROGRESS_BAR_HPP
#define PROGRESS_BAR_HPP

#include <simskel/update/update_int.hpp>
#include <utilities/stepsize/time_keeper.hpp>
#include <iostream>

namespace rmt {
namespace simulation {

class Progress_Bar : public ::simskel::Simple_Update
{
public:
  using time_value_type = stepsize::Time_Keeper::value_type;

  Progress_Bar(std::shared_ptr<stepsize::Time_Keeper> timer,
               time_value_type end_time, std::size_t width = 50)
      : timer_{timer}, final_time_{end_time}, width_{width} {};

private:
  void DoUpdate() override
  {
    auto progress = timer_->time() / final_time_;
    std::size_t num_prog = std::round(progress*width_);

    std::cout << std::string(width_ + 10,' ') << "\r";
    std::cout << "[";
    if (num_prog > 0)
      std::cout << std::string(num_prog-1,'=');

    std::cout << ">";

    if (num_prog < width_)
      std::cout << std::string(width_ - num_prog, ' ');

    std::cout << "] " << std::round(100.*progress) << " %\r";
    std::cout.flush();
  }

  std::shared_ptr<stepsize::Time_Keeper> timer_;
  stepsize::Time_Keeper::value_type final_time_;

  std::size_t width_;
};

} // namespace simulation
} // namespace rmt

#endif /* PROGRESS_BAR_HPP */
