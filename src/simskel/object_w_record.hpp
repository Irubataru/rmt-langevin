
/*
 * Created: 31-08-2017
 * Modified: Fri 01 Sep 2017 10:28:41 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OBJECT_W_RECORD_HPP
#define OBJECT_W_RECORD_HPP

#include <vector>

namespace rmt {
namespace observable {

template <typename Value_Type>
class Object_With_Record
{
public:
  using value_type = Value_Type;
  using container_type = std::vector<value_type>;
  using iterator_type = typename container_type::const_iterator;
  using size_type = typename container_type::size_type;

  Object_With_Record() = default;
  Object_With_Record(Object_With_Record const &) = default;
  Object_With_Record(Object_With_Record &&) = default;

  explicit Object_With_Record(std::vector<Value_Type> const &rec) : record_{rec}
  {
  }

  iterator_type begin() const
  {
    return record_.begin();
  }

  iterator_type end() const
  {
    return record_.end();
  }

  size_type size() const
  {
    return record_.size();
  }

  void clear()
  {
    record_.clear();
  }

  void reserve(size_type reserved_size)
  {
    record_.reserve(reserved_size);
  }

  container_type const &data() const
  {
    return record_;
  }

  container_type extract()
  {
    auto rec = std::move(record_);
    clear();
    return rec;
  }

  void new_element()
  {
    record_.push_back(calculate_value());
  }

  virtual ~Object_With_Record()
  {
  }

private:
  virtual value_type calculate_value() = 0;

  container_type record_;
};

} // namespace observable
} // namespace rmt

#endif /* OBJECT_W_RECORD_HPP */
