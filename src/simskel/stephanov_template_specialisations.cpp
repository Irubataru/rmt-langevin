
/*
 * Created: 07-03-2017
 * Modified: Tue 07 Mar 2017 14:38:53 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "stephanov_template_specialisations.hpp"

namespace model = rmt::stephanov;

namespace rmt {
namespace utility {

template <>
void stephanov::cartesian::utility::Norm_Transformation_Logger::DoUpdate()
{
  using model::cooling::norm::Norm_Arguments;

  Norm_Arguments args{storage->read(model::M_name::X),
                      storage->read(model::M_name::XC),
                      storage->read_mu(), storage->read_mass()};

  if (!norm_ptr)
    throw std::runtime_error{"We have null!"};

  auto norm0 = norm_ptr->compute(args);

  update->Update();

  auto norm1 = norm_ptr->compute(args);

  ofs << norm0 << " " << norm1 << std::endl;
}


} // namespace utility 
} // namespace rmt 
