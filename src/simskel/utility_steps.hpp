
/*
 * Created: 07-03-2017
 * Modified: Tue 05 Sep 2017 17:44:26 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef SIMSKEL_UTILITY_STEPS_HPP
#define SIMSKEL_UTILITY_STEPS_HPP

#include <fstream>
#include <iomanip>
#include <memory>
#include <simskel/update/update_int.hpp>

namespace rmt {
namespace utility {

class Repeat_Step : public ::simskel::Simple_Update
{
public:
  Repeat_Step(std::shared_ptr<::simskel::Simple_Update> step,
              std::size_t num_steps)
      : update{step}, times{num_steps}
  {
  }

  void set_times(std::size_t num_steps)
  {
    times = num_steps;
  }

  virtual ~Repeat_Step() {}

private:
  virtual void DoUpdate() override
  {
    for (auto i = 0u; i < times; ++i)
      update->Update();
  }

private:
  std::shared_ptr<::simskel::Simple_Update> update;
  std::size_t times;
};

class Every_N_Step : public ::simskel::Simple_Update
{
public:
  Every_N_Step(std::shared_ptr<::simskel::Simple_Update> step,
              std::size_t n)
      : update{step}, every{n}, current{1}
  {
  }

  void set_times(std::size_t n)
  {
    every = n;
  }

  virtual ~Every_N_Step() {}

private:
  virtual void DoUpdate() override
  {
    if (current % every == 0)
      update->Update();

    ++current;
  }

private:
  std::shared_ptr<::simskel::Simple_Update> update;
  std::size_t every;
  std::size_t current;
};

template <typename Storage_Type, typename Norm_Type>
class Norm_Transformation_Logger_Base : public ::simskel::Simple_Update
{
public:
  Norm_Transformation_Logger_Base(std::shared_ptr<const Storage_Type> s,
                                  std::shared_ptr<Norm_Type> nptr,
                                  std::shared_ptr<::simskel::Simple_Update> u,
                                  std::string filename)
      : storage{s}, norm_ptr{nptr}, update{u}, ofs{filename}
  {
    if (!ofs)
      throw std::runtime_error{"Cannot open file \"" + filename + "\""};

    ofs << std::setprecision(10);
  }

  virtual ~Norm_Transformation_Logger_Base() {}

private:
  virtual void DoUpdate() override;

private:
  std::shared_ptr<const Storage_Type> storage;
  std::shared_ptr<Norm_Type> norm_ptr;
  std::shared_ptr<::simskel::Simple_Update> update;
  std::ofstream ofs;
};

} // namespace utility
} // namespace rmt

#endif /* SIMSKEL_UTILITY_STEPS_HPP */
