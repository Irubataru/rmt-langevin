
/*
 * Created: 07-03-2017
 * Modified: Tue 07 Mar 2017 14:33:43 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OSBORN_TEMPLATE_SPECIALISATIONS_HPP
#define OSBORN_TEMPLATE_SPECIALISATIONS_HPP

#include "utility_steps.hpp"
#include <osborn/resource/resource.hpp>
#include <osborn/cooling/norms/norm_interface.hpp>

namespace rmt {
namespace osborn {
namespace cartesian {
namespace utility {

using Norm_Transformation_Logger =
    ::rmt::utility::Norm_Transformation_Logger_Base<
        ::rmt::osborn::cartesian::Matrix_Storage,
        ::rmt::osborn::cooling::norm::Norm>;

} // namespace utility
} // namespace cartesian
} // namespace osborn
} // namespace rmt

#endif /* OSBORN_TEMPLATE_SPECIALISATIONS_HPP */
