
/*
 * Created: 05-09-2017
 * Modified: Tue 05 Sep 2017 12:05:37 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef INIT_UPDATE_HPP
#define INIT_UPDATE_HPP

#include <builders/type_utils/type_dict.hpp>
#include <simskel/update/update_int.hpp>
#include <types.hpp>
#include <common/initialiser_alg.hpp>

namespace rmt {
namespace simulation {

template <Model_Type model, Storage_Type storage>
class Init_Update_Step : public ::simskel::Simple_Update
{
public:
  using storage_type =
      typename build::core::type_dict<model, storage>::storage_type;

  Init_Update_Step(std::shared_ptr<storage_type> const &config,
                   Physical_Parameters const& phys_params,
                   std::shared_ptr<Generator_Type> generator)
      :  matrix_storage{config}, params(phys_params), rng{generator} {};

  virtual ~Init_Update_Step(){};

private:
  void DoUpdate() override
  {
    auto init_alg = Initialiser<model, storage>{};
    init_alg(matrix_storage, params, rng);
  }

  std::shared_ptr<storage_type> matrix_storage;
  Physical_Parameters params;
  std::shared_ptr<Generator_Type> rng;
};

} // namespace simulation
} // namespace rmt

#endif /* INIT_UPDATE_HPP */
