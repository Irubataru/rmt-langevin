
/*
 * Created: 04-09-2017
 * Modified: Mon 04 Sep 2017 19:48:15 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef SAVE_UPDATE_HPP
#define SAVE_UPDATE_HPP

#include <simskel/update/update_int.hpp>
#include <utilities/filename_generator.hpp>
#include <utilities/printers/file_printer.hpp>
#include <utilities/printers/printable_object.hpp>

namespace rmt {
namespace simulation {

class Save_Update : public ::simskel::Simple_Update
{
public:
  using printable_type = ::rmt::output::Printable;
  using printable_ptr_type = std::shared_ptr<printable_type>;

  Save_Update() = delete;
  Save_Update(build::Filename_Generator const &fg,
              Physical_Parameters physical_params,
              Model_Parameters model_params)
      : fp_{fg, physical_params, model_params} {};

  void push_back(printable_ptr_type const& new_printable)
  {
    printables_.push_back(new_printable);
  }

  void push_back(printable_type* &&new_printable)
  {
    printables_.emplace_back(new_printable);
  }

  virtual ~Save_Update(){};

private:
  void DoUpdate() override
  {
    for (auto const &print_obj : printables_)
      print_obj->print_and_empty(&fp_);
  }

  std::vector<printable_ptr_type> printables_;
  output::File_Printer fp_;
};

} // namespace simulation
} // namespace rmt

#endif /* SAVE_UPDATE_HPP */
