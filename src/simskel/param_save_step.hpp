
/*
 * Created: 05-09-2017
 * Modified: Tue 05 Sep 2017 21:20:26 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef PARAM_SAVE_STEP_HPP
#define PARAM_SAVE_STEP_HPP

#include <fstream>
#include <nlohmann/json.hpp>
#include <simskel/update/update_int.hpp>
#include <utilities/filename_generator.hpp>

namespace rmt {
namespace simulation {

class Param_Save_Update : public ::simskel::Simple_Update
{
public:
  Param_Save_Update(nlohmann::json const &tree,
                    build::Filename_Generator const &fg,
                    Physical_Parameters pparms, Model_Parameters mparms)
      : phparams_(pparms),
        mparams_(mparms),
        filename_gen_{fg},
        main_tree_(tree){};

  virtual ~Param_Save_Update(){};

private:
  void DoUpdate() override
  {
    auto cfg_filename =
        filename_gen_.filename(phparams_, mparams_, "config_file");

    std::ofstream ofs{cfg_filename};

    if (!ofs)
      throw std::runtime_error{"unable to open file \"" + cfg_filename +
                               "\" for writing"};

    ofs << main_tree_.dump(2);
  }

  Physical_Parameters phparams_;
  Model_Parameters mparams_;

  build::Filename_Generator filename_gen_;
  nlohmann::json main_tree_;
};

} // namespace simulation
} // namespace rmt

#endif /* PARAM_SAVE_STEP_HPP */
