
/*
 * Created: 07-03-2017
 * Modified: Tue 07 Mar 2017 14:31:03 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef STEPHANOV_TEMPLATE_SPECIALISATIONS_HPP
#define STEPHANOV_TEMPLATE_SPECIALISATIONS_HPP

#include "utility_steps.hpp"
#include <stephanov/cooling/cooling_norm/norm_interface.hpp>
#include <stephanov/resource/resource.hpp>

namespace rmt {
namespace stephanov {
namespace cartesian {
namespace utility {

using Norm_Transformation_Logger =
    ::rmt::utility::Norm_Transformation_Logger_Base<
        ::rmt::stephanov::cartesian::Matrix_Storage,
        ::rmt::stephanov::cooling::norm::Norm>;

} // namespace utility
} // namespace cartesian
} // namespace stephanov
} // namespace rmt

#endif /* STEPHANOV_TEMPLATE_SPECIALISATIONS_HPP */
