/*
 * Created: 07-04-2016
 * Modified: Thu 09 Nov 2017 16:16:10 GMT
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef RANDOM_MATRIX_INITIALISER_HPP
#define RANDOM_MATRIX_INITIALISER_HPP

#include <algorithm>
#include <random>
#include <types.hpp>

namespace rmt {
namespace utility {

template <typename Generator>
class Random_Gaussian_Initialiser
{
public:
  explicit Random_Gaussian_Initialiser(Generator &g_ref, double mu = 0.,
                                       double sigma = 1./numeric::sqrt(2.*M_size))
      : g(g_ref), dist{mu, sigma}
  {
  }

  template <typename... Tail>
  void operator()(M_type &m, Tail... tail)
  {
    this->operator()(m);
    this->operator()(tail...);
  }

  void operator()(M_type &m)
  {
    m = blaze::forEach(m, [this](const rmt::numeric::complex_type &) {
      return rmt::numeric::number_type{dist(g)};
    });
  }

private:
  Generator &g;
  std::normal_distribution<> dist;
};

template <typename Generator>
class Random_Polar_Initialiser
{
public:
  explicit Random_Polar_Initialiser(Generator &g_ref, double mu = 0.,
                                    double sigma = 1.)
      : g(g_ref), norm_dist{mu, sigma}, uniform_dist{-M_PI, M_PI}
  {
  }

  void operator()(M_type &r_mat, M_type &phi_mat)
  {
    r_mat = blaze::forEach(r_mat, [this](const rmt::numeric::complex_type &) {
      return rmt::numeric::number_type{norm_dist(g)};
    });
    phi_mat =
        blaze::forEach(phi_mat, [this](const rmt::numeric::complex_type &) {
          return rmt::numeric::number_type{uniform_dist(g)};
        });
  }

public:
  Generator &g;
  std::normal_distribution<> norm_dist;
  std::uniform_real_distribution<> uniform_dist;
};

} // namespace utility
} // namespace rmt

#endif /* RANDOM_MATRIX_INITIALISER_HPP */
