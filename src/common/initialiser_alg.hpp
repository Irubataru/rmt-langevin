
/*
 * Created: 05-09-2017
 * Modified: Tue 05 Sep 2017 12:02:43 BST
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef INITIALISER_ALG_HPP
#define INITIALISER_ALG_HPP

#include <osborn/resource/initialiser.hpp>
#include <stephanov/resource/initialiser.hpp>
#include "random_matrix_initialiser.hpp"

namespace rmt {

template <Model_Type model, Storage_Type storage>
class Initialiser
{
};

template <>
class Initialiser<Model_Type::Stephanov, Storage_Type::Cartesian>
{
public:
  using storage_type = stephanov::cartesian::Matrix_Storage;

  void operator()(std::shared_ptr<storage_type> config,
                  Physical_Parameters const &params,
                  std::shared_ptr<Generator_Type> rng)
  {
    auto init_alg = utility::Random_Gaussian_Initialiser<Generator_Type>{*rng};
    stephanov::cartesian::Initialise(*config, params, init_alg);
  }
};

template <>
class Initialiser<Model_Type::Stephanov, Storage_Type::Polar>
{
public:
  using storage_type = stephanov::polar::Matrix_Storage;

  void operator()(std::shared_ptr<storage_type> config,
                  Physical_Parameters const &params,
                  std::shared_ptr<Generator_Type> rng)
  {
    auto init_alg = utility::Random_Polar_Initialiser<Generator_Type>{*rng};
    stephanov::polar::Initialise(*config, params, init_alg);
  }
};

template <>
class Initialiser<Model_Type::Osborn, Storage_Type::Cartesian>
{
public:
  using storage_type = osborn::cartesian::Matrix_Storage;

  void operator()(std::shared_ptr<storage_type> config,
                  Physical_Parameters const &params,
                  std::shared_ptr<Generator_Type> rng)
  {
    auto init_alg = utility::Random_Gaussian_Initialiser<Generator_Type>{*rng};
    osborn::cartesian::Initialise(*config, params, init_alg);
  }
};

} // namespace rmt

#endif /* INITIALISER_ALG_HPP */
