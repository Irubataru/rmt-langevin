#!/bin/bash

# add_sibtree_subfolder.sh [remote-url] [remote-name] [remote-folder] [target-folder] 
# Command that adds a subfolder of a remote repo to a subfolder of current repo

CURRENT_BRANCH="$(git branch)"

git remote add -f -t master --no-tags $2 $1
git checkout "$2/master"
git subtree split -P $3 -b temporary-split-branch
git checkout ${CURRENT_BRANCH}
git subtree add --squash -P $4 temporary-split-branch
git branch -D temporary-split-branch

