#!/bin/bash

# update_subtree_subfolder.sh [remote-name] [remote-folder] [target-folder] 
# Command that updates a subfolder of a remote repo to a subfolder of current repo

CURRENT_BRANCH="$(git branch)"

git fetch $1
git checkout "$1/master"
git subtree split -P $2 -b temporary-split-branch
git checkout ${CURRENT_BRANCH}
git subtree pull --squash -P $3 . temporary-split-branch
git branch -D temporary-split-branch
